<?php

use Illuminate\Support\Str;

//----------------------------------------------------------
// memcachedの設定
//----------------------------------------------------------
$_memcached = [
	'driver' => 'memcached',
	'persistent_id' => env('MEMCACHED_PERSISTENT_ID'),
	'sasl' => [
		env('MEMCACHED_USERNAME'),
		env('MEMCACHED_PASSWORD'),
	],
	'options' => [
		// Memcached::OPT_CONNECT_TIMEOUT => 2000,
	],
	'servers' => [
	],
];

$_memcached_num = env('MEMCACHED_NUM', '1');
$_servers = [];
for ($i = 1; $i <= $_memcached_num; $i++)
{
	$_prefix = sprintf('_%02d', $i);
	$_server = [
		'host' => env('MEMCACHED_HOST' . $_prefix, 'memcached'),
		'port' => env('MEMCACHED_PORT' . $_prefix, '11211'),
		'weight' => env('MEMCACHED_WEIGHT' . $_prefix, '100'),
	];
	$_servers[] = $_server;
}
$_memcached['servers'] = $_servers;



return [

    /*
    |--------------------------------------------------------------------------
    | Default Cache Store
    |--------------------------------------------------------------------------
    |
    | This option controls the default cache connection that gets used while
    | using this caching library. This connection is used when another is
    | not explicitly specified when executing a given caching function.
    |
    | Supported: "apc", "array", "database", "file", "memcached", "redis"
    |
    */

    'default' => env('CACHE_DRIVER', 'memcached'),

    /*
    |--------------------------------------------------------------------------
    | Cache Stores
    |--------------------------------------------------------------------------
    |
    | Here you may define all of the cache "stores" for your application as
    | well as their drivers. You may even define multiple stores for the
    | same cache driver to group types of items stored in your caches.
    |
    */

    'stores' => [

        'apc' => [
            'driver' => 'apc',
        ],

        'array' => [
            'driver' => 'array',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'cache',
            'connection' => null,
        ],

        'file' => [
            'driver' => 'file',
            'path' => storage_path('framework/cache/data'),
        ],

        'memcached' => $_memcached,

        'redis' => [
            'driver' => 'redis',
            'connection' => 'cache',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Cache Key Prefix
    |--------------------------------------------------------------------------
    |
    | When utilizing a RAM based store such as APC or Memcached, there might
    | be other applications utilizing the same cache. So, we'll specify a
    | value to get prefixed to all our keys so we can avoid collisions.
    |
    */

    'prefix' => env('CACHE_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_cache'),

];
