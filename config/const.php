<?php

return [
    // お知らせタイプ
    'InformationType' => [
        'ALL' => 1,
        'MAINTENANCE' => 2,
        'UPDATE' => 3,
        'BUG_REPORT' => 4,
    ],

    // お問い合わせタイプ ※ContactService.phpのタイプと合わせる
    'ContactType' => [
        'PRODUCT_BUY' => 1,
        'TRANSIT' => 2,
        'BUG_REPORT' => 3,
        'REQUEST' => 4,
    ],

    // お問い合わせメール
    'ContactMail' => [
        'main' => 'bbdw_contact',
        'domain' => '@bbdw.jp'
    ],

];
