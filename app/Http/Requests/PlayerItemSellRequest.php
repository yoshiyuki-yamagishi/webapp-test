<?php
/**
 * プレイヤアイテム売却 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテム売却 のリクエストパラメータ
 *
 */
class PlayerItemSellRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_item_id' => 'required',
            'num' => 'required',
        ]);
        return $base->all();
	}

}
