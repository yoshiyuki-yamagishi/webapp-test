<?php
/**
 * フォロー削除 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

/**
 * フォロー削除 のリクエストパラメータ
 *
 */
class FollowDeleteRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'follow_player_id' => 'required',
        ]);
        return $base->all();
	}

}
