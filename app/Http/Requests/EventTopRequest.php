<?php
    /**
     * イベントindex のリクエストパラメータ
     *
     */

    namespace App\Http\Requests;

    /**
     * イベントindex のリクエストパラメータ
     *
     */
    class EventTopRequest extends AuthenticatedRequest
    {
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            $base = collect(parent::rules());
            $base->merge([
                'event_id' => 'required',
            ]);
            return $base->all();
        }

    }
