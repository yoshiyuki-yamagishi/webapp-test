<?php
/**
 * プレイヤパーティ更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use App\Models\PlayerParty;

/**
 * プレイヤパーティ更新 のリクエストパラメータ
 *
 */
class PlayerPartyUpdateRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'party_no' => 'required|integer|min:1|max:10',
            'party_character_list' => 'required',
            'party_character_list.*.player_character_id' => 'required|integer',
            'party_character_list.*.player_grimoire_id' => 'required|integer',
        ]);
        return $base->all();
	}

	/**
	 * パーティのキャラクター数を返す
	 *
	 * @return integer キャラクター数
	 */
	public function mainCharacterCount()
	{
        $charaCount = 0;
        $count = count($this->party_character_list);
        for ($i = 1; $i <= PlayerParty::MAX_MAIN_POSITIONS; ++ $i)
        {
            if ($i > $count)
                break;

            $partyChara = $this->party_character_list[$i - 1];
            if (empty($partyChara))
                continue;

            if ($partyChara['player_character_id'] > 0)
                ++ $charaCount;
        }

        return $charaCount;
    }

	/**
	 * 重複チェック
	 *
	 * @param string $prefix 'player_character' or
	 */
	public function checkDup($prefix)
	{
        $count = count($this->party_character_list);

        for ($i = 0; $i < $count - 1; ++ $i)
        {
            $partyCharaI = $this->party_character_list[$i];
            if (empty($partyCharaI))
                continue;

            $idI = $partyCharaI[$prefix . '_id'];
            if ($idI <= 0)
                continue;

            for ($j = $i + 1; $j < $count; ++ $j)
            {
                $partyCharaJ = $this->party_character_list[$j];
                if (empty($partyCharaJ))
                    continue;

                $idJ = $partyCharaJ[$prefix . '_id'];
                if ($idJ <= 0)
                    continue;

                if ($idI == $idJ)
                {
                    throw \App\Exceptions\ParamException::make(
                        'dupulicated ' . $prefix . '_id: ' . $idI
                    );
                }
            }
        }
    }

	/**
	 * キャラクターがいないのに魔道書装備していないかチェック
	 *
	 */
	public function checkNonCharacterGrimore()
	{
        $count = count($this->party_character_list);

        for ($i = 0; $i < $count; ++ $i)
        {
            $partyChara = $this->party_character_list[$i];
            if (empty($partyChara))
                continue;

            if ($partyChara['player_character_id'] <= 0 &&
                $partyChara['player_grimoire_id'] > 0)
            {
                throw \App\Exceptions\ParamException::make(
                    'null character equips grimoire, position: ' . ($i + 1)
                );
            }
        }
    }

}
