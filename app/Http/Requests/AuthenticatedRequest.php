<?php
/**
 * 認証済み のリクエストベースパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

/**
 * 認証済み のリクエストベースパラメータ
 *
 */
class AuthenticatedRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    $base = collect(parent::rules());
	    $base->merge([
            'auth_code' => 'required',
            'player_id' => 'required',
        ]);
		return $base->all();
	}

}
