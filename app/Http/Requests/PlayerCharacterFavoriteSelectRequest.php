<?php
/**
 * プレイヤキャラクタお気に入り選択 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクお気に入り選択 のリクエストパラメータ
 *
 */
class PlayerCharacterFavoriteSelectRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_character_id'		=> 'required',
            'favorite_flag'				=> 'required|min:0|max:1',
        ]);
        return $base->all();
	}

}
