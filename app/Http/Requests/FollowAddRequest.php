<?php
/**
 * フォロー追加 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

/**
 * フォロー追加 のリクエストパラメータ
 *
 */
class FollowAddRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 * @deprecated
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'follow_player_id' => 'required',
        ]);
        return $base->all();
	}

}
