<?php
/**
 * 製品登録 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品登録 のリクエストパラメータ
 *
 */
class ProductRegistRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_buy_id' => 'required',
            'receipt' => 'required',
        ]);
        return $base->all();
	}

}
