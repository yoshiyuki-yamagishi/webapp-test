<?php
/**
 * デバッグでのアイテム追加 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

/**
 * デバッグでのアイテム追加 のリクエストパラメータ
 *
 */
class DebugCommandAddItemRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'item_id' => 'required',
            'item_num' => 'required',
        ]);
        return $base->all();
	}

}
