<?php
/**
 * ガチャ実行 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ実行 のリクエストパラメータ
 *
 */
class GachaExecRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'gacha_id' => 'required',
            'gacha_count' => 'required',
        ]);
        return $base->all();
	}

}
