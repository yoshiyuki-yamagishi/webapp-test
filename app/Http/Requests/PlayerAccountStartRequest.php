<?php
/**
 * プレイヤアカウント開始 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤアカウント開始 のリクエストパラメータ
 *
 */
class PlayerAccountStartRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'os_type' => 'required|integer|min:1|max:2',
            'token' => 'required',
        ]);
        return $base->all();
	}

}
