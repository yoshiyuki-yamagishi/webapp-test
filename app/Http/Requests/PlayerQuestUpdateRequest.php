<?php
/**
 * プレイヤクエスト更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

/**
 * プレイヤクエスト更新 のリクエストパラメータ
 *
 */
class PlayerQuestUpdateRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'quest_category' => 'required|integer|min:1|max:3',
            'chapter_id' => 'required',
            'quest_id' => 'required',
        ]);
        return $base->all();
	}

}
