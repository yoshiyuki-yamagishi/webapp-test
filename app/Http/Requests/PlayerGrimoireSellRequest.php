<?php
/**
 * プレイヤ魔道書売却 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ魔道書売却 のリクエストパラメータ
 *
 */
class PlayerGrimoireSellRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_grimoire_list'						=> 'required',
            'player_grimoire_list.*.player_grimoire_id'	=> 'required',
        ]);
        return $base->all();
	}

}
