<?php
/**
 * プレイヤミッション報酬受取 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤミッション報酬受取 のリクエストパラメータ
 *
 */
class PlayerMissionRewardTakeRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'take_type'					=> 'required',
        ]);
        return $base->all();
	}

}
