<?php
/**
 * シンプルなリクエスト
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;


/**
 * シンプルなリクエスト
 *
 */
class SimpleRequest extends BaseRequest
{
    // Authを通さない場合False
    protected $needAuth = false;
}
