<?php
/**
 * リクエストパラメータの基底クラス
 *
 */

namespace App\Http\Requests;

use App\Models\PlayerCommon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Collection;

use App\Http\Responses\ApiResponse;
use App\Exceptions\ApiException;
use App\Utils\EncryptUtil;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;
use App\Services\SessionService;


/**
 * リクエストパラメータの基底クラス
 *
 */
class BaseRequest extends FormRequest
{
	public $authStatus = null;
    public $authMessage = '';

    protected $needAuth = true;

	/**
	 * Get data to be validated from the request.
	 *
	 * @return array
	 */
	protected function validationData()
	{
		// ここで個別のバリデーションが呼ばれる
		return parent::validationData();
	}

	/**
	 * Determine if the player is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (EncryptUtil::isDecryptRequest())
		{
			$param = $this->all();

			//------------------------------
			// 復号化処理
			//------------------------------

			if (!isset($param['__req_data']) || !isset($param['__req_code']))
			{
				$this->authStatus = ApiException::E_PARAM;
                $this->authMessage = '__req_data/__req_code is not set';
				return false;
			}

            $values = EncryptUtil::decryptRequest(
                $param['__req_data'], $param['__req_code']
            );
			if (!isset($values))
			{
				$this->authStatus = ApiException::E_PARAM;
                $this->authMessage = '__req_data/__req_code is invalid';
				return false;
			}

			$_POST = array_merge($_POST, $values);
			$this->merge($values);
		}

		if (!$this->needAuth)
		{
			return true; // 認証が不要
		}
		else if ($this->_api == 'api_noauth')
		{
			return true; // 認証が不要
		}
		else
		{
			if (!isset($this->player_id, $this->auth_code))
			{
				$this->authStatus = ApiException::E_PARAM;
                $this->authMessage = 'player_id/auth_code is not set';
				return false;
			}

			// セッション開始
            $playerCommon = PlayerCommon::getByPlayerId($this->player_id);
			SessionService::start($playerCommon->unique_id);

			// 認証
			$this->authStatus = SessionService::checkAuthCode($this->auth_code);
			if ($this->authStatus != ApiException::S_OK)
			{
                $this->authMessage = 'auth error';
				return false;
			}

			return true;
		}

		return true;
	}



	/**
	 * Handle a failed authorization attempt.
	 *
	 * @return void
	 *
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	protected function failedAuthorization()
	{
		//@todo 認証エラーを返す
        $response = ApiResponse::getInstance();
		$response->setError($this->authStatus, 'auth error');

        switch ($this->authStatus)
        {
        case ApiException::E_PARAM:
            throw \App\Exceptions\ParamException::make(
                $this->authMessage
            );
        case ApiException::E_AUTH:
            throw \App\Exceptions\AuthException::make(
                $this->authMessage
            );
        case ApiException::E_AUTH_EXPIRED:
            throw \App\Exceptions\AuthExpiredException::make(
                $this->authMessage
            );
        default:
            throw \App\Exceptions\UnknownException::make(
                $this->authMessage
            );
        }
	}

	/**
	 * Handle a failed validation attempt.
	 *
	 * @param  \Illuminate\Contracts\Validation\Validator  $validator
	 * @return void
	 *
	 * @throws \Illuminate\Validation\ValidationException
	 */
	protected function failedValidation(Validator $validator)
	{
		$errors = $validator->errors();
		$errorMessages = '';

		foreach ($errors->getMessages() as $key => $errorMessageList)
		{
			foreach ($errorMessageList as $errorMessage)
			{
				if (!empty($errorMessages))
                    $errorMessages .= ' ';
				$errorMessages .= sprintf('[%s] %s', $key, $errorMessage);
			}
		}

        $response = ApiResponse::getInstance();
		$response->setError(ApiException::E_PARAM, $errorMessages);

        $e = new ValidationException($validator, $response->toResponse());
        $e->status(200);
		throw $e;
	}

    /**
     * プラットフォーム取得
     * @return int
     */
	public function getPlatform()
    {
        return StrUtil::getPlatform4userAgent($this->userAgent());
    }

    /**
     * Auth必要かを取得
     * @return bool
     */
    public function getNeedAuth()
    {
        return $this->needAuth;
    }

    /**
     * Requesutの根幹共通パラメータ
     * @return array
     */
    public function rules()
    {
        return [
            'app_version' => 'required',
            'unique_id' => 'required',
        ];
    }

}
