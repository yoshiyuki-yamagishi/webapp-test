<?php
    /**
     * イベントプレイヤークエストリストのリクエストパラメータ
     */

    namespace App\Http\Requests;

    class EventPlayerQuestListRequest extends AuthenticatedRequest
    {
        /**
         * @return array
         */
        public function rules()
        {
            $base = collect(parent::rules());
            $base->merge([
                'event_id' => 'required',
            ]);
            return $base->all();
        }
    }
