<?php
/**
 * プレイヤ魔道書枠増加 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ魔道書枠増加 のリクエストパラメータ
 *
 */
class PlayerGrimoireLimitUpRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'limit_up_num'				=> 'required',
        ]);
        return $base->all();
	}

}
