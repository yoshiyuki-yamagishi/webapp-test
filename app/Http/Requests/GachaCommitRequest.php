<?php
/**
 * ガチャ確定 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ確定 のリクエストパラメータ
 *
 */
class GachaCommitRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_gacha_id' => 'required',
        ]);
        return $base->all();
	}

}
