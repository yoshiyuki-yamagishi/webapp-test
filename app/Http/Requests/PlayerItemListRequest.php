<?php
/**
 * プレイヤアイテム一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテム一覧 のリクエストパラメータ
 *
 */
class PlayerItemListRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'item_category' => 'nullable|integer|min:0',
        ]);
        return $base->all();
	}

}
