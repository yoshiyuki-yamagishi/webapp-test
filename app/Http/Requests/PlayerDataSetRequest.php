<?php
/**
 * プレイヤデータ設定 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤデータ設定 のリクエストパラメータ
 *
 */
class PlayerDataSetRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'type'						=> 'required',
            'value'						=> 'required',
        ]);
        return $base->all();
	}

}
