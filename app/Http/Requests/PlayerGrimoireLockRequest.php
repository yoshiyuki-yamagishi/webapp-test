<?php
/**
 * プレイヤ魔道書保護 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ魔道書保護 のリクエストパラメータ
 *
 */
class PlayerGrimoireLockRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'lock_grimoire_list.*.player_grimoire_id' => 'required|integer',
            'unlock_grimoire_list.*.player_grimoire_id' => 'required|integer',
        ]);
        return $base->all();
	}

}
