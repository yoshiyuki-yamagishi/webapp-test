<?php
/**
 * バトルスキップ のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * バトルスキップ のリクエストパラメータ
 *
 */
class BattleSkipRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'quest_category' => 'required|integer|min:1|max:3',
            'chapter_id' => 'required',
            'quest_id' => 'required',
            'skip_ticket_id' => 'required|integer',
            'skip_count' => 'required',
        ]);
        return $base->all();
	}

}
