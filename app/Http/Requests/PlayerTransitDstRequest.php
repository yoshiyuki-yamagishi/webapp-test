<?php
/**
 * プレイヤ引継ぎ先 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ引継ぎ先 のリクエストパラメータ
 *
 */
class PlayerTransitDstRequest extends SimpleRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_id' => 'required',
            'passcode' => 'required',
            'password' => 'required',
            'os_type' => 'required|integer|min:1|max:2',
            'os_version' => 'required|string',
            'model_name' => 'required|string',
        ]);
        return $base->all();
	}

}
