<?php
    /**
     * ショップラインナップ のリクエストパラメータ
     *
     */

    namespace App\Http\Requests;

    /**
     * ショップラインナップ のリクエストパラメータ
     *
     */
    class EventShopLineupRequest extends AuthenticatedRequest
    {

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            $base = collect(parent::rules());
            $base->merge([
                'shop_id' => 'required',
                'event_id' => 'required',
            ]);
            return $base->all();
        }

    }
