<?php
/**
 * プレイヤ登録 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Support\Collection;


/**
 * プレイヤ登録 のリクエストパラメータ
 *
 */
class PlayerRegistRequest extends SimpleRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'unique_id' => 'required|string',
            'os_type' => 'required|integer|min:1|max:2',
            'os_version' => 'required|string',
            'model_name' => 'required|string',
        ]);
        return $base->all();
	}

}
