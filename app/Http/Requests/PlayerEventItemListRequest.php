<?php
    /**
     * プレイヤイベントアイテム一覧 のリクエストパラメータ
     *
     */

    namespace App\Http\Requests;


    /**
     * プレイヤイベントアイテム一覧 のリクエストパラメータ
     *
     */
    class PlayerEventItemListRequest extends AuthenticatedRequest
    {
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            $base = collect(parent::rules());
            $base->merge([
                'event_id' => 'required',
                'item_category' => 'nullable|integer|min:0',
            ]);

            return $base->all();
        }

    }
