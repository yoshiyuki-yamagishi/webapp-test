<?php
/**
 * ガチャ引き直し のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ引き直し のリクエストパラメータ
 *
 */
class GachaRetryRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_gacha_id' => 'required',
        ]);
        return $base->all();
	}

}
