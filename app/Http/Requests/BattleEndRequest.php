<?php
/**
 * バトル終了 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * バトル終了 のリクエストパラメータ
 *
 */
class BattleEndRequest extends AuthenticatedRequest
{
    const RESULT_LOSE = 0; // 敗北
    const RESULT_WIN = 1; // 勝利

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'battle_code' => 'required',
            'result' => 'required|integer|min:0|max:1',
            'dead_flags' => 'required',
        ]);
        return $base->all();
	}

}
