<?php
/**
 * ショップ購入・交換 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ショップ購入・交換 のリクエストパラメータ
 *
 */
class ShopBuyRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_shop_item_id' => 'required',
        ]);
        return $base->all();
	}

}
