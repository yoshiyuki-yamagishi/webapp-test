<?php
/**
 * ログイン のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ログイン のリクエストパラメータ
 *
 */
class LoginRequest extends SimpleRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_id' => 'required',
        ]);
        return $base->all();
	}

}
