<?php
/**
 * プレイヤアイテム欠片パウダー精製 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテム欠片パウダー精製 のリクエストパラメータ
 *
 */
class PlayerItemPowderPurifyRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'powder' => 'required|integer|min:1',
            'player_item_list' => 'required',
            'player_item_list.*.player_item_id' => 'required|integer',
            'player_item_list.*.item_num' => 'required|integer',
        ]);
        return $base->all();
	}

}
