<?php
/**
 * プレイヤキャラクタ更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタ更新 のリクエストパラメータ
 *
 */
class PlayerCharacterUpdateRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_character_id' => 'required',
            'illust_number' => 'required|integer',
            'spine' => 'required|integer',
        ]);
        return $base->all();
	}

}
