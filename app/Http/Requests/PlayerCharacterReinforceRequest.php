<?php
/**
 * プレイヤキャラクタ強化 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタ強化 のリクエストパラメータ
 *
 */
class PlayerCharacterReinforceRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_character_id'				=> 'required',
            'player_item_list'					=> 'required',
            'player_item_list.*.player_item_id'	=> 'required|integer',
            'player_item_list.*.item_num'		=> 'required|integer',
        ]);
        return $base->all();
	}

}
