<?php
/**
 * プレイヤ魔道書強化 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ魔道書強化 のリクエストパラメータ
 *
 */
class PlayerGrimoireReinforceRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_grimoire_id' => 'required',
            'reinforce_type' => 'required|integer|min:0|max:2',
            'slot_no' => 'required_if:reinforce_type,1|required_if:reinforce_type,2|integer',
            'player_item_id' => 'required_if:reinforce_type,0|required_if:reinforce_type,2',
        ]);
        return $base->all();
	}

}
