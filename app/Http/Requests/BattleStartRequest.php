<?php
/**
 * バトル開始 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * バトル開始 のリクエストパラメータ
 *
 */
class BattleStartRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'quest_category' => 'required|integer|min:1|max:3',
            'chapter_id' => 'required',
            'quest_id' => 'required',
            'party_no' => 'required',
        ]);
        return $base->all();
	}

}
