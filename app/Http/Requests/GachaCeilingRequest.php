<?php


namespace App\Http\Requests;


class GachaCeilingRequest extends AuthenticatedRequest
{
    public function rules()
    {
        $base = collect(parent::rules());
        $base->merge([
            'gacha_id' => 'required',
        ]);
        return $base->all();
    }
}
