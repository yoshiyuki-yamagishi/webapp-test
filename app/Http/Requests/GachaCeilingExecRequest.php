<?php


namespace App\Http\Requests;


class GachaCeilingExecRequest extends AuthenticatedRequest
{
    public function rules()
    {
        $base = collect(parent::rules());
        $base->merge([
            'gacha_id' => 'required',
            'ceiling_id' => 'required',
            'exchange_id' => 'required',
        ]);
        return $base->all();
    }
}
