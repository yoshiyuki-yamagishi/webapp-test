<?php
/**
 * 製品一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品一覧 のリクエストパラメータ
 *
 */
class ProductListRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'category' => 'nullable|integer|min:0|max:2',
        ]);
        return $base->all();
	}

}
