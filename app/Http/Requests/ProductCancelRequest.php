<?php
/**
 * 製品キャンセル のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品キャンセル のリクエストパラメータ
 *
 */
class ProductCancelRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_buy_id' => 'required',
        ]);
        return $base->all();
	}

}
