<?php
/**
 * プレイヤプレゼント一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤプレゼント一覧 のリクエストパラメータ
 *
 */
class PlayerPresentListRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'take_flag'					=> 'required',
            'count'					    => 'required',
        ]);
        return $base->all();
	}

}
