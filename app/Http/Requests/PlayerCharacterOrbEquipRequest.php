<?php
/**
 * プレイヤキャラクタオーブ装備 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタオーブ装備 のリクエストパラメータ
 *
 */
class PlayerCharacterOrbEquipRequest extends AuthenticatedRequest
{
    const GRADE_UP_NO = 0;
    const GRADE_UP_YES = 1;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_character_id' => 'required|integer|min:1',
            'equip_mask' => 'required|integer|min:0',
            'player_item_list.*.player_item_id' => 'required|integer',
            'player_item_list.*.item_num' => 'required|integer',
            'platinum_dollar' => 'required|integer|min:0',
            'grade_up' => 'required|integer',
        ]);
        return $base->all();
	}

	/**
	 * グレードアップするかどうかを返す
	 *
	 * @return boolean true: グレードアップする
	 */
    public function gradeUp()
    {
        return $this->grade_up == self::GRADE_UP_YES;
    }

}
