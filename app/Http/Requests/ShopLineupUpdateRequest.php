<?php
/**
 * ショップラインナップ更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * ショップラインナップ更新 のリクエストパラメータ
 *
 */
class ShopLineupUpdateRequest extends AuthenticatedRequest
{
    const PAY_ITEM_NO = 0;
    const PAY_ITEM_YES = 1;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'shop_id' => 'required',
        ]);
        return $base->all();
	}

	/**
	 * アイテムを払って手動で更新するか判定する
	 *
	 * @return true: 払う、false: 払わない
	 */
	public function payItem()
    {
        if (empty($this->pay_item))
            return false;
        return $this->pay_item != self::PAY_ITEM_NO;
    }

}
