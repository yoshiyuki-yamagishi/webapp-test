<?php
/**
 * プレイヤプレゼント受取 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤプレゼント受取 のリクエストパラメータ
 *
 */
class PlayerPresentTakeRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'current_date' => 'required',
            'take_type' => 'required|integer|min:1|max:2',
            'player_present_id' => 'required_if:take_type,1',
            'count' => 'required_if:take_type,2',
        ]);
        return $base->all();
	}

}
