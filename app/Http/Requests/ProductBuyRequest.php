<?php
/**
 * 製品購入 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品購入 のリクエストパラメータ
 *
 */
class ProductBuyRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'store_id' => 'required',
            'product_id' => 'required',
            'num' => 'required',
            'currency' => 'required',
            'amount' => 'required',
        ]);
        return $base->all();
	}

}
