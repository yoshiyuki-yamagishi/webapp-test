<?php
/**
 * プレイヤチュートリアル進行度更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤチュートリアル進行度更新 のリクエストパラメータ
 *
 */
class PlayerTutorialUpdateRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'tutorial_progress' => 'required|integer|min:1|max:255',
            'tutorial_flag' => 'required|integer|min:0',
        ]);
        return $base->all();
	}

}
