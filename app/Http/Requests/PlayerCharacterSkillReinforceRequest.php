<?php
/**
 * プレイヤキャラクタスキル強化 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタスキル強化 のリクエストパラメータ
 *
 */
class PlayerCharacterSkillReinforceRequest extends AuthenticatedRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $base = collect(parent::rules());
        $base->merge([
            'player_character_id' => 'required',
            'skill_id' => 'required',
            'platinum_dollar' => 'required',
        ]);
        return $base->all();
	}

}
