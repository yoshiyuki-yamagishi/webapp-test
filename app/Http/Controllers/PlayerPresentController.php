<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PlayerPresentListRequest;
use App\Http\Requests\PlayerPresentTakeRequest;
use App\Services\PlayerPresentService;

class PlayerPresentController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerPresentListRequest $request
	 * @return
	 */
	public function list(PlayerPresentListRequest $request)
	{
		$response = PlayerPresentService::list($request);
		return $response->toResponse();
	}

	/**
	 * 受取
	 * @param PlayerPresentTakeRequest $request
	 * @return
	 */
	public function take(PlayerPresentTakeRequest $request)
	{
		$response = PlayerPresentService::take($request);
		return $response->toResponse();
	}
}
