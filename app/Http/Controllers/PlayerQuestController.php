<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerQuestListRequest;
use App\Http\Requests\PlayerQuestUpdateRequest;
use App\Services\PlayerQuestService;

class PlayerQuestController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerQuestListRequest $request
	 * @return
	 */
	public function list(PlayerQuestListRequest $request)
	{
		$response = PlayerQuestService::list($request);
		return $response->toResponse();
	}

	/**
	 * 更新
	 * @param PlayerQuestUpdateRequest $request
	 * @return
	 */
	public function update(PlayerQuestUpdateRequest $request)
	{
		$response = PlayerQuestService::update($request);
		return $response->toResponse();
	}
}
