<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Requests\ShopLineupUpdateRequest;
use App\Http\Requests\ShopBuyRequest;
use App\Services\ShopService;



class ShopController extends Controller
{
	/**
	 * ショップ一覧
	 * @return string[]
	 */
	public function list(AuthenticatedRequest $request)
	{
		// ショップ一覧を返す
		$response = ShopService::list($request);
		return $response->toResponse();
	}

	/**
	 * ショップラインナップ更新
	 * @return string[]
	 */
	public function lineupUpdate(ShopLineupUpdateRequest $request)
	{
		// ショップ一覧を返す
		$response = ShopService::lineupUpdate($request);
		return $response->toResponse();
	}

	/**
	 * 購入・交換
	 * @return string[]
	 */
	public function buy(ShopBuyRequest $request)
	{
		// 購入・交換を行う
		// ・イベントアイテム
		// ・キャラクター交換素材
		// ・アイテム交換素材
		// ・レアアイテム交換素材
		$response = ShopService::buy($request);
		return $response->toResponse();
	}

	/**
	 * ショップラインナップ更新
	 * @return string[]
	 */
	public function itemList(AuthenticatedRequest $request)
	{
		// ショップラインナップを更新する
		$response = ShopService::itemList($request);
		return $response->toResponse();
	}
}
