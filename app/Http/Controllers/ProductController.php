<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductListRequest;
use App\Http\Requests\ProductBuyRequest;
use App\Http\Requests\ProductRegistRequest;
use App\Http\Requests\ProductResumeRequest;
use App\Http\Requests\ProductCancelRequest;
use App\Services\ProductService;

class ProductController extends Controller
{
	/**
	 * 一覧
	 * @param ProductListRequest $request
	 * @return
	 */
	public function list(ProductListRequest $request)
	{
		// レシートをチェックし、正しければ、蒼の結晶を増やす
		$response = ProductService::list($request);
		return $response->toResponse();
	}

	/**
	 * 購入
	 * @param ProductBuyRequest $request
	 * @return
	 */
	public function buy(ProductBuyRequest $request)
	{
		// レシートをチェックし、正しければ、蒼の結晶を増やす
		$response = ProductService::buy($request);
		return $response->toResponse();
	}

	/**
	 * 登録
	 * @param ProductRegistRequest $request
	 * @return
	 */
	public function regist(ProductRegistRequest $request)
	{
		// レシートをチェックし、正しければ、蒼の結晶を増やす
		$response = ProductService::regist($request);
		return $response->toResponse();
	}

	/**
	 * レジューム
	 * @param ProductResumeRequest $request
	 * @return
	 */
	public function resume(ProductResumeRequest $request)
	{
		// レシートをチェックし、正しければ、蒼の結晶を増やす
		$response = ProductService::resume($request);
		return $response->toResponse();
	}

	/**
	 * キャンセル
	 * @param ProductCancelRequest $request
	 * @return
	 */
	public function cancel(ProductCancelRequest $request)
	{
		// レシートをチェックし、正しければ、蒼の結晶を増やす
		$response = ProductService::cancel($request);
		return $response->toResponse();
	}

}
