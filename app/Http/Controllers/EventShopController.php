<?php

    namespace App\Http\Controllers;

    use App\Http\Requests\EventShopLineupRequest;
    use App\Services\EventShopService;


    class EventShopController extends Controller
    {

        /**
         * ショップラインナップ更新
         * @return string[]
         */
        public function lineup(EventShopLineupRequest $request)
        {
            // ショップ一覧を返す
            $response = EventShopService::lineup($request);
            return $response->toResponse();
        }

        /**
         * 購入・交換
         * @return string[]
         */
        public function buy(EventShopBuyRequest $request)
        {
            // 購入・交換を行う
            // ・イベントアイテム
            // ・キャラクター交換素材
            // ・アイテム交換素材
            // ・レアアイテム交換素材
            $response = EventShopService::buy($request);
            return $response->toResponse();
        }
    }
