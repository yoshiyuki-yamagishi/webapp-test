<?php

namespace App\Http\Controllers;

use App\Http\Requests\FriendListRequest;
use App\Services\FriendService;

class FriendController extends Controller
{
	/**
	 * 一覧
	 * @return string[]
	 */
	public function list(FriendListRequest $request)
	{
		$response = FriendService::list($request);
		return $response->toResponse();
	}

}
