<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventPlayerQuestListRequest;
use App\Http\Requests\EventTopRequest;
use App\Services\EventService;

class EventController extends Controller
{
	/**
	 * イベントTOP
	 * @param EventTopRequest $request
	 * @return
	 */
	public function top(EventTopRequest $request)
	{
		$response = EventService::top($request);
		return $response->toResponse();
	}

    /**
     * バトル終了後用クリアクエストリスト取得
     * @param EventPlayerQuestListRequest $request
     * @return mixed
     */
    public function playerQuestList(EventPlayerQuestListRequest $request)
    {
        $response = EventService::playerQuestList($request);
        return $response->toResponse();
    }
}
