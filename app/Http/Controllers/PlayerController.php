<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerTransitDstRequest;
use App\Http\Requests\PlayerAccountStartRequest;
use App\Http\Requests\PlayerAccountEndRequest;
use App\Http\Requests\PlayerAccountInfoRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Http\Requests\PlayerTutorialUpdateRequest;
use App\Services\PlayerService;
use App\Http\Requests\PlayerRegistRequest;

class PlayerController extends Controller
{
	/**
	 * 登録
	 * @param PlayerRegistRequest $request
	 * @return
	 */
	public function regist(PlayerRegistRequest $request)
	{
		$response = PlayerService::regist($request);
		return $response->toResponse();
	}


	/**
	 * 取得
	 * @param AuthenticatedRequest $request
	 * @return
	 */
	public function get(AuthenticatedRequest $request)
	{
		$response = PlayerService::get($request);
		return $response->toResponse();
	}

	/**
	 * 更新
	 * @param PlayerUpdateRequest $request
	 * @return
	 */
	public function update(PlayerUpdateRequest $request)
	{
		// プレイヤー名、性別、年齢認証
		$response = PlayerService::update($request);
		return $response->toResponse();
	}

	/**
	 * チュートリアル進行度更新
	 * @param PlayerTutorialUpdateRequest $request
	 * @return
	 */
	public function tutorialUpdate(PlayerTutorialUpdateRequest $request)
	{
		$response = PlayerService::tutorialUpdate($request);
		return $response->toResponse();
	}

	/**
	 * AL回復
	 * @param PlayerAlRecoverRequest $request
	 * @return
	 */
	public function alRecover(PlayerAlRecoverRequest $request)
	{
		$response = PlayerService::alRecover($request);
		return $response->toResponse();
	}

	/**
	 * 引継ぎ元
	 * @param PlayerTransitSrcRequest $request
	 * @return
	 */
	public function transitSrc(PlayerTransitSrcRequest $request)
	{
		$response = PlayerService::transitSrc($request);
		return $response->toResponse();
	}

	/**
	 * 引継ぎ先
	 * @param PlayerTransitDstRequest $request
	 * @return
	 */
	public function transitDst(PlayerTransitDstRequest $request)
	{
		$response = PlayerService::transitDst($request);
		return $response->toResponse();
	}

	/**
	 * アカウント開始
	 * @param PlayerAccountStartRequest $request
	 * @return
	 */
	public function accountStart(PlayerAccountStartRequest $request)
	{
		$response = PlayerService::accountStart($request);
		return $response->toResponse();
	}

	/**
	 * アカウント終了
	 * @param AuthenticatedRequest $request
	 * @return
	 */
	public function accountEnd(AuthenticatedRequest $request)
	{
		$response = PlayerService::accountEnd($request);
		return $response->toResponse();
	}

	/**
	 * アカウント情報
	 * @param PlayerAccountInfoRequest $request
	 * @return
	 */
	public function accountInfo(PlayerAccountInfoRequest $request)
	{
		$response = PlayerService::accountInfo($request);
		return $response->toResponse();
	}

	/**
	 * アカウント復元
	 * @param PlayerAccountRestoreRequest $request
	 * @return
	 */
	public function accountRestore(PlayerAccountRestoreRequest $request)
	{
		$response = PlayerService::accountRestore($request);
		return $response->toResponse();
	}

}
