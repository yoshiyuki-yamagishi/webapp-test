<?php

    namespace App\Http\Controllers;

    use App\Http\Requests\PlayerEventItemListRequest;
    use App\Services\PlayerEventItemService;

    class PlayerEventItemController extends Controller
    {
        /**
         * 一覧
         * @param PlayerEventItemListRequest $request
         * @return
         */
        public function list(PlayerEventItemListRequest $request)
        {
            $response = PlayerEventItemService::list($request);
            return $response->toResponse();
        }

    }
