<?php

namespace App\Http\Controllers;

use App\Http\Requests\SimpleRequest;
use App\Http\Responses\ApiResponse;
use App\Http\Responses\AppVerResponse;
use App\Models\VersionData;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;

/**
 * バージョン情報を返す
 * Class AppVerController
 * @package App\Http\Controllers
 */
class AppVerController extends Controller
{
	/**
	 * バージョン情報を渡す
	 * @param Request $request
	 * @return
	 */
	public function index(SimpleRequest $request)
	{
        $response = ApiResponse::getInstance();

	    // User-AgentからPlatformを取得
        $platform = StrUtil::getPlatform4userAgent($request->userAgent());

        // 指定プラットフォームの最新バージョンを取得
        $versionData = VersionData::getNewestOne($platform);

        // バージョンデータからレスポンス情報作成
        $response->body = AppVerResponse::make($versionData);

		return $response->toResponse();
	}

}
