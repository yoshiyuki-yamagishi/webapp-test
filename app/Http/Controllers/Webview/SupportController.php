<?php

namespace App\Http\Controllers\Webview;

use App\Http\Controllers\Controller;
use App\Services\ContactService;
use App\Services\SessionService;
use Illuminate\Http\Request;
use App\Utils\DateTimeUtil;

class SupportController extends Controller
{
    /**
     * Webview SupportTop
     * @param Request $request
     * @return
     */
    public function top(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now
        ];
        return view('support.top', $param);
    }

    /**
     * Webview SupportFaq
     * @param Request $request
     * @return
     */
    public function faq(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now
        ];
        return view('support.faq', $param);
    }

    /**
     * Webview SupportTerms
     * @param Request $request
     * @return
     */
    public function terms(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now,
            'viewReturn' => true
        ];
        return view('support.terms', $param);
    }

    /**
     * Webview SupportPrivacy
     * @param Request $request
     * @return
     */
    public function privacy(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now,
            'viewReturn' => true
        ];
        return view('support.privacy', $param);
    }

    /**
     * Webview SupportCopyright
     * @param Request $request
     * @return
     */
    public function copyright(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now
        ];
        return view('support.copyright', $param);
    }

    /**
     * Webview SupportContactSelect
     * @param Request $request
     * @return
     */
    public function ContactSelect(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);

        // 各種パラメータを設定する
        $params = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,

            'subject_list' => ContactService::SUBJECT_LIST,
        ];

        return view('support.contactSelect', $params);
    }

    /**
     * Webview SupportContact
     * @param Request $request
     * @return
     */
    public function Contact(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);

        // 各種パラメータを設定する
        $params = ContactService::makeMail($request);

        return view('support.contact', $params);
    }

    // Topから遷移用
    /**
     * Webview SupportTermsTop
     * @return
     */
    public function terms_top()
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'viewReturn' => false
        ];
        return view('support.terms', $param);
    }

    /**
     * Webview SupportPrivacy
     * @return
     */
    public function privacy_top()
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'viewReturn' => false
        ];
        return view('support.privacy', $param);
    }
}
