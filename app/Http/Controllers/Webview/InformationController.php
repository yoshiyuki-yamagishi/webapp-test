<?php

namespace App\Http\Controllers\Webview;

use App\Http\Controllers\Controller;
use App\Models\Information;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;
use Illuminate\Http\Request;
use App\Utils\DateTimeUtil;

class InformationController extends Controller
{
    /**
     * Webview InfoList
     * @param Request $request
     * @return
     */
    public function list(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        // Platform取得
        $platform = StrUtil::getPlatform4userAgent($request->userAgent());

        $infoType = Information::INFORMATION_TYPE_ALL;
        $infoList = [];

        if (!isset($request->info_type) || $request->info_type == Information::INFORMATION_TYPE_ALL) {
            // お知らせタイプが未セット or ALL
            $infoType = Information::INFORMATION_TYPE_ALL;
            $infoList = Information::getAllViewList($platform, $now);
        } else {
            // タイプが指定されている
            $infoType = (int)$request->info_type;
            $infoList = Information::getTypeViewList($infoType, $platform, $now);
        }

        $param = [
            // デフォルト群
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now,

            // 必要パラメータ
            'info_type' => $infoType,   // お知らせタイプ
            'info_list' => $infoList,   // お知らせリスト
        ];
        return view('information.list', $param);
    }

    /**
     * Webview info
     * @param Request $request
     * @return
     */
    public function info(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        // データ取得
        $info = Information::getOne($request->info_id);

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now,

            // 必要パラメータ
            'info_type' => $request->info_type,   // お知らせタイプ
            'info' => $info
        ];
        return view('information.info', $param);
    }
}
