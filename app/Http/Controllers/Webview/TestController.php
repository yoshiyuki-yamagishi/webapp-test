<?php

namespace App\Http\Controllers\Webview;

use App\Http\Controllers\Controller;
use App\Models\MasterModelManager;
use App\Services\SessionService;
use Illuminate\Http\Request;
use App\Http\Responses\AppVerResponse;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;
use App\Utils\DateTimeUtil;

use App\Models\MasterModels\HomeBanner;
use App\Models\VersionData;
use App\Models\MasterModels\Mission;
use App\Models\Information;

class TestController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        // ================================================================
        $manager = new MasterModelManager();
        $manager->reload();
//        $str = "";
//        $platform = StrUtil::getPlatform4userAgent($request->userAgent());
//        $list = Information::getAllViewList($platform, $now);
//        $data = VersionData::getNewestOne($platform);
//        DebugUtil::e_log_ex("Test.log", "ReturnData", $data);
//        $res = AppVerResponse::make($data);
//        DebugUtil::e_log_ex("Test.log", "ResponseData", $res);


//        $one = $list[HomeBanner::BANNER_LIST_TYPE_HOME][HomeBanner::BANNER_TYPE_MAIN];
//        $m = Mission::getOne(101001);
        // columns
//        $vars = get_object_vars($m);
////        $vars = get_class_vars(get_class($m));
//        $keys = array_keys($vars);
//        $cols = $list[0]->getColumns();
//        $str .= implode(" ,", $cols). "\n";
//        DebugUtil::e_log("Test.log", "Tables columns", $str);
//        // params
//        $str = "";
//        foreach ($list as $info)
//        {
//            $val = [];
//            foreach ($cols as $col) { $val[] = $info->{$col}; }
//            $str .= $info->id. ": ". implode(" ,", $val). "\n";
//        }
////        $missionTypes = array(Mission::TYPE_DAILY, Mission::TYPE_NORMAL, Mission::TYPE_EVENT);
////        $missions = Mission::getAllByKeyId($now, $missionTypes);
////        foreach ($missions as $id => $mission)
////        {
////            if ($mission->remuneration_item_type != 2) continue;
////            $values = [];
////            foreach ($keys as $k) { if ("isValidate" != $k && "_TSPEC" != $k) $values[] = $mission->{$k}; }
////            $str .= $id.": ". implode(" ,", $values). "\n";
////        }
//        DebugUtil::e_log("Test.log", "Tables values", $str);
        // ================================================================

        // テスト用パラメータ
        $param = [
            'player_id' => 101,
            'app_version' => "1.0.0"
        ];
        return view('browse.test', $param);
    }
}
