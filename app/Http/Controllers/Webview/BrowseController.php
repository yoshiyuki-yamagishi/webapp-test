<?php

namespace App\Http\Controllers\Webview;

use App\Http\Controllers\Controller;
use App\Services\SessionService;
use Illuminate\Http\Request;
use App\Utils\DateTimeUtil;

class BrowseController extends Controller
{
    /**
     * Webview BrowseHelp
     * @param Request $request
     * @return
     */
    public function help(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now
        ];
        return view('browse.help', $param);
    }

    /**
     * Webview BrowseCredit
     * @param Request $request
     * @return
     */
    public function credit(Request $request)
    {
        SessionService::start(0, SessionService::SS_WEBVIEW);
        $now = DateTimeUtil::getNOW();

        $param = [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now
        ];
        return view('browse.credit', $param);
    }
}
