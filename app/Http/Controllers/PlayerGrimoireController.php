<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireLockRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Services\PlayerGrimoireService;

class PlayerGrimoireController extends Controller
{
	/**
	 * 一覧
	 * @param AuthenticatedRequest $request
	 * @return
	 */
	public function list(AuthenticatedRequest $request)
	{
		$response = PlayerGrimoireService::list($request);
		return $response->toResponse();
	}

	/**
	 * 保護
	 * @param PlayerGrimoireLockRequest $request
	 * @return
	 */
	public function lock(PlayerGrimoireLockRequest $request)
	{
		$response = PlayerGrimoireService::lock($request);
		return $response->toResponse();
	}

	/**
	 * 強化
	 * @param PlayerGrimoireReinforceRequest $request
	 * @return
	 */
	public function reinforce(PlayerGrimoireReinforceRequest $request)
	{
		$response = PlayerGrimoireService::reinforce($request);
		return $response->toResponse();
	}

	/**
	 * 枠増加
	 * @param PlayerGrimoireLimitUpRequest $request
	 * @return
	 */
	public function limitUp(PlayerGrimoireLimitUpRequest $request)
	{
		$response = PlayerGrimoireService::limitUp($request);
		return $response->toResponse();
	}

	/**
	 * 売却
	 * @param PlayerGrimoireSellRequest $request
	 * @return
	 */
	public function sell(PlayerGrimoireSellRequest $request)
	{
		$response = PlayerGrimoireService::sell($request);
		return $response->toResponse();
	}
}
