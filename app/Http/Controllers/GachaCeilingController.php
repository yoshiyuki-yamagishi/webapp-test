<?php


namespace App\Http\Controllers;


use App\Http\Requests\GachaCeilingRequest;
use App\Http\Requests\GachaCeilingExecRequest;
use App\Services\GachaCeilingPointService;

class GachaCeilingController extends Controller
{
    /**
     * @param GachaCeilingRequest $request
     * @return mixed
     */
    public function list(GachaCeilingRequest $request)
    {
        $response = GachaCeilingPointService::list($request);
        return $response->toResponse();
    }

    /**
     * 実行
     * @param GachaCeilingExecRequest $request
     * @return
     */
    public function exec(GachaCeilingExecRequest $request)
    {
        $response = GachaCeilingPointService::exec($request);
        return $response->toResponse();
    }

}
