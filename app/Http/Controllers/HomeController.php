<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Services\HomeService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	/**
	 * ホーム
	 * @param AuthenticatedRequest $request
	 * @return
	 */
	public function index(AuthenticatedRequest $request)
	{
		$response = HomeService::index($request);
		return $response->toResponse();
	}
}
