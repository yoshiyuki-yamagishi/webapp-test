<?php

namespace App\Http\Controllers;

use App\Http\Requests\FollowAddRequest;
use App\Http\Requests\FollowDeleteRequest;
use App\Services\FollowService;

class FollowController extends Controller
{
	/**
	 * 追加
	 * @return string[]
	 */
	public function add(FollowAddRequest $request)
	{
		$response = FollowService::add($request);
		return $response->toResponse();
	}

	/**
	 * 削除
	 * @return string[]
	 */
	public function delete(FollowDeleteRequest $request)
	{
		$response = FollowService::delete($request);
		return $response->toResponse();
	}

}
