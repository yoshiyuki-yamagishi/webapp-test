<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Requests\PlayerDataSetRequest;
use App\Services\PlayerDataService;



class PlayerDataController extends Controller
{
	/**
	 * 取得
	 * @param AuthenticatedRequest $request
	 * @return
	 */
	public function get(AuthenticatedRequest $request)
	{
		$response = PlayerDataService::get($request);
		return $response->toResponse();
	}

	/**
	 * 設定
	 * @param PlayerDataSetRequest $request
	 * @return
	 */
	public function set(PlayerDataSetRequest $request)
	{
		$response = PlayerDataService::set($request);
		return $response->toResponse();
	}
}
