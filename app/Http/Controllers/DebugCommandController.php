<?php

namespace App\Http\Controllers;

use App\Http\Requests\DebugCommandAddItemRequest;
use App\Services\DebugCommandService;

class DebugCommandController extends Controller
{
	/**
	 * アイテム追加
	 * @param DebugCommandAddItemRequest $request
	 * @return
	 */
	public function addItem(DebugCommandAddItemRequest $request)
	{
		$response = DebugCommandService::addItem($request);
		return $response->toResponse();
	}

}
