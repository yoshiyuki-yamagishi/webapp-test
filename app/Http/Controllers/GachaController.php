<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Requests\GachaExecRequest;
use App\Http\Requests\GachaCommitRequest;
use App\Http\Requests\GachaRetryRequest;
use App\Services\GachaService;



class GachaController extends Controller
{
	/**
	 * 実行
	 * @param AuthenticatedRequest $request
	 * @return
	 */
	public function list(AuthenticatedRequest $request)
	{
		$response = GachaService::list($request);
		return $response->toResponse();
	}

	/**
	 * 実行
	 * @param GachaExecRequest $request
	 * @return
	 */
	public function exec(GachaExecRequest $request)
	{
		$response = GachaService::exec($request);
		return $response->toResponse();
	}

	/**
	 * 引き直し
	 * @param GachaRetryRequest $request
	 * @return
	 */
	public function retry(GachaRetryRequest $request)
	{
		$response = GachaService::retry($request);
		return $response->toResponse();
	}

	/**
	 * 実行
	 * @param GachaCommitRequest $request
	 * @return
	 */
	public function commit(GachaCommitRequest $request)
	{
		$response = GachaService::commit($request);
		return $response->toResponse();
	}

}
