<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Http\Requests\PlayerItemPowderPurifyRequest;
use App\Http\Requests\PlayerItemOrbSynthesizeRequest;
use App\Services\PlayerItemService;

class PlayerItemController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerItemListRequest $request
	 * @return
	 */
	public function list(PlayerItemListRequest $request)
	{
		$response = PlayerItemService::list($request);
		return $response->toResponse();
	}

	/**
	 * 売却
	 * @param PlayerItemSellRequest $request
	 * @return
	 */
	public function sell(PlayerItemSellRequest $request)
	{
		$response = PlayerItemService::sell($request);
		return $response->toResponse();
	}

	/**
	 * 欠片パウダー精製
	 * @param PlayerItemPowderPurifyRequest $request
	 * @return
	 */
	public function powderPurify(PlayerItemPowderPurifyRequest $request)
	{
		$response = PlayerItemService::powderPurify($request);
		return $response->toResponse();
	}

	/**
	 * オーブ合成
	 * @param PlayerItemOrbSynthesizeRequest $request
	 * @return
	 */
	public function orbSynthesize(PlayerItemOrbSynthesizeRequest $request)
	{
		$response = PlayerItemService::orbSynthesize($request);
		return $response->toResponse();
	}

}
