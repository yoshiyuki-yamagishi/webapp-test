<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Requests\LoginRequest;
use App\Services\LoginService;

class LoginController extends Controller
{
	/**
	 * ログイン
	 */
	public function index(LoginRequest $request) {
		// ログイン
		$response = LoginService::index($request);
		return $response->toResponse();
	}

	/**
	 * 認証コード更新
	 */
	public function update(LoginRequest $request) {
		// 認証コード更新
		$response = LoginService::update($request);
		return $response->toResponse();
	}

    /**
     * バトル中断確認用
     */
    public function battleCode(AuthenticatedRequest $request) {
        // バトル中断確認用
        $response = LoginService::getBattleCode($request);
        return $response->toResponse();
    }

}
