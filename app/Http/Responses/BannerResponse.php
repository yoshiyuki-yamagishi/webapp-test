<?php
/**
 * Bannerのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerBanner;



/**
 * Bannerのレスポンス
 *
 */
class BannerResponse
{
	/**
	 * Bannerのレスポンス作成
	 *
	 * @param Banner $banner Bannerのインスタンス
	 * @return list Bannerのレスポンス
	 */
	public static function make($banner)
	{
        $params = [
            "id",
            "banner_type",
            "priority",
            "banner_pic",
            "link_type",
            "link_id",
            "start_day",
            "end_day",
        ];

        foreach ($params as $param)
            $body[$param] = $banner->$param;

		return $body;
	}
}