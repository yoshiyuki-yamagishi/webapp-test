<?php
/**
 * PlayerMissionのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Utils\DebugUtil;


/**
 * PlayerMissionのレスポンス
 *
 */
class PlayerMissionResponse
{
    const STATE_NONE = 0;
    const STATE_NEW_RELEASED = 1; // 今回、開放
    const STATE_NEW_ACHIVED = 2; // 今回、達成

	/**
	 * PlayerMissionのレスポンス作成
	 *
	 * @param PlayerMission $playerMission PlayerMissionのインスタンス
	 * @return array PlayerMissionのレスポンス
	 */
	public static function make($playerMission, $takeFlag)
	{
        $stateFlag = self::STATE_NONE; // 報告時以外は、0

		$body = [
			'mission_id' => $playerMission->mission_id,
			'player_mission_id' => $playerMission->id,
            'mission_type' => $playerMission->mission_type,
            'progress_count' => $playerMission->progress_count,
            'count' => $playerMission->count,
            'take_flag' => $takeFlag,
            'state_flag' => $stateFlag,
            'remuneration_type' => $playerMission->remuneration_type,
            'remuneration' => $playerMission->remuneration,
            'remuneration_count' => $playerMission->remuneration_count,
            'start_day' => $playerMission->start_day,
            'end_day' => $playerMission->end_day,
		];

		return $body;
	}

	/**
	 * PlayerMissionのレスポンス作成
	 *
	 * @param Mission $mission Missionのインスタンス
	 * @param PlayerMission $playerMission PlayerMissionのインスタンス
	 * @return array PlayerMissionのレスポンス
	 */
	public static function makeEx($mission, $playerMission, $released)
	{
        $isPm = isset($playerMission);
        $isTaked = $isPm && isset($playerMission->taked_at);
        $newState = false;

        if($isPm){
            $newState = $playerMission->getNewState();
        }

        if ($released)
            $stateFlag = self::STATE_NEW_RELEASED;
        else if ($newState)
            $stateFlag = self::STATE_NEW_RELEASED;
        else if ($isPm && $playerMission->unreported())
            $stateFlag = self::STATE_NEW_ACHIVED;
        else
            $stateFlag = self::STATE_NONE;

		$body = [
			'mission_id' => $mission->id,
			'player_mission_id' => ($isPm ? $playerMission->id : 0),
            'mission_type' => $mission->mission_type,
            'progress_count' => ($isPm ? $playerMission->progress_count : 0),
            'count' => $mission->count,
            'take_flag' => ($isTaked ? 1 : 0),
            'state_flag' => $stateFlag,
            'remuneration_type' => $mission->remuneration_item_type,
            'remuneration' => $mission->mission_remuneration,
            'remuneration_count' => $mission->mission_remuneration_count,
            'start_day' => $mission->start_day,
            'end_day' => $mission->end_day,
		];

        // DebugUtil::e_log('PMR', 'mission', $mission);
		return $body;
	}

}
