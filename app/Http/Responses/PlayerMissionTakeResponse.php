<?php
/**
 * PlayerMissionTakeのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Utils\DebugUtil;


/**
 * PlayerMissionTakeのレスポンス
 *
 */
class PlayerMissionTakeResponse
{
	/**
	 * PlayerMissionTakeのレスポンス作成
	 *
	 * @param PlayerMission $playerMission PlayerMissionのインスタンス
	 * @return array PlayerMissionTakeのレスポンス
	 */
	public static function make($playerMission)
	{
        // $isTaked = isset($playerMission->taked_at);
        
		$body = [
			'mission_id' => $playerMission->mission_id,
			'player_mission_id' => $playerMission->id,
            'mission_type' => $playerMission->mission_type,
            'remuneration' => $playerMission->remuneration,
            'remuneration_count' => $playerMission->remuneration_count,
		];
        
        // DebugUtil::e_log('PMTR', 'playerMission', $playerMission);
		return $body;
	}
}
