<?php
/**
 * ShopItemListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Http\Responses\ShopResponse;
use App\Utils\DebugUtil;



/**
 * ShopItemListのレスポンス
 *
 */
class ShopItemListResponse
{
	/**
	 * ShopItemListのレスポンス作成
	 *
	 * @param array $shopItemList PlayerShopItem の配列
	 * @return array ShopItemListのレスポンス
	 */
	public static function make($shopItemList)
	{
		$body = [];
		foreach ($shopItemList as $shopItem)
		{
			$body[] = ShopItemResponse::make($shopItem);
		}
		return $body;
	}
    
}