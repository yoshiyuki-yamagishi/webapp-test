<?php
/**
 * PlayerPresentのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerPresentのレスポンス
 *
 */
class PlayerPresentResponse
{
	/**
	 * PlayerPresentのレスポンス作成
	 *
	 * @param PlayerPresent $playerPresent PlayerPresentのインスタンス
	 * @return array PlayerPresentのレスポンス
	 */
	public static function make($playerPresent)
	{
        // pic は廃止
		$body = [
			'player_present_id' => $playerPresent->id,
			'type' => $playerPresent->item_type,
			'general_id' => $playerPresent->item_id,
			'num' => $playerPresent->item_num,
			'message' => $playerPresent->message,
			// 'pic' => $playerPresent->pic,
			'taked_at' => $playerPresent->taked_at,
			'expired_at' => $playerPresent->expired_at,
		];
		return $body;
	}
    
}