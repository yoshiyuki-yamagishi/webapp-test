<?php
/**
 * LoginBonusのレスポンス
 *
 */

namespace App\Http\Responses;

/**
 * LoginBonusのレスポンス
 *
 */
class LoginBonusResponse
{
    const NOT_TAKE = 0;
    const TAKE = 1;

	/**
	 * login_bonus のレスポンス作成
	 *
	 * @param array $loginBonus マスター login_bonus
	 * @param array $reward マスター login_bonus_reward
	 * @param array $takeFlag 取得フラグ
	 * @return LoginBonus レスポンス
	 */
	public static function make(
        $loginBonus, $reward, $takeFlag
    )
	{
        // pic, name, stated_at, ended_at, priority は廃止

        $ret = [
            'id' => $reward->login_bonus_id,
            // 'pic' => $loginBonus->title_pic,
            // 'name' => $loginBonus->login_bonus_description,
            // 'stated_at' => $loginBonus->start_day,
            // 'ended_at' => $loginBonus->end_day,
            'count' => $reward->count,
            'remuneration_type' => $reward->remuneration_item_type,
            'remuneration' => $reward->remuneration,
            'num' => $reward->number,
            // 'priority' => $loginBonus->priority,
            'take_flag' => $takeFlag,
        ];
        return $ret;
	}

}
