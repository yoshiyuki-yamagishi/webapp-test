<?php
/**
 * ShopItemのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ShopExchange;

/**
 * ShopItemのレスポンス
 *
 */
class ShopItemResponse
{
	/**
	 * ShopItemのレスポンス作成
	 *
	 * @param object $shopItem PlayerShopItem のインスタンス
	 * @return array ShopItemのレスポンス
	 */
	public static function make($shopItem)
	{
        $shopExchange = ShopExchange::getOne($shopItem->shop_exchange_id);
        if (empty($shopExchange))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'shop_exchange', 'id', $shopItem->shop_exchange_id
            );
        }

		$body = [
			'shop_exchange_id' => $shopItem->shop_exchange_id,
            'player_shop_item_id' => $shopItem->id,
			'shop_id' => $shopItem->shop_id,
			'item_type' => $shopExchange->lineup_category,
			'item_id' => $shopExchange->lineup_id,
			'get_count' => $shopExchange->get_count,
			'priority' => $shopExchange->priority,
			'pay_item_contents_id' => $shopExchange->pay_item_contents_id,
			'pay_item_count' => $shopExchange->pay_item_count,
			'buy_count' => $shopItem->buy_count,
			'limit' => $shopExchange->limit,
		];

		return $body;
	}
}
