<?php
/**
 * PlayerQuestListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerBattle;
use App\Utils\ListUtil;

/**
 * PlayerQuestListのレスポンス
 *
 */
class PlayerQuestListResponse
{
	/**
	 * PlayerQuestListのレスポンス作成
	 *
	 * @param array $playerQuestList PlayerQuestの配列
a	 * @param string $now 現在日付
	 * @return array PlayerQuestListのレスポンス
	 */
	public static function make($playerQuestList, $now)
	{
        $dailyCounts = []; // キャッシュ
		$body = [];
		foreach ($playerQuestList as $playerQuest)
		{
            $dailyCount = 0; // 不要の場合は 0

            if ($playerQuest->needDailyClearCount())
            {
                // キャラクタークエスト、イベントで
                // 章ごとのデイリークリア回数を返す
                
                $keys = [
                    $playerQuest->player_id,
                    $playerQuest->quest_category,
                    $playerQuest->chapter_id
                ];

                $dailyCount = ListUtil::get_if($dailyCounts, $keys, -1);
                if ($dailyCount < 0)
                {
                    $dailyCount = 0;
                    $playerBattleList = PlayerBattle::getChapterDailyClearCount(
                        $keys[0], $keys[1], $keys[2], $now
                    );
                    if($playerBattleList){
                        foreach ($playerBattleList as $playerBattle){
                            $dailyCount += $playerBattle->repeat_count;
                        }
                    }
                }
            }
			$body[] = PlayerQuestResponse::make($playerQuest, $dailyCount);
		}
        
		return $body;
	}
    
}
