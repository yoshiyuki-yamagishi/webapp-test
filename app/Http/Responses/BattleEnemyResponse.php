<?php
/**
 * BattleEnemyのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\MasterModels\Enemy;
use App\Models\MasterModels\EnemyAI;
use App\Utils\DebugUtil;

/**
 * BattleEnemyのレスポンス
 *
 */
class BattleEnemyResponse
{
	/**
	 * BattleEnemyのレスポンス作成
	 *
	 * @param Enemy $enemy Enemyのインスタンス
	 * @return array BattleEnemyのレスポンス
	 */
	public static function make($battleEnemy)
	{
        // DebugUtil::e_log('BER', 'battleEnemy', $battleEnemy);
        
		// エネミー取得
		// $enemy = Enemy::getOne($battleEnemy->enemy_id);
        // DebugUtil::e_log('BER', 'enemy', $enemy);

		// エネミーAI取得
		// $enemyAI = EnemyAI::getOne($battleEnemy->enemy_ai_id);
        // DebugUtil::e_log('BER', 'enemyAI', $enemyAI);

        // 現在、検収中のバージョンでは、
        // info_sort, hp_bar, resist_abnormal_id がない

        $params = [
            "wave",
            "position",
            "enemy_id",
            "info_sort",
            "boss_flag",
            "level",
            "element",
            "hp_bar",
            "hp_coefficient",
            "atk_coefficient",
            "def_coefficient",
            "resist_abnormal_id",
            "active1",
            "active2",
            "enemy_ai_id",
            "battle_info_id",
            "gold_rate",
            "silver_rate",
            "copper_rate",
        ];

        foreach ($params as $param)
        {
            /*
            if ($param == 'enemy_id')
            {
                $body['enemy'] = EnemyResponse::make($enemy);
                continue;
            }
            if ($param == 'enemy_ai_id')
            {
                $body['enemy_ai'] = EnemyAIResponse::make($enemyAI);
                continue;
            }
            */
            $body[$param] = $battleEnemy->$param;
        }
        
		return $body;
	}

	/**
	 * レスポンスに不要な情報を削る
	 *
	 * @param array $item BattleEnemyのレスポンス
	 */
	public static function fixResponse(&$item)
	{
        unset($item['gold_rate']);
        unset($item['silver_rate']);
        unset($item['copper_rate']);
        unset($item['drop_items']);
    }
    
}