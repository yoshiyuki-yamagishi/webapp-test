<?php
/**
 * PlayerCharacterListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerCharacterListのレスポンス
 *
 */
class PlayerCharacterListResponse
{
	/**
	 * PlayerCharacterListのレスポンス作成
	 *
	 * @param array $playerCharacterList PlayerCharacterの配列
	 * @return array PlayerCharacterListのレスポンス
	 */
	public static function make($player, $playerCharacterList)
	{
		$body = [];
		foreach ($playerCharacterList as $playerCharacter)
		{
            if (empty($playerCharacter))
                continue;
            
			$body[]	= PlayerCharacterResponse::make($player, $playerCharacter);
		}
		return $body;
	}
}