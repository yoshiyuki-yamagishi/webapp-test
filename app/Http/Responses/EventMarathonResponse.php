<?php
    /**
     * PlayerQuestListのレスポンス
     *
     */

    namespace App\Http\Responses;

    use App\Services\QuestService;
    use App\Models\PlayerBattle;
    use App\Utils\DebugUtil;
    use App\Utils\ListUtil;

    /**
     * マラソンイベントTOPのレスポンス
     *
     */
    class EventMarathonResponse
    {
        /**
         * マラソンイベントTOPのレスポンス作成
         *
         * @param array $playerQuestList PlayerQuestの配列
         * a     * @param string $now 現在日付
         * @return array PlayerQuestListのレスポンス
         */
        public static function make($playerEventData, $eventStatus)
        {
            $playerQuestList = [];
            $playerEventItemDataList = [];
            foreach ($playerEventData['player_quest_list'] as $playerQuest){
                $playerQuestList[] = PlayerQuestResponse::make($playerQuest,0);
            }

            foreach ($playerEventData['player_event_status']->event_item_dataList as $playerEventItemData){
                $playerEventItemDataList[] = PlayerEventItemResponse::make($playerEventItemData);
            }
            $playerEventStatus = [
                'event_id' => $playerEventData['player_event_status']->event_id,
                'point' => $playerEventData['player_event_status']->event_id,
                'clear_percent' => $playerEventData['player_event_status']->clear_percent,
                'introduction_id' => (string)$playerEventData['player_event_status']->introduction_id,
                'player_event_item' => $playerEventItemDataList
            ];

            $body = [
                'player_quest_list' => $playerQuestList,
                'player_event_status' => $playerEventStatus,
                'event_status' => $eventStatus
            ];
            return $body;
        }
    }
