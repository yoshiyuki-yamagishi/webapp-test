<?php
/**
 * BannerListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Http\Responses\BannerResponse;



/**
 * BannerListのレスポンス
 *
 */
class BannerListResponse
{
	/**
	 * BannerListのレスポンス作成
	 *
	 * @param array $bannerList bannerListの配列
	 * @return array BannerListのレスポンス
	 */
	public static function make($bannerList)
	{
		$body = [];
		foreach ($bannerList as $banner)
		{
			$body[] = BannerResponse::make($banner);
		}
		return $body;
	}
    
}
