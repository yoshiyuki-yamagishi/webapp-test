<?php
/**
 * PlayerPresentListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerPresentListのレスポンス
 *
 */
class PlayerPresentListResponse
{
	/**
	 * PlayerPresentListのレスポンス作成
	 *
	 * @param array $playerPresentList PlayerPresentの配列
	 * @return array $playerPresentListのレスポンス
	 */
	public static function make($playerPresentList)
	{
		$body = [];
		foreach ($playerPresentList as $playerPresent)
		{
			$body[] = PlayerPresentResponse::make($playerPresent);
		}
		return $body;
	}
}