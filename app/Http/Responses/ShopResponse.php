<?php
/**
 * Shopのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * Shopのレスポンス
 *
 */
class ShopResponse
{
	/**
	 * Shopのレスポンス作成
	 *
	 * @param Shop $shop Shopのインスタンス
	 * @return array Shopのレスポンス
	 */
	public static function make($shop)
	{
		$body = [
			'shop_id'			=> $shop->id,
			'shop_category'		=> $shop->shop_category,
			'start_day'			=> $shop->start_day,
			'end_day'			=> $shop->end_day,
			'reset_time1'		=> $shop->reset_time1,
			'reset_time2'		=> $shop->reset_time2,
			'reset_time3'		=> $shop->reset_time3,
			'reset_item_id'		=> $shop->reset_item_id,
			'reset_item_count'	=> $shop->reset_item_count,
			'event_banner'		=> $shop->event_banner,
		];
		return $body;
	}
}