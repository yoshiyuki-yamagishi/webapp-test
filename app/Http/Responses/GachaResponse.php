<?php
/**
 * Gachaのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerGacha;
use App\Services\GachaCeilingPointService;
use App\Utils\DebugUtil;


/**
 * Gachaのレスポンス
 *
 */
class GachaResponse
{
	/**
	 * Gachaのレスポンス作成
	 *
	 * @param integer $playerId プレイヤID
	 * @param Gacha $gacha Gachaのインスタンス
	 * @param string $now 現在日付
     * @param boolean $gachaCeilingOpenFlag　天井有無フラグ
	 * @return list Gachaのレスポンス
	 */
	public static function make($playerId, $gacha, $now)
	{
        $limitCount = PlayerGacha::getLimitCount($playerId, $gacha, $now);
        $gachaPointFlag = GachaCeilingPointService::isGachaCeilingOpen($gacha->id, $now);
        $playerGachaPoint = GachaCeilingPointService::getPlayerGachaPointData($playerId, $gacha->id);
        //交換できる物があるか
        $exchangeAbleGachaPointItem = GachaCeilingPointService::checkExchangeAbleList($playerId, $gacha->id);
        $params = [
            'gacha_id',
            // 'gacha_name_id',
            // 'gacha_type',
            // 'priority',
            // 'item_id',
            // 'single_item',
            // 'single_item_get_count',
            // 'ten_item',
            // 'ten_item_get_count',
            // 'gacha_loop',
            // 'limit_type',
            'limit_count',
            // 'gacha_limit',
            // 'gacha_banner',
            // 'gacha_pic',
            // 'gacha_lot_id',
            // 'confirm',
            // 'release_day',
            // 'end_day',
            'gacha_point_flag',
            'player_gacha_point',
            'exchangeable_gacha_ceiling_point_item'
        ];

        foreach ($params as $param)
        {
            if ($param == 'gacha_id')
            {
                $body[$param] = $gacha->id;
                continue;
            }
            else if ($param == 'limit_count')
            {
                $body[$param] = $limitCount;
                continue;
            }
            else if($param == 'gacha_point_flag')
            {
                $body[$param] = $gachaPointFlag;
                continue;
            }
            else if($param == 'player_gacha_point')
            {
                $body[$param] = $playerGachaPoint;
                continue;
            }
            else if($param == 'exchangeable_gacha_ceiling_point_item')
            {
                $body[$param] = $exchangeAbleGachaPointItem;
                continue;
            }
                $body[$param] = $gacha->$param;
        }

		return $body;
	}
}
