<?php
/**
 * FriendListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * FriendListのレスポンス
 *
 */
class FriendListResponse
{
	/**
	 * FriendListのレスポンス
	 *
	 * @param array $playerFriendList FriendListの配列
	 * @param boolean $useFollow id として follow を使うか
	 * @return array FriendListのレスポンス
	 */
	public static function make($playerFriendList, $useFollow)
	{
		$body = [];
		foreach ($playerFriendList as $playerFriend)
		{
			$body[]	= FriendResponse::make($playerFriend, $useFollow);
		}
		return $body;
	}
}
