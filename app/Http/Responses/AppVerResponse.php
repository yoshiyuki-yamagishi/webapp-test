<?php
/**
 * AppVerのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * AppVerのレスポンス
 *
 */
class AppVerResponse
{
    /**
     * VersionDataのレスポンス情報作成
     *
     * @param $versionData
     * @return array
     */
	public static function make($versionData)
	{
	    return [
	        'app_version' => $versionData->app_version,
            'master_version' => $versionData->master_version,
            'ab_version' => $versionData->ab_version
        ];
	}
}
