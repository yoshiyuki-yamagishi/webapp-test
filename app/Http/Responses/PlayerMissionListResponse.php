<?php
/**
 * PlayerMissionListのレスポンス
 *
 */

namespace App\Http\Responses;

use App\Services\GiveOrPayParam;
use App\Services\MissionChecker\MissionChecker;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;

/**
 * PlayerMissionListのレスポンス
 *
 */
class PlayerMissionListResponse
{
	/**
	 * PlayerMissionListのレスポンス生成
	 *
	 * @param array $missionList ミッションの配列
	 * @param array $playerMissionList プレイヤミッションの配列
	 * @return array PlayerMissionListのレスポンス
	 */

	public static function make(
        $playerMissionList
    )
	{
		$body = [];

        foreach ($playerMissionList as $playerMission)
        {
            if (isset($playerMission->taked_at))
                $takeFlag = GiveOrPayParam::TAKE_FLAG_TAKE;
            else
                $takeFlag = GiveOrPayParam::TAKE_FLAG_NONE;
                
			$body[]	= PlayerMissionResponse::make(
                $playerMission, $takeFlag
            );
        }
        
		return $body;
	}
    
}
