<?php
/**
 * QuestRewardのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\MasterModels\Item;
use App\Utils\DebugUtil;
use App\Services\GiveOrPayParam;

/**
 * QuestRewardのレスポンス
 *
 */
class QuestRewardResponse
{
	/**
	 * PlayerQuestRewardのレスポンス作成
	 *
	 * @param PlayerBattleReward $playerReward
	 * @return QuestRewardのレスポンス
	 */
	public static function make($playerReward, $takeFlag)
	{
		$body = [
			'frame_count' => $playerReward->frame_count,
			'item_type' => $playerReward->item_type,
			'item_id' => $playerReward->item_id,
			'item_num' => $playerReward->item_num,
			'take_flag' => $takeFlag,
		];
		return $body;
	}

	/**
	 * QuestReward からQuestRewardのレスポンス作成
	 *
	 * @param array $questReward QuestReward
	 * @return array QuestRewardのレスポンス
	 */
	public static function makeByQuestReward($questReward)
	{
		$body = [
			'frame_count' => $questReward->quest_reward_frame_count,
			'item_type' => $questReward->quest_reward_item_type,
			'item_id' => $questReward->quest_reward_item_id,
			'item_num' => $questReward->quest_reward_count,
			'take_flag' => GiveOrPayParam::TAKE_FLAG_NONE,
		];
		return $body;
	}

	/**
	 * DropReward からQuestRewardのレスポンス作成
	 *
	 * @param array $dropReward DropReward
	 * @return array QuestRewardのレスポンス
	 */
	public static function makeByDropReward($dropReward)
	{
		$body = [
			'frame_count' => $dropReward->drop_reward_frame_count,
			'item_type' => $dropReward->drop_item_type,
			'item_id' => $dropReward->drop_item_id,
			'item_num' => $dropReward->drop_item_count,
			'take_flag' => GiveOrPayParam::TAKE_FLAG_NONE,
		];
		return $body;
	}

	/**
	 * Mission からQuestRewardのレスポンス作成
	 *
	 * @param array $mission Mission
	 * @return array QuestRewardのレスポンス
	 */
	public static function makeByPlayerMission($playerMission, $takeFlag)
	{
		$body = [
			'frame_count' => 1,
			'item_type' => Item::TYPE_ITEM,
			'item_id' => $playerMission->remuneration,
			'item_num' => $playerMission->remuneration_count,
			'take_flag' => $takeFlag,
		];
		return $body;
	}

}
