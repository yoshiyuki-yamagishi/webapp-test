<?php
/**
 * Friendのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerFriend;


/**
 * Friendのレスポンス
 *
 */
class FriendResponse
{
	/**
	 * Friendのレスポンス作成
	 *
	 * @param Friend $playerFriend Friendのインスタンス
	 * @param boolean $useFollow id として follow を使うか
	 * @return array Friendのレスポンス
	 */
	public static function make($playerFriend, $useFollow)
	{
        $pid_name = $useFollow ? 'follow_id' : 'player_id';

		$body = [
            'id' => $playerFriend->$pid_name,
            'friend_type' => (int)$playerFriend->friend_type,
            'disp_id' => $playerFriend->player_disp_id,
            'name' => $playerFriend->player_name,
            'lv' => $playerFriend->player_lv,
            'main_character' => $playerFriend->main_character,
            'message' => $playerFriend->message,
            'last_login_at' => $playerFriend->last_login_at,
		];
		return $body;
	}
    
}