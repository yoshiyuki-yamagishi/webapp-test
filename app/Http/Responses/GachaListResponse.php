<?php
/**
 * GachaListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Http\Responses\GachaResponse;



/**
 * GachaListのレスポンス
 *
 */
class GachaListResponse
{
	/**
	 * GachaListのレスポンス作成
	 *
	 * @param integer $playerId プレイヤID
	 * @param array $gachaList gachaListの配列
	 * @param string $now 現在日付
	 * @return array GachaListのレスポンス
	 */
	public static function make($playerId, $gachaList, $now)
	{
		$body = [];
		foreach ($gachaList as $gacha)
		{
			$body[] = GachaResponse::make($playerId, $gacha, $now);
		}
		return $body;
	}
}