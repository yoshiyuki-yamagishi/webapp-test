<?php
/**
 * PlayerCharacterのレスポンス
 *
 */

namespace App\Http\Responses;

use App\Models\PlayerItem;
use App\Models\PlayerParty;
use App\Models\PlayerCharacter;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterExpTable;
use App\Models\MasterModels\LevelCoefficient;
use App\Models\MasterModels\RarityCoefficient;
use App\Utils\DebugUtil;

/**
 * PlayerCharacterのレスポンス
 *
 */
class PlayerCharacterResponse
{
	// パーティ使用中フラグ
	const PARTY_FLAG_NO		= 0; // 未使用
	const PARTY_FLAG_YES	= 1; // 使用中

	// お気に入りフラグ
	const FAVORITE_FLAG_NO	= 0;	// お気に入りでない
	const FAVORITE_FLAG_YES	= 1;	// お気に入り

	/**
	 * PlayeCharacterのレスポンス作成
	 *
	 * @param PlayeCharacter $playerCharacter PlayeCharacterのインスタンス
	 * @return array PlayeCharacterのレスポンス
	 */
	public static function make($player, $playerCharacter)
	{
        $playerId = $playerCharacter->player_id;
        $charaId = $playerCharacter->character_id;

        $charaBase = CharacterBase::getOne($charaId);
        if (empty($charaBase))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'character_base', 'id', $charaId
            );
        }

        // 経験値の計算
        $lv = $playerCharacter->character_lv;
        $exp = $playerCharacter->experience;
        $nextExp = 0;

        $levels = CharacterExpTable::getLevels(
            $charaBase->character_exp_table_id, $lv, 2
        );
        assert($levels > 0);
        $startExp = $levels[0]->exp_character;

        if (count($levels) > 1)
        {
            if ($levels[1]->exp_character > $exp)
            {
                $nextExp = $levels[1]->exp_character - $exp;
            }
        }

		// パーティに入っているか確認
		$playerPartyList = PlayerParty::getByPlayerCharacterId(
            $playerCharacter->id
        );
		if ($playerPartyList->count() > 0)
		{
			$partyFlag = self::PARTY_FLAG_YES;
		}
		else
		{
			$partyFlag = self::PARTY_FLAG_NO;
		}

		// 欠片の数 (汎用欠片は含まない、あったとしても)
        $fragmentId = Character::fragmentIdFrom($charaId);
        $fragment = PlayerItem::getOne($playerId, $fragmentId);
        $fragmentNum = isset($fragment) ? $fragment->num : 0;

        // イラストID に変換
        //$illustId = Character::calcIllustId($charaId, $playerCharacter->illust_number);
        $illustId = $charaId * 10 + $playerCharacter->spine;

        $favoriteFlag = self::FAVORITE_FLAG_NO;
        if ($player->favorite_character == $playerCharacter->id)
        {
            $favoriteFlag = self::FAVORITE_FLAG_YES;
        }

        // レスポンス設定
		$body = [
			'character_id' => $illustId,
			'character_base_id' => $charaId,
			'player_character_id' => $playerCharacter->id,
            'illust_number' => $playerCharacter->illust_number,
            'spine' => $playerCharacter->spine,
            'evolve' => $playerCharacter->evolve,
            'repeat' => $playerCharacter->repeat,
            'grade' => $playerCharacter->grade,
			'level' => $lv,
			'acquired_at' => $playerCharacter->acquired_at,
			'experience' => $exp,
			'start_experience' => $startExp,
			'next_experience' => $nextExp,
			'party_flag' => $partyFlag,
			'favorite_flag' => $favoriteFlag,
			'piece_num' => $fragmentNum,
			'active_skill_1_level' => $playerCharacter->active_skill_1_lv,
			'active_skill_2_level' => $playerCharacter->active_skill_2_lv,
			'player_orb_id_1' => $playerCharacter->player_orb_id_1,
			'player_orb_id_2' => $playerCharacter->player_orb_id_2,
			'player_orb_id_3' => $playerCharacter->player_orb_id_3,
			'player_orb_id_4' => $playerCharacter->player_orb_id_4,
			'player_orb_id_5' => $playerCharacter->player_orb_id_5,
			'player_orb_id_6' => $playerCharacter->player_orb_id_6,
		];
		return $body;
	}
}
