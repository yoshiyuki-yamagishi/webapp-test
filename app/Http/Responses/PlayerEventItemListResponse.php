<?php
    /**
     * PlayerEventItemListのレスポンス
     *
     */

    namespace App\Http\Responses;


    /**
     * PlayerEventItemListのレスポンス
     *
     */
    class PlayerEventItemListResponse
    {
        /**
         * PlayerEventItemListのレスポンス
         *
         * @param array $playerItemList PlayerItemListの配列
         * @return array PlayerItemListのレスポンス
         */
        public static function make($playerEventItemList)
        {
            $body = [];
            foreach ($playerEventItemList as $playerItem) {
                //TODO:PlayerItemResponseそのまま使う（問題あれば新しくレスポンス作り直す）
                $body[] = PlayerItemResponse::make($playerItem);
            }
            return $body;
        }
    }