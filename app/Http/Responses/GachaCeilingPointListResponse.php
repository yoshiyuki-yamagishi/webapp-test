<?php


namespace App\Http\Responses;
use App\Http\Responses\GachaCeilingPointResponse;

class GachaCeilingPointListResponse
{


    public static function make($playerId, $ceilingPointExchangeList, $gachaId, $now)
    {
        $body = [];
        foreach ($ceilingPointExchangeList as $ceilingPointExchangeData)
        {
            $body [] = GachaCeilingPointResponse::make(
                        $playerId, $ceilingPointExchangeData, $gachaId, $now
                   );
        }
        return $body;
    }
}
