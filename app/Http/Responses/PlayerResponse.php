<?php
/**
 * Playerのレスポンス
 *
 */

namespace App\Http\Responses;

use App\Models\PlayerCharacter;
use App\Models\PlayerGrimoire;
use App\Models\MasterModels\PlayerLevel;
use App\Utils\DateTimeUtil;

/**
 * Playerのレスポンス
 *
 */
class PlayerResponse
{
	/**
	 * Playerのレスポンス作成
	 *
	 * @param Player $player Playerのインスタンス
	 * @return array Playerのレスポンス
	 */
	public static function make($player)
	{
        $now = ApiResponse::getInstance()->currentDateDB();
        $exp = $player->experience;
        $startExp = 0;
        $nextExp = 0;

        $levels = PlayerLevel::getLevels($player->player_lv, 2);
        assert($levels > 0);
        $startExp = $levels[0]->exp_player;
        
        if (count($levels) > 1)
        {
            if ($levels[1]->exp_player > $exp)
                $nextExp = $levels[1]->exp_player - $exp;
        }
        
		// 所持キャラクタ数
		$characterNum = PlayerCharacter::countByPlayerId($player->id);

		// 所持魔道書数
		$grimoireNum = PlayerGrimoire::countByPlayerId($player->id);

		$body = [
			'name' =>
                $player->player_name,
			'birthday' =>
                DateTimeUtil::DB_to_YMD($player->birthday),
            'legal_birthday' =>
                DateTimeUtil::DB_to_YMD($player->legal_birthday),
			'gender' =>
                (int)$player->gender,
			'tutorial_progress' =>
                (int)$player->tutorial_progress,
			'tutorial_flag' =>
                (int)$player->tutorial_flag,
			'lv' =>
                $player->player_lv,
			'experience' =>
                $exp,
			'start_experience' =>
                $startExp,
			'next_experience' =>
                $nextExp,
			'al' =>
                $player->al,
			'al_recovery_at' =>
                $player->al_recovery_at,
			'max_al' =>
                $player->max_al,
			'message' =>
                $player->message,
			'character_num' =>
                $characterNum,
			'grimoire_num' =>
                $grimoireNum,
			'free_blue_crystal_num' =>
                $player->freeBlueCrystalNum($now),
			'blue_crystal_num' =>
                $player->chargedBlueCrystalNum($now),
			'platinum_dollar_num' =>
                $player->platinum_dollar,
            'powder' =>
                $player->powder,
            'magic_num' =>
                $player->magic_num,
            'friend_point' =>
                $player->friend_point,
            'max_grimoire' =>
                $player->max_grimoire,
            'favorite_character' =>
                $player->favorite_character,
		];

		return $body;
	}
}