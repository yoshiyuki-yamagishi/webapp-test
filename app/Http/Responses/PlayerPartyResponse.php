<?php
/**
 * PlayerPartyのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerCharacter;
use App\Models\PlayerParty;

/**
 * PlayerPartyのレスポンス
 *
 */
class PlayerPartyResponse
{
	/**
	 * PlayerPartyのレスポンス作成
	 *
	 * @param PlayerParty $playerParty PlayerPartyのインスタンス
	 * @return array PlayerPartyのレスポンス
	 */
	public static function make($playerParty)
	{
		$_partyCharacterList = [];

		// パーティキャラクタ情報取得
        for ($i = 1; $i <= PlayerParty::MAX_POSITIONS; ++ $i)
        {
            $charaProp = 'player_character_id_' . $i;
            $grimoireProp = 'player_grimoire_id_' . $i;

			$_partyCharacterList[] = [
				'position' => $i,
				'player_character_id' => $playerParty->$charaProp,
				'player_grimoire_id' => $playerParty->$grimoireProp,
			];
        }

		$body = [
			'player_party_id'		=> $playerParty->id,
			'party_no'				=> $playerParty->party_no,
			'party_character_list'	=> $_partyCharacterList,
		];
		return $body;
	}
}