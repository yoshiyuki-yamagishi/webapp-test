<?php
/**
 * PlayerQuestのレスポンス
 *
 */

namespace App\Http\Responses;

/**
 * PlayerEventItemのレスポンス
 *
 */
class PlayerEventItemResponse
{
    /**
     * PlayerEventItemのレスポンス作成
     *
     * @param integer 章ごとのクリア回数 (日毎)
     * @return array PlayerQuestのレスポンス
     */
	public static function make($playerEventItemData)
	{
		$body = [
            'item_id' => $playerEventItemData->item_id,
            'item_num' => $playerEventItemData->num,
		];
		return $body;
	}

}
