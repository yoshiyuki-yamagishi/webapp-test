<?php


namespace App\Http\Responses;


use App\Models\PlayerCharacter;
use App\Config\Constants;
use App\Models\PlayerGachaCeilingPoint;
use App\Services\GachaCeilingPointService;
use App\Models\MasterModels\Item;
class GachaCeilingPointResponse
{
    /**
     * @param $playerId
     * @param $ceilingPointExchangeData
     * @param $now
     * @return
     */
    public static function make($playerId, $ceilingPointExchangeData, $gachaId, $now)
    {
        $isGachaCeilingOpenFlag = FALSE;
        $isGachaCeilingOpenFlag = GachaCeilingPointService::isGachaCeilingOpen($gachaId, $now);
        $playerGachaCeilingPointData = PlayerGachaCeilingPoint::getByPlayerIdAndCeilingId($playerId, $ceilingPointExchangeData->ceiling_id);
        $playerGachaPoint = $playerGachaCeilingPointData->gacha_point;
        //別途追加予定とりあえず入れる
        $exchangeable = $playerGachaPoint >= $ceilingPointExchangeData->ceiling_count ? true : false;

        switch($ceilingPointExchangeData->item_type)
        {
            case ITEM::TYPE_CHARACTER:
                $player_chara_data = PlayerCharacter::getOne($playerId, $ceilingPointExchangeData->item_id);
            break;
        }
        $playerCharaRepeatCount = 0;
        //重複ボーナス最大回数の仕様が現在3回までっぽいので仕様が決まり次第変更するかも
        if( isset($player_chara_data) )
        {
            $playerCharaRepeatCount =  $player_chara_data->repeat;
        }

        $params = [
            'ceiling_id' => $ceilingPointExchangeData->ceiling_id,
            'exchange_id' => $ceilingPointExchangeData->exchange_id,
            'item_type' => $ceilingPointExchangeData->item_type ,
            'item_id' => $ceilingPointExchangeData->item_id ,
            'item_count' => $ceilingPointExchangeData->item_count,
            'ceiling_count' => $ceilingPointExchangeData->ceiling_count ,
            'player_ceiling_point' => $playerGachaCeilingPointData->gacha_point ,
            'gacha_ceiling_open_flag' => $isGachaCeilingOpenFlag,
            'exchangeable' => $exchangeable,
            'gachaId' => $gachaId,
            'exchange_character_repeat_count' => $playerCharaRepeatCount,
            'max_repeat_count' => Constants::REPEAT_BONUS_GOLD
        ];


        return $params;
    }
}
