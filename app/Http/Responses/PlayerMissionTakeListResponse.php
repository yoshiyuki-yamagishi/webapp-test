<?php
/**
 * PlayerMissionTakeListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerMissionTakeListのレスポンス
 *
 */
class PlayerMissionTakeListResponse
{
	/**
	 * PlayerMissionTakeListのレスポンス
	 *
	 * @param array $playerMissionList プレイヤミッションの配列
	 * @return array PlayerMissionTakeListのレスポンス
	 */
	public static function make($playerMissionList)
	{
		$body = [];

        foreach ($playerMissionList as $playerMission)
        {
			$body[]	= PlayerMissionTakeResponse::make(
                $playerMission
            );
        }
        
		return $body;
	}
}
