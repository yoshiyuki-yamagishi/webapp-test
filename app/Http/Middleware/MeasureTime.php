<?php

namespace App\Http\Middleware;

use Closure;
use App\Utils\DebugUtil;
use App\Utils\HttpUtil;

class MeasureTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $startMem = memory_get_usage();

        $start = microtime(true); // ほぼ、時間はかからないでしょう
        $response = $next($request);

        // 計測するか、リクエストで判定する
        $flag = $request->input('_measure_time', 0);
        if ($flag != 1)
            return $response;
        
        $time = microtime(true) - $start;

        $endMem = memory_get_usage();

        $reqSize = HttpUtil::calcRequestSize($request);
        $resSize = HttpUtil::calcResponseSize($response);

        $cpus = sys_getloadavg();
        
        DebugUtil::e_log_line(
            'm_time', $request->path()
            . "\t" . sprintf('%.8f', $time)
            . "\t" . sprintf('%.4f', $cpus[0])
            . "\t" . $reqSize
            . "\t" . $resSize
            . "\t" . $startMem
            . "\t" . $endMem
        );
        
        return $response;
    }
}
