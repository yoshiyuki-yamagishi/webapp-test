<?php
/**
 * レシート使用済み の例外
 *
 */

namespace App\Exceptions;

class ReceiptUsedException extends ApiException
{
    public static $CODE = self::S_RECEIPT_USED;
}
