<?php
/**
 * 同一端末の引継ぎ の例外
 *
 */

namespace App\Exceptions;

class SameTerminalException extends ApiException
{
    public static $CODE = self::S_SAME_TERMINAL;
}
