<?php
/**
 * ゲーム仕様による例外
 *
 */

namespace App\Exceptions;

class GameException extends ApiException
{
    public static $CODE = self::E_GAME;
}
