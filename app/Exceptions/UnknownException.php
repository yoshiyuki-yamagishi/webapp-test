<?php
/**
 * 不明 の例外
 *
 */

namespace App\Exceptions;

class UnknownException extends ApiException
{
    public static $CODE = self::E_UNKNOWN;
}
