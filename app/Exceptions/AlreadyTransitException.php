<?php
/**
 * 引継ぎ済み端末動作例外
 *
 */

namespace App\Exceptions;

class AlreadyTransitException extends ApiException
{
    public static $CODE = self::E_ALREADY_TRANSIT;
}
