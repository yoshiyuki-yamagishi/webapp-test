<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use App\Http\Responses\ApiResponse;
use App\Utils\DebugUtil;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        // DebugUtil::e_log('Handler', 'class', get_class($e));

        $appName = config('app.name');
        if ($appName != 'BBDW-API')
            return parent::render($request, $e);
        
        return $this->prepareApiResponse($request, $e);
    }

    protected function prepareApiResponse($request, Exception $e)
    {
        $response = ApiResponse::getInstance();

        /*
        $errArr = [
            'message' => $e->getMessage(),
            'exception' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => collect($e->getTrace())->map(function ($trace) {
                return Arr::except($trace, ['args']);
            })->all(),
        ];
        DebugUtil::e_log('Handler', 'errArr', $errArr);
        */

        if ($e instanceof ApiException)
        {
            $response->setError(
                $e->code, $e->message
            );
        }
        else if ($e instanceof \Illuminate\Database\QueryException ||
                 $e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException ||
                 $e instanceof \Illuminate\Database\Eloquent\JsonEncodingException ||
                 $e instanceof \Illuminate\Database\Eloquent\MassAssignmentException ||
                 $e instanceof \Illuminate\Database\Eloquent\RelationNotFoundException)
        {
			$response->setError(
                ApiException::E_DATABASE, $e->getMessage()
            );
        }
        else if ($e instanceof \Illuminate\Validation\UnauthorizedException ||
                 $e instanceof \Illuminate\Validation\ValidationException)
        {
			$response->setError(
                ApiException::E_PARAM, $e->getMessage()
            );
        }
        else
        {
            $response->setError(
                ApiException::E_UNKNOWN, $e->getMessage()
            );
        }

		return $response->toResponse();
    }
}


/*
// 
// C:\Docker\php72\api\vendor\laravel\framework\src\Illuminate\ 配下の例外
// 
Foundation\Testing\HttpException
Foundation\Http\Exceptions\MaintenanceModeException
Database\QueryException
Database\Eloquent\ModelNotFoundException
Database\Eloquent\JsonEncodingException
Database\Eloquent\MassAssignmentException
Database\Eloquent\RelationNotFoundException
Auth\Access\AuthorizationException
Auth\AuthenticationException
Contracts\Redis\LimiterTimeoutException
Contracts\Cache\LockTimeoutException
Contracts\Encryption\DecryptException
Contracts\Encryption\EncryptException
Contracts\Queue\EntityNotFoundException
Contracts\Container\BindingResolutionException
Contracts\Filesystem\FileNotFoundException
Contracts\Filesystem\FileExistsException
Broadcasting\BroadcastException
Validation\UnauthorizedException
Validation\ValidationException
Routing\Exceptions\InvalidSignatureException
Routing\Exceptions\UrlGenerationException
Queue\InvalidPayloadException
Queue\MaxAttemptsExceededException
Queue\ManuallyFailedException
Session\TokenMismatchException
Container\EntryNotFoundException
Http\Exceptions\HttpResponseException
Http\Exceptions\PostTooLargeException
Http\Exceptions\ThrottleRequestsException
*/
