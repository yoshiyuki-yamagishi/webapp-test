<?php
/**
 * ゲームデータ の例外 (足りない系)
 *
 */

namespace App\Exceptions;

class NotEnoughException extends ApiException
{
    public static $CODE = self::E_NOT_ENOUGH;
}
