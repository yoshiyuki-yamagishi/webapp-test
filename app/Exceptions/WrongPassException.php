<?php
/**
 * 引き継ぎパスコード、パスワードが違う例外
 *
 */

namespace App\Exceptions;

class WrongPassException extends ApiException
{
    public static $CODE = self::E_WRONG_PASS;
}
