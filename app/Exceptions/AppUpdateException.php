<?php
/**
 * 強制アップデート による例外
 *
 */

namespace App\Exceptions;

class AppUpdateException extends ApiException
{
    public static $CODE = self::E_APP_UPDATE;
}
