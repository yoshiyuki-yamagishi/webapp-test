<?php
/**
 * レシート の例外
 *
 */

namespace App\Exceptions;

class ReceiptException extends ApiException
{
    public static $CODE = self::S_RECEIPT;
}
