<?php
/**
 * パラメータ の例外
 *
 */

namespace App\Exceptions;

class ParamException extends ApiException
{
    public static $CODE = self::E_PARAM;
}
