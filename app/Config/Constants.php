<?php
/**
 * 定数設定
 *
 */

namespace App\Config;

/**
 * 定数一元管理用
 *
 */
class Constants
{
    /** 基本系  **/
    const OFF_FLG = 0;
    const ON_FLG = 1;

    // OSタイプ
    const OS_TYPE_NONE = 0;
    const OS_TYPE_ANDROID = 1;  // Unityもここに含まれるが区別されてないので、無視します・・・
    const OS_TYPE_IOS = 2;

    // OSタイプ名
    const OS_TYPE_NAME = [
        self::OS_TYPE_NONE => "",
        self::OS_TYPE_ANDROID => "Android",
        self::OS_TYPE_IOS => "iOS",
    ];

    // Platform
    const PLATFORM_IOS = 1;
    const PLATFORM_ANDROID = 2;

    //ボーナスタイプ
    const BONUS_TYPE_EXP         = 1;
    const BONUS_TYPE_MONEY       = 2;
    const BONUS_TYPE_DROP        = 3;
    const BONUS_TYPE_CHARA_EXP   = 4;

    // クエスト種類
    const QUEST_CATEGORY_ALL = 0; // 全て
    const QUEST_CATEGORY_STORY = 1; // ストーリ
    const QUEST_CATEGORY_CHARACTER = 2; // キャラクタ
    const QUEST_CATEGORY_EVENT = 3; // イベント

    //アイテムタイプ
    const ITEM_TYPE_CHARACTER = 1;
    const ITEM_TYPE_GRIMOIRE  = 2;
    const ITEM_TYPE_ITEM      = 3;
    //ミッションタイプ
    const MISSION_TYPE_DAILY = 1; // 1：デイリー
    const MISSION_TYPE_NORMAL = 2; // 2：ノーマル
    const MISSION_TYPE_EVENT = 3; // 3：イベント
    const MISSION_TYPE_QUEST = 4; // 4：クエスト達成ミッション
    const MISSION_TYPE_BEGINNER = 5; // 5：初心者

    // 引継ぎコード作成施行回数
    const TRANSIT_CODE_CREATE_TRY_MAX = 100;
    // 引継ぎコード文字数
    const TRANSIT_CODE_STR_NUM = 10;

    /*************************************/


    /** キャラクター系 **/

    const REPEAT_BONUS_NONE   = 0;
    const REPEAT_BONUS_BRONZE = 1;
    const REPEAT_BONUS_SILVER = 2;
    const REPEAT_BONUS_GOLD   = 3;

    /*************************************/



    /** イベント系 **/

    //イベント開催系ステータス
    const EVENT_STATUS_OPEN_START = 0;
    const EVENT_STATUS_START_END = 1;
    const EVENT_STATUS_END_CLOSE = 2;
    const EVENT_STATUS_CLOSE = 3;

    //イベント種別
    const EVENT_TYPE_MARATHON_NORMAL = 1; //通常マラソン
    const EVENT_TYPE_MARATHON_BOSS = 2; //降臨
    const EVENT_TYPE_DAILY = 3; //デイリー

    //導入シナリオの確認用、questデータのpriority
    const INTRO_STORY = 0;

    // イベント特攻タイプ
    const SP_EFFECT_BONUS_TYPE_DROP_UP = 1; //ドロップアップ
    const SP_EFFECT_BONUS_TYPE_ATTACK_UP = 2; //攻撃力アップ

    /*************************************/


}
