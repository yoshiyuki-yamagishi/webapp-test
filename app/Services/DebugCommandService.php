<?php
/**
 * デバッグコマンド のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerItemListResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerItem;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\PlayerLevel;
use App\Services\PlayerItemService;
use App\Services\PlayerPartyGrimoireLogger;
use App\Services\MissionChecker\MissionCheckerBattle;
use App\Services\MissionChecker\MissionCheckerQuest;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;


/**
 * デバッグコマンド のサービス
 *
 */
class DebugCommandService extends BaseService
{
	const DEBUG_COMMAND_CODE_PREFIX	= 'dbgcmdp';

	/**
	 * アイテム追加
	 *
	 * @param DebugCommandAddItemRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function addItem($request)
	{
        $response = self::getResponse($request);

        $playerId = $request->player_id;

		// プレイヤ取得
		$player = Player::find_($playerId);

        // 付与処理
        {
			BaseGameModel::beginTransaction();

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemId = $request->item_id;
            $param->count = $request->item_num;
            $param->srcType = SrcType::DEBUG;
            $param->srcId = 0;

            PlayerService::giveOrPay($param);

			BaseGameModel::commit();
        }

        // プレイヤアイテム更新

		$playerItem = PlayerItem::getOne($request->player_id, $request->item_id);
        $playerItemList = [$playerItem];

		$body = [
			'player_item_list' =>
                PlayerItemListResponse::make($playerItemList),
			'platinum_dollar_num' =>
                Player::getPDollar($player->id)
		];
		$response->body = $body;

		return $response;
    }
 }
