<?php
/**
 * プレイヤプレゼント のサービス
 *
 */

namespace App\Services;

use App\Config\Constants;
use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerPresentResponse;
use App\Models\BaseGameModel;
use App\Models\PlayerPresent;
use App\Http\Responses\PlayerPresentListResponse;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;


/**
 * プレイヤプレゼント のサービス
 *
 */
class PlayerPresentService extends BaseService
{
	// 受取種別
	const TAKE_TYPE_ONE		= 1;	// 個別受取
	const TAKE_TYPE_BULK	= 2;	// 一括受取
    const TAKE_TYPE_CHARA	= 3;	// キャラ一括
    const TAKE_TYPE_GRIMOIRE= 4;	// 魔導書一括
    const TAKE_TYPE_ITEM	= 5;	// アイテム一括
    const TAKE_TYPE_OTHER	= 6;	// その他一括


	/**
	 * 一覧
	 *
	 * @param PlayerPresentListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

		$overFlag = 0;

		// プレイヤプレゼント取得
		$playerPresentList = PlayerPresent::getByPlayerId(
			$request->player_id, $now,
            $request->take_flag, $request->count + 1
		);

		$retCount = count($playerPresentList);
		// DebugUtil::e_log('PPS_list', 'retCount', $retCount);

		// プレゼント取得行数が101件の場合
		// フラグを立てて101件目を削除
		if ($retCount > $request->count)
		{
			$overFlag = 1;
			-- $retCount;
		}

		// 未受取のプレゼントを配列に格納する
		$_playerPresentList = [];

		for ($i = 0; $i < $retCount; ++ $i)
		{
			$playerPresent = $playerPresentList[$i];
			$_playerPresentList[] = PlayerPresentResponse::make($playerPresent);
		}

		$body = [
			'player_present_list' => $_playerPresentList,
            'over_flag' => $overFlag,
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * 受取
	 *
	 * @param PlayerPresentTakeRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function take($request)
	{
        $response = self::getResponse($request);
        $now = $request->current_date; // 指定の日時を使う
        // $now = $response->currentDateDB();

        $presentList = [];

        switch ($request->take_type){

            case self::TAKE_TYPE_ONE:
                $present = PlayerPresent::getOne(
                    $request->player_present_id,
                    PlayerPresent::TAKE_FLAG_NO
                );
                if (!isset($present))
                {
                    throw \App\Exceptions\DataException::makeNotFound(
                        'player_present', 'id', $request->player_present_id
                    );
                }
                $presentList[] = $present;
                break;

            case self::TAKE_TYPE_BULK:
                // 指定件数までプレイヤプレゼントを取得
                $presentList = PlayerPresent::getByPlayerId(
                    $request->player_id, $now,
                    PlayerPresent::TAKE_FLAG_NO, $request->count
                );
                break;

            case self::TAKE_TYPE_CHARA:
                // 指定件数までプレイヤプレゼントを取得
                $presentList = PlayerPresent::getByPlayerIdAndItemType(
                    $request->player_id, $now,
                    PlayerPresent::TAKE_FLAG_NO, Constants::ITEM_TYPE_CHARACTER, $request->count
                );
                break;

            case self::TAKE_TYPE_GRIMOIRE:
                $presentList = PlayerPresent::getByPlayerIdAndItemType(
                    $request->player_id, $now,
                    PlayerPresent::TAKE_FLAG_NO, Constants::ITEM_TYPE_GRIMOIRE, $request->count
                );
                break;

            case self::TAKE_TYPE_ITEM:
                $presentList = PlayerPresent::getByPlayerIdAndItemType(
                    $request->player_id, $now,
                    PlayerPresent::TAKE_FLAG_NO, Constants::ITEM_TYPE_ITEM, $request->count
                );
                break;

            case self::TAKE_TYPE_OTHER:
                $presentList = PlayerPresent::getByPlayerIdForOther(
                    $request->player_id, $now,
                    PlayerPresent::TAKE_FLAG_NO, $request->count
                );
                break;

            default:
                throw \App\Exceptions\ParamException::makeInvalid(
                    '', 'take_type', $request->take_type
                );
        }

		$notTaken = [];

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

            foreach ($presentList as $present)
            {
                try
                {
                    $param = new GiveOrPayParam();
                    $param->playerId = $request->player_id;
                    $param->itemType = $present->item_type;
                    $param->itemId = $present->item_id;
                    $param->count = $present->item_num;
                    $param->srcType = SrcType::PRESENT_TAKE;
                    $param->srcId = $present->id;
                    PlayerService::giveOrPay($param);

                    $present->take_flag = PlayerPresent::TAKE_FLAG_YES;
                    $present->taked_at = $now;
                    $present->save();

                    // $taken[] = $present;
                }
                catch (\App\Exceptions\OverflowException $e)
                {
                    $notTaken[]	= $present;
                }
            }

			BaseGameModel::commit();
		}

		$body = [
			// 'take_list' => PlayerPresentListResponse::make($taken),
            'take_over_list' =>
                PlayerPresentListResponse::make($notTaken),
		];
		$response->body = $body;

		return $response;
	}

}
