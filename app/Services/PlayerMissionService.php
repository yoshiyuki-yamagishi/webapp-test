<?php
/**
 * プレイヤミッション のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerMissionResponse;
use App\Http\Responses\PlayerMissionListResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerMission;
use App\Models\PlayerPresent;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\Mission;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use App\Services\MissionChecker\MissionChecker;
use App\Services\PlayerService;
use Symfony\Component\Debug\Debug;

/**
 * プレイヤミッション のサービス
 *
 */
class PlayerMissionService extends BaseService
{
    const TAKE_TYPE_ONE = 1;
    const TAKE_TYPE_ALL = 2;

    const TAKE_FLAG_NONE = 0;
    const TAKE_FLAG_OVER = 1;

	/**
	 * 一覧
	 *
	 * @param PlayerMissionListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();
        //先にrelease_triggerがないレコード追加
        self::preparePlayerMissionRegist($request->player_id, $now);
        // ミッションチェッカーを初期化

        $missionChecker = MissionChecker::getInstance();
        $missionChecker->init($request->player_id, $now);

        $missionList = &$missionChecker->missionList;
        $playerMissionList = &$missionChecker->playerMissionList;



        $achiveMissionList = []; // 今回のチェックで達成 (使用しない)
        $reportMissionList = []; // 今回報告リスト
        $missionListRes = [];
        $prepareMissionIdList = [];

        {
            // トランザクション開始
            BaseGameModel::beginTransaction();

            // ミッション達成チェック
            $missionChecker->updateMission($achiveMissionList, false);

            foreach ($missionList as $mission)
            {
                // 進捗状況を取得

                $playerMission = $playerMissionList->where(
                    'mission_id', $mission->id
                )->first();

                // トリガー判定の準備

                $missionChecker->addMissionData($mission, $playerMission);
                $released = false; // 今回開放されたものか？

                // トリガー条件を満たさないものはスキップする

                if (empty($playerMission))
                {
                    if (!MissionChecker::matchTriggerImpl(
                        $mission,
                        $missionChecker->data['triggers'],
                        null,
                        $playerMissionList, false, $released
                    ))
                        continue;
                }

                // レスポンス生成

                $missionListRes[] = PlayerMissionResponse::makeEx(
                    $mission, $playerMission, $released
                );

                // 今回報告のものを追加

                if (isset($playerMission) && $playerMission->unreported())
                    $reportMissionList[] = $playerMission;

                if(isset($playerMission)){
                    if($playerMission->getNewState()){
                        $reportMissionList[] = $playerMission;
                    }
                }
            }

            // ミッション報告
            // $reportMissionList = $missionChecker->unreportedMissions(
            // $request->player_id, $now
            // );

            // ミッションを報告済みに設定する
            $missionChecker->setReported($reportMissionList);

            // コミット
            BaseGameModel::commit();
        }

		$body = [
            'mission_list' => $missionListRes
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 報酬受取
	 *
	 * @param PlayerMissionRewardTakeRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function rewardTake($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // 呼び出し元の時刻から過ぎているので、
        // 普通に、取得できない場合もある

        $takableList = [];

        switch ($request->take_type)
        {
        case self::TAKE_TYPE_ONE:
            $takableList = PlayerMission::getTakableOnes(
                $request->player_mission_id, $now
            );
            break;
        case self::TAKE_TYPE_ALL:
            $takableList = PlayerMission::getTakable(
                $request->player_id, $now
            );
            break;
        default:
            throw \App\Exceptions\ParamException::makeNotFound(
                '', 'take_type', $request->take_type
            );
            break;
        }

        // missionテーブルから報酬を取得
        $missionTbl = Mission::getAllByKeyId($now);

        $takeList = [];
        $takeOverList = [];

        {
            // トランザクション開始
            BaseGameModel::beginTransaction();

            foreach ($takableList as $takable)
            {

                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemType = $missionTbl[$takable->mission_id]->remuneration_item_type;
                $param->itemId = $missionTbl[$takable->mission_id]->mission_remuneration;
                $param->count = $missionTbl[$takable->mission_id]->mission_remuneration_count;
                $param->srcType = SrcType::MISSION;
                $param->srcId = $takable->id;
                try
                {
                    PlayerService::giveOrPay($param);
                }
                catch (\App\Exceptions\OverflowException $e)
                {
                    $takeOver = PlayerMissionResponse::make(
                        $takable, $param->takeFlag
                    );
                    $takeOver['item_type'] = $e->itemType;
                    $takeOver['item_id'] = $e->item;
                    $takeOver['item_num'] = $e->count;
                    $takeOver['take_flag'] = GiveOrPayParam::TAKE_FLAG_OVER;
                    $takeOverList[]	= $takeOver;

                    // プレゼントボックスに追加

                    $msg = Mission::missionTypeName($takable->mission_type)
                         . 'ミッション達成報酬です';

                    PlayerPresent::regist(
                        $request->player_id,
                        $takeOver['item_type'],
                        $takeOver['item_id'],
                        $takeOver['item_num'],
                        SrcType::MISSION,
                        $takable->id,
                        $msg,
                        PlayerPresent::defaultExpiredAt()
                    );
                }

                $takeList[]	= PlayerMissionResponse::make(
                    $takable, $param->takeFlag
                );

                // 取得日付を設定する
                $takable->taked_at = $now;
                $takable->save();
            }

            // コミット
            BaseGameModel::commit();
        }

        $takeFlag = self::TAKE_FLAG_NONE;
        if (count($takeOverList) > 0)
            $takeFlag |= self::TAKE_FLAG_OVER;

		$body = [
            'take_flag' => $takeFlag,
            'take_list' => $takeList,
            // 'take_over_list' => $takeOverList,
		];

		$response->body = $body;
		return $response;
	}

    /**
     * ミッション情報更新
     * @param $playerId
     * @param $missionList
     * @param $playerMissionList
     * @param $achiveMissionList
     * @throws \App\Exceptions\ApiException
     */
    public static function updateMission(
        $playerId, $missionList, &$playerMissionList,
        &$achiveMissionList
    )
    {
        $missionChecker = MissionChecker::getInstance();

        // データはミッションチェッカーの外で用意する
        // 例えば、プレイヤーレベルが上がっただけの場合は、
        // プレイヤーテーブルのみチェックすればよいから

        // ホーム画面表示時などに全チェックをかける仕様っぽいので
        // ココで必要なデーターを全部用意する実装にしておく

        // プレイヤ取得

		$player = Player::find_($playerId);

        $data = [];
        $data["player"] = $player;
        // DebugUtil::e_log('PMS', 'data', $data);

        foreach ($missionList as $mission)
        {
            $playerMission = $playerMissionList->where(
                'mission_id', $mission->mission_id
            )->first();

            $isPm = isset($playerMission);

            $updated = false;
            $missionChecker->check(
                $data, $mission, $playerMission, $updated
            );

            if (!$updated)
                continue;

            // 達成したか？

            $achived = ($playerMission->progress_count >= $mission->count);

            // 今回達成リストに追加する

            if ($achived)
                $achiveMissionList[] = $mission;

            // もともと行が無かった場合、リストに追加する

            if (!$isPm)
                $playerMissionList[] = $playerMission;

            // DB 更新 //

            if ($achived)
                $playerMission->cleared_at = $missionChecker->now;

            $playerMission->save();
        }
    }

    /**
     * リリーストリガーがないミッションを事前にテーブルに登録
     * @param $playerId
     * @param $now
     */
    public static function preparePlayerMissionRegist($playerId, $now){
        //事前作成
        $prepareOpenMissionDataList = Mission::getPrepareOpenMissionList($now);
        $prepareOpenMissionIdList = [];
        $prepareInsertPlayerMissionDataList = [];

        foreach($prepareOpenMissionDataList as $prepareOpenMissionData){
            $prepareOpenMissionIdList[] = $prepareOpenMissionData->id;
            $prepareInsertPlayerMissionDataList[$prepareOpenMissionData->id] = $prepareOpenMissionData;
        }

        $playerMissionDataList = PlayerMission::getPlayerMissonByPlayerIdAndMissionIds($playerId, $prepareOpenMissionIdList, $now);


        //先行で入れるデーター作成
        foreach($playerMissionDataList as $playerMissionData) {
            //playerミッションデーター省く
            if(isset($prepareInsertPlayerMissionDataList[$playerMissionData->mission_id])){
                unset($prepareInsertPlayerMissionDataList[$playerMissionData->mission_id]);
            }
        }
            if(count($prepareInsertPlayerMissionDataList) > 0) {
                $prepareInsertPlayerMissionDataList = array_values($prepareInsertPlayerMissionDataList);
                BaseGameModel::beginTransaction();

                PlayerMission::preRegistPlayerMission($playerId, $prepareInsertPlayerMissionDataList, $now);

                BaseGameModel::commit();
            }
    }
}
