<?php
/**
 * プレイヤ魔道書 のサービス
 *
 */

namespace App\Services;


use App\Http\Responses\ApiResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerGrimoire;
use App\Models\PlayerGrimoireLog;
use App\Models\PlayerParty;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireAwakeCoefficient;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\PlayerInitialSettings;
use App\Http\Responses\PlayerGrimoireListResponse;
use App\Http\Responses\PlayerGrimoireResponse;
use App\Services\MissionChecker\MissionCheckerGrimoireReinforce;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * プレイヤ魔道書 のサービス
 *
 */
class PlayerGrimoireService extends BaseService
{
	// 魔道書強化種別
	const REINFORCE_TYPE_AWAKE = 0;	// 覚醒
	const REINFORCE_TYPE_RELEASE = 1;	// スロット解放
	const REINFORCE_TYPE_EQUIP = 2;	// 強化パーツ装備

	/**
	 * 一覧
	 *
	 * @param PlayerCharacterListRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
		$usedGrimoire = [];

		// プレイヤ魔道書取得

		$playerGrimoireList = PlayerGrimoire::getByPlayerId(
            $request->player_id
        );

		// 装備している魔道書一覧を取得

		$usedGrimoire = self::calcUsedGrimoire($request->player_id);

        // レスポンス生成

		$body = [
			'player_grimoire_list' => PlayerGrimoireListResponse::make(
				$playerGrimoireList, $usedGrimoire
			),
			'player_grimoire_num' => count($playerGrimoireList),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 保護
	 *
	 * @param PlayerCharacterLockRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function lock($request)
	{
        $response = self::getResponse($request);

        $unlockIds = [];
        foreach ($request->unlock_grimoire_list as $pg)
        {
            $unlockIds[] = $pg['player_grimoire_id'];
        }
		// DebugUtil::e_log('PGS', 'unlockIds', $unlockIds);

        $lockIds = [];
        foreach ($request->lock_grimoire_list as $pg)
        {
            $lockIds[] = $pg['player_grimoire_id'];
        }
		// DebugUtil::e_log('PGS', 'lockIds', $lockIds);

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

            // ロック解除する

            PlayerGrimoire::lockByIds(
                $unlockIds, PlayerGrimoire::LOCK_FLAG_NO
            );

            // ロックする (ロック優先なので、後から)

            PlayerGrimoire::lockByIds(
                $lockIds, PlayerGrimoire::LOCK_FLAG_YES
            );

			BaseGameModel::commit();
		}

        // レスポンス生成

		$body = [
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 強化
	 * @param PlayerGrimoireReinforceRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function reinforce($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerGrimoireReinforce::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤ取得

		$player = Player::find_($request->player_id);

		// プレイヤ魔道書取得

		$playerGrimoire = PlayerGrimoire::getOne(
            $request->player_grimoire_id
        );
		if (!isset($playerGrimoire))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_grimoire', 'id', $request->player_grimoire_id
            );
		}

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

			switch ($request->reinforce_type)
            {
            case self::REINFORCE_TYPE_AWAKE: // 覚醒
                self::reinforceAwake(
                    $response, $playerGrimoire, $request
                );
                break;
            case self::REINFORCE_TYPE_RELEASE: // スロット解放
                self::reinforceRelease(
                    $response, $playerGrimoire, $request
                );
                break;
            case self::REINFORCE_TYPE_EQUIP:
                self::reinforceEquip(
                    $response, $playerGrimoire, $request
                );
                break;
            default:
                throw \App\Exceptions\ParamException::makeInvalid(
                    '', 'reinforce_type', $request->reinforce_type
                );
			}

			$playerGrimoire->save();

            // ミッション達成チェック

            $missionChecker->addData('reinforceType', $request->reinforce_type);
            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerGrimoire', $playerGrimoire);

            $awakeCounts = PlayerGrimoireLog::getAwakeCounts(
                $player->id
            );
            $missionChecker->addData('awakeCounts', $awakeCounts);

            $rarityAwakeCounts = PlayerGrimoireLog::calcRarityAwakeCounts(
                $awakeCounts
            );
            $missionChecker->addData('rarityAwakeCounts', $rarityAwakeCounts);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

        // レスポンス生成

        $used = PlayerParty::isUsedGrimoire(
            $request->player_id, $playerGrimoire->id
        );
		$body = [
			'player_grimoire' =>
                PlayerGrimoireResponse::make($playerGrimoire, $used),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 魔道書の覚醒 実装
	 * @param ApiResponse $response レスポンス
	 * @param PlayerGrimoire $playerGrimoire プレイヤ魔道書
	 * @param PlayerGrimoireReinforceRequest $request
	 * @return boolean true: 成功、false: 失敗
	 */
	public static function reinforceAwake(
        &$response, &$playerGrimoire, $request
    )
	{
        // 覚醒最大値を取得

        $awakeLimit = Constant::grimoireAwakeLimit();

        // 覚醒できるか

        if ($playerGrimoire->awake >= $awakeLimit->value1)
        {
            throw \App\Exceptions\GameException::make(
                'awake is already: ' . $awakeLimit->value1
            );
        }

        // 消費する魔道書を取得

        $usePlayerGrimoire = PlayerGrimoire::getOne(
            $request->player_item_id
        );
        if (!isset($usePlayerGrimoire))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_grimoire', 'id', $request->player_item_id
            );
        }

        // 消費する魔道書がロックされていないか

        if ($usePlayerGrimoire->isLocked())
        {
            throw \App\Exceptions\GameException::make(
                'grimoire is locked: ' . $request->player_item_id
            );
        }

        // 消費可能な魔道書か

        if ($playerGrimoire->grimoire_id == $usePlayerGrimoire->grimoire_id)
        {
            // 使用可能
        }
        else
        {
            // マスタの確認が必要

            $grimoire = Grimoire::getOne($playerGrimoire->grimoire_id);
            if (!isset($grimoire))
            {
                throw \App\Exceptions\MasterException::makeNotFound(
                    'grimoire', 'id', $playerGrimoire->grimoire_id
                );
            }

            $useGrimoire = Grimoire::getOne($usePlayerGrimoire->grimoire_id);
            if (!isset($useGrimoire))
            {
                throw \App\Exceptions\MasterException::makeNotFound(
                    'grimoire', 'id', $usePlayerGrimoire->grimoire_id
                );
            }

            if (!Grimoire::forAwake($useGrimoire))
            {
                throw \App\Exceptions\GameException::make(
                    'specified grimoire to use is not for awake: '
                    . $usePlayerGrimoire->grimoire_id
                );
            }

            if ($grimoire->grimoire_initial_rarity !=
                $useGrimoire->grimoire_initial_rarity)
            {
                throw \App\Exceptions\GameException::make(
                    'specified grimoire to use has different initial_rarity: '
                    . $useGrimoire->grimoire_initial_rarity
                );
            }
        }

        // 覚醒させる
        //覚醒数値とベースを考慮して実際枚数
        $materialUsedGrimoireCount = $usePlayerGrimoire->awake + 1;
        //素材の覚醒で使った分でベース覚醒の値計算
        $playerGrimoire->awake += $materialUsedGrimoireCount;

        //覚醒リミット補正
        if($playerGrimoire->awake > $awakeLimit->value1){
            $playerGrimoire->awake = $awakeLimit->value1;
        }

        $log = LogService::playerGrimoireAwake(
            $playerGrimoire, SrcType::GRIMOIRE_AWAKE, 0
        );

        // 消費する

        $param = new GiveOrPayParam();
        $param->playerId = $request->player_id;
        $param->itemType = Item::TYPE_GRIMOIRE;
        $param->itemId = $usePlayerGrimoire->grimoire_id;
        $param->playerGrimoireIds[] = $usePlayerGrimoire->id;
        $param->count = - 1;
        $param->srcType = SrcType::GRIMOIRE_AWAKE;
        $param->srcId = $log->id;

        PlayerService::giveOrPay($param);

        return true;
    }

	/**
	 * 魔道書のスロット開放 実装
	 * @param ApiResponse $response レスポンス
	 * @param PlayerGrimoire $playerGrimoire プレイヤ魔道書
	 * @param PlayerGrimoireReinforceRequest $request
	 * @return boolean true: 成功、false: 失敗
	 */
	public static function reinforceRelease(
        &$response, &$playerGrimoire, $request
    )
    {
        $slotNo = $request->slot_no;

        // スロットの数値チェック
        // (スロットを使用しない場合は範囲チェックしないため)

        if ($slotNo < 1 || $slotNo > 3)
        {
            throw \App\Exceptions\ParamException::make(
                'slot_no is invalid: ' . $slotNo
            );
        }

        // スロットが解放済みでないか

        if ($playerGrimoire->isSlotReleased($slotNo))
        {
            throw \App\Exceptions\GameException::make(
                'grimoire is already released: ' . $slotNo
            );
        }

        // TODO: 特に何も消費していないけど？

        $playerGrimoire->setSlotReleased($slotNo);
        return true;
    }

	/**
	 * 魔道書強化パーツ装備 実装
	 * @param ApiResponse $response レスポンス
	 * @param PlayerGrimoire $playerGrimoire プレイヤ魔道書
	 * @param PlayerGrimoireReinforceRequest $request
	 * @return boolean true: 成功、false: 失敗
	 */
	public static function reinforceEquip(
        &$response, &$playerGrimoire, $request
    )
    {
        $slotNo = $request->slot_no;
        $itemId = $request->player_item_id;

        $playerGrimoire->setSlotEquip($slotNo, $itemId);
        return true;
    }

	/**
	 * 枠増加
	 * @param PlayerGrimoireLimitUpRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function limitUp($request)
	{
        $response = self::getResponse($request);

        $now = DateTimeUtil::getNOW();

		$player = Player::find_($request->player_id);

        $upNum = $request->limit_up_num;

        // マスタ取得

        $iniPs = PlayerInitialSettings::getOne_(1);

        $grimoreConstants = Constant::getRange(
            Constant::GRIMOIRE_INCREASE_UNIT,
            Constant::GRIMOIRE_INCREASE_LIMIT
        );

        $addUnit = $grimoreConstants[0]->value1;
        $useUnit = $grimoreConstants[0]->value2;
        $addLimit = $grimoreConstants[1]->value1;
        $maxLimit = $iniPs->max_grimoire + $addLimit;

        // 最大魔道書枠を超えた場合はエラー

        if ($player->max_grimoire + $upNum > $maxLimit)
        {
            throw \App\Exceptions\GameException::make(
                'max_grimoire is already max: ' . $maxLimit
            );
        }

        // 追加単位数より小さくてはダメ

        if ($upNum < $addUnit)
        {
            throw \App\Exceptions\ParamException::make(
                'crysal_num must be larger or equals: ' . $addUnit
            );
        }

        // 追加単位数の倍数でないとダメ

        if (($upNum % $addUnit) != 0)
        {
            throw \App\Exceptions\ParamException::make(
                'limit_up_num must be multiple of : ' . $addUnit
            );
        }

		{
			BaseGameModel::beginTransaction();

            // 蒼の結晶の必要数を計算

            $need = intdiv($upNum, $addUnit) * $useUnit;

			// 枠増加

            $player->max_grimoire += $upNum;
			$player->save();

            // ログ追加

            $log = LogService::playerGrimoireLimitUp(
                $player, $upNum, $need, SrcType::GRIMOIRE_LIMIT_UP, 0
            );

			// 蒼の結晶消費

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = Item::ID_FREE_BLUE_CRYSTAL;
            $param->count = - $need;
            $param->srcType = SrcType::GRIMOIRE_LIMIT_UP;
            $param->srcId = $log->id;

            PlayerService::giveOrPay($param);

			BaseGameModel::commit();
		}

		$body = [
			'limit' =>
                $player->max_grimoire,
			'free_blue_crystal_num' => PlayerBlueCrystal::getFreeNum(
                $request->player_id, $now
            ),
			'blue_crystal_num' => PlayerBlueCrystal::getChargedNum(
                $request->player_id, $now
            ),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 装備中の魔道書一覧を返す
	 * @param int $playerId
	 * @return array 装備中の魔道書一覧
	 */
	public static function calcUsedGrimoire($playerId)
	{
		$playerPartyList = PlayerParty::getByPlayerId($playerId);
		$usedGrimoire = [];

		// 装備しているかどうかを判断する
		// player_party を player_id でセレクト
		// 装備していたら、フラグをたてる = 1
		foreach ($playerPartyList as $playerParty)
		{
			$usedGrimoire[$playerParty->player_grimoire_id_1] = 1;
			$usedGrimoire[$playerParty->player_grimoire_id_2] = 1;
			$usedGrimoire[$playerParty->player_grimoire_id_3] = 1;
			$usedGrimoire[$playerParty->player_grimoire_id_4] = 1;
			$usedGrimoire[$playerParty->player_grimoire_id_5] = 1;
			$usedGrimoire[$playerParty->player_grimoire_id_6] = 1;
		}
		unset($usedGrimoire[null]);
		unset($usedGrimoire[0]);
		// DebugUtil::e_log('playerPartyList.txt', 'playerPartyList', $playerPartyList);

		return $usedGrimoire;
	}

	/**
	 * 売却
	 * @param PlayerGrimoireSellRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function sell($request)
	{
        $response = self::getResponse($request);

		// プレイヤ取得

		$player = Player::find_($request->player_id);

		{
			BaseGameModel::beginTransaction();

			foreach ($request->player_grimoire_list as $player_grimoire)
			{
				$playerGrimoireId = $player_grimoire['player_grimoire_id'];

                // 売却するプレイヤ魔道書を取得

                $playerGrimoire = PlayerGrimoire::getOne_($playerGrimoireId);

                // ロックされていたら売却できない

                if ($playerGrimoire->isLocked())
                {
                    throw \App\Exceptions\GameException::make(
                        'locked grimoire: ' . $playerGrimoireId
                    );
                }

                // 魔素の計算

                $grimoire = Grimoire::getOne_($playerGrimoire->grimoire_id);
                $magicNum = $grimoire->sale_magic_elementary;

                // 覚醒による補正

                $grimoireAwake = GrimoireAwakeCoefficient::getOne_(
                    $playerGrimoire->awake
                );
                $magicNum *= $grimoireAwake->sale_coefficient;

                // 魔素を付与

                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemId = Item::ID_ELEMENT;
                $param->count = $magicNum;
                $param->srcType = SrcType::GRIMOIRE_SELL;
                $param->srcId = 0;

                PlayerService::giveOrPay($param);
                $log = $param->logs[0];

                // 消費する

                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemType = Item::TYPE_GRIMOIRE;
                $param->itemId = $playerGrimoire->grimoire_id;
                $param->playerGrimoireIds[] = $playerGrimoire->id;
                $param->count = - 1;
                $param->srcType = SrcType::GRIMOIRE_SELL;
                $param->srcId = $log->id;

                PlayerService::giveOrPay($param);
			}

			BaseGameModel::commit();
		}

		// $playerGrimoireList = PlayerGrimoire::getByPlayerId(
        // $request->player_id
        // );

		// 装備している魔道書一覧を取得
		// $usedGrimoire = self::calcUsedGrimoire($request->player_id);

		$body = [
			'magic_num' =>
                Player::getElement($request->player_id),
			// 'player_grimoire_list' => PlayerGrimoireListResponse::make(
            // $playerGrimoireList, $usedGrimoire
            // ),
		];
		$response->body = $body;

		return $response;
	}


	/**
	 * スロット解放フラグを取得
	 *
	 * @param PlayerGrimoire $playerGrimoire
	 * @param integer $slotNo スロット番号
	 * @return integer スロット解放フラグ(0:未解放、1:未装着、2:装着済み)
	 */
	private static function _getSlotReleaseFlag($playerGrimoire, $slotNo)
	{
		$slotReleaseFlag = 0;
		$slotFlag = sprintf('slot_%d_flag', $slotNo);
		$slot = sprintf('slot_%d', $slotNo);

		if ($playerGrimoire->$slotFlag == PlayerGrimoire::SLOT_RELEASE_FLAG_NO)
		{
			$slotReleaseFlag = 0;
		}
		else
		{
			if (!isset($playerGrimoire->$slot))
			{
				$slotReleaseFlag = 1;
			}
			else
			{
				$slotReleaseFlag = 2;
			}
		}
		return $slotReleaseFlag;
	}

	/**
	 * 魔道書個数取得
	 *
	 * @param object $param 付与/消費パラメータ
	 * @return integer 個数
	 */
	public static function getCount($param)
    {
        assert($param->itemType == Item::TYPE_GRIMOIRE);

        return PlayerGrimoire::countGrimoire($param->playerId, $param->itemId);
    }

	/**
	 * 魔道書付与/消費
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPay($param)
    {
        if ($param->count == 0)
            return;

        if ($param->count < 0)
        {
            // 魔道書の売却は特殊 (スロット開放、装備などがある)

            if (count($param->playerGrimoireIds) != - $param->count)
            {
                throw \App\Exceptions\UnknownException::make(
                    'giveOrPay playerGrimoireIds must be specified'
                );
            }

            foreach ($param->playerGrimoireIds as $playerGrimoireId)
            {
                $playerGrimoire = PlayerGrimoire::getOne_($playerGrimoireId);

				// ロックされていたら売却できない
                if ($playerGrimoire->isLocked())
                {
                    throw \App\Exceptions\GameException::make(
                        'locked grimoire: ' . $playerGrimoire->id
                    );
                }

                if (!$param->onlyCheck)
                {
                    $playerGrimoire->delete_flag
                        = PlayerGrimoire::DELETE_FLAG_YES;
                    $playerGrimoire->save();

                    // 消費ログ
                    $log = LogService::playerGrimoireGiveOrPay(
                        $playerGrimoire, $param->itemId, - 1, 0,
                        $param->srcType, $param->srcId
                    );
                    $param->logs[] = $log;
                }
            }
            return;
        }

		// プレイヤの取得

		$player = Player::find_($param->playerId);

        // 最大魔道書枠のチェック

        $grimoireCount = PlayerGrimoire::countByPlayerId($param->playerId);

        $spaceCount = $player->max_grimoire - $grimoireCount;

        if ($spaceCount <= 0)
        {
            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_OVER;
            throw \App\Exceptions\OverflowException::makeOverflow(
                '', '', 'player_grimoire',
                Item::TYPE_GRIMOIRE, $param->itemId,
                $param->count,
                $player->max_grimoire
            );
        }

        if (!$param->onlyCheck)
        {
            for ($i = 0; $i < $param->count; ++ $i)
            {
                $playerGrimoire = PlayerGrimoire::regist(
                    $param->playerId, $param->itemId
                );
                $param->playerGrimoireIds[] = $playerGrimoire->id;
                $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_TAKE;

                // 付与ログ
                $log = LogService::playerGrimoireGiveOrPay(
                    $playerGrimoire, $param->itemId, 1, 1,
                    $param->srcType, $param->srcId
                );
                $param->logs[] = $log;
            }
        }
    }

}
