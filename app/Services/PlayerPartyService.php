<?php
/**
 * プレイヤパーティ のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Models\Player;
use App\Models\PlayerCharacter;
use App\Models\PlayerGrimoire;
use App\Models\PlayerParty;
use App\Models\BaseGameModel;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Http\Responses\PlayerPartyListResponse;
use App\Http\Responses\PlayerPartyResponse;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\PlayerLevel;
use App\Services\MissionChecker\MissionCheckerPartyUpdate;
use App\Utils\DebugUtil;

/**
 * プレイヤパーティのサービス
 *
 */
class PlayerPartyService extends BaseService
{
	/**
	 * 一覧
	 *
	 * @param PlayerPartyListRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);

		// プレイヤパーティ取得
		$playerPartyList = PlayerParty::getByPlayerId($request->player_id);

		$body = [
			'player_party_list'	=> PlayerPartyListResponse::make(
                $playerPartyList
            ),
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * 更新
	 *
	 * @param PlayerPartyUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function update($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerPartyUpdate::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤパーティ取得
		$playerParty = PlayerParty::getByPartyNo(
            $request->player_id, $request->party_no
        );
		if (!isset($playerParty))
		{
			// 新規作成
			$playerParty = new PlayerParty();
			$playerParty->player_id	= $request->player_id;
			$playerParty->party_no	= $request->party_no;
		}

        $list = $request->party_character_list;
        $listCount = count($list);

        // キャラクター重複チェック
        $request->checkDup('player_character');

        // 魔道書重複チェック
        $request->checkDup('player_grimoire');

        // キャラ無し、魔道書装備チェック
        $request->checkNonCharacterGrimore();

		if ($request->party_no == 1)
        {
            // 4 人対応による、仕様変更
            // 1 人もキャラクターがいない場合にエラー
            if ($request->mainCharacterCount() <= 0)
            {
                throw \App\Exceptions\GameException::make(
                    "1st party needs 1 character"
                );
            }

            /*
            // 6 人パーティ用の仕様
            if ($listCount == 0 || $list[0]['player_character_id'] <= 0)
            {
                throw \App\Exceptions\GameException::make(
                    "1st party's first character is need"
                );
            }
            */
        }

		{
			BaseGameModel::beginTransaction();

			$totalCost = 0; // 合計コストを 0 に設定する

            for ($i = 1; $i <= PlayerParty::MAX_POSITIONS; ++ $i)
            {
                // キャラクター設定 //

                $partyChara = null;
                if ($i <= $listCount)
                    $partyChara = $list[$i - 1];

                $playerCharacterId = 0;
                if (isset($partyChara))
                    $playerCharacterId = $partyChara['player_character_id'];

                $playerParty->setCharacter($i, $playerCharacterId);

                // キャラクタのコストを加算する //

                if ($playerCharacterId > 0)
                {
                    $playerCharacter = PlayerCharacter::find_(
                        $playerCharacterId
                    );

                    $characterBase = CharacterBase::getOne(
                        $playerCharacter->character_id
                    );

                    if (!isset($characterBase))
                    {
                        throw \App\Exceptions\MasterException::makeNotFound(
                            'character_base', 'character_base_id',
                            $playerCharacter->character_id
                        );
                    }

                    $totalCost += $characterBase->cost;
                }

                // 魔道書設定 //

                $playerGrimoireId = 0;
                if (isset($partyChara))
                    $playerGrimoireId = $partyChara['player_grimoire_id'];

                $playerParty->setGrimoire($i, $playerGrimoireId);

                // 魔道書のコストを加算する //

				if ($playerGrimoireId != 0)
				{
                    $playerGrimoire = PlayerGrimoire::getOne(
                        $playerGrimoireId
                    );
                    if (!isset($playerGrimoire))
                    {
                        throw \App\Exceptions\DataException::makeNotFound(
                            'player_grimoire', 'id', $playerGrimoireId
                        );
                    }

					$grimoire = Grimoire::getOne(
                        $playerGrimoire->grimoire_id
                    );
                    if (!isset($grimoire))
                    {
                        throw \App\Exceptions\MasterException::makeNotFound(
                            'grimoire', 'grimoire_id',
                            $playerGrimoire->grimoire_id
                        );
                    }

					$totalCost += $grimoire->cost;
				}
            }

			// プレイヤー取得
			$player = Player::find_($request->player_id);

			$playerLv = $player->player_lv;

			// 上限コスト取得
			$playerLevel = PlayerLevel::getOne($playerLv);
            if (!isset($playerLevel))
            {
                throw \App\Exceptions\MasterException::makeNotFound(
                    'player_level', 'lv', $playerLv
                );
            }

			$maxPartyCost = $playerLevel->max_party_cost;

			// コスト上限を超えている場合、エラーを返す
			if ($totalCost > $maxPartyCost)
			{
                throw \App\Exceptions\GameException::make(
                    'exceed party cost: ' . $totalCost . '>' . $maxPartyCost
                );
			}

			$playerParty->save();

            // ミッション達成チェック

            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerParty', $playerParty);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

		$body = [
			'player_party'	=> PlayerPartyResponse::make($playerParty),
		];
		$response->body = $body;

		return $response;
	}
}
