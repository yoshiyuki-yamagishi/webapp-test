<?php
/**
 * フォロー のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Models\Player;
use App\Models\PlayerFriend;
use App\Models\MasterModels\PlayerLevel;
use App\Utils\DebugUtil;

/**
 * フォロー のサービス
 *
 */
class FollowService extends BaseService
{
	/**
	 * 追加
	 *
	 * @param FollowAddRequest $request
	 * @return ApiResponse
	 */
	public static function add($request)
	{
        // DebugUtil::e_log('FS_add', 'request', $request->all());
        $response = self::getResponse($request);

        $playerId = $request->player_id;
        $followId = $request->follow_player_id;

        // プレイヤー取得
        $player = Player::find_($playerId);

        // プレイヤーレベルマスタから、最大フォロー数取得
        $playerLevel = PlayerLevel::getOne_($player->player_lv);
        $maxFollow = $playerLevel->max_follow;
        // DebugUtil::e_log('FS_add', 'maxFollow', $maxFollow);

        $follow = PlayerFriend::getOne($playerId, $followId);
        if (isset($follow))
        {
            throw \App\Exceptions\ParamException::make(
                'specified follow is already exists'
            );
        }

        // フォロー数取得
        $followNum = PlayerFriend::getCount(
            $request->player_id, FriendService::TYPE_FOLLOW
        );
        // DebugUtil::e_log('FS_add', 'followNum', $followNum);

        // フォロー件数チェック
        if ($followNum >= $maxFollow)
        {
            throw \App\Exceptions\GameException::make(
                'already max follow: ' . $followNum . '/' . $maxFollow
            );
        }

        // 1 件挿入、トランザクション不要かな //

        $playerFriend = new PlayerFriend();
        $playerFriend->player_id = $playerId;
        $playerFriend->follow_id = $followId;
        $playerFriend->save();

        //  ナイーブな実装

        $followerNum = PlayerFriend::getCount(
            $request->player_id, FriendService::TYPE_FOLLOWER
        );
        $friendNum = PlayerFriend::getCount(
            $request->player_id, FriendService::TYPE_FRIEND
        );

		$body = [
            'follow_num' => ($followNum + 1) - $friendNum,
            'follower_num' => $followerNum - $friendNum,
            'friend_num' => $friendNum,
		];
		$response->body = $body;

		return $response;
    }

	/**
	 * 削除
	 *
	 * @param FollowDeleteRequest $request
	 * @return ApiResponse
	 */
	public static function delete($request)
	{
        // DebugUtil::e_log('FS_delete', 'request', $request->all());
        $response = self::getResponse($request);

        $playerId = $request->player_id;
        $followId = $request->follow_player_id;

        $follow = PlayerFriend::getOne($playerId, $followId);
        if (empty($follow))
        {
            throw \App\Exceptions\ParamException::makeNotFound(
                'player_friend',
                ['player_id', 'follow_id'],
                [$playerId, $followId]
            );
        }

        $followNum = PlayerFriend::getCount(
            $request->player_id, FriendService::TYPE_FOLLOW
        );

        // 1 件削除、トランザクション不要かな //

        $follow->delete();

        //  ナイーブな実装

        $followerNum = PlayerFriend::getCount(
            $request->player_id, FriendService::TYPE_FOLLOWER
        );
        $friendNum = PlayerFriend::getCount(
            $request->player_id, FriendService::TYPE_FRIEND
        );

		$body = [
            'follow_num' => ($followNum - 1) - $friendNum,
            'follower_num' => $followerNum - $friendNum,
            'friend_num' => $friendNum,
		];
		$response->body = $body;

		return $response;
    }

}
