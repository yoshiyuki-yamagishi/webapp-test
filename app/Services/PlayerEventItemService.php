<?php
    /**
     * プレイヤイベントアイテム のサービス
     *
     */

    namespace App\Services;

    use App\Http\Requests\PlayerEventItemListRequest;
    use App\Http\Responses\ApiResponse;
    use App\Http\Responses\PlayerEventItemListResponse;
    use App\Models\PlayerEventItem;

    /**
     * プレイヤイベントアイテム のサービス
     *
     */
    class PlayerEventItemService extends BaseService
    {
        /**
         * 一覧
         *
         * @param PlayerEventItemListRequest $request リクエスト
         * @return ApiResponse レスポンス
         */
        public static function list($request)
        {
            $response = self::getResponse($request);

            $playerEventItemList = PlayerEventItem::getListByPlayerIdAndEventId(
                $request->player_id, $request->event_id
            );
            $body = [
                'player_event_item_list' => PlayerEventItemListResponse::make($playerEventItemList),
            ];

            $response->body = $body;

            return $response;
        }
    }
