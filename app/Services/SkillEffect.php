<?php
/**
 * スキル効果
 *
 */

namespace App\Services;

/**
 * スキル効果
 *
 */
class SkillEffect
{
    // サーバーで使用するスキルは限られているため単純化する

    // 獲得PlEXPアップ
    // BBDW_魔道書パラメータ・スキルリスト.xlsx
    // ※効果重複ありだと想定が色々崩れる可能性あり。要調整
    // https://rdm.linkedbrain.jp/issues/4251
    
    const TYPE_NONE = 0;
    const TYPE_PLAYER_EXP_UP = 1; // 直アップ
    const TYPE_CHARA_EXP_UP = 2; // % アップ
    const TYPE_MONEY_UP = 3; // % アップ
    const TYPE_FP_UP = 4; // % アップ
    
    public $type;
    public $value;

    /**
     * 生成する
	 * @param list $param 連想配列
	 * @return object self
     *
     */
    public static function make($param)
    {
        $type = self::typeFromText(trim($param['effect']));
        if ($type == 0)
            return null;

        $effect = new self();
        $effect->type = $type;
        $effect->value = trim($param['value']);
        return $effect;
    }

    /**
     * 文字列のタイプを整数のタイプに変換
	 * @param string $type
	 * @return integer タイプ
     *
     */
    public static function typeFromText($type)
    {
        if (strcasecmp($type, 'get_playerEXP_UP') == 0)
            return self::TYPE_PLAYER_EXP_UP;
        else if (strcasecmp($type, 'get_charaEXP_UP') == 0)
            return self::TYPE_CHARA_EXP_UP;
        else if (strcasecmp($type, 'get_money_UP') == 0)
            return self::TYPE_MONEY_UP;
        else if (strcasecmp($type, 'get_FP_UP') == 0)
            return self::TYPE_FP_UP;
        
        return 0;
    }

    public function applyUp(&$value)
    {
        switch ($this->type)
        {
        case self::TYPE_PLAYER_EXP_UP:
            $value += $this->value;
            return;
        case self::TYPE_CHARA_EXP_UP:
        case self::TYPE_MONEY_UP:
        case self::TYPE_FP_UP:
            $value = intdiv($value * (100 + $this->value), 100);
            return;
        }
        assert(false);
    }
    
}
