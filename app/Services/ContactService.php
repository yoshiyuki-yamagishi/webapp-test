<?php
/**
 * お問い合わせ のサービス
 *
 */

namespace App\Services;

use App\Models\Player;
use App\Models\PlayerCommon;
use App\Utils\DateTimeUtil;


/**
 * お問い合わせ のサービス
 *
 */
class ContactService extends BaseService
{
    // お問い合わせタイプ ※config/const.phpのタイプと合わせる
    const CONTACT_TYPE_PRODUCT_BUY = 1;
    const CONTACT_TYPE_TRANSIT = 2;
    const CONTACT_TYPE_BUG_REPORT = 3;
    const CONTACT_TYPE_REQUEST = 4;

    // SubjectList
    const SUBJECT_LIST = [
        self::CONTACT_TYPE_PRODUCT_BUY => 'アプリ内購入に関するお問い合わせ',
        self::CONTACT_TYPE_TRANSIT => 'データ引き継ぎに関するお問い合わせ',
        self::CONTACT_TYPE_BUG_REPORT => '不具合に関するお問い合わせ',
        self::CONTACT_TYPE_REQUEST => 'ご意見・ご要望',
    ];

    // 共通本文
    const BASE_BODY = <<<EOD
お問い合わせ時刻：__NOW__%0D%0A
プレイヤーID： __PL_ID__%0D%0A
プレイヤー名：__PL_NAME__%0D%0A
端末情報：__MDL_NAME__%0D%0A
OSバージョン：__OS_VER__%0D%0A
アプリバージョン：Ver.__APP_VER__%0D%0A
---以下入力をお願いいたします---%0D%0A
EOD;

    // 報告系本文
    const REPORT_BODY = <<<EOD
通信状況（4G、LTE、WiFiなど）：%0D%0A
発生日時：%0D%0A
発生場所：%0D%0A
症状：%0D%0A
EOD;

    // 報告系本文
    const CONSTACT_BODY = <<<EOD
お問い合わせ内容：%0D%0A
EOD;

    // 要望系本文
    const REQUEST_BODY = <<<EOD
ご意見・ご要望：%0D%0A
EOD;

    // BodyList
    const BODY_LIST = [
        self::CONTACT_TYPE_PRODUCT_BUY => self::REPORT_BODY,
        self::CONTACT_TYPE_TRANSIT => self::CONSTACT_BODY,
        self::CONTACT_TYPE_BUG_REPORT => self::REPORT_BODY,
        self::CONTACT_TYPE_REQUEST => self::REQUEST_BODY,
    ];

    /**
     * メール文章作成
     * 必要な情報をDBから取得
     *
     * @param $request
     * @return array
     */
    public static function makeMail($request)
    {
        $now = DateTimeUtil::getNOW();

        return [
            'player_id' => $request->player_id,
            'app_version' => $request->app_version,
            'now' => $now,

            'subject' => self::SUBJECT_LIST[$request->contact_type],
            'body' => self::_makeBaseBody($request, $now),
        ];
    }

    /**
     * メール本文をDB情報から作成
     *
     * @param $request
     * @param $now
     * @return string|string[]
     * @throws \App\Exceptions\ApiException
     */
    private static function _makeBaseBody($request, $now)
    {
        // プレイヤ情報を取得
        $player = Player::find_($request->player_id);
        // プレイヤ共通テーブル (共通DB) からプレイヤ情報を取得
        $playerCommon = PlayerCommon::getByPlayerId_($request->player_id);

        // 各種情報を置換する ===========================
        $baseBody = self::BASE_BODY;
        // 日時
        $baseBody = str_replace("__NOW__", $now, $baseBody);
        // PlayerID
        $baseBody = str_replace("__PL_ID__", $player->id, $baseBody);
        // PlayerName
        $baseBody = str_replace("__PL_NAME__", $player->player_name, $baseBody);
        // 端末名
        $baseBody = str_replace("__MDL_NAME__", $playerCommon->model_name, $baseBody);
        // 端末名
        $baseBody = str_replace("__MDL_NAME__", $playerCommon->model_name, $baseBody);
        // OSバージョン
        $baseBody = str_replace("__OS_VER__", $playerCommon->os_version, $baseBody);
        // アプリバージョン
        $baseBody = str_replace("__APP_VER__", $request->app_version, $baseBody);
        // 各種情報を置換する ===========================

        // タイプ別本文と追加
        $body = $baseBody . self::BODY_LIST[(int)$request->contact_type];

        return $body;
    }
}
