<?php
/**
 * ログ のサービス
 *
 */

namespace App\Services;
use App\Models\PlayerLog;
use App\Models\PlayerCharacterLog;
use App\Models\PlayerGrimoireLog;
use App\Models\PlayerItemLog;
use App\Models\PlayerBlueCrystalLog;

/**
 * ログ のサービス
 *
 */
class LogService extends BaseService
{
	/**
	 * プレイヤ付与/消費ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $itemId アイテムID
	 * @param integer $itemCount アイテム数増減
	 * @param integer $itemValue 現在のアイテム数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function playerGiveOrPay(
        $player, $itemId, $itemCount, $itemValue, $srcType, $srcId
    )
    {
        return PlayerLog::registerGiveOrPay(
            $player, $itemId, $itemCount, $itemValue, $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤ経験値ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $exp 取得経験値
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerExp(
        $player, $exp, $srcType, $srcId
    )
    {
        return PlayerLog::registerExp(
            $player, $exp,
            $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤレベルログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $lv 取得レベル
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerLv(
        $player, $lv, $srcType, $srcId
    )
    {
        return PlayerLog::registerLv(
            $player, $lv,
            $srcType, $srcId
        );
    }

	/**
	 * プレイヤAL回復ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $upNum 増加量
	 * @param integer $itemId 消費アイテムID
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerAlRecover(
        $player, $upNum, $itemId, $srcType, $srcId
    )
    {
        return PlayerLog::registerAlRecover(
            $player, $upNum, $itemId, $srcType, $srcId
        );
    }

	/**
	 * プレイヤチュートリアル進行度ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $upNum 増加量
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerTutorialUpdate(
        $player, $upNum, $srcType, $srcId
    )
    {
        return PlayerLog::registerTutorialUpdate(
            $player, $upNum, $srcType, $srcId
        );
    }

	/**
	 * プレイヤ魔道書枠増加ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $upNum 枠増加
	 * @param integer $crystalNum 消費蒼の結晶数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerGrimoireLimitUp(
        $player, $upNum, $crystalNum, $srcType, $srcId
    )
    {
        return PlayerLog::registerGrimoireLimitUp(
            $player, $upNum, $crystalNum, 
            $srcType, $srcId
        );
    }

	/**
	 * プレイヤキャラクタ付与/消費ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $itemId アイテムID
	 * @param integer $itemCount アイテム数増減
	 * @param integer $itemValue 現在のアイテム数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function playerCharacterGiveOrPay(
        $playerCharacter, $itemId, $itemCount, $itemValue, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerGiveOrPay(
            $playerCharacter, $itemId, $itemCount, $itemValue, $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤキャラクタ経験値ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $exp 取得経験値
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerCharacterExp(
        $playerCharacter, $exp, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerExp(
            $playerCharacter, $exp,
            $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤキャラクターレベルログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $lv 取得レベル
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerCharacterLv(
        $playerCharacter, $lv, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerLv(
            $playerCharacter, $lv,
            $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤキャラクタースキルレベルログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $skillNo 何番目のスキルか
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerCharacterSkillLv(
        $playerCharacter, $skillNo, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerSkillLv(
            $playerCharacter, $skillNo,
            $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤキャラクターオーブ装備ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $orbFlags 今回装備フラグ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerCharacterOrbEquip(
        $playerCharacter, $orbFlags, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerOrbEquip(
            $playerCharacter, $orbFlags,
            $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤキャラクターグレードアップログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $orbFlags 今回装備フラグ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerCharacterGradeUp(
        $playerCharacter, $orbFlags, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerGradeUp(
            $playerCharacter, $orbFlags, $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤキャラクター進化ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerCharacterEvolve(
        $playerCharacter, $srcType, $srcId
    )
    {
        return PlayerCharacterLog::registerEvolve(
            $playerCharacter, $srcType, $srcId
        );
    }

	/**
	 * プレイヤ魔道書付与/消費ログを登録
     *
	 * @param object $playerGrimoire プレイヤ魔道書
	 * @param integer $itemId アイテムID
	 * @param integer $itemCount アイテム数増減
	 * @param integer $itemValue 現在のアイテム数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function playerGrimoireGiveOrPay(
        $playerGrimoire, $itemId, $itemCount, $itemValue, $srcType, $srcId
    )
    {
        return PlayerGrimoireLog::registerGiveOrPay(
            $playerGrimoire, $itemId, $itemCount, $itemValue, $srcType, $srcId
        );
    }
    
	/**
	 * プレイヤ魔道書覚醒ログを登録
     *
	 * @param object $playerGrimoire プレイヤ魔道書
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 */
	public static function playerGrimoireAwake(
        $playerGrimoire, $srcType, $srcId
    )
    {
        return PlayerGrimoireLog::registerAwake(
            $playerGrimoire, $srcType, $srcId
        );
    }

	/**
	 * プレイヤアイテム付与/消費ログを登録
     *
	 * @param object $playerItem プレイヤアイテム
	 * @param integer $itemId アイテムID
	 * @param integer $itemCount アイテム数増減
	 * @param integer $itemValue 現在のアイテム数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function playerItemGiveOrPay(
        $playerItem, $itemId, $itemCount, $itemValue, $srcType, $srcId
    )
    {
        return PlayerItemLog::registerGiveOrPay(
            $playerItem, $itemId, $itemCount, $itemValue, $srcType, $srcId
        );
    }

	/**
	 * プレイヤ蒼の結晶付与/消費ログを登録
     *
	 * @param object $playerBlueCrystal プレイヤ蒼の結晶
	 * @param integer $itemId 蒼の結晶ID
	 * @param integer $itemCount 蒼の結晶数増減
	 * @param integer $itemValue 現在の蒼の結晶数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function playerBlueCrystalGiveOrPay(
        $playerBlueCrystal, $itemId, $itemCount, $itemValue, $srcType, $srcId
    )
    {
        return PlayerBlueCrystalLog::registerGiveOrPay(
            $playerBlueCrystal, $itemId, $itemCount, $itemValue,
            $srcType, $srcId
        );
    }
    
}
