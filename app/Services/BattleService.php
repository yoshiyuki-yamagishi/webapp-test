<?php
/**
 * バトル のサービス
 *
 */

namespace App\Services;

use App\Config\Constants;
use App\Http\Requests\BattleEndRequest;
use App\Http\Requests\BattleSkipRequest;
use App\Http\Responses\ApiResponse;
use App\Http\Responses\BattleEnemyListResponse;
use App\Http\Responses\PlayerMissionResponse;
use App\Http\Responses\PlayerPartyResponse;
use App\Http\Responses\PlayerResponse;
use App\Http\Responses\PlayerCharacterListResponse;
use App\Http\Responses\PlayerGrimoireListResponse;
use App\Http\Responses\QuestRewardResponse;
use App\Http\Responses\QuestRewardListResponse;
use App\Models\BaseGameModel;
use App\Models\EventSpecialEffect;
use App\Models\MasterModels\EventQuest;
use App\Models\MasterModels\Mission;
use App\Models\Player;
use App\Models\PlayerBattle;
use App\Models\PlayerBattleCharacter;
use App\Models\PlayerBattleGrimoire;
use App\Models\PlayerBattleResult;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerCharacter;
use App\Models\PlayerCharacterQuest;
use App\Models\PlayerCommonCache;
use App\Models\PlayerData;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\PlayerParty;
use App\Models\PlayerPresent;
use App\Models\PlayerQuest;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\PlayerLevel;
use App\Models\QuestBonusSetting;
use App\Services\KpiLogger\PlayerPartyGrimoireLogger;
use App\Services\KpiLogger\PlayerQuestCharacterLogger;
use App\Services\KpiLogger\PlayerQuestPartyLogger;
use App\Services\KpiLogger\QuestLogger;
use App\Services\MissionChecker\MissionCheckerBattle;
use App\Services\MissionChecker\MissionCheckerEventItemCount;
use App\Services\MissionChecker\MissionCheckerQuest;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use Symfony\Component\Debug\Debug;


/**
 * バトル のサービス
 *
 */
class BattleService extends BaseService
{
	const BATTLE_CODE_PREFIX	= 'btlp';

	/**
	 * 開始
	 *
	 * @param BattleStartRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function start($request)
	{
        return self::startImpl($request);
	}

	/**
	 * 再開
	 *
	 * @param BattleRestartRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function restart($request)
	{
        return self::startImpl($request);
	}

	/**
	 * 開始、再開の実装
	 *
	 * @param BattleStartRequest/BattleRestartRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	protected static function startImpl($request)
	{
	    $response = self::getResponse($request);
        $now = $response->currentDateDB();
        $_now = $response->currentDate;

        $playerBattle = PlayerBattle::getActive(
            $request->player_id
        );

        $battleCode = '';
        $questCategory = 0;
        $chapterId = 0;
        $questId = 0;
        $partyNo = 0;
        $restartFlg = false;
        $useAl = true;

        if ($request instanceof \App\Http\Requests\BattleStartRequest)
        {
            if (PlayerBattle::isValidBattleCode($playerBattle))
            {
                throw \App\Exceptions\GameException::make(
                    'battle is already started: ' . $playerBattle->battle_code
                );
            }

            $battleCode	= self::_makeBattleCode($request->player_id);
            $questCategory = $request->quest_category;
            $chapterId = $request->chapter_id;
            $questId = $request->quest_id;
            $partyNo = $request->party_no;

            $playerBattle = new PlayerBattle();
            $playerBattle->player_id = $request->player_id;
			$playerBattle->battle_code = $battleCode;
			$playerBattle->quest_category = $questCategory;
			$playerBattle->chapter_id = $chapterId;
			$playerBattle->quest_id = $questId;
			$playerBattle->party_no = $partyNo;
        }
        else if ($request instanceof \App\Http\Requests\BattleRestartRequest)
        {
            if (!PlayerBattle::isValidBattleCode($playerBattle))
            {
                throw \App\Exceptions\DataException::makeNotFound(
                    'player_battle',
                    'player_id',
                    $request->player_id
                );
            }
            if ($playerBattle->battle_code != $request->battle_code)
            {
                throw \App\Exceptions\ParamException::makeInvalid(
                    'player_battle',
                    'battle_code',
                    $request->battle_code
                );
            }

            $restartFlg = true;
            $useAl = false;
            $battleCode = $playerBattle->battle_code;
            $questCategory = $playerBattle->quest_category;
            $chapterId = $playerBattle->chapter_id;
            $questId = $playerBattle->quest_id;
            $partyNo = $playerBattle->party_no;
        }
        else
        {
            throw \App\Exceptions\UnknownException::make(
                'unexpected request type: ' . get_class($request)
            );
        }

        // プレイヤ取得
		$player = Player::find_($request->player_id);

		// プレイヤパーティ取得
		$playerParty = PlayerParty::getByPartyNo(
            $request->player_id, $partyNo
        );
		if (!isset($playerParty))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_party', ['player_id', 'party_no'],
                [$request->player_id, $partyNo]
            );
		}

        $charas = PlayerCharacter::getByPlayerParty($playerParty);
        $grims = PlayerGrimoire::getByPlayerParty($playerParty);

        // クエスト章、クエスト、バトル、初回報酬 は
        // マスタデーターから取得する必要がある

        $questService = QuestService::make($questCategory);

        $chapter = $questService->getChapter(
            $chapterId
        );

        // 章の日付をチェックする
        {
            $startDay = new \DateTime($chapter->start_day);
            $endDay = new \DateTime($chapter->end_day);

            if ($startDay > $_now || $endDay < $now)
            {
                throw \App\Exceptions\GameException::make(
                    'xx_quest_chapter is out of date'
                );
            }
        }

        $quest = $questService->getQuest(
            $chapterId,
            $questId
        );

        // クエストの日付をチェックする
        $questService->checkStartEndDay($quest, $now);

        // クエストタイプをチェックする
        if ($quest->quest_type == QuestService::QUEST_TYPE_STORY)
        {
            throw \App\Exceptions\ParamException::make(
                'specified quest\'s quest_type is invalid: '
                . $quest->quest_type
            );
        }

        $battleList = $questService->getBattleByQuest($quest);


        // クリア済み情報を得る

        $playerQuest = $questService->getPlayerQuest(
            $request->player_id,
            $chapterId,
            $questId
        );

        $isFirst = !QuestService::IsCleared_PQ($playerQuest);

        // バトルエネミーリストの計算
        $_battleList = BattleEnemyListResponse::make($battleList);

        if($restartFlg){
            $firstRewards = []; // 無い場合もある
            if ($isFirst){
                $firstRewards = $questService->getRestartReward($playerBattle->id, QuestService::REWARD_TYPE_FIRST);
            }

            $fixRewards = $questService->getRestartReward($playerBattle->id, QuestService::REWARD_TYPE_FIX);
            $dropItems = $questService->getRestartReward($playerBattle->id, QuestService::REWARD_TYPE_DROP);
            $bonusRewards = $questService->getRestartReward($playerBattle->id, QuestService::REWARD_TYPE_BONUS);

        }else{
            // 初回報酬を計算する
            $firstRewards = []; // 無い場合もある
            if ($isFirst)
            {
                $firstRewards = $questService->calcFirstReward($quest);
            }
            // 固定報酬を計算する
            $fixRewards = $questService->calcFixReward($quest);
            // ドロップ報酬を計算する
            $dropItems = $questService->calcDropReward($quest, $_battleList);
            // ボーナス報酬を計算
            $bonusRewards = [];
            // イベントは特攻のみの計算（今後イベントもボーナスがある場合はイベントのマスターにフラグつけて分ける）
            $eventFlg = Constants::OFF_FLG;
            if($questCategory == Constants::QUEST_CATEGORY_EVENT){
                if($chapter->type != Constants::EVENT_TYPE_DAILY){
                    $eventFlg = Constants::ON_FLG;
                }
            }
            if($eventFlg){
                //特攻データとる→所持確認する→あればボーナスかける
                $spEffectDataList = EventSpecialEffect::getByChapterIdAndBonusType($chapterId,Constants::SP_EFFECT_BONUS_TYPE_DROP_UP);
                if(!$spEffectDataList->isEmpty()){
                    $bonusRewards = $questService->calcSpEffectBonusReward($chapter->item, $spEffectDataList, $fixRewards, $dropItems, $charas, $grims);
                }
            }else{
                $bonusData = $questService->getQuestBonusData($chapterId, $questCategory, Constants::BONUS_TYPE_DROP, $now);
                if ($bonusData) {
                    $dailyCount = PlayerBattle::getChapterDailyClearCount($request->player_id, $questCategory, $chapterId, $now);
                    $bonusRewards = $questService->calcBonusReward($bonusData, $fixRewards, $dropItems, $dailyCount);
                }
            }
        }

        {
			// トランザクション開始
			BaseGameModel::beginTransaction();

			// バトルコード登録/更新 (id を発行)
			$playerBattle->save();

			// AL 消費
            if ($useAl)
                $player->useAl($now, $quest->al, true);


			if(!$restartFlg) {
                // 計算した報酬を登録する (初回)
                if ($isFirst && is_array($firstRewards) && !empty($firstRewards)) {
                    $questService->registPlayerBattleReward(
                        $playerBattle, 0, // スキップ番号
                        QuestService::REWARD_TYPE_FIRST,
                        $firstRewards
                    );
                }

                // 計算した報酬を登録する (固定)
                if (is_array($fixRewards) && !empty($fixRewards)) {
                    $questService->registPlayerBattleReward(
                        $playerBattle, 0, // スキップ番号
                        QuestService::REWARD_TYPE_FIX,
                        $fixRewards
                    );
                }

                // 計算した報酬を登録する (ドロップ)
                if (is_array($dropItems) && !empty($dropItems)) {
                    $questService->registPlayerBattleReward(
                        $playerBattle, 0, // スキップ番号
                        QuestService::REWARD_TYPE_DROP,
                        $dropItems
                    );
                }

                // 計算した報酬を登録する (ボーナス)
                if ($bonusRewards) {
                    $questService->registPlayerBattleReward(
                        $playerBattle, 0, // スキップ番号
                        QuestService::REWARD_TYPE_BONUS,
                        $bonusRewards
                    );
                }
            }

            // 挑戦時にプレイヤクエストを登録する //
            $questService->preparePlayerQuest(
                $playerQuest,
                $request->player_id,
                $chapterId,
                $questId
            );
            $playerQuest->save();

			BaseGameModel::commit();
		}



        if ($request instanceof \App\Http\Requests\BattleStartRequest)
        {
            $body = [
                'battle_code' =>
                    $battleCode,
            ];
        }
        else if ($request instanceof \App\Http\Requests\BattleRestartRequest)
        {
            $body = [
                'quest_category' =>
                    $questCategory,
                'chapter_id' =>
                    $chapterId,
                'quest_id' =>
                    $questId,
                'party_no' =>
                    $partyNo,
            ];
        }
        else
        {
            throw \App\Exceptions\UnknownException::make(
                'unexpected request type: ' . get_class($request)
            );
        }

        // 価格を取得

        $cPrice = Constant::getOne_(Constant::BATTLE_CONTINUE_PRICE);

        // レスポンスに不要な情報を除去する (高速化)

        BattleEnemyListResponse::fixResponse($_battleList);

        // 共通のボディ

        $body['player_party']
            = PlayerPartyResponse::make($playerParty);
        $body['player_character_list']
            = PlayerCharacterListResponse::make($player, $charas);
        $body['player_grimoire_list']
            = PlayerGrimoireListResponse::make($grims, true);
        $body['battle_enemy_list']
            = $_battleList;
        $body['first_reward_list']
            = $firstRewards;
        $body['fix_reward_list']
            = $fixRewards;
        $body['continue_blue_crystal_num']
            = $cPrice->value1;

		$response->body = $body;
		return $response;
    }


	/**
	 * 終了
	 *
	 * @param BattleEndRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function end($request)
	{

        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerBattle::getInstance();
        $missionChecker->init($request->player_id, $now);

        $eventMissionChecker = MissionCheckerEventItemCount::getInstance();
        $eventMissionChecker->initEvent($request->player_id, $now);

		// プレイヤ取得

		$player = Player::find_($request->player_id);
        $playerLv0 = $player->player_lv; // 開始前のレベルを保存

        // バトル取得

        $playerBattle = PlayerBattle::getByBattleCode(
            $request->player_id, $request->battle_code
        );

        if (!PlayerBattle::isValidBattleCode($playerBattle))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_battle',
                ['player_id', 'battle_code'],
                [$request->player_id, $request->battle_code]
            );
        }

		// プレイヤパーティ取得

		$playerParty = PlayerParty::getByPartyNo(
            $request->player_id, $playerBattle->party_no
        );
		if (!isset($playerParty))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_party',
                ['player_id', 'party_no'],
                [$request->player_id, $playerBattle->party_no]
            );
		}

        // プレイヤパーティのキャラクターリスト取得

		$characterList = PlayerCharacter::getByPlayerParty(
            $playerParty
        );
		if (!isset($characterList))
		{
            throw \App\Exceptions\DataException::make(
                'no player_party characters'
            );
		}

        // プレイヤパーティの魔道書リスト取得

		$grimoireList = PlayerGrimoire::getByPlayerParty(
            $playerParty
        );

        // クエストサービス取得

        $questService = QuestService::make($playerBattle->quest_category);

        // クエスト取得

        $quest = $questService->getQuest(
            $playerBattle->chapter_id,
            $playerBattle->quest_id
        );

        // クリア済み情報を得る

        $playerQuest = $questService->getPlayerQuest(
            $request->player_id,
            $playerBattle->chapter_id,
            $playerBattle->quest_id
        );

        if (empty($playerQuest))
        {
            // start 時に登録したハズ
            throw \App\Exceptions\DataException::makeNotFound(
                'player_quest', '[player_id, quest_id]',
                [$request->player_id, $playerBattle->quest_id]
            );
        }

        // クエストミッションチェッカーを初期化

        $_missionChecker = MissionCheckerQuest::getInstance();
        $_missionChecker->initQuest($playerQuest, $quest, $now);

        $firstRewards = [];
        $fixRewards = [];
        $dropList = [];
        $bonusRewards = [];
        $missionRewards = [];

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

            // プレイヤバトル更新

			if ($request->result == BattleEndRequest::RESULT_WIN)
                $playerBattle->result = PlayerBattle::RESULT_WIN;
            else
                $playerBattle->result = PlayerBattle::RESULT_LOSE_END;

			$playerBattle->save();

			// バトル履歴追加

            $playerBattleResult = PlayerBattleResult::regist(
                $playerBattle, $request
            );

            // バトルキャラクター履歴追加

            $battleCharacters = PlayerBattleCharacter::register(
                $playerBattle, $characterList
            );

            // バトル魔道書履歴追加

            $battleGrimoires = PlayerBattleGrimoire::register(
                $playerBattle, $grimoireList
            );

			// 勝利の場合クエスト報酬を付与する

			if ($request->result == BattleEndRequest::RESULT_WIN)
			{
                $takeOverList = []; // あふれたもの
                $missionTakeOverList = []; // あふれたもの

                // クリア済みフラグを立てる
                if ($playerQuest->clear_flag != QuestService::CLEAR_FLAG_YES)
                {
                    $playerQuest->clear_flag = QuestService::CLEAR_FLAG_YES;
                    $playerQuest->cleared_at = $now;
                }

                // クリア数を増加する
                ++ $playerQuest->clear_count;

                // クエストミッションの達成チェック

                $_missionChecker->addData(
                    'player', $player
                );
                $_missionChecker->addData(
                    'playerBattle', $playerBattle
                );
                $_missionChecker->addData(
                    'playerBattleResult', $playerBattleResult
                );
                $_missionChecker->addData(
                    'playerParty', $playerParty
                );
                $_missionChecker->addData(
                    'partyCharacterList', $characterList
                );

                $_achiveMissionList = [];
                $_missionChecker->updateMission($_achiveMissionList, true);

				// 初回報酬取得

                self::_giveRewardItems(
                    $takeOverList, $firstRewards,
                    $questService, $playerBattle,
                    QuestService::REWARD_TYPE_FIRST
                );

				// 固定報酬取得

                self::_giveRewardItems(
                    $takeOverList, $fixRewards,
                    $questService, $playerBattle,
                    QuestService::REWARD_TYPE_FIX
                );

				// ドロップ報酬取得

                self::_giveRewardItems(
                    $takeOverList, $dropList,
                    $questService, $playerBattle,
                    QuestService::REWARD_TYPE_DROP
                );

                // ボーナス報酬取得
                self::_giveRewardItems(
                    $takeOverList, $bonusRewards,
                    $questService, $playerBattle,
                    QuestService::REWARD_TYPE_BONUS
                );

                // ミッション報酬取得

                self::_giveMissionItems(
                    $missionTakeOverList, $missionRewards,
                    $_achiveMissionList, $playerBattle
                );

				// バトル結果追加(経験値、p$)

				self::_giveQuestReward(
                    $questService,
                    $playerBattle,
                    $player,
                    $characterList,
                    $grimoireList,
                    $request
                );

                // あふれたアイテムをプレゼントボックスに入れる

                self::_presentRewardItems(
                    $playerBattle, $takeOverList, false
                );
                self::_presentRewardItems(
                    $playerBattle, $missionTakeOverList, true
                );

                // プレイヤクエスト更新

                $playerQuest->save();
			}

            // ホームミッション更新

            $missionChecker->addData(
                'player', $player
            );
            $missionChecker->addData(
                'playerQuest', $playerQuest
            );
            $missionChecker->addData(
                'playerBattle', $playerBattle
            );
            $missionChecker->addData(
                'playerBattleResult', $playerBattleResult
            );
            $missionChecker->addData(
                'playerParty', $playerParty
            );
            $missionChecker->addData(
                'partyCharacterList', $characterList
            );

            // 絶対値になったため、クリアしたクエスト以外の判定も必要だろう //

            $playerQuests = PlayerQuest::getByPlayerId(
                $player->id, PlayerQuest::QUEST_CATEGORY_ALL, 0, $now
            );
            $missionChecker->addData(
                'playerQuests', $playerQuests
            );

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

            //イベント用ミッションチェック
            if($playerQuest->quest_category == Constants::QUEST_CATEGORY_EVENT){
                $eventMissionChecker->addData(
                    'player', $player
                );
                $eventMissionChecker->addData(
                    'playerQuest', $playerQuest
                );

                $eventMissionChecker->updateMission($achiveMissionList, false);
            }
            BaseGameModel::commit();
		}

        $takeFlag = GiveOrPayParam::TAKE_FLAG_NONE;

        // プレイヤー情報の更新
		$player = Player::find_($request->player_id);

        if ($player->player_lv != $playerLv0) // プレイヤレベルが変わった
        {
            // 1 SQL なので、トランザクション不要
            // 失敗しても、さほど、問題無し
            PlayerCommonCache::updateCache($player);
        }

        //------------------------------
        // KPIログ登録
        //------------------------------
        //  クエスト情報の記録
/*        $logger = new QuestLogger();
        $logger->addData($player, $playerBattle, $request->result, $_achiveMissionList, $playerQuest, $playerBattleResult, $now);*/

        //  ユーザクエストパーティ情報の記録
/*        $playerPartyLogger = new PlayerQuestPartyLogger();
        $playerPartyLogger->register($player->player_id, $playerBattle->player_battle_id, $playerParty, $now);*/

        //  ユーザクエストパーティのキャラクター情報記録
/*        $playerQuestCharacterLogger = new PlayerQuestCharacterLogger();
        $playerQuestCharacterLogger->addData($playerBattle->player_battle_id, $battleCharacters, $now);*/

        //  ユーザーパーティのグリモア情報記録
/*        $playerPartyGrimoireLogger = new PlayerQuestGrimoireLogger();
        $playerPartyGrimoireLogger->addData($battleGrimoires, $grimoireList, $now);*/

		$body = [
			'player' =>
                PlayerResponse::make($player),
            'player_character_list' =>
                PlayerCharacterListResponse::make($player, $characterList),
            'first_reward_list' =>
                $firstRewards,
            'fix_reward_list' =>
                $fixRewards,
            'drop_reward_list' =>
                $dropList,
            'bonus_reward_list' =>
                $bonusRewards,
            'mission_reward_list' =>
                $missionRewards,
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * コンティニュー
	 *
	 * @param BattleContinueRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function continue_($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

		$player = Player::find_($request->player_id);

		// バトルコードチェック

		$playerBattle = PlayerBattle::getByBattleCode(
            $request->player_id, $request->battle_code
        );
        if (!PlayerBattle::isValidBattleCode($playerBattle))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_battle',
                ['player_id', 'battle_code'],
                [$request->player_id, $request->battle_code]
            );
		}

        // 価格を取得
        $cPrice = Constant::getOne_(Constant::BATTLE_CONTINUE_PRICE);

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

			// 蒼の結晶消費

			$need = $cPrice->value1;

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = Item::ID_FREE_BLUE_CRYSTAL;
            $param->count = - $need;
            $param->srcType = SrcType::CONTINUE;
            $param->srcId = $playerBattle->id;

            PlayerService::giveOrPay($param);

			// バトル更新

            $playerBattle->continue_count += 1;
            $playerBattle->save();

			// バトル履歴追加

            $playerBattleResult = PlayerBattleResult::regist(
                $playerBattle, $request
            );

			BaseGameModel::commit();
		}

        $freeNum = $player->freeBlueCrystalNum($now);
        $chargeNum = $player->chargedBlueCrystalNum($now);

		$body = [
			'free_blue_crystal_num' => $freeNum,
			'blue_crystal_num' => $chargeNum,
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * スキップ
	 *
	 * @param BattleSkipRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function skip($request)
	{

        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerBattle::getInstance();
        $missionChecker->init($request->player_id, $now);

        $playerBattle = PlayerBattle::getActive(
            $request->player_id
        );
        if (PlayerBattle::isValidBattleCode($playerBattle))
        {
            throw \App\Exceptions\GameException::make(
                'battle is already started: ' . $playerBattle->battle_code
            );
        }

        // バトルコード発行

        $battleCode	= self::_makeBattleCode($request->player_id);

        $playerBattle = new PlayerBattle();
        $playerBattle->player_id = $request->player_id;
        $playerBattle->battle_code = $battleCode;
        $playerBattle->quest_category = $request->quest_category;
        $playerBattle->chapter_id = $request->chapter_id;
        $playerBattle->quest_id = $request->quest_id;
        $playerBattle->repeat_count = $request->skip_count;
        $playerBattle->result = PlayerBattle::RESULT_SKIP;

		// プレイヤ取得

		$player = Player::find_($request->player_id);
        $playerLv0 = $player->player_lv; // 開始前のレベルを保存

        // スキップ時、プレイヤパーティ、キャラクターは無し

        /*
		$playerParty = PlayerParty::getByPartyNo(
            $request->player_id, $request->party_no
        );
		if (!isset($playerParty))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_party',
                ['player_id', 'party_no'],
                [$request->player_id, $request->party_no]
            );
		}

        // プレイヤパーティのキャラクターリスト取得

		$characterList = PlayerCharacter::getByPlayerParty(
            $playerParty
        );
		if (!isset($characterList))
		{
            throw \App\Exceptions\DataException::make(
                'player_party character not found'
            );
		}
        */

        $characterList = [];
        $grimoireList = [];

        // クエストマスタ取得

        $questService = QuestService::make($request->quest_category);

        $quest = $questService->getQuest(
            $request->chapter_id,
            $request->quest_id
        );

        $battleList = $questService->getBattleByQuest($quest);

        // クリア済み情報を得る

        $playerQuest = $questService->getPlayerQuest(
            $request->player_id,
            $request->chapter_id,
            $request->quest_id
        );

        // クリア済みかチェックする

        if (!QuestService::IsAllCleared_PQ($playerQuest))
        {
            throw \App\Exceptions\GameException::make(
                'quest is not all cleared'
            );
        }

        $skipCount = $request->skip_count;
        // スキップチケットチェック

        $skipTicket = PlayerItem::find_($request->skip_ticket_id);
        if (!isset($skipTicket))
        {
            throw \App\Exceptions\DataException::makeNotEnough(
                'player_item', 'item_id', Item::ID_SKIP_TICKET,
                $skipCount, 0
            );
        }
        if ($skipTicket->num < $skipCount)
        {
            throw \App\Exceptions\DataException::makeNotEnough(
                'player_item', 'item_id', Item::ID_SKIP_TICKET,
                $skipCount, $skipTicket->num
            );
        }

        // スキップチケットのカテゴリチェック

        $itemCategory = Item::categoryFromId($skipTicket->item_id);
        if ($itemCategory != Item::CATEGORY_SKIP_TICKET)
        {
            throw \App\Exceptions\ParamException::make(
                'specified skip_ticket_id\'s category is invalid: '
                . $skipTicket->item_id
            );
        }

        // バトルエネミーリストの計算

        $_battleList = BattleEnemyListResponse::make($battleList);

        // ドロップ報酬を計算する
        $bonusFlg = false;
        $bonusData = $questService->getQuestBonusData($request->chapter_id, $request->quest_category,Constants::BONUS_TYPE_DROP, $now);
        if($bonusData){
            $bonusFlg = true;
            $dailyCount = self::getDailyClearCount($request->player_id, $request->quest_category,$request->chapter_id,$now);
        }

        $skipQuestRewardList = [];
        for ($i = 0; $i < $skipCount; ++ $i)
        {
            $skipQuestReward = [];

            $skipQuestReward["fix_reward_list"] =
                $questService->calcFixReward($quest, $_battleList);
            $skipQuestReward["drop_reward_list"] =
                $questService->calcDropReward($quest, $_battleList);
            $skipQuestReward['bonus_reward_list'] = [];
            if($bonusFlg){
                $skipQuestReward['bonus_reward_list'] =
                    $questService->calcBonusReward($bonusData, $skipQuestReward['fix_reward_list'],$skipQuestReward['drop_reward_list'],$dailyCount);
                //スキップは足していかないといけない（戒め）
                $dailyCount++;
            }

            $skipQuestRewardList[] = $skipQuestReward;
        }

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

            // プレイヤバトル保存 (id 発行)

            $playerBattle->save();

            $takeOverList = []; // あふれたもの

            // バトル結果追加(経験値、p$)

            self::_giveQuestReward(
                $questService,
                $playerBattle,
                $player,
                $characterList,
                $grimoireList,
                $request
            );

            // 固定、ドロップ報酬登録 (履歴)

            self::_registSkipRewardItems(
                $questService, $playerBattle, $skipQuestRewardList
            );

            // 固定、ドロップ報酬取得

            self::_giveSkipRewardItems(
                $takeOverList, $playerBattle, $skipQuestRewardList
            );

            // あふれたアイテムをプレゼントボックスに入れる
            self::_presentRewardItems(
                $playerBattle, $takeOverList, false
            );

			// プレイヤチケット消費

            $skipTicket->num -= $skipCount;
            $skipTicket->save();

            // AL 消費
            $al = $quest->al * $skipCount;

            $player->useAl($now, $al, true);

            // クリア数を増加する
            $playerQuest->clear_count += $skipCount;

            // プレイヤクエスト更新
            $playerQuest->save();

            // ホームミッション更新
            $missionChecker->addData(
                'player', $player
            );
            $missionChecker->addData(
                'playerQuest', $playerQuest
            );
            $missionChecker->addData(
                'playerBattle', $playerBattle
            );

            // 絶対値になったため、クリアしたクエスト以外の判定も必要だろう //

            $playerQuests = PlayerQuest::getByPlayerId(
                $player->id, PlayerQuest::QUEST_CATEGORY_ALL, 0, $now
            );
            $missionChecker->addData(
                'playerQuests', $playerQuests
            );

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

        // プレイヤー情報の更新
		$player = Player::find_($request->player_id);

        if ($player->player_lv != $playerLv0) // プレイヤレベルが変わった
        {
            // 1 SQL なので、トランザクション不要
            // 失敗しても、さほど、問題無し
            PlayerCommonCache::updateCache($player);
        }

		$body = [
			'player' =>
                PlayerResponse::make($player),
			'ticket_num' =>
                $skipTicket->num,
			'skip_quest_reward_list' =>
                $skipQuestRewardList
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 中断
	 *
	 * @param BattleInterruptRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function interrupt($request)
	{
        $response = self::getResponse($request);

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

			// バトルコードチェック
			$playerBattle = PlayerBattle::getActive($request->player_id);

            // 継続中のバトルは無い
			if (!PlayerBattle::isValidBattleCode($playerBattle))
            {
                throw \App\Exceptions\DataException::make(
                    'active battle is none'
                );
            }

            /*
            // バトルコードがわからない場合もありえるので廃止
			if ($playerBattle->battle_code != $request->battle_code)
			{
                throw \App\Exceptions\DataException::makeNotFound(
                    'player_battle', ['player_id', 'battle_code'],
                    [$request->player_id, $request->battle_code]
                );
			}
			// バトルコード無効化
			$playerBattle->result = PlayerBattle::RESULT_TERMINATE;
			$playerBattle->save();
            */

            // 継続中のバトルを全て中断
            PlayerBattle::TerminateAll($request->player_id);

			BaseGameModel::commit();
		}

		return $response;
	}

	/**
	 * バトルコードを生成
	 *
	 * @param integer $playerId プレイヤID
	 * @return string バトルコード
	 */
	private static function _makeBattleCode($playerId)
	{
		$ymdhis = DateTimeUtil::getNOW('YmdHis');
		$battleCode = sha1(self::BATTLE_CODE_PREFIX . $playerId . $ymdhis);
		return $battleCode;
	}

	/**
	 * 経験値、p$を付与する
	 *
	 * @param QuestService $questService QuestService のインスタンス
	 * @param PlayerBattle $playerBattle PlayerBattle のインスタンス
	 * @param CharacterList $characterList パーティキャラクター
	 * @param Request $chapterId クエスト章
	 * @param integer $questId クエストID
	 * @param Request $request end() または skip() のレクエスト
	 */
	private static function _giveQuestReward(
        $questService, $playerBattle,
        &$player, &$characterList, $grimoireList,
        $request
    )
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();
        $dailyCount = self::getDailyClearCount($playerBattle->player_id, $playerBattle->quest_category,$playerBattle->chapter_id,$now);

        $skip = ($request instanceof \App\Http\Requests\BattleSkipRequest);

        $repeatCount = 1;
        if ($skip)
            $repeatCount = $request->skip_count;

        $quest = $questService->getQuest(
            $playerBattle->chapter_id, $playerBattle->quest_id
        );

        // パッシブスキル効果計算

        $effects = [];

        $MM = PlayerParty::MAX_POSITIONS;
        if (!$skip)
        {
            // ポジション毎にキャラクター、魔道書がある
            for ($i = 0; $i < $MM; ++ $i)
            {
                if (($request->dead_flags >> $i) & 1 != 0)
                    continue; // 死亡中のキャラクター、魔道書はスキップ

                $charaEffects = SkillService::calcCharacterEffects(
                    $characterList[$i]
                );
                SkillService::merge($effects, $charaEffects);

                $grimEffects = SkillService::calcGrimoireEffects(
                    $grimoireList[$i]
                );
                SkillService::merge($effects, $grimEffects);
            }
        }


		// プレイヤ経験値付与

        $tmpAddExp = $quest->player_exp;

        // クエストボーナス計算
        $bonusExpFlg = false;
        $bonusExpData = $questService->getQuestBonusData($playerBattle->chapter_id, $playerBattle->quest_category,Constants::BONUS_TYPE_EXP, $now);
        if($bonusExpData){
            $bonusExpFlg = true;
            $bonusExp = $tmpAddExp * ($bonusExpData->bonus_value / 1000);
            if (isset($effects[SkillEffect::TYPE_PLAYER_EXP_UP]))
            {
                $effects[SkillEffect::TYPE_PLAYER_EXP_UP]->applyUp($bonusExp);
            }
            $tmpDailyCount = $dailyCount;
        }

        if (isset($effects[SkillEffect::TYPE_PLAYER_EXP_UP]))
        {
            $effects[SkillEffect::TYPE_PLAYER_EXP_UP]->applyUp($tmpAddExp);
        }

        // スキップ回数の適用(ボーナス)
        $tmpExp = 0;
        for ($i=1; $i <= $repeatCount; $i++){
            $tmpExp += $tmpAddExp;
            if($bonusExpFlg){
                if($tmpDailyCount < $bonusExpData->limit || $bonusExpData->limit == 0){
                    $tmpExp += $bonusExp;
                    $tmpDailyCount++;
                }
            }
        }
        $addExp = $tmpExp;


        $newExp = $player->experience + $addExp;
        PlayerLevel::cutExperience($newExp); // 上限カット処理

        $diffExp = $newExp - $player->experience;
        $player->experience = $newExp;

		// プレイヤレベル更新

        if ($diffExp > 0)
        {
            // 経験値ログ保存
            LogService::playerExp(
                $player, $diffExp,
                SrcType::QUEST,
                $playerBattle->id
            );
        }

        $oldLv = $player->player_lv; // 更新前レベル

        $player->updateLevel($now, false); // レベル更新
        $player->save(); // レベルが更新されなくとも保存する

        $diffLv = $player->player_lv - $oldLv; // 増加量を計算

        if ($diffLv > 0)
        {
            // ログ保存
            LogService::playerLv(
                $player, $diffLv,
                SrcType::QUEST,
                $playerBattle->id
            );
        }

		// p$付与

        $tmpAddMoney = $quest->money;
        // P$ボーナス計算
        $bonusMoneyFlg = false;
        $bonusMoneyData = $questService->getQuestBonusData($playerBattle->chapter_id, $playerBattle->quest_category,Constants::BONUS_TYPE_MONEY, $now);
        if($bonusMoneyData){
            $bonusMoneyFlg = true;
            $bonusMoney = $tmpAddMoney * ($bonusMoneyData->bonus_value / 1000);
            if (isset($effects[SkillEffect::TYPE_MONEY_UP]))
            {
                $effects[SkillEffect::TYPE_MONEY_UP]->applyUp($bonusMoney);
            }
            $tmpDailyCount = $dailyCount;
        }

        if (isset($effects[SkillEffect::TYPE_MONEY_UP]))
        {
            $effects[SkillEffect::TYPE_MONEY_UP]->applyUp($tmpAddMoney);
        }

        // スキップ回数の適用
        $tmpMoney = 0;
        for ($i=1; $i <= $repeatCount; $i++){
            $tmpMoney += $tmpAddMoney;
            if($bonusMoneyFlg){
                if($tmpDailyCount < $bonusMoneyData->limit || $bonusMoneyData->limit == 0){
                    $tmpMoney += $bonusMoney;
                    $tmpDailyCount++;
                }
            }
        }
        $diffMoney = $tmpMoney;

        $param = new GiveOrPayParam();
        $param->playerId = $playerBattle->player_id;
        $param->itemId = Item::ID_P_DOLLAR;
        $param->count = $diffMoney;
        $param->srcType = SrcType::QUEST;
        $param->srcId = $playerBattle->id;
        PlayerService::giveOrPay($param);

        // キャラクター経験値付与

        if (!$skip)
        {
            foreach ($characterList as &$character)
            {
                if (empty($character))
                    continue;

                // キャラクター経験値計算

                $addCharaExp = $quest->character_exp;

                if (isset($effects[SkillEffect::TYPE_CHARA_EXP_UP])) {
                    $effects[SkillEffect::TYPE_CHARA_EXP_UP]->applyUp($addCharaExp);
                }
                // キャラ経験値ボーナス計算
                $bonusCharaExpData = $questService->getQuestBonusData($playerBattle->chapter_id, $playerBattle->quest_category,Constants::BONUS_TYPE_CHARA_EXP, $now);
                if($bonusCharaExpData){
                    $bonusCharaExp = $addCharaExp * ($bonusCharaExpData->bonus_value / 1000);
                    if (isset($effects[SkillEffect::TYPE_CHARA_EXP_UP])) {
                        $effects[SkillEffect::TYPE_CHARA_EXP_UP]->applyUp($bonusCharaExp);
                    }
                    if($dailyCount < $bonusCharaExpData->limit || $bonusCharaExpData->limit == 0){
                        $addCharaExp += $bonusCharaExp;
                    }
                }
                // !$skip なので不要 (必要になったときのために、書いてある)
                // $exp *= $repeatCount; // スキップ回数の適用

                // キャラクター経験値付与

                $character->experience += $addCharaExp;

                LogService::playerCharacterExp(
                    $character, $addCharaExp, SrcType::QUEST, $playerBattle->id
                );

                // キャラクターレベル更新

                $prevCharaLv = $character->character_lv;
                $character->updateLevel($player->player_lv);

                if ($character->character_lv != $prevCharaLv)
                {
                    LogService::playerCharacterLv(
                        $character,
                        $character->character_lv - $prevCharaLv,
                        SrcType::QUEST, $playerBattle->id
                    );
                }

                $character->save();
            }
        }
	}

	/**
	 * クエストの結果を付与
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $questId ステージID
	 * @param integer $questCategory クエスト種類
	 */
	private static function _giveRewardItems(
        &$takeOverList, &$rewardList,
        $questService, $playerBattle,
        $rewardType
    )
    {
        $_rewardList = $questService->getPlayerBattleReward(
            $playerBattle->id, $rewardType
        );

        $srcType = SrcType::fromQuestRewardType($rewardType);

        // クエスト報酬付与

        $rewardList = [];
        foreach ($_rewardList as $reward)
        {
            $param = new GiveOrPayParam();
            $param->playerId = $playerBattle->player_id;
            $param->itemType = $reward->item_type;
            $param->itemId = $reward->item_id;
            $param->count = $reward->item_num;
            $param->srcType = $srcType;
            $param->srcId = $playerBattle->id;
            $param->chapterId = $playerBattle->chapter_id;

            try
            {
                PlayerService::giveOrPay($param);
            }
            catch (\App\Exceptions\OverflowException $e)
            {
                // 欠片変換を考慮 //
                $takeOver = QuestRewardResponse::make(
                    $reward, $param->takeFlag
                );
                $takeOver['item_type'] = $e->itemType;
                $takeOver['item_id'] = $e->item;
                $takeOver['item_num'] = $e->count;
                $takeOver['take_flag'] = GiveOrPayParam::TAKE_FLAG_OVER;
                $takeOverList[]	= $takeOver;
            }

            $rewardList[] = QuestRewardResponse::make(
                $reward, $param->takeFlag
            );
        }
    }

	/**
	 * クエストミッションの報酬を付与
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $questId ステージID
	 * @param integer $questCategory クエスト種類
	 * @param object $playerBattle プレイヤバトル
	 */
	private static function _giveMissionItems(
        &$takeOverList, &$rewardList, $playerMissionList, $playerBattle
    )
    {
        // クエスト報酬付与

        $rewardList = [];
        foreach ($playerMissionList as $playerMission)
        {
            $param = new GiveOrPayParam();
            $param->playerId = $playerBattle->player_id;
            $param->itemType = $playerMission->remuneration_type;
            $param->itemId = $playerMission->remuneration;
            $param->count = $playerMission->remuneration_count;
            $param->srcType = SrcType::QUEST_MISSION;
            $param->srcId = $playerBattle->id;
            try
            {
                PlayerService::giveOrPay($param);
            }
            catch (\App\Exceptions\OverflowException $e)
            {
                $takeOver = QuestRewardResponse::makeByPlayerMission(
                    $playerMission, $param->takeFlag
                );
                $takeOver['item_type'] = $e->itemType;
                $takeOver['item_id'] = $e->item;
                $takeOver['item_num'] = $e->count;
                $takeOver['take_flag'] = GiveOrPayParam::TAKE_FLAG_OVER;
                $takeOverList[]	= $takeOver;
            }
            $rewardList[] = PlayerMissionResponse::make(
                $playerMission, $param->takeFlag
            );
        }
    }

	/**
	 * クエスト報酬を登録 (スキップ時)
	 *
	 * @param integer $playerId プレイヤID
	 * @param SkipQuestRewardList $skipQuestRewardList スキップ報酬一覧
	 * @param bool $result 成功 / 失敗
	 */
	private static function _registSkipRewardItems(
        $questService, $playerBattle, &$skipQuestRewardList
    )
    {
        // クエスト報酬付与

        $listNames = [
            QuestService::REWARD_TYPE_FIX => 'fix_reward_list',
            QuestService::REWARD_TYPE_DROP => 'drop_reward_list',
            QuestService::REWARD_TYPE_BONUS => 'bonus_reward_list',
        ];

        $skipCount = count($skipQuestRewardList);
        for ($i = 0; $i < $skipCount; ++ $i)
        {
            foreach ($listNames as $listType => $listName)
            {
                $questService->registPlayerBattleReward(
                    $playerBattle, $i + 1,
                    $listType,
                    $skipQuestRewardList[$i][$listName]
                );
            }
        }
    }

	/**
	 * クエスト報酬を付与 (スキップ時)
	 *
	 * @param object $playerBattle プレイヤバトル
	 * @param SkipQuestRewardList $skipQuestRewardList スキップ報酬一覧
	 * @param bool $result 成功 / 失敗
	 */
	private static function _giveSkipRewardItems(
        &$takeOverList, $playerBattle, &$skipQuestRewardList
    )
    {
        // クエスト報酬付与

        $listNames = [
            QuestService::REWARD_TYPE_FIX => 'fix_reward_list',
            QuestService::REWARD_TYPE_DROP => 'drop_reward_list',
            QuestService::REWARD_TYPE_BONUS => 'bonus_reward_list',
        ];

        foreach ($skipQuestRewardList as &$skipQuestReward)
        {
            foreach ($listNames as $listType => $listName)
            {
                $srcType = SrcType::fromQuestRewardType($listType);

                foreach ($skipQuestReward[$listName] as &$reward)
                {
                    $param = new GiveOrPayParam();
                    $param->playerId = $playerBattle->player_id;
                    $param->itemType = $reward['item_type'];
                    $param->itemId = $reward['item_id'];
                    $param->count = $reward['item_num'];
                    $param->srcType = $srcType;
                    $param->srcId = $playerBattle->id;
                    try
                    {
                        PlayerService::giveOrPay($param);
                    }
                    catch (\App\Exceptions\OverflowException $e)
                    {
                        // $takeOver = clone($reward);
                        $takeOver = $reward;
                        $takeOver['item_type'] = $e->itemType;
                        $takeOver['item_id'] = $e->item;
                        $takeOver['item_num'] = $e->count;
                        $takeOver['take_flag'] = GiveOrPayParam::TAKE_FLAG_OVER;
                        $takeOverList[]	= $takeOver;
                    }
                    $reward['take_flag'] = $param->takeFlag;
                }
            }
        }
    }

	/**
	 * あふれたクエスト報酬を集計して、プレゼントボックスに入れる
	 *
	 * @param object $playerBattle プレイヤクエストモデル
	 * @param array $takeOverList クエスト報酬一覧
	 * @param boolean $isMission ミッションの場合 true
	 */
	private static function _presentRewardItems(
        $playerBattle, $takeOverList, $isMission
    )
    {
        if (empty($takeOverList))
            return;

        // 集計処理 （集計するとはいってない）
        // 現状まとめる処理は不要な仕様とのことなので、コメントアウト
        // 必要になったら関数の中身も修正すること、処理がおかしいので
        //$sumItems = QuestRewardListResponse::makeSum($takeOverList);

        // 受け取り期限

        $expiredAt = PlayerPresent::defaultExpiredAt();

        // メッセージは決め打ちでよい

        if ($isMission)
            $msg = 'クエストミッション達成報酬です';
        else
            $msg = 'クエストドロップ報酬です';

        // プレゼントボックスに追加する

        foreach ($takeOverList as $item)
        {
            PlayerPresent::regist(
                $playerBattle->player_id,
                $item['item_type'],
                $item['item_id'],
                $item['item_num'],
                SrcType::QUEST,
                $playerBattle->id,
                $msg,
                $expiredAt
            );
        }
    }

    /**
     * デイリーのクリアカウント取得
     *
     * @param $playerId
     * @param $questCategory
     * @param $chapterId
     * @param $now
     * @return int
     */
    private static function getDailyClearCount(
        $playerId, $questCategory, $chapterId, $now
    )
    {
        $count = 0;
        $playerBattleList = PlayerBattle::getChapterDailyClearCount($playerId, $questCategory,$chapterId,$now);
        if($playerBattleList){
            foreach ($playerBattleList as $playerBattle){
                $count += $playerBattle->repeat_count;
            }
        }

        return $count;
    }

 }
