<?php
/**
 * イベントクエスト のサービス
 *
 */

namespace App\Services;

/**
 * イベントクエスト のサービス
 *
 */
class EventQuestService extends QuestService
{
    protected $questCategory = self::QUEST_CATEGORY_EVENT;
	protected $chapterModel = 'EventQuest';
	protected $questModel = 'EventQuestQuest';
	protected $battleModel = 'EventQuestBattle';
    
	protected $colNamePrefix = 'event_';
    protected $colNameFirstRewardId = 'first_reward_id';
    protected $colNameFixRewardId = 'fix_reward_id';
    protected $colNameDropRewardId = 'drop_reward_id';
}
