<?php
/**
 * SrcType アイテム等の増減の原因を表す
 *
 */

namespace App\Services;

use App\Models\MasterModels\Item;
use App\Utils\DebugUtil;

/**
 * アイテム等の増減の原因を表す
 *
 */
class SrcType
{
    // 基本 (取得系)
    const LOGIN_BONUS       =   1; // ログインボーナス (player_login_bonus)
    const QUEST             =   2; // クエスト汎用 (player_battle)
    const QUEST_FIRST       =   3; // クエスト初回 (player_battle)
    const QUEST_FIX         =   4; // クエスト固定 (player_battle)
    const QUEST_DROP        =   5; // クエストドロップ (player_battle)
    const QUEST_MISSION     =   6; // クエストミッション (player_battle)
    const MISSION           =   7; // ミッション (player_mission)
    const GACHA             =   8; // ガチャ (player_gacha)
    const SHOP              =   9; // 店で購入 (player_item_buy)
    const PRODUCT           =  10; // リアルマネーで購入 (player_product_buy)
    const DAILY_PACK        =  11; // デイリーパック付与 (player_daily_give)
    const PRESENT_TAKE      =  12; // プレゼントボックス (player_present)
    const INITIAL           =  13; // 初期設定
    const DIRECT            =  14; // 管理ツール付与 (player_direct_present)
    const LOGIN_POINT       =  15; // ログインポイント (player_login)
    const DEBUG             =  16; // デバッグ機能による付与
    const QUEST_BONUS       =  17; // クエストボーナス (player_battle)
    const GACHA_CEILING_EXCHANGE = 18; // ガチャポイント交換
    // キャラクター (消費系)
    const REINFORCE         = 111; // 強化に使用 (player_character_log)
    const SKILL_REINFORCE   = 112; // スキル強化に使用 (player_character_log)
    const ORB_EQUIP         = 113; // 装備して消費 (player_character_log)
    const GRADE_UP          = 114; // 装備して消費 (player_character_log)
    const EVOLVE            = 115; // 進化に使用 (player_character_log)
    // 魔道書 (消費系)
    const GRIMOIRE_SELL     = 121; // 魔道書売却 (player_log)
    const GRIMOIRE_AWAKE    = 122; // 魔道書覚醒 (player_grimoire_log)
    const GRIMOIRE_RELEASE  = 123; // 魔道書スロット開放 (player_grimoire_log)
    const GRIMOIRE_EQUIP    = 124; // 魔道書装備 (player_grimoire_log)
    // アイテム (消費系)
    const AL_RECOVER        = 131; // 使用して消費 (player_log)
    const GRIMOIRE_LIMIT_UP = 132; // 魔道書枠増加 (player_log)
    const LINEUP_UPDATE     = 133; // 店ラインナップ更新 (なし)
    const PURIFY            = 134; // 欠片パウダーに変換 (player_log)
    const ORB_SYNTHESIZE    = 135; // 合成して消費 (player_item_log)
    const SELL              = 136; // アイテム売却 (player_log)
    const CONTINUE          = 137; // コンティニュー (player_battle)


	/**
	 * クエスト報酬種別から、SrcType を計算する
	 *
	 * @param integer $rewardType クエスト報酬種別
	 * @return integer SrcType の値
	 */
    public static function fromQuestRewardType($rewardType)
    {
        switch ($rewardType)
        {
        case QuestService::REWARD_TYPE_FIRST:
            return self::QUEST_FIRST;
        case QuestService::REWARD_TYPE_FIX:
            return self::QUEST_FIX;
        case QuestService::REWARD_TYPE_DROP:
            return self::QUEST_DROP;
        case QuestService::REWARD_TYPE_BONUS:
            return self::QUEST_BONUS;
        }
        assert(false);
        return self::QUEST;
    }

}
