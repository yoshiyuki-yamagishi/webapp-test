<?php


namespace App\Services\KpiLogger;


use App\Models\PlayerParty;

class PlayerQuestGrimoireLogger extends BaseKpiLogger
{
    static $table_name = 'player_quest_grimoire_logger';

    public function addData($battleGrimoires, $grimoireList, $now)
    {

        foreach ($battleGrimoires as $key => $grimoire)
        {
            //グリモアのマスター配列。$battleGrimoiresと紐づいているため、null値チェックはしていない。
            $grim = $grimoireList[$key];
            $grimoire->master_grimoire_id = $grim['id'];

            $data = [
                'player_battle_id'=>$grimoire->player_battle_id,
                'position'=>$grimoire->position,
                'player_grimore_id'=>$grimoire->player_grimore_id,
                'grimoire_id'=>$grim->id,
                'awake'=>$grimoire->awake,
                // スロット装備は、リリース時に含まない
                'slot_1'=>$grimoire->slot_1 ?? null,
                'slot_2'=>$grimoire->slot_2 ?? null,
                'slot_3'=>$grimoire->slot_3 ?? null,
                'get_at'=>$now
            ];

            $this->register($data);
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    public function register($data)
    {
        return static::resistImpl($data);
    }
}
