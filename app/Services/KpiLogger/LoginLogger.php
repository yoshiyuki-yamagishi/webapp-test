<?php


namespace App\Services\KpiLogger;

/**
 * KPIログインログ出力クラス
 * Class LoginLogger
 * @package App\Services\KpiLogger
 */
class LoginLogger extends BaseKpiLogger
{
    static $table_name = 'login_history';
    /**
     * ログイン履歴を配列に整形して登録
     * @param $playerId
     * @param $playerLv
     * @param $now
     * @return bool
     */
    public function register($playerId, $playerLv, $now)
    {
        $data = [
                'player_id' => $playerId,
                'player_lv' => $playerLv,
                'get_at'=>$now
                ];

        return static::resistImpl($data);
    }
}
