<?php


namespace App\Services\KpiLogger;


class PlayerQuestCharacterLogger extends BaseKpiLogger
{
    static $table_name = 'player_quest_character_logger';

    public function addData($playerBattleId, $battleCharacters, $now)
    {

        //キャラクターリストとプレイヤーバトルキャラクターのポジションが一致するものを保存していく
        for ($i = 0; $i <= PlayerParty::MAX_POSITIONS; ++$i)
        {
            $player_character = $battleCharacters[$i];

            //ポジションが一致しなければcontinue
            if($player_character->position !== ($i + 1))
                continue;

            for ($j = 0; $j < PlayerCharacter::MAX_ORB_EQUIP; ++ $j)
            {
                $prop = 'orb_id_' . ($j + 1);
                if (empty($player_character->$prop))
                    continue;

                $data[$prop] = $player_character;
            }

            $data = [
                'player_battle_id' => $playerBattleId,
                'position' => $player_character->position,
                'character_id' => $player_character->character_id,
                'evolve' => $player_character->evolve,
                'character_lv' => $player_character->character_lv,
                'active_skill_1_lv' =>  $player_character->active_skill_1_lv,
                'active_skill_2_lv' =>  $player_character->active_skill_2_lv,
                'got_at' => $now
            ];
            $this->register($data);
        }
    }

    //登録する
    public function register($data)
    {
        return static::resistImpl($data);
    }

}
