<?php


namespace App\Services\KpiLogger;

/**
 * ガチャ結果の出力クラス
 * Class GachaLogger
 * @package App\Services\KpiLogger
 */
class GachaLogger extends BaseKpiLogger
{
    static $table_name = 'gacha_logger';

    /**
     * ガチャ結果を配列に整形して保存。DBに保存されているデータと同じもの。
     * @param $playerId
     * @param $playerLv
     * @param $playerGacha
     * @param $gachaId
     * @param $gachaCount
     * @param $now
     * @return bool
     */
    public function register($playerId, $playerLv, $playerGacha, $gachaId, $gachaCount, $now)
    {
        $data = [
            'player_id'=>$playerId,
            'player_lv'=>$playerLv,
            'player_gacha_id'=>$playerGacha->id,
            'gacha_id'=>$gachaId,
            'gacha_count'=>$gachaCount,
            'gacha_loop'=>$playerGacha->gacha_loop,
            'loop_count'=>$playerGacha->loop_count,
            'get_at'=>$now
        ];

       return static::resistImpl($data);
    }
}
