<?php


namespace App\Services\KpiLogger;


class MissionEndLogger extends BaseKpiLogger
{
    static $table_name = 'mission_end_logger';

    public function register($playerMission, $now)
    {
        $data = [
            'player_id'=>$playerMission->player_id,
            'mission_id'=>$playerMission->mission_id,
            'mission_type'=>$playerMission->mission_type,
            'progress_count'=>$playerMission->progress_count,
            'count'=>$playerMission->count,
            'start_day'=>$playerMission->start_day,
            'end_day'=>$playerMission->end_day,
            'report_flag'=>$playerMission->report_flag,
            'taked_at'=>$playerMission->taked_at,
            'get_at'=>$now
        ];


        return static::resistImpl($data);
    }
}
