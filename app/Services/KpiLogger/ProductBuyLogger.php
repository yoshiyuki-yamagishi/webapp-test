<?php


namespace App\Services\KpiLogger;

/**
 * 課金情報の出力クラス
 * Class ProductBuyLogger
 * @package App\Services\KpiLogger
 */
class ProductBuyLogger extends BaseKpiLogger
{
    static $table_name = 'product_buy';

    /**
     * 課金情報を配列に整形し、保存
     * @param $playerId
     * @param $playerLv
     * @param $playerBuy
     * @param $now
     * @return bool
     */
    public function register($playerId, $playerLv, $playerBuy, $now)
    {
        $data = [
            'player_id'=>$playerId,
            'player_lv'=>$playerLv,
            'store_id'=>$playerBuy->store_id,
            'product_id'=>$playerBuy->product_id,
            'num'=>$playerBuy->num,
            'currency'=>$playerBuy->currency,
            'amount'=>$playerBuy->amount,
            'get_at'=>$now
        ];

        return static::resistImpl($data);
    }
}
