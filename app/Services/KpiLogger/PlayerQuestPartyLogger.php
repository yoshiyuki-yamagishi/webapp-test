<?php


namespace App\Services\KpiLogger;


class PlayerQuestPartyLogger extends BaseKpiLogger
{
    static $table_name = 'player_quest_party_logger';
    public function register($playerId, $playerBattleId, $playerParty, $now)
    {

        $data = [
            'player_id'=>$playerId,
            'player_battle_id'=>$playerBattleId,
            'player_character_id_1'=>$playerParty->player_character_id_1,
            'player_character_id_2'=>$playerParty->player_character_id_2,
            'player_character_id_3'=>$playerParty->player_character_id_3,
            'player_character_id_4'=>$playerParty->player_character_id_4,
            'player_character_id_5'=>$playerParty->player_character_id_5,
            'player_character_id_6'=>$playerParty->player_character_id_6,
            'player_character_id_7'=>$playerParty->player_character_id_7,
            'player_character_id_8'=>$playerParty->player_character_id_8,
            'player_grimoire_id_1'=>$playerParty->player_grimoire_id_1,
            'player_grimoire_id_2'=>$playerParty->player_grimoire_id_2,
            'player_grimoire_id_3'=>$playerParty->player_grimoire_id_3,
            'player_grimoire_id_4'=>$playerParty->player_grimoire_id_4,
            'player_grimoire_id_5'=>$playerParty->player_grimoire_id_5,
            'player_grimoire_id_6'=>$playerParty->player_grimoire_id_6,
            'player_grimoire_id_7'=>$playerParty->player_grimoire_id_7,
            'player_grimoire_id_8'=>$playerParty->player_grimoire_id_8,
            'get_at'=>$now
        ];

        return static::resistImpl($data);
    }
}
