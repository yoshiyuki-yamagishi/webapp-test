<?php


namespace App\Services\KpiLogger;


class QuestLogger extends BaseKpiLogger
{
    static $table_name = 'quest_logger';
    private $data = [];

    public function addData($player, $playerBattle, $result, $missionList, $playerQuest, $playerBattleResult, $now)
    {
        $missionData = $this->getMissionData($missionList);


        $data = [
            'player_id'=>$player->player_id,
            'player_lv'=>$player->player_lv,
            'player_battle_id'=>$playerBattle->id,
            'quest_id'=>$playerBattle->quest_id,
            'continue_count'=>$playerBattle->continue_count,
            'result'=>$result,
            'mission_id_1'=>$missionData->missionId1,
            'mission_id_2'=>$missionData->missionId2,
            'mission_id_3'=>$missionData->missionId3,
            'mission_flag_1'=>$playerQuest->mission_flag_1,
            'mission_flag_2'=>$playerQuest->mission_flag_2,
            'mission_flag_3'=>$playerQuest->mission_flag_3,
            'total_damage_1'=>$playerBattleResult->total_damage_1,
            'total_damage_2'=>$playerBattleResult->total_damage_2,
            'total_damage_3'=>$playerBattleResult->total_damage_3,
            'total_receive_damage_1'=>$playerBattleResult->total_receive_damage_1,
            'total_receive_damage_2'=>$playerBattleResult->total_receive_damage_3,
            'total_receive_damage_3'=>$playerBattleResult->total_receive_damage_2,
            'turn_count_1'=>$playerBattleResult->turn_count_1,
            'turn_count_2'=>$playerBattleResult->turn_count_2,
            'turn_count_3'=>$playerBattleResult->turn_count_3,
            'get_at'=>$now
        ];

        return $this->register($data);
    }

    public function getMissionData($missionList)
    {
        $returnData = new \stdClass();
        $returnData->missionId1 = $returnData->missionId2 = $returnData->missionId3 = $count = 0;
        //  クエストミッションIDの取得
        foreach ($missionList as $mission)
        {
            if($mission->mission_type === Mission::TYPE_QUEST)
            {
                $count++;
                $returnData->missionId{$count} = $mission->mission_id;
            }
            //  一つのクエストにつきミッションは３つまで
            if($count > 3)
            {
                break;
            }
        }
        return $returnData;
    }

    public function register($data)
    {
        return static::resistImpl($data);
    }


}
