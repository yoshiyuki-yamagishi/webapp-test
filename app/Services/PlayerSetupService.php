<?php
/**
 * プレイヤ初期データー登録サービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Models\Player;
use App\Models\PlayerCharacter;
use App\Models\PlayerData;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerParty;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\PlayerInitialSettings;
use App\Models\MasterModels\PlayerLevel;
use App\Utils\DebugUtil;

/**
 * プレイヤ初期データー登録サービス
 *
 */
class PlayerSetupService extends BaseService
{
    // 初期キャラクター //

    public static $INI_CHARAS = [
        [
            'character_id' => 0,
            'evolve' => 0,
            'lv' => 1,
            'exp' => 0,
        ],
        [
            'character_id' => 100,
            'evolve' => 0, // 初期レア2
            'lv' => 1,
            'exp' => 0,
        ],
        [
            'character_id' => 0,
            'evolve' => 0,
            'lv' => 1,
            'exp' => 0,
        ],
        [
            'character_id' => 0,
            'evolve' => 0,
            'lv' => 1,
            'exp' => 0,
        ],
        [
            'character_id' => 0,
            'evolve' => 0,
            'lv' => 1,
            'exp' => 0,
        ],
        [
            'character_id' => 4800,
            'evolve' => 0,
            'lv' => 1,
            'exp' => 0,
        ],
    ];

    public $playerInitialSettings = null;
    public $playerId = null;
    public $player = null;

    function __construct($playerId) {
        $this->playerId = $playerId;
    }

	public function setup()
	{
        // プレイヤ登録
        self::_setupPlayer();

        // プレイヤデータ登録
        PlayerData::regist($this->playerId);

        // キャラクター登録
        self::_setupCharacter();

        // マスタから、全魔道書追加
        self::_setupGrimoire();

        // マスタから、アイテム追加
        self::_setupItem();

        // 蒼の結晶を追加
        self::_setupBlueCrystal();

        // 初期パーティ登録
        self::_setupParty();
    }

	private function _setupPlayer()
	{
        $response = ApiResponse::getInstance();
        $now = $response->currentDateDB();

        $this->playerInitialSettings = PlayerInitialSettings::getOne_(1);
        $iniPs = $this->playerInitialSettings;
        // DebugUtil::e_log('PSS', 'iniPs', $iniPs);


        $playerLevel = PlayerLevel::getOne($iniPs->player_lv);
        if (empty($playerLevel))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'player_level', 'lv', $iniPs->player_lv
            );
        }

        $player = new Player();

		$player->id = $this->playerId;
        $player->player_name = $iniPs->player_name;
        $player->player_lv = $iniPs->player_lv;
        $player->experience = $iniPs->player_experience;

        $birthNo = $iniPs->birthday;
        // 簡易テスト
        // $birthNo = 229;
        $player->birthday = PlayerInitialSettings::getBirthDb($birthNo);

        // $player->legal_birthday
        $player->gender = $iniPs->gender;
        $player->tutorial_progress = 0;
        $player->tutorial_flag = 0;
        $player->platinum_dollar = $iniPs->owned_platinum_dollar;
        $player->powder = $iniPs->owned_powder;
        $player->magic_num = $iniPs->owned_magic_num;
        $player->friend_point = $iniPs->owned_friend_point;
        $player->al = $iniPs->owned_al;
        $player->max_al = $playerLevel->max_al;
        $player->al_recovery_at = $now;
        $player->max_grimoire = $iniPs->max_grimoire;
        $player->message = $iniPs->message;
        // $player->login_days
        $player->first_login_at = $now;
        $player->last_login_at = $now;
        // $player->created_at
        // $player->updated_at

		$player->save();

        $this->player = $player;

        // プロパティ (サーバーには保存しない予定)
        // $iniPs->image_quality
        // $iniPs->flame_rate
        // $iniPs->bgm_volume
        // $iniPs->se_volume
        // $iniPs->voice_volume
    }

	private function _setupCharacter()
	{
        // TODO: テスト段階のため、
        // プレイヤー初期設定の:
        // owned_chara,
        // は、反映不可

        // $charaList = CharacterBase::getAll();
        // DebugUtil::e_log('PSS', 'charaList', $charaList);

        // 暫定的に初期パーティのキャラ追加


        // お気に入りキャラ
        // マスタではイラスト ID になってるので / 10

        $favCharaId = $this->playerInitialSettings->favorite_chara / 10;
        // DebugUtil::e_log('PSS', 'favCharaId', $favCharaId);

        $favChara = null;
        foreach (self::$INI_CHARAS as $iniChara)
        {
            $charaId = $iniChara['character_id'];
            if ($charaId <= 0)
                continue;

            $evolve = $iniChara['evolve'];
            $lv = $iniChara['lv'];
            $exp = $iniChara['exp'];

            $pc = PlayerCharacter::regist(
                $this->playerId, $charaId, $evolve, $lv, $exp
            );
            if ($charaId == $favCharaId)
                $favChara = $pc;
        }

        // 初期お気に入りがあれば設定する
        if (isset($favChara))
        {
            // DebugUtil::e_log('PSS', 'favChara', $favChara);
            $player = Player::find_($this->playerId);
            $player->favorite_character = $favChara->id;
            $player->save();
        }
    }

	private function _setupGrimoire()
	{
        $grimoireList = Grimoire::getAll();
        foreach ($grimoireList as $grimoire)
        {
            PlayerGrimoire::regist(
                $this->playerId, $grimoire->id
            );
        }
    }

	private function _setupItem()
	{
        $itemList = Item::getAll();
        $itemCount = 100;
        $pieceCount = 500;
        foreach ($itemList as $item)
        {
            $itemCat = Item::categoryFromId($item->id);
            if (!PlayerItem::isTargetCategory($itemCat))
                continue;

            $count = $itemCount;
            if ($itemCat == Item::CATEGORY_FRAGMENT)
                $count = $pieceCount;

            $param = new GiveOrPayParam();
            $param->playerId = $this->playerId;
            $param->itemId = $item->id;
            $param->count = $count;
            $param->srcType = SrcType::INITIAL;
            PlayerItemService::giveOrPay($param);
        }
    }

	private function _setupBlueCrystal()
	{
        // TODO: テスト段階のため、$iniPs->owned_blue_crystal
        // は無効

        // TODO: 現在のところ、取得元不明で登録
        $srcType = 0;
        $srcId = 0;

        PlayerBlueCrystal::regist(
            $this->playerId, 0, $srcType, $srcId, 10000, null
        );
        PlayerBlueCrystal::regist(
            $this->playerId, 1, $srcType, $srcId, 10000, null
        );
        PlayerBlueCrystal::regist(
            $this->playerId, 2, $srcType, $srcId, 10000, null
        );
    }

	private function _setupParty()
	{
        $pcs = [];
        $iniCharaCount = count(self::$INI_CHARAS);
        for ($i = 0; $i < 6; ++ $i)
        {
            $c = 0;
            if ($i < $iniCharaCount)
                $c = self::$INI_CHARAS[$i]['character_id'];

            if ($c != 0)
            {
                $pc = PlayerCharacter::getOne($this->playerId, $c);
                $pcs[] = $pc->id;
            }
            else
            {
                $pcs[] = null;
            }
        }

        PlayerParty::regist(
            $this->playerId, 1,
            $pcs[0],
            $pcs[1],
            $pcs[2],
            $pcs[3],
            $pcs[4],
            $pcs[5]
        );
    }

}
