<?php
    /**
     * プレイヤクエスト のサービス
     *
     */

    namespace App\Services;

    use App\Config\Constants;
    use App\Http\Requests\EventTopRequest;
    use App\Http\Responses\ApiResponse;
    use App\Http\Responses\EventMarathonResponse;
    use App\Http\Responses\PlayerQuestResponse;
    use App\Models\BaseGameModel;
    use App\Models\MasterModels\EventQuest;
    use App\Models\MasterModels\EventQuestQuest;
    use App\Models\MasterModels\Item;
    use App\Models\Player;
    use App\Models\PlayerEventItem;
    use App\Models\PlayerEventStatus;
    use App\Models\PlayerQuest;
    use App\Services\MissionChecker\MissionCheckerEventTop;
    use App\Utils\DebugUtil;

    /**
     * プレイヤクエスト のサービス
     *
     */
    class EventService extends BaseService
    {

        /**
         * 一覧
         *
         * @param EventTopRequest $request
         * @return ApiResponse レスポンス
         */
        public static function top($request)
        {
            $response = self::getResponse($request);
            $now = $response->currentDateDB();

            //イベントデータ取得（取得できなかった場合エラー）
            $eventData = EventQuest::getOne_($request->event_id);

            //開催確認（念のため&マスター取得で開催確認取れないので）
            $eventStatus = self::checkEventStatus($eventData, $now);

            //typeでイベント種類分けて別の関数に飛ばす
            switch ($eventData->type) {
                case Constants::EVENT_TYPE_MARATHON_BOSS:
                case Constants::EVENT_TYPE_MARATHON_NORMAL:
                    $responseEventDataList = self::getMarathonPlayerData($request, $eventData, $now);
                    $body = EventMarathonResponse::make($responseEventDataList,$eventStatus);
                    break;
                default:
                    //基本的にはエラーで返していいはず、存在しないタイプはゆるされない(戒め)
                    throw \App\Exceptions\ParamException::make('存在しないイベントタイプです' . $eventData->type);
                    break;
            }
            $response->body = $body;

            return $response;
        }


        /**
         * 開催ステータス取得
         * @param $eventData
         * @param $now
         * @return int
         */
        public static function checkEventStatus($eventData, $now){
            $_now = strtotime($now);
            //開催確認（念のため&マスターから開催確認取れないので）
            if ($_now < strtotime($eventData->start_day)) {
                $eventStatus = Constants::EVENT_STATUS_OPEN_START;
            } elseif ($_now < strtotime($eventData->end_day)) {
                $eventStatus = Constants::EVENT_STATUS_START_END;
            } elseif ($_now < strtotime($eventData->close_day)) {
                $eventStatus = Constants::EVENT_STATUS_END_CLOSE;
            }else{
                $eventStatus = Constants::EVENT_STATUS_CLOSE;
            }

            return $eventStatus;
        }


        /**
         * マラソンイベントデータ取得
         * @param $request
         * @param $eventData
         */
        public static function getMarathonPlayerData($request, $eventData, $now)
        {
            $introductionId = "";
            $eventQuestList = EventQuestQuest::getByChapterId($eventData->id);
            $playerEventQuestList = PlayerQuest::getByPlayerIdAndChapterId($request->player_id, $eventData->id);
            $playerEventStatus = PlayerEventStatus::getOneByPlayerIdAndEventId($request->player_id, $eventData->id);
            //イベントミッション用準備
            $player = Player::find_($request->player_id);
            $missionChecker = MissionCheckerEventTop::getInstance();
            $missionChecker->initEvent($request->player_id, $now);
            $missionChecker->addData(
                'player', $player
            );
            $questClearPercent = 0;
            $achiveMissionList = [];

            {
                BaseGameModel::beginTransaction();
            //イベントデータない時は導入シナリオチェック必要
            if (!$playerEventStatus) {
                //導入シナリオあるかどうか?
                foreach ($eventQuestList as $eventQuest) {
                    if ($eventQuest->priority == Constants::INTRO_STORY){
                        $introductionId      =  $eventQuest->id;
                    }
                }
                //データ新規作成
                $playerEventStatus = PlayerEventStatus::insert($request->player_id, $eventData->id);
            }else{
                //クリアパーセント計算
                if($playerEventStatus->clear_percent != 100 && $playerEventQuestList) {
                    $questClearPercent = floor(count($playerEventQuestList) / count($eventQuestList) * 100);
                    if ($playerEventStatus->clear_percent < $questClearPercent) {
                        PlayerEventStatus::updateClearPercent($playerEventStatus->id, $questClearPercent);
                    }
                }
            }


                //イベントミッションチェック
                $missionChecker->addData('clear_percent',$questClearPercent);
                $missionChecker->updateMission($achiveMissionList, false);

                BaseGameModel::commit();
            }

            //イベントアイテム所持情報取得
            $eventItemDataList = PlayerEventItem::getListByPlayerIdAndEventId($request->player_id, $eventData->id);
            if($eventItemDataList->isEmpty()){
                $itemIdList = explode(",",$eventData->item1);
                //イベントアイテムが存在してない場合もある
                $eventItemDataList = [];
                if($itemIdList){
                    foreach ($itemIdList as $itemId){
                        //アイテム存在チェック、一応入れとく
                        $itemData = [];
                        $itemData = Item::getOne($itemId);
                        if(empty($itemData)){
                            throw \App\Exceptions\MasterException::makeNotFound(
                                'Item', 'id', $itemId
                            );
                        }
                        $eventItemDataList[] = PlayerEventItem::insert($request->player_id, $eventData->id, $itemId);
                    }
                }
            }

            $playerEventStatus->introduction_id = $introductionId;
            $playerEventStatus->event_item_dataList = $eventItemDataList;

            $response = [
                    'player_quest_list' => $playerEventQuestList ,
                    'player_event_status' => $playerEventStatus
            ];

            return $response;
        }

        /**
         * クリアしたプレイヤーのクエストリスト
         * @param $request
         * @return mixed
         */
        public static function playerQuestList($request){

            $response = self::getResponse($request);
            $now = $response->currentDateDB();
            //イベントデータ取得（取得できなかった場合エラー）
            $eventData = EventQuest::getOne_($request->event_id);

            //開催確認
            $eventStatus = self::checkEventStatus($eventData, $now);
            $playerQuestList = [];
            $playerEventQuestList = PlayerQuest::getByPlayerIdAndChapterId($request->player_id, $request->event_id);
            foreach ($playerEventQuestList as $playerQuest){
                $playerQuestList[] = PlayerQuestResponse::make($playerQuest,0);
            }
            $body = [
                'player_quest_list' => $playerQuestList,
                'event_status' => $eventStatus
            ];

            $response->body = $body;

            return $response;
        }

    }