<?php
    /**
     * ミッション - イベントアイテム・ポイント系チェッカー
     *
     */

    namespace App\Services\MissionChecker;

    use App\Models\MasterModels\Mission;
    use App\Models\PlayerEventItem;
    use App\Utils\DebugUtil;

    /**
     * ミッション - ショップ購入チェッカー  クラス
     *
     */
    class EventCountChecker extends BaseMissionChecker
    {
        /**
         * ミッション達成状況を更新する
         *
         * @param Mission $mission ミッション
         * @param PlayerMission $playerMission プレイヤミッション
         * @param boolean $updated true:更新した,false: 更新してない
         * @return boolean true:処理終了,false:処理続行
         */
        public function check($mission, &$playerMission, &$updated)
        {
            switch ($mission->mission_success_type) {
                case Mission::ST_EVENT_QUEST_EVENT_ITEM_COUNT:

                    $mscs = $this->data("mscs");
                    if (!$mscs) {
                        throw \App\Exceptions\MasterException::make(
                            'ミッション種類' . $mission->mission_success_type . 'ミッションID' . $mission->id . 'のクリアアイテムの指定がないよ'
                        );
                    } else {
                        if (count($mscs) > 1) {
                            throw \App\Exceptions\MasterException::make(
                                'ミッション種類' . $mission->mission_success_type . 'ミッションID' . $mission->id . 'のアイテムの指定が多いよ1つにしてね'
                            );
                        }
                    }
                    $countTargetId = $mscs[0];
                    $player = $this->date('player');
                    $playerQuest = $this->date('playerQuest');
                    $playerEventItemData = PlayerEventItem::getOne($player->id, $playerQuest->chapter_id, $countTargetId);

                    if ($this->setAchived($mission, $playerMission, $playerEventItemData->total_num)) {
                        $updated = true;
                    }
                    break;


                default:
                    return false;
            }

            return false; // 何もしない
        }
    }
