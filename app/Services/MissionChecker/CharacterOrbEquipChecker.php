<?php
/**
 * ミッション - キャラクターオーブ装備チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerCharacter;
use App\Utils\DebugUtil;

/**
 * ミッション - キャラクターオーブ装備チェッカー クラス
 *
 */
class CharacterOrbEquipChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type
            != Mission::ST_CHARACTER_ORB_EQUIP)
            return false;

        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 今のところ、オーブの指定はできない //
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') can\'t use mission_success_contents id: '
                . $mission->id
            );
        }

        $orbEquipCount = $this->data('orbEquipCount');

        if ($orbEquipCount <= 0)
            return false;

        if ($this->setAchived($mission, $playerMission, $orbEquipCount))
            $updated = true;
        
        // DebugUtil::e_log('COEC', 'mission', $mission);
        // DebugUtil::e_log('COEC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
