<?php
/**
 * ミッション - バトルクリアチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerParty;
use App\Utils\DebugUtil;

/**
 * ミッション - バトルクリアチェッカー クラス
 *
 */
class BattleClearChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_CLEAR:
            break;
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        return false; // 何もしない
	}
    
}
