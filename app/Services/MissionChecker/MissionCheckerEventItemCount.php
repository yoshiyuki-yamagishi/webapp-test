<?php
    /**
     * ミッションチェッカーのクラス (イベントアイテム・ポイント用)
     *
     *
     */

    namespace App\Services\MissionChecker;


    /**
     * ミッションチェッカーのクラス (イベントアイテム・ポイント用)
     *
     */
    class MissionCheckerEventItemCount extends MissionChecker
    {
        // シングルトン //

        private static $singleton;

        public static function getInstance()
        {
            if (!isset(self::$singleton)) {
                self::$singleton = new MissionCheckerEventItemCount();
            }
            return self::$singleton;
        }

        // 実装 //

        private function __construct()
        {
            // チェッカーリストを用意する
            $this->checkerList[] = new EventCountChecker($this);

            // ミッションは最後
            $this->checkerList[] = new MissionClearChecker($this);
        }

    }
