<?php
/**
 * ミッション - バトル死亡チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - バトル死亡チェッカー クラス
 *
 */
class BattleDeadChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_NOT_DEAD:
            break;
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        $deadCount = 1; // ディフォルト
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 複数回数の指定は無し
            
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            
            $deadCount = $mscs[0];
        }

        // DebugUtil::e_log('BDC', 'count', $playerBattleResult->deadCount());
        // DebugUtil::e_log('BDC', 'threshold', $deadCount);

        if ($playerBattleResult->deadCount() >= $deadCount)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        return false; // 何もしない
	}
}
