<?php
/**
 * ミッション - ログイン日数チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - ログイン日数チェッカー クラス
 *
 */
class LoginDaysChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_LOGIN_DAYS:
            break;
        default:
            return false;
        }
        
        $player = $this->data('player');
        
        $loginDays = 1; // ディフォルト
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 複数回数の指定は無し
            
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            
            $loginDays = $mscs[0];
        }

        $_loginDays = $this->data('loginDays');
        if ($_loginDays < $loginDays)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        return false; // 何もしない
	}
    
}
