<?php
/**
 * ミッションチェッカーの統合用クラス (キャラクタースキル強化用)
 *
 */

namespace App\Services\MissionChecker;
use App\Utils\DebugUtil;

/**
 * ミッションチェッカーのの統合用クラス (キャラクタースキル強化用)
 *
 */
class MissionCheckerCharacterSkillReinforce extends MissionChecker
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionCheckerCharacterSkillReinforce();
        }
        return self::$singleton;
    }

    // 実装 //
   
    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new CharacterSkillReinforceChecker($this);

        // ミッションは最後
        $this->checkerList[] = new MissionClearChecker($this);
    }

}
