<?php
/**
 * ミッション - バトルスキル未使用チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - バトルスキル未使用チェッカー クラス
 *
 */
class BattleNoSkillChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_NO_ACTIVE_SKILL)
            return false;
        if (!$this->isWin())
            return false;

        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        if ($playerBattleResult->used_skill_count != 0)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('BNSC', 'mission', $mission);
        // DebugUtil::e_log('BNSC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
