<?php
    /**
     * ミッション - イベントクリア達成チェッカー
     *
     */

    namespace App\Services\MissionChecker;

    use App\Models\MasterModels\Mission;
    use App\Models\PlayerEventItem;
    use App\Utils\DebugUtil;

    /**
     * ミッション - イベントクリア達成率  クラス
     *
     */
    class EventClearPercentChecker extends BaseMissionChecker
    {
        /**
         * ミッション達成状況を更新する
         *
         * @param Mission $mission ミッション
         * @param PlayerMission $playerMission プレイヤミッション
         * @param boolean $updated true:更新した,false: 更新してない
         * @return boolean true:処理終了,false:処理続行
         */
        public function check($mission, &$playerMission, &$updated)
        {
            switch ($mission->mission_success_type) {
                case Mission::ST_EVENT_QUEST_CLEAR_PERCENT:

                    $nowPercent = $this->date('clear_percent');
                    if ($this->setAchived($mission, $playerMission, $nowPercent)) {
                        $updated = true;
                    }
                    break;

                default:
                    return false;
            }

            return false; // 何もしない
        }
    }
