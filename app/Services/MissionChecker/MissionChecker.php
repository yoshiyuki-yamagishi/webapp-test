<?php
/**
 * ミッションチェッカーの統合用クラス
 *
 */

namespace App\Services\MissionChecker;
use App\Config\Constants;
use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Mission;
use App\Models\PlayerMission;
use App\Services\KpiLogger\MissionEndLogger;
use App\Services\KpiLogger\MissionStartLogger;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;

/**
 * ミッションチェッカーのの統合用クラス
 *
 */
class MissionChecker extends BaseMissionChecker
{
    // シングルトン //

    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionChecker();
        }
        return self::$singleton;
    }

    // 実装 //

    public $now = null;
    public $data = [];
    public $missionList = [];
    public $playerMissionList = [];
    public $achivedIds = []; // 今回達成ミッションIDの配列
        
	protected $checkerList = [];

    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new PlayerLevelChecker($this);
    }

    public function init($playerId, $now)
    {
        $this->now = $now;
        $this->missionList = $this->prepareMissions($now);
        $this->playerMissionList = $this->preparePlayerMissions(
            $playerId, $now
        );
    }

    public function initEvent($playerId, $now)
    {
        $this->now = $now;
        $this->missionList = $this->prepareEventMissions($now);
        $this->playerMissionList = $this->preparePlayerEventMissions(
            $playerId, $now
        );

    }

    public function prepareMissions($now)
    {
        return Mission::getDailyNormal($now);
    }

    /*
     * イベントミッション取得
     */
    public function prepareEventMissions($now)
    {
        return Mission::getEventMissionList($now);
    }

    public function preparePlayerMissions($playerId, $now)
    {
        return PlayerMission::getByPlayerId(
            $playerId, $now
        );
    }

    public function preparePlayerEventMissions($playerId, $now)
    {
        return PlayerMission::getByPlayerIdAndMissionType(
            $playerId, $now, Constants::MISSION_TYPE_EVENT
        );
    }

    public function addData($key, $data)
    {
        $this->data[$key] = $data;
    }

    public function addMissionData($mission, $playerMission)
    {
        // パース済みの mission_success_contents を用意する //
        {
            $msc = trim($mission->mission_success_contents);
            $this->addData('msc', $msc);

            $mscs = StrUtil::decodeSimpleCsv($msc);
            $this->addData('mscs', $mscs);

        }

        // パース済みの release_trigger を用意する //
        {
            $trigger = $mission->release_trigger;
            $triggers = StrUtil::decodeSimpleCsv($trigger);

            // TODO: 現在は、-1 が指定してあるが、空にしてもらえば不要な処理
            if (count($triggers) == 1 && $triggers[0] <= 0)
                $triggers = [];

            $this->addData('triggers', $triggers);
        }

    }

    public function updateMission(&$achiveMissionList, $report)
    {
        foreach ($this->missionList as $mission)
        {
            $playerMission = $this->playerMissionList->where(
                'mission_id', $mission->id
            )->first();

            // 達成済みチェック

            if ($this->checkAchived($mission, $playerMission))
                continue;

            $isPm = isset($playerMission);

            // ミッション毎のデータを設定

            $this->addMissionData($mission, $playerMission);

            // トリガー条件

            if (!$isPm)
            {
                $released = false;
                if (!static::matchTriggerImpl(
                    $mission,
                    $this->data['triggers'],
                    $this->achivedIds,
                    $this->playerMissionList,
                    true,
                    $released
                ))
                {
                    continue;
                }
            }

            $updated = false;
            $this->check(
                $mission, $playerMission, $updated
            );

            if (!$updated)
                continue;

            // 達成したか？

            $achived = ($playerMission->progress_count >= $mission->count);

            // 今回達成リストに追加する

            if ($achived)
            {
                $achiveMissionList[] = $playerMission;
                //------------------------------
                // KPIログ登録
                //------------------------------
/*                $endLogger = new MissionEndLogger();
                $endLogger->register($playerMission, $this->now);*/
            }

            // もともと行が無かった場合、リストに追加する

            if (!$isPm)
            {
                $this->playerMissionList[] = $playerMission;
                //------------------------------
                // KPIログ登録
                //------------------------------
/*                $startLogger = new MissionStartLogger();
                $startLogger->register($playerMission, $this->now);*/
            }

            if ($achived && $report)
                $playerMission->setReported();

            // DB 更新 //

            if ($achived)
            {
                $playerMission->cleared_at = $this->now;
                $this->achivedIds[] = $playerMission->mission_id;
            }

            $playerMission->save();
        }
    }

    /**
	 * 達成済みで未報告の有効なミッション一覧を取得する
	 *
	 * @return array プレイヤミッションの配列
	 */
    public function unreportedMissions($playerId, $now)
    {
        return PlayerMission::getUnreported($playerId, $now);
    }

    /**
	 * 報告済みにする
	 *
	 * @param array $playerMissionList プレイヤミッションの配列
	 */
    public function setReported($playerMissionList)
    {
        foreach ($playerMissionList as $playerMission)
        {
            $playerMission->setReported();
            $playerMission->save();
        }
    }

	/**
	 * トリガー条件をチェックする
	 *
	 * @param integer $clearType クリアタイプ
	 * @param array $triggers トリガー条件となる、クエスト ID の配列
	 * @param array $playerMissionList プレイヤミッションの配列
	 * @param boolean $checkReported 報告済みかの判定を行う場合 true
	 * @param boolean $released true: 今回開放された
	 * @return boolean true: トリガー条件を満たす
	 */
	public static function matchTriggerImpl(
        $mission, $triggers, $achivedIds,
        $playerMissionList, $checkReported, &$released
    )
    {
        $released = false;

        foreach ($triggers as $trigger)
        {
            if ($trigger <= 0)
                continue;

            $playerMission = $playerMissionList->where(
                'mission_id', $trigger
            )->first();

            if (empty($playerMission))
                return false; // 進捗してない
            if (!$playerMission->cleared())
                return false; // 進捗しているが未クリア

            if ($checkReported)
            {
                // 達成判定
                if ($mission->clear_type == Mission::CLEAR_TYPE_REPORT)
                {
                    if (!$playerMission->reported())
                        return false; // クリア済みだが、未報告
                }
                else if (Mission::isCountType($mission))
                {
                    // カウントの場合、開放直後のミッションはカウントしない
                    if (in_array($playerMission->mission_id, $achivedIds))
                        return false;
                }
            }
            else
            {
                // 報告時
                if (!$playerMission->reported())
                    $released = true; // 今回、開放されたミッション
            }
        }

        return true;
    }

    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        foreach ($this->checkerList as $checker)
        {
            if ($checker->check($mission, $playerMission, $updated))
                break;
        }
	}

}
