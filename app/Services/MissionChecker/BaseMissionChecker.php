<?php
/**
 * ミッションチェッカーの基底クラス
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerMission;
use App\Models\PlayerQuestMission;
use App\Models\PlayerBattle;
use App\Models\PlayerBattleResult;


/**
 * ミッションチェッカーの基底クラス
 *
 */
class BaseMissionChecker
{
    public $rootChecker = null;

    public function __construct($rootChecker)
    {
        $this->rootChecker = $rootChecker;
    }

    public function data($key)
    {
        if (!array_key_exists($key, $this->rootChecker->data))
            return null;
        return $this->rootChecker->data[$key];
    }

    public function isWin()
    {
        $result = $this->data('playerBattleResult');
        if (!isset($result))
            return false;
        return $result->result == PlayerBattle::RESULT_WIN;
    }

    /**
	 * ミッション達成の基本チエック
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @return boolean true:達成済み,false:未達成
	 */
	public function checkAchived($mission, $playerMission)
	{
        if (!isset($playerMission))
            return false;
        if ($playerMission->progress_count < $mission->count)
            return false;
        return true;
    }

    /**
	 * ミッション達成済み状態を 1 進める
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @return 更新された場合、true
	 */
	public function incAchived($mission, &$playerMission, $inc = 1)
	{
        $count = 0;
        if (isset($playerMission))
            $count = $playerMission->progress_count;

        $count += $inc;
        return $this->setAchived($mission, $playerMission, $count);
    }

    /**
	 * ミッション達成済み状態を変更する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @return 更新された場合、true
	 */
	public function setAchived($mission, &$playerMission, $count)
	{
        if ($count <= 0)
            return false;

        if ($count > $mission->count)
            $count = $mission->count;

        if (!isset($playerMission))
        {
            if (Mission::isQuestLike($mission))
            {
                $playerMission = new PlayerQuestMission();
            }
            else
            {
                $playerMission = new PlayerMission();
                $playerMission->player_id = $this->data('player')->id;
            }

            $playerMission->mission_id = $mission->id;
            $playerMission->mission_type = $mission->mission_type;
            $playerMission->count = $mission->count;
            $playerMission->remuneration_type = $mission->remuneration_item_type;
            $playerMission->remuneration = $mission->mission_remuneration;
            $playerMission->remuneration_count = $mission->mission_remuneration_count;
            $playerMission->start_day = $mission->start_day;
            $playerMission->end_day = $mission->end_day;
        }
        else if ($playerMission->progress_count == $count)
        {
            return false;
        }

        $playerMission->progress_count = $count;
        return true;
    }

}
