<?php
/**
 * ミッション - ガチャ実行チェッカー
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - ガチャ実行チェッカー クラス
 *
 */
class GachaExecChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_GACHA_EXEC)
            return false;

        $playerGacha = $this->data("playerGacha");
        if (empty($playerGacha))
            return false;
        if (empty($playerGacha->taked_at))
            return false;

        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 今のところ、ガチャ 1 つだけ指定可能 //
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }

            if ($playerGacha->gacha_id != $mscs[0])
                return false;
        }

        // リクエストで指定の値

        $gacha = $this->data('gacha');

        $reqCount = $playerGacha->gacha_count; // 指定の数
        $realCount = Gacha::calcRealCount($gacha, $reqCount); // 実際引ける数
        // DebugUtil::e_log('GEC', 'reqCount', $reqCount);
        // DebugUtil::e_log('GEC', 'realCount', $realCount);

        if ($this->incAchived($mission, $playerMission, $realCount))
            $updated = true;

        // DebugUtil::e_log('GEC', 'mission', $mission);
        // DebugUtil::e_log('GEC', 'playerMission', $playerMission);
        return false; // 何もしない
	}

}
