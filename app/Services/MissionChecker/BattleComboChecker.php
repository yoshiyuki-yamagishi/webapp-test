<?php
/**
 * ミッション - バトルコンボチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - バトルコンボチェッカー クラス
 *
 */
class BattleComboChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        $code = "";
        switch ($mission->mission_success_type)
        {
        case Mission::ST_COMBO_COUNT:
            $code = "combo";
            break;
            // チェインはまだ無い
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        $comboCount = 1; // ディフォルト
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 複数回数の指定は無し
            
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            
            $comboCount = $mscs[0];
        }

        $prop = 'max_' . $code . '_count';
        if ($playerBattleResult->$prop < $comboCount)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('BCC', 'mission', $mission);
        // DebugUtil::e_log('BCC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
