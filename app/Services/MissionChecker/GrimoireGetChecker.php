<?php
/**
 * ミッション - 魔道書取得チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Mission;
use App\Models\PlayerGrimoire;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * ミッション - 魔道書取得チェッカー クラス
 *
 */
class GrimoireGetChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_GRIMOIRE_GET)
            return false;

        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 今のところ、魔道書の指定はできない //
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') can\'t use mission_success_contents id: '
                . $mission->id
            );
        }

        $player = $this->data("player");
        
        $dayStart = null;
        if (Mission::isDaily($mission))
        {
            $now = ApiResponse::getInstance()->currentDateDB();
            $dayStartDate = DateTimeUtil::dailyMissionStartDate($now);
            $dayStart = DateTimeUtil::formatDB($dayStartDate);
        }
        
        $grimCount = PlayerGrimoire::countAcquired(
            $player->id, true, $dayStart
        );
        if ($grimCount < 1)
            return false;
        
        // DebugUtil::e_log('GGC', 'grimCount', $grimCount);
        if ($this->setAchived($mission, $playerMission, $grimCount))
            $updated = true;
        
        // DebugUtil::e_log('GGC', 'mission', $mission);
        // DebugUtil::e_log('GGC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
