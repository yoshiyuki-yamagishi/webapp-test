<?php
/**
 * ミッション - バトルパーティチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerParty;
use App\Utils\DebugUtil;

/**
 * ミッション - バトルパーティチェッカー クラス
 *
 */
class BattlePartyChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_FEW_CLEAR:
            break;
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        $playerParty = $this->data('playerParty');
        if (empty($playerParty))
            return false; // スキップ

        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        $charaCount = 1; // ディフォルト
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 複数回数の指定は無し
            
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            
            $charaCount = $mscs[0];
        }

        // DebugUtil::e_log('PPC', 'count', $playerParty->getCharacterCount());
        // DebugUtil::e_log('PPC', 'threshold', $charaCount);

        if ($playerParty->getCharacterCount() > $charaCount)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        return false; // 何もしない
	}
}
