<?php
    /**
     * ミッションチェッカーの統合用クラス (イベントアイテム・ポイント用)
     *
     *
     */

    namespace App\Services\MissionChecker;


    /**
     * ミッションチェッカーのの統合用クラス (イベントアイテム・ポイント用)
     *
     */
    class MissionCheckerEventTop extends MissionChecker
    {
        // シングルトン //

        private static $singleton;

        public static function getInstance()
        {
            if (!isset(self::$singleton)) {
                self::$singleton = new MissionCheckerEventTop();
            }
            return self::$singleton;
        }

        // 実装 //

        private function __construct()
        {
            // チェッカーリストを用意する
            $this->checkerList[] = new EventClearPercentChecker($this);

            // ミッションは最後
            $this->checkerList[] = new MissionClearChecker($this);
        }

    }
