<?php
    /**
     * ミッション - クエストクリアチェッカー
     *
     */

    namespace App\Services\MissionChecker;

    use App\Config\Constants;
    use App\Models\MasterModels\EventQuestQuest;
    use App\Models\MasterModels\Mission;
    use App\Models\PlayerBattle;
    use App\Services\QuestService;
    use App\Utils\DebugUtil;

    /**
     * ミッション - クエストクリアチェッカー クラス
     *
     */
//TODO:リファクタリング対象、頭のカテゴリーわける部分からsuccessタイプで分岐する形のがわかりやすいし、まとめやすい
    class QuestClearChecker extends BaseMissionChecker
    {
        /**
         * ミッション達成状況を更新する
         *
         * @param Mission $mission ミッション
         * @param PlayerMission $playerMission プレイヤミッション
         * @param boolean $updated true:更新した,false: 更新してない
         * @return boolean true:処理終了,false:処理続行
         */
        public function check($mission, &$playerMission, &$updated)
        {
            $questCat = QuestService::QUEST_CATEGORY_ALL;
            switch ($mission->mission_success_type) {
                case Mission::ST_QUEST_CLEAR:
                    break;
                case Mission::ST_STORY_QUEST_CLEAR:
                    $questCat = QuestService::QUEST_CATEGORY_STORY;
                    break;
                case Mission::ST_CHARACTER_QUEST_CLEAR:
                    $questCat = QuestService::QUEST_CATEGORY_CHARACTER;
                    break;
                case Mission::ST_EVENT_QUEST_CLEAR_COUNT_ONE:
                case Mission::ST_EVENT_QUEST_CLEAR_COUNT_ALL:
                    $questCat = Constants::QUEST_CATEGORY_EVENT;
                    break;
                default:
                    return false;
            }

            // クリアしたかどうかの判定

            $playerBattle = $this->data('playerBattle');
            if (isset($playerBattle)) {
                switch ($playerBattle->result) {
                    case PlayerBattle::RESULT_WIN:
                    case PlayerBattle::RESULT_SKIP:
                        break;
                    default:
                        return false;
                }
            }

            $questId = 0;
            $mscs = $this->data("mscs");
            if ($mscs) {
                // 今のところ、クエスト 1 つだけ指定可能  ←拡張性は持たせておきましょう
                if (count($mscs) > 1) {
                    throw \App\Exceptions\MasterException::make(
                        'mission_success_type ('
                        . $mission->mission_success_type
                        . ') can\'t use multiple mission_success_contents id: '
                        . $mission->id
                    );
                }
                $questId = $mscs[0];
            }


            // クエストの指定が無い場合は、
            // 戦闘のないクエストはクリア対象としない
            if ($questId <= 0 && !isset($playerBattle)) {
                return false;
            } // 戦闘の無いクエスト

            $playerQuest = $this->data('playerQuest');
            assert(!empty($playerQuest));

            switch ($mission->mission_type) {
                case Constants::MISSION_TYPE_DAILY:
                    if ($questCat != QuestService::QUEST_CATEGORY_ALL &&
                        $playerQuest->quest_category != $questCat) {
                        return false;
                    } // 指定のクエストカテゴリと違う

                    if ($questId > 0 &&
                        $playerQuest->quest_id != $questId) {
                        return false;
                    } // 指定のクエスト ID と違う

                    $repeat = 1;
                    if (isset($playerBattle)) {
                        $repeat = $playerBattle->repeat_count;
                    }

                    if ($this->incAchived($mission, $playerMission, $repeat)) {
                        $updated = true;
                    }
                    break;

                case Constants::MISSION_TYPE_EVENT:
                    if ($questCat != QuestService::QUEST_CATEGORY_EVENT &&
                        $playerQuest->quest_category != $questCat) {
                        return false;
                    } // 指定のクエストカテゴリと違う

                    $repeat = 1;
                    if ($playerBattle) {
                        $repeat = $playerBattle->repeat_count;
                    }

                    //switch in switch がなんとなくいやなので一旦if分岐
                    if ($mission->mission_success_type == Mission::ST_EVENT_QUEST_CLEAR_COUNT_ALL) {
                        if ($questId > 0 &&
                            $playerQuest->chapter_id != $questId) {
                            return false;
                        } // 指定のイベント と違う

                        $eventQuestList = EventQuestQuest::getByChapterId($questId);
                        if (!$eventQuestList) {
                            throw \App\Exceptions\MasterException::make(
                                'イベントID' . $questId . 'のクエストリストがないよ'
                            );
                        }
                        foreach ($eventQuestList as $eventQuestData) {
                            if ($playerBattle->quest_id == $eventQuestData->id) {
                                if ($this->incAchived($mission, $playerMission, $repeat)) {
                                    $updated = true;
                                }
                            }
                        }
                    }

                    if($mission->mission_success_type == Mission::ST_EVENT_QUEST_CLEAR_COUNT_ONE){
                        if ($questId > 0 &&
                            $playerQuest->quest_id != $questId) {
                            return false;
                        } // 指定のイベント と違う
                        if($playerQuest->quest_id != $questId) {
                            if ($this->incAchived($mission, $playerMission, $repeat)) {
                                $updated = true;
                            }
                        }
                    }

                    break;

                default:
                    // クリア回数を取得する方式
                    if ($questCat == QuestService::QUEST_CATEGORY_ALL) {
                        // カテゴリを指定する必要がある
                        throw \App\Exceptions\MasterException::make(
                            'mission_success_type ('
                            . $mission->mission_success_type
                            . ') must be daily id: '
                            . $mission->id
                        );
                    }
                    if ($questId <= 0) {
                        // クエストを指定する必要がある
                        throw \App\Exceptions\MasterException::make(
                            'mission_success_type ('
                            . $mission->mission_success_type
                            . ') must need mission_success_contents id: '
                            . $mission->id
                        );
                    }

                    $playerQuests = $this->data('playerQuests');

                    $clearCount = 0;
                    foreach ($playerQuests as $playerQuest) {
                        if ($playerQuest->quest_category == $questCat &&
                            $playerQuest->quest_id == $questId) {
                            $clearCount = $playerQuest->clear_count;
                            break;
                        }
                    }

                    if ($clearCount <= 0) {
                        return false;
                    }

                    if ($this->setAchived($mission, $playerMission, $clearCount)) {
                        $updated = true;
                    }
            }

            return false; // 何もしない
        }
    }
