<?php
/**
 * ミッション - パーティ編成チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerParty;
use App\Services\QuestService;
use App\Utils\DebugUtil;

/**
 * ミッション - パーティ編成チェッカー クラス
 *
 */
class PartyUpdateChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_PARTY_UPDATE:
            break;
        default:
            return false;
        }
        
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') can\'t use mission_success_contents id: '
                . $mission->id
            );
        }

        $player = $this->data("player");
        $partyCount = PlayerParty::countParty($player->id);
        if ($partyCount < 1)
            return false;

        // DebugUtil::e_log('PUC', 'partyCount', $partyCount);
        if ($this->setAchived($mission, $playerMission, $partyCount))
            $updated = true;
        
        // DebugUtil::e_log('PUC', 'mission', $mission);
        // DebugUtil::e_log('PUC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
