<?php
/**
 * ミッションチェッカーの統合用クラス (ガチャ用)
 *
 */

namespace App\Services\MissionChecker;
use App\Utils\DebugUtil;

/**
 * ミッションチェッカーのの統合用クラス (ガチャ用)
 *
 */
class MissionCheckerGacha extends MissionChecker
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionCheckerGacha();
        }
        return self::$singleton;
    }

    // 実装 //
   
    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new GachaExecChecker($this);
        $this->checkerList[] = new CharacterGetChecker($this);
        $this->checkerList[] = new GrimoireGetChecker($this);

        // ミッションは最後
        $this->checkerList[] = new MissionClearChecker($this);
    }

}
