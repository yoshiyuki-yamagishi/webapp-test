<?php
/**
 * ミッション - ショップ購入チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - ショップ購入チェッカー  クラス
 *
 */
class ShopBuyChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_SHOP_BUY:
            break;
        default:
            return false;
        }
        
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') can\'t use mission_success_contents id: '
                . $mission->id
            );
        }

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('SBC', 'mission', $mission);
        // DebugUtil::e_log('SBC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
