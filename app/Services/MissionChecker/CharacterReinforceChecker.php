<?php
/**
 * ミッション - キャラクター強化チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerCharacter;
use App\Utils\DebugUtil;

/**
 * ミッション - キャラクター強化チェッカー クラス
 *
 */
class CharacterReinforceChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_CHARACTER_REINFORCE)
            return false;

        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 今のところ、キャラクターの指定はできない //
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') can\'t use mission_success_contents id: '
                . $mission->id
            );
        }

        // 強化しただけで達成でよい (レベルアップしなくてもよい)

        $player = $this->data("player");

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('CGC', 'mission', $mission);
        // DebugUtil::e_log('CGC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
