<?php
/**
 * ミッション - プレイヤーレベルチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - プレイヤーレベルチェッカー クラス
 *
 */
class PlayerLevelChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_PLAYER_LV)
            return false;

        $player = $this->data("player");
        if (empty($player))
            return false;
        if ($player->player_lv < $mission->mission_success_contents)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('PLC', 'mission', $mission);
        // DebugUtil::e_log('PLC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
