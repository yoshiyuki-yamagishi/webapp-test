<?php
/**
 * ショップ のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerGrimoireListResponse;
use App\Http\Responses\PlayerItemListResponse;
use App\Http\Responses\PlayerResponse;
use App\Http\Responses\ShopListResponse;
use App\Http\Responses\ShopItemListResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\PlayerItemBuy;
use App\Models\PlayerShopItem;
use App\Models\MasterModels\Shop;
use App\Models\MasterModels\ShopExchange;
use App\Models\MasterModels\Item;
use App\Services\KpiLogger\ItemBuyLogger;
use App\Services\MissionChecker\MissionCheckerShopBuy;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * ショップ のサービス
 *
 */
class ShopService extends BaseService
{
	/**
	 * ショップ一覧
	 *
	 * @param ShopListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        $shopList = Shop::getByCategory($now, $request->shop_category);
		// DebugUtil::e_log('SS', 'shopList', $shopList);

		$body = [
			'shop_list' => ShopListResponse::make($shopList)
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 	ショップラインナップ更新
	 *
	 * @param ShopLineupUpdateRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function lineupUpdate($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

		// ショップを1件セレクト

		$shop = Shop::getOne($request->shop_id);
		if (!isset($shop))
		{
            throw \App\Exceptions\MasterException::makeNotFound(
                'shop', 'shop_id', $request->shop_id
            );
		}

        // プレイヤショップアイテムをセレクト

        $shopItems = PlayerShopItem::getByShopId(
            $request->player_id, $request->shop_id
        );

        // 手動更新の場合、アイテムが足りているかチェックする

        if ($request->payItem())
        {
            PlayerService::canPay(
                $request->player_id, Item::TYPE_ITEM,
                $shop->reset_item_id, $shop->reset_item_count
            );
        }

        // 更新するかどうかを判定する

        $update = self::_checkLineupUpdate($request, $now, $shop, $shopItems);

        if ($update)
        {
			BaseGameModel::beginTransaction();

            // 更新処理

            PlayerShopItem::updateLineup(
                $request->player_id, $shop, $now
            );

            $shopItems = PlayerShopItem::getByShopId(
                $request->player_id, $request->shop_id
            );
            assert(count($shopItems) > 0);

            // アイテム消費

            if ($request->payItem())
            {
                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemType = Item::TYPE_ITEM;
                $param->itemId = $shop->reset_item_id;
                $param->count = - $shop->reset_item_count;
                $param->srcType = SrcType::LINEUP_UPDATE;

                PlayerService::giveOrPay($param);
            }

			BaseGameModel::commit();
        }

        $payItemCount = 0;
        if ($request->payItem())
            $payItemCount = PlayerService::getCount($param);

		$body = [
			'shop_item_list' =>
                ShopItemListResponse::make($shopItems),
            'pay_item_count' =>
                $payItemCount,
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * ショップ購入・交換
	 *
	 * @param ShopBuyRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function buy($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerShopBuy::getInstance();
        $missionChecker->init($request->player_id, $now);

        // プレイヤショップアイテム取得

        $shopItem = PlayerShopItem::find_(
            $request->player_shop_item_id
        );

        // ショップ交換所取得

		$shopExchange = ShopExchange::getOne_(
            $shopItem->shop_exchange_id
		);

        // ショップ取得

        $shop = Shop::getOne_($shopExchange->shop_id);

        // 購入数のチェック

        if ($shopExchange->limit > 0 &&
            $shopItem->buy_count >= $shopExchange->limit)
        {
            throw \App\Exceptions\GameException::make(
                'already buy limit: ' .
                $shopItem->buy_count . '/' . $shopExchange->limit
            );
        }

        $itemIds = [];
        $playerGrimoireIds = [];

		{
			BaseGameModel::beginTransaction();

			// プレイヤアイテム購入に行を追加

			$itemBuy = PlayerItemBuy::regist($shopItem, $shop, $shopExchange);

			// プレイヤアイテム付与

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemType = $shopExchange->lineup_category;
            $param->itemId = $shopExchange->lineup_id;
            $param->count = $shopExchange->get_count;
            $param->srcType = SrcType::SHOP;
            $param->srcId = $itemBuy->id;

            PlayerService::giveOrPay($param);

            // プレイヤ魔道書 ID を追加

            $playerGrimoireIds = array_merge(
                $playerGrimoireIds, $param->playerGrimoireIds
            );

            // アイテム ID を追加

            if (PlayerItem::isTargetItemId($shopExchange->lineup_id))
                $itemIds[] = $shopExchange->lineup_id;

			// プレイヤの消費アイテムを減らす

            $_param = new GiveOrPayParam();
            $_param->playerId = $request->player_id;
            $_param->itemType = Item::TYPE_ITEM;
            $_param->itemId = $shopExchange->pay_item_contents_id;
            $_param->count = - $shopExchange->pay_item_count;
            $_param->srcType = SrcType::SHOP;
            $_param->srcId = $itemBuy->id;

			PlayerService::giveOrPay($_param);

            // 購入数を増やす

            ++ $shopItem->buy_count;
            $shopItem->save();

            // レスポンスリストに追加

            if (PlayerItem::isTargetItemId($shopExchange->pay_item_contents_id))
                $itemIds[] = $shopExchange->pay_item_contents_id;

            // プレイヤ取得

            $player = Player::find_($request->player_id);

            // ミッション達成チェック

            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerShopItem', $shopItem);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

		// プレイヤアイテム取得

		$playerItemList = PlayerItem::getByItemIds(
            $request->player_id, $itemIds
        );

		// プレイヤ魔道書取得 (取得したばかりなので、装備はしてない)

		$playerGrimoireList = PlayerGrimoire::getByIds($playerGrimoireIds);

        //------------------------------
        // KPIログ登録
        //------------------------------
        //$logger = new ItemBuyLogger();
        //$logger->register($request->player_id, $player->player_lv, $shop, $shopExchange, $now);

		// レスポンス計算

		$body = [
			'player' =>
                PlayerResponse::make($player),
			'player_item_list' =>
                PlayerItemListResponse::make($playerItemList),
			'player_grimoire_list' =>
                PlayerGrimoireListResponse::make($playerGrimoireList, false),
		];
		$response->body = $body;

		return $response;
	}

    private static function _checkLineupUpdate(
        $request, $now, $shop, $shopItems
    )
    {
        if (count($shopItems) <= 0)
            return true;
        if ($request->payItem())
            return true;

		// reset_time1～3の中から現在時刻より前で最近のものを取得

        $_now = new \DateTime($now);
		$oneDay = \DateInterval::createFromDateString("1 day");

		$lastResetTime = new \DateTime("1900-01-01 00:00:00");
		for ($i = 1; $i <= 3; ++ $i)
		{
			$colName = "reset_time" . $i;
			$times = explode(":", $shop->$colName);
			if (count($times) != 3)
			{
				continue;
			}

			$date = clone $_now;
			$date->setTime($times[0], $times[1], $times[2]);

			if ($date > $_now)
			{
				$date->sub($oneDay);
			}

			if ($date > $lastResetTime)
			{
				$lastResetTime = $date;
			}
		}

        assert($lastResetTime <= $_now);

        // 前回の更新日付

        $lastUpdate = new \DateTime($shopItems[0]->created_at);

        // 最後の更新日付をはさんでいる場合、更新する

        if ($lastUpdate < $lastResetTime)
            return true;

        return false;
    }

}
