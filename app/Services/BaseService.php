<?php
/**
 * サービスの基底クラス
 *
 */

namespace App\Services;



use App\Http\Responses\ApiResponse;
use App\Models\PlayerCommon;
use App\Utils\DebugUtil;

/**
 * サービスの基底クラス
 *
 */
class BaseService
{
    /**
     * 各サービスの最初で実行し、Responseのベースを取得
     *
     * @param $request
     * @return mixed
     */
    public static function getResponse($request)
    {
        $response = ApiResponse::getInstance();

        // プラットフォーム設定
        $response->setPlatform($request->getPlatform());

        // 強制アップデート判定
        $response->checkAppUpdate($request->app_version);

        if (isset($request->player_id)) {
            // uniqueIdのチェック
            $response->checkUniqueId($request);

            // Ban判定
            $response->checkBan($request->player_id);
        }

        return $response;
    }

}
