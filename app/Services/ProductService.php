<?php
/**
 * 製品 のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerResponse;
use App\Http\Responses\PlayerItemListResponse;
use App\Http\Responses\ProductListResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerItem;
use App\Models\PlayerPresent;
use App\Models\PlayerProductBuy;
use App\Models\PlayerProductBuyItem;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ProductShop;
use App\Models\MasterModels\ProductContents;
use App\Services\KpiLogger\ProductBuyLogger;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use App\Utils\MathUtil;

/**
 * 製品 のサービス
 *
 */
class ProductService extends BaseService
{
    public static $APPLE_VERIFY_URLS = [
        // "https://buy.itunes.apple.com/verifyReceipt",
        "https://sandbox.itunes.apple.com/verifyReceipt",
    ];

    public static $GOOGLE_PUB_KEY = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhZLogBW2ON7IN7zsHVP/sz0BVS51KMu9RcHPxRERsNG1/YxQ/zATYP9wm1JZpsnWVDXcmdwJEKlxnWvVpIpvbdKspqTM/tCOmHjqybEGu8v+mLuuCLvkXyTaWTpvAaARl/BktTLyu4BF0sfa1frE3Po4KpFZECVfx5QM4NwJMkJBFNPNL+W+sQ3S08sDKhX1UruXynHodBVwcGYGc3tSHdi7jWISPqOoKlNjNWGFfgLuPTto3Q3EJrsTjTOlc8rC8yUIuoUGMt6Vgm6n2/ormnp8kRwNlsktlbr+G7KJ4VXspVtRBGdfOa8g/Ec0pVoUv7qTloGsQFcAnUNAL2s7DwIDAQAB';

    const REMAIN_FLAG_NO = 0;
    const REMAIN_FLAG_YES = 1;

	/**
	 * 一覧
	 *
	 * @param ProductListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        $products = ProductShop::getAll($now, $request->category);
        // DebugUtil::e_log('PS_list', 'products', $products);

        $monthlyAmount = PlayerProductBuy::getAmountMonth(
            $request->player_id, $now
        );

		$body = [
            'product_list' => ProductListResponse::make(
                $products, $request->player_id, $now
            ),
            'monthly_amount' => $monthlyAmount
        ];
		$response->body = $body;

        // DebugUtil::e_log('PS_list', 'response', $products);
		return $response;
    }

	/**
	 * 購入
	 *
	 * @param ProductBuyRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function buy($request)
	{
        // DebugUtil::e_log('PS_buy', 'request', $request->all());
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        $product = ProductShop::getOne_($request->product_id);
        // DebugUtil::e_log('PS_buy', 'product', $product);

        // 購入個数のチェック //

        if ($product->limit > 0)
        {
            // 購入数のチェック
            $buyCount = PlayerProductBuy::getNumDay(
                $request->player_id, $product->id, $now
            );
            // DebugUtil::e_log('PS_buy', 'buyCount', $buyCount);

            if ($buyCount + $request->num > $product->limit)
            {
                throw \App\Exceptions\GameException::make(
                    'buy count exceed limit: ' .
                    $buyCount . ' + ' . $request->num . ' > ' . $product->limit
                );
            }
        }

        // DB 更新 //

        {
            $playerBuy = new PlayerProductBuy();
            $playerBuy->player_id = $request->player_id;
            $playerBuy->store_id = $request->store_id;
            $playerBuy->payment_id = null;
            $playerBuy->product_id = $request->product_id;
            $playerBuy->daily_flg = MathUtil::nTo0($product->daily_flg);
            $playerBuy->valid_period = MathUtil::nTo0($product->valid_period);
            $playerBuy->num = $request->num;
            $playerBuy->currency = $request->currency;
            $playerBuy->amount = $request->amount;
            $playerBuy->state = PlayerProductBuy::STATE_INITIAL;
            $playerBuy->save();
        }

        //------------------------------
        // KPIログ登録
        //------------------------------
/*        $player = Player::find_($request->player_id);
        $logger = new ProductBuyLogger();
        $logger->register($request->player_id, $player->player_lv, $playerBuy, $now);*/

		$body = [
            'player_buy_id' => $playerBuy->id,
        ];
		$response->body = $body;

		return $response;
    }

	/**
	 * 登録
	 *
	 * @param ProductRegistRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function regist($request)
	{
        // DebugUtil::e_log('PS_regist', 'request', $request->all());
        return static::_registImpl($request);
	}

	/**
	 * レジューム
	 *
	 * @param ProductResumeRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function resume($request)
	{
        // DebugUtil::e_log('PS_resume', 'request', $request->all());
        return static::_registImpl($request);
    }

	/**
	 * キャンセル
	 *
	 * @param ProductCancelRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function cancel($request)
	{
        // DebugUtil::e_log('PS_cancel', 'request', $request->all());
        $response = self::getResponse($request);
        $playerId = $request->player_id;

        $playerBuy = PlayerProductBuy::find($request->player_buy_id);
        if (empty($playerBuy))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_product_buy', 'id', $request->player_buy_id
            );
        }

        if ($playerBuy->player_id != $playerId)
        {
            throw \App\Exceptions\ParamException::make(
                'player_product_buy', 'player_id', $playerId
            );
        }

		{
			BaseGameModel::beginTransaction();

            $playerBuy->state = PlayerProductBuy::STATE_CANCELED;
            $playerBuy->save();

			BaseGameModel::commit();
        }

		return $response;
    }

	private static function _registImpl($request)
    {
        $response = self::getResponse($request);
        $now = $response->currentDateDB();
        $playerId = $request->player_id;

        $playerBuy = null;
        if ($request->player_buy_id > 0)
        {
            // プレイヤ製品購入取得

            $playerBuy = PlayerProductBuy::find($request->player_buy_id);
            if (empty($playerBuy))
            {
                throw \App\Exceptions\DataException::makeNotFound(
                    'player_product_buy', 'id', $request->player_buy_id
                );
            }
        }

        $checkResult = static::_checkReceipt($request);
        // DebugUtil::e_log('PS_regist', 'checkResult', $checkResult);

        // 未処理の有効なレシートを検索する //

        $receiptItems = [];
        foreach ($checkResult['receipt_items'] as $item)
        {
            $productId = static::productIdFromName($item['product_name']);
            $item['product_id'] = $productId;

            // トランザクション ID でのチェックが間違いないが
            // 未処理の場合は見つからない

            $_playerBuy = PlayerProductBuy::getByPaymentId(
                $checkResult['store_id'], $item['payment_id'],
                PlayerProductBuy::STATE_NOT_USED_MAX
            );

            if (isset($_playerBuy))
            {
                if ($_playerBuy->player_id != $playerId)
                    continue; // 何故か他の人のレシート。無視する

                $item['player_buy'] = $_playerBuy;
                $receiptItems[] = $item; // 未処理で有効なレシート
                continue;
            }

            // 製品IDと個数で検索

            $_playerBuys = PlayerProductBuy::getByProductId(
                $productId, $item['num'], PlayerProductBuy::STATE_NOT_USED_MAX
            );
            // DebugUtil::e_log('PS_regist', '_playerBuys', $_playerBuys);

            if ($_playerBuys->isNotEmpty())
            {
                // 最も購入日付が近いものを選んだほうが良いが・・・
                $_playerBuy = $_playerBuys[0];

                if ($_playerBuy->player_id != $playerId)
                    continue; // 何故か他の人のレシート。無視する

                $item['player_buy'] = $_playerBuy;
                $receiptItems[] = $item; // 未処理で有効なレシート
                continue;
            }

            // 使用済みか、有効なレシートでない
        }

        if (empty($receiptItems))
        {
            throw \App\Exceptions\ReceiptUsedException::make(
                'all receipt items are already used / invalid'
            );
        }

        $receiptItem = null;
        if (empty($playerBuy))
        {
            // 有効な最初の商品を処理する

            $receiptItem = $receiptItems[0];
            $playerBuy = $receiptItem['player_buy'];
        }
        else
        {
            // 今回、購入のものを検索する

            foreach ($receiptItems as $item)
            {
                if ($item['product_id'] == $playerBuy->product_id &&
                    $item['num'] == $playerBuy->num)
                {
                    $receiptItem = $item;
                    break;
                }
            }

            if (empty($receiptItem))
            {
                throw \App\Exceptions\ReceiptException::make(
                    'no receipt is match to player_product_buy'
                );
            }
        }

        // 製品マスタ取得

        $product = ProductShop::getOne_($receiptItem['product_id']);
        // DebugUtil::e_log('PS_regist', 'product', $product);

        $contents = ProductContents::getAll($product->product_contents_id);
        if (empty($contents))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'product_contents', 'id', $product->product_contents_id
            );
        }
        // DebugUtil::e_log('PS_regist', 'contents', $contents);

        $directItemIds = [];

		{
			BaseGameModel::beginTransaction();

            foreach ($contents as $content)
            {
                $playerBuyItem = PlayerProductBuyItem::register(
                    $playerBuy, $product, $content
                );

                static::_give(
                    $directItemIds, $playerBuy, $playerBuyItem
                );
            }

            // DebugUtil::e_log('PS_regist_rec', 'receiptItem', $receiptItem);

            $playerBuy->payment_id = $receiptItem['payment_id'];
            $playerBuy->state = PlayerProductBuy::STATE_FINISHED;
            $playerBuy->granted_at = $now; // 付与
            $playerBuy->save();

			BaseGameModel::commit();
        }

        $remainFlag = self::REMAIN_FLAG_NO;
        if (count($receiptItems) > 1)
            $remainFlag = self::REMAIN_FLAG_YES;

		// プレイヤの取得

		$player = Player::find_($playerId);

        // プレイヤアイテムの取得

        $playerItems = PlayerItem::getByItemIds($playerId, $directItemIds);

		$body = [
            'player_buy_id' =>
                $playerBuy->id,
            'product_id' =>
                $playerBuy->product_id,
            'remain_flag' =>
                $remainFlag,
			'player' =>
                PlayerResponse::make($player),
			'player_item_list' =>
                PlayerItemListResponse::make($playerItems),
        ];
		$response->body = $body;

		return $response;
    }

 	/**
	 * アイテム付与
	 *
	 * @param PlayerProductItemBuy $playerBuyItem プレイヤ製品購入アイテム
	 */
    protected static function _give(
        &$directItemIds, $playerBuy, $playerBuyItem
    )
    {
        // デイリーパック、プレゼントボックス等の対応が必要

        $daily = false;
        $direct = true;
        switch ($playerBuyItem->grant_type)
        {
        case ProductContents::GRANT_TYPE_NORMAL:
            break;
        case ProductContents::GRANT_TYPE_PRESENT_BOX:
            $direct = false; // プレボ受け取り
            break;
        case ProductContents::GRANT_TYPE_DAILY:
            $daily = true;
            $direct = false; // プレボ受け取り
            break;
        default:
            throw \App\Exceptions\MasterException::makeInvalid(
                'product_contents', 'grant_type', $playerBuyItem->grant_type
            );
        }

        // 直接受け取りで、あふれた場合はエラー (基本、あふれない蒼の結晶)

        if ($direct)
        {
            $param = new GiveOrPayParam();
            $param->playerId = $playerBuy->player_id;
            $param->itemType = $playerBuyItem->item_type;
            $param->itemId = $playerBuyItem->item_id;
            $param->count = $playerBuyItem->item_count;
            $param->srcType = SrcType::PRODUCT;
            $param->srcId = $playerBuy->id;
            $param->paymentType = $playerBuy->store_id;

            PlayerService::giveOrPay($param);

            if (PlayerItem::isTargetItemId($param->itemId))
                $directItemIds[] = $param->itemId;

            return; // 正常終了
        }

        // プレゼントボックス受け取り

        if ($daily)
        {
            $msg = '蒼の結晶デイリーパック'
                 . $playerBuyItem->grant_progress . '/'
                 . $playerBuyItem->grant_count . '回目の獲得アイテムです';
        }
        else
        {
            $msg = 'お得な詰め合わせパックで獲得したアイテムです';
        }

        PlayerPresent::regist(
            $playerBuy->player_id,
            $playerBuyItem->item_type,
            $playerBuyItem->item_id,
            $playerBuyItem->item_count,
            SrcType::PRODUCT,
            $playerBuy->id,
            $msg
        );
    }

 	/**
	 * レシートチェック
	 *
	 * @param ProductRegistRequest $request リクエスト
	 * @return list 結果の連想配列
	 */
    protected static function _checkReceipt($request)
    {
        // DebugUtil::e_log('PS_regist', 'receipt', $request->receipt);

        $receipt = json_decode($request->receipt);
        if (empty($receipt))
        {
            throw \App\Exceptions\ParamException::make(
                'request.receipt is not valid json data'
            );
        }

        switch ($receipt->Store)
        {
        case 'AppleAppStore':
            return static::_checkApple($receipt);
        case 'GooglePlay':
            return static::_checkGoogle($receipt);
        }

        throw \App\Exceptions\ParamException::make(
            'request.receipt has unknown format'
        );
    }

 	/**
	 * アップストア、レシートチェック
	 *
	 * @param ProductRegistRequest $request リクエスト
	 * @return list 結果の連想配列
	 */
    protected static function _checkApple($receipt)
    {
        // リクエストデータの用意 //
        $receiptItems = [];

        $data = [
            'receipt-data' => $receipt->Payload,
        ];
        $req_data = json_encode($data);
        // DebugUtil::e_log('PS_regist', 'req_data', $req_data);

        // 本番環境でエラーが起きた場合に、サンドボックスで試す

        $paymentId = null;
        $res = [];
        $count = count(static::$APPLE_VERIFY_URLS);
        for ($i = 0; $i < $count; ++ $i)
        {
            $paymentId = null;
            $res = [];
            $url = static::$APPLE_VERIFY_URLS[$i];
            $isLast = ($i == $count - 1);

            $curl = curl_init($url);
            curl_setopt_array($curl, [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $req_data,
            ]);

            $jsonRes = curl_exec($curl);
            curl_close($curl);

            $res = json_decode($jsonRes);
            // DebugUtil::e_log('PS_regist_app_res', 'res', $res);

            if (!isset($res->status))
            {
                throw \App\Exceptions\ReceiptException::make(
                    'status is not set'
                );
            }
            if ($res->status != 0)
            {
                if (!$isLast)
                    continue; // 次の URL を試す

                throw \App\Exceptions\ReceiptException::make(
                    'status is not 0: ' . $res->status
                );
            }

            // 成功

            $apps = $res->receipt->in_app; // 複数返る場合があるらしい

            foreach ($apps as $app)
            {
                $receiptItem = [];
                $receiptItem['num'] = $app->quantity;
                $receiptItem['product_name'] = $app->product_id;
                $receiptItem['date'] = new \DateTime($app->purchase_date);

                if (isset($app->original_transaction_id))
                {
                    $receiptItem['payment_id'] = $app->original_transaction_id;
                }
                else if (isset($app->transaction_id))
                {
                    $receiptItem['payment_id'] = $app->transaction_id;
                }

                $receiptItems[] = $receiptItem;
            }
        }

        return [
            'store_id' => PlayerProductBuy::STORE_APPLE,
            'receipt_items' => $receiptItems,
            'res' => $res,
        ];
    }

 	/**
	 * Google プレイストア、レシートチェック
	 *
	 * @param ProductRegistRequest $request リクエスト
	 * @return list 結果の連想配列
	 */
    protected static function _checkGoogle($receipt)
    {
        $receiptItems = [];

        $payload = json_decode($receipt->Payload);
        // DebugUtil::e_log('_checkGoogle', 'payload', $payload);

        // キーの準備

        $pem = "-----BEGIN PUBLIC KEY-----\n"
             . chunk_split(self::$GOOGLE_PUB_KEY, 64, "\n")
             . "-----END PUBLIC KEY-----\n";
        // DebugUtil::e_log('_checkGoogle', 'pem', $pem);

        $pubKeyId = openssl_pkey_get_public($pem);
        // DebugUtil::e_log('_checkGoogle', 'pubKeyId', $pubKeyId);

        // 署名の準備

        $signature = base64_decode($payload->signature);

        // 認証

        $result = (int)openssl_verify($payload->json, $signature, $pubKeyId);
        // DebugUtil::e_log('_checkGoogle', 'result', $result);

        // キーの開放

        openssl_free_key($pubKeyId);

        if ($result === 0)
        {
            throw \App\Exceptions\ReceiptException::make(
                'receipt is not valid'
            );
        }
        else if ($result < 0)
        {
            throw \App\Exceptions\ReceiptException::make(
                'receipt validation error: ' . $result
            );
        }

        // データの取得

        $data = json_decode($payload->json);
        // DebugUtil::e_log('_checkGoogle', 'data', $data);

        $skuDetails = json_decode($payload->skuDetails);
        // DebugUtil::e_log('_checkGoogle', 'skuDetails', $skuDetails);

        {
            $purchaseTime = DateTimeUtil::FromTimestampMs($data->purchaseTime);

            $receiptItem = [];
            $receiptItem['num'] = 1;
            $receiptItem['product_name'] = $data->productId;
            $receiptItem['date'] = $purchaseTime;
            $receiptItem['payment_id'] = $data->orderId;

            $receiptItems[] = $receiptItem;
        }
        // DebugUtil::e_log('_checkGoogle', 'receiptItems', $receiptItems);

        return [
            'store_id' => PlayerProductBuy::STORE_GOOGLE,
            'receipt_items' => $receiptItems,
        ];
    }

 	/**
	 * ストア登録の商品名から、product_id を求める
	 *
	 * @param string $productName
	 * @return integer product_id
	 */
    public static function productIdFromName($productName)
    {
        // TODO: サンプルが仮なので、調整する

        if (strpos($productName, 'android') === false)
        {
            // Product_Buy_TestCase を合わせる必要あり
            // $productName = 'bbdw_product_20003'; // デイリーテスト
            // $productName = 'bbdw_product_20002'; // 複数アイテムテスト
            $productName = 'bbdw_product_10007';
        }
        else
        {
            $productName = 'bbdw_product_10001';
        }

        // 末尾の連続した数値を抜き出す処理

        $productId = 0;
        $matches = [];
        if (preg_match('_([0-9]+)$_', $productName, $matches))
        {
            $productId = intval($matches[1]);
        }

        // DebugUtil::e_log('productIdFromName', $productName,  $productId);
        return $productId;
   }

}
