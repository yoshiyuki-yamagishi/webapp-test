<?php
/**
 * GiveOrPay パラメータ
 *
 */

namespace App\Services;

use App\Models\MasterModels\Item;

/**
 * 付与/消費パラメータ
 *
 */
class GiveOrPayParam
{
    const TAKE_FLAG_NONE = 0; // 未受取
	const TAKE_FLAG_TAKE = 1; // 受取済み
	const TAKE_FLAG_CHARACTER = 2; // キャラクター受取
	const TAKE_FLAG_CONVERT = 4; // 欠片変換受取
	const TAKE_FLAG_OVER = 8; // 所持数オーバー

    const PAYMENT_TYPE_FREE = 0;
    const PAYMENT_TYPE_GOOGLE = 1;
    const PAYMENT_TYPE_APPLE = 2;
    
    public $takeFlag = self::TAKE_FLAG_NONE;
    public $repeat = 0;
    public $playerId = 0;
    public $player = null; // オプション
    public $itemType = Item::TYPE_ITEM;
    public $itemId = 0;
    public $count = 0;
    public $srcType = 0;
    public $srcId = 0;
    public $paymentType = 0;
    public $onlyCheck = false;
    public $playerGrimoireIds = [];
    public $logs = [];
    public $chapterId = 0;
}
