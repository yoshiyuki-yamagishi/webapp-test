<?php
    /**
     * ショップ のサービス
     *
     */

    namespace App\Services;

    use App\Http\Requests\EventShopLineupRequest;
    use App\Http\Responses\ApiResponse;
    use App\Http\Responses\PlayerGrimoireListResponse;
    use App\Http\Responses\PlayerItemListResponse;
    use App\Http\Responses\PlayerResponse;
    use App\Http\Responses\ShopItemListResponse;
    use App\Models\BaseGameModel;
    use App\Models\Player;
    use App\Models\PlayerEventItem;
    use App\Models\PlayerGrimoire;
    use App\Models\PlayerItem;
    use App\Models\PlayerItemBuy;
    use App\Models\PlayerShopItem;
    use App\Models\MasterModels\Shop;
    use App\Models\MasterModels\ShopExchange;
    use App\Models\MasterModels\Item;
    use App\Services\MissionChecker\MissionCheckerShopBuy;

    /**
     * ショップ のサービス
     *
     */
    class EventShopService extends BaseService
    {


        /**
         *    ショップラインナップ
         *
         * @param EventShopLineupRequest $request リクエスト
         * @return ApiResponse レスポンス
         */
        public static function lineup($request)
        {
            $response = self::getResponse($request);
            $now = $response->currentDateDB();

            // ショップを1件セレクト

            $shop = Shop::getOne($request->shop_id);
            if (!isset($shop)) {
                throw \App\Exceptions\MasterException::makeNotFound(
                    'shop', 'shop_id', $request->shop_id
                );
            }

            // プレイヤショップアイテムをセレクト

            $shopItems = PlayerShopItem::getByShopId(
                $request->player_id, $request->shop_id
            );

            // 更新するかどうかを判定する

            $update = self::_checkLineup($shopItems);

            if ($update) {
                BaseGameModel::beginTransaction();

                // 更新処理

                PlayerShopItem::updateLineup(
                    $request->player_id, $shop, $now
                );

                $shopItems = PlayerShopItem::getByShopId(
                    $request->player_id, $request->shop_id
                );

                //プレイヤーのショップアイテムがあったらエラー
                assert(count($shopItems) > 0);

                BaseGameModel::commit();
            }

            $body = [
                'shop_item_list' =>
                    ShopItemListResponse::make($shopItems),
            ];

            $response->body = $body;

            return $response;
        }

        /**
         * ショップ購入・交換
         *
         * @param EventShopBuyRequest $request リクエスト
         * @return ApiResponse レスポンス
         */
        public static function buy($request)
        {
            $response = self::getResponse($request);
            $now = $response->currentDateDB();

            // ミッションチェッカーを初期化

            $missionChecker = MissionCheckerShopBuy::getInstance();
            $missionChecker->init($request->player_id, $now);

            // プレイヤショップアイテム取得
            $shopItem = PlayerShopItem::find_(
                $request->player_shop_item_id
            );

            // ショップ交換所取得

            $shopExchange = ShopExchange::getOne_(
                $shopItem->shop_exchange_id
            );

            // ショップ取得

            $shop = Shop::getOne_($shopExchange->shop_id);

            // 購入数のチェック

            if ($shopExchange->limit > 0 &&
                $shopItem->buy_count >= $shopExchange->limit) {
                throw \App\Exceptions\GameException::make(
                    'already buy limit: ' .
                    $shopItem->buy_count . '/' . $shopExchange->limit
                );
            }

            $itemIds = [];
            $playerGrimoireIds = [];

            {
                BaseGameModel::beginTransaction();

                // プレイヤアイテム購入に行を追加

                $itemBuy = PlayerItemBuy::regist($shopItem, $shop, $shopExchange);

                // プレイヤアイテム付与

                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemType = $shopExchange->lineup_category;
                $param->itemId = $shopExchange->lineup_id;
                $param->count = $shopExchange->get_count;
                $param->srcType = SrcType::SHOP;
                $param->srcId = $itemBuy->id;
                $param->chapterId = $request->event_id;

                PlayerService::giveOrPay($param);

                // プレイヤ魔道書 ID を追加

                $playerGrimoireIds = array_merge(
                    $playerGrimoireIds, $param->playerGrimoireIds
                );

                // アイテム ID を追加

                if (PlayerItem::isTargetItemId($shopExchange->lineup_id)) {
                    $itemIds[] = $shopExchange->lineup_id;
                }

                // プレイヤの消費アイテムを減らす

                $_param = new GiveOrPayParam();
                $_param->playerId = $request->player_id;
                $_param->itemType = Item::TYPE_ITEM;
                $_param->itemId = $shopExchange->pay_item_contents_id;
                $_param->count = -$shopExchange->pay_item_count;
                $_param->srcType = SrcType::SHOP;
                $_param->srcId = $itemBuy->id;
                $_param->chapterId = $request->event_id;
                PlayerService::giveOrPay($_param);

                // 購入数を増やす

                ++$shopItem->buy_count;
                $shopItem->save();

                // レスポンスリストに追加

                if (PlayerItem::isTargetItemId($shopExchange->pay_item_contents_id)) {
                    $itemIds[] = $shopExchange->pay_item_contents_id;
                }

                // プレイヤ取得

                $player = Player::find_($request->player_id);

                // ミッション達成チェック

                $missionChecker->addData('player', $player);
                $missionChecker->addData('playerShopItem', $shopItem);

                $achiveMissionList = [];
                $missionChecker->updateMission($achiveMissionList, false);

                BaseGameModel::commit();
            }

            // プレイヤアイテム取得

            $playerItemList = self::getPlayerItemList($request, $itemIds);

            // プレイヤ魔道書取得 (取得したばかりなので、装備はしてない)

            $playerGrimoireList = PlayerGrimoire::getByIds($playerGrimoireIds);

            //------------------------------
            // KPIログ登録
            //------------------------------
            //$logger = new ItemBuyLogger();
            //$logger->register($request->player_id, $player->player_lv, $shop, $shopExchange, $now);

            // レスポンス計算

            $body = [
                'player' =>
                    PlayerResponse::make($player),
                'player_item_list' =>
                    PlayerItemListResponse::make($playerItemList),
                'player_grimoire_list' =>
                    PlayerGrimoireListResponse::make($playerGrimoireList, false),
            ];
            $response->body = $body;

            return $response;
        }

        private static function _checkLineup(
            $shopItems
        ) {
            if (count($shopItems) <= 0) {
                return true;
            }

            return false;
        }

        /**
         * アイテムがイベントアイテムとそれ以外で混同してる場合
         * @param $request
         * @param $itemIds
         * @return array
         */
        public static function getPlayerItemList($request, $itemIds){
            $playerItemList = [];
            foreach ($itemIds as $itemId){
                $itemCategory = Item::categoryFromId($itemId);
                switch($itemCategory){
                    case Item::CATEGORY_EVENT:
                        $playerItemList[] = PlayerEventItem::getOne($request->player_id, $itemId, $request->event_id);
                        break;
                    default:
                        $playerItemList[] = PlayerItem::getOne($request->player_id, $itemId);
                        break;
                }
            }
            return $playerItemList;
        }


    }
