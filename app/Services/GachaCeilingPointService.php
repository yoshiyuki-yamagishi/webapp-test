<?php
/**
 *
 */

namespace App\Services;


use App\Http\Requests\GachaCeilingExecRequest;
use App\Http\Requests\GachaCeilingRequest;

use App\Http\Responses\GachaCeilingPointListResponse;
use App\Models\BaseGameModel;
use App\Models\GachaCeilingPointExchange;
use App\Models\GachaCeilingSchedule;


use App\Models\PlayerGachaCeilingPoint;
use App\Models\PlayerPresent;
use App\Utils\DateTimeUtil;


class GachaCeilingPointService extends BaseService
{
        /**
         * @param GachaCeilingRequest $request
         */
        public static function list(GachaCeilingRequest $request)
        {
            $body = [];
            $response = self::getResponse($request);
            $gachaId = $request->gacha_id;
            $playerId = $request->player_id;

            $gachaCeilingScheduleData = self::_gachaCeilingPointScheduleData($gachaId);
            $ceilingId = $gachaCeilingScheduleData->ceiling_id;
            //交換対象リスト
            $now = DateTimeUtil::getNOW();

            $ceilingExchangeList = self::getExchangeListByCeilingId($ceilingId);
            $body = [
                'exchange_list' =>  GachaCeilingPointListResponse::make(
                    $playerId, $ceilingExchangeList, $gachaId, $now
                )
            ];
            $response->body = $body;
            return $response;
        }
        public static function exec(GachaCeilingExecRequest $request)
        {
            $response = self::getResponse($request);
            $playerId =  $request->player_id;
            $gachaId =  $request->gacha_id;
            $ceilingId =  $request->ceiling_id;
            $exchangeId =  $request->exchange_id;

            $response->body = self::execExchangeItem($playerId, $gachaId, $ceilingId, $exchangeId);
            return $response;
        }


    /**
     * 交換１件に対して
     * @param $playerId
     * @param $gachaId
     * @param $ceiling_id
     * @param $exchange_id
     * @return array
     * @throws \App\Exceptions\ApiException
     */
        public static function execExchangeItem($playerId, $gachaId, $ceiling_id, $exchange_id)
        {
            // プレゼントボックスに入れる処理
           $body = [];
           $now = DateTimeUtil::getNOW();
           if(!self::isGachaCeilingOpen($gachaId, $now))
           {
               throw \App\Exceptions\GameException::make(
                   '終了しました。'
               );
           }

           $msg = 'ガチャptで交換したアイテムです。';
           $exchangeItemData = self::getExchangeItemData($ceiling_id,$exchange_id);
           $playerGachaPoint = self::getPlayerGachaPointData($playerId, $gachaId);
            if($playerGachaPoint  < $exchangeItemData->ceiling_count)
            {
                throw \App\Exceptions\DataException::make(
                    'ポイントが足りません。'. $playerGachaPoint .'/'. $exchangeItemData->ceiling_count
                );
           }
           if( $playerGachaPoint  >= $exchangeItemData->ceiling_count )
           {
               BaseGameModel::beginTransaction();
               //付与
               PlayerPresent::regist(
                    $playerId,
                    $exchangeItemData->item_type,
                    $exchangeItemData->item_id,
                    $exchangeItemData->item_count,
                    SrcType::GACHA_CEILING_EXCHANGE,
                    $exchange_id,
                    $msg,
                    null
               );

               $playerGachaPoint = self::saveGachaPoint($playerId, $gachaId, $now, -$exchangeItemData->ceiling_count);
               BaseGameModel::commit();

               $body = [
                   'exchage_item_type' => $exchangeItemData->item_type,
                   'exchage_item_id' => $exchangeItemData->item_id,
                   'exchage_item_count' => $exchangeItemData->item_count,
                   'ceiling_count' => $exchangeItemData->ceiling_count,
                   'player_gacha_ceiling_point' => $playerGachaPoint,
               ];
           }
           return $body;
        }
    /**
     * 交換できるものがあるかチェック
     * @param $playerGachaPoint
     * @param $gachaId
     * @return bool
     */
    public static function checkExchangeAbleList($playerId, $gachaId)
    {
        $exchangeAble = false;
        $gachaCeilingPointScheduleData = self::_gachaCeilingPointScheduleData($gachaId);
        if( !isset($gachaCeilingPointScheduleData) )
        {
            return $exchangeAble;
        }

        $ceilingId = $gachaCeilingPointScheduleData->ceiling_id;

        $exchangeDataList = self::getExchangeListByCeilingId($ceilingId);
        $playerGachaPoint = self::getPlayerGachaPointData($playerId,$gachaId);
        foreach ($exchangeDataList as $exchangeData)
        {
            if( $exchangeData->ceiling_count > $playerGachaPoint)
            {
                continue;
            }
            $exchangeAble = true;
        }
        return $exchangeAble;
    }


        /**
         * @param $gacha_id
         * @param $now
         */
        public static function isGachaCeilingOpen($gacha_id, $now)
        {
            //ガチャ天井のスケジュールデーター取得
            $gachaCeilingScheduleData = self::_gachaCeilingPointScheduleData($gacha_id);
            $gachaCeilingOpenFlag = false;
            //データーチェック
            if (isset($gachaCeilingScheduleData)) {
                //開催中なのか判定
                 $gachaCeilingOpenFlag = DateTimeUtil::isBetween( $now, $gachaCeilingScheduleData->started_at, $gachaCeilingScheduleData->end_at );
            }
            return $gachaCeilingOpenFlag;
        }

        /**
         * @param $playerId
         * @param $gachaId
         * @return int ユーザーのガチャポイント
         */
        public static function getPlayerGachaPointData($playerId, $gachaId)
        {

            $playerGachaCeilingPoint = 0;
            $now = DateTimeUtil::getNOW();
            if(self::isGachaCeilingOpen($gachaId, $now)) {
                $gachaCeilingScheduleData = self::_gachaCeilingPointScheduleData($gachaId);
                $ceilingId = $gachaCeilingScheduleData->ceiling_id;
                $playerGachaCeilingPointData = PlayerGachaCeilingPoint::getByPlayerIdAndCeilingId($playerId, $ceilingId);
                if( !isset($playerGachaCeilingPointData) )
                {
                    PlayerGachaCeilingPoint::regist(
                        $playerId, $gachaCeilingScheduleData->ceiling_id
                    );
                    $playerGachaCeilingPointData = PlayerGachaCeilingPoint::getByPlayerIdAndCeilingId($playerId, $ceilingId);

                }
                $playerGachaCeilingPoint = $playerGachaCeilingPointData->gacha_point;
            }
            return $playerGachaCeilingPoint;
        }


        /**
         * ガチャポイント更新
         * @param $player_id
         * @param $gacha_id
         * @param $now
         * @param $gacha_count
         * @return int ガチャポイント
         */
        public static function saveGachaPoint($player_id, $gacha_id, $now, $gacha_point_count)
        {
            $calcPlayerCeilingGachaPoint = 0;
            if(self::isGachaCeilingOpen($gacha_id, $now))
            {
                $gachaCeilingScheduleData = self::_gachaCeilingPointScheduleData($gacha_id);
                $ceilingId = $gachaCeilingScheduleData->ceiling_id;
                $playerGachaCeilingPointData = PlayerGachaCeilingPoint::playerCeilingPointSave($player_id, $ceilingId, $gacha_point_count);
                return $playerGachaCeilingPointData->gacha_point;
            }

            return $calcPlayerCeilingGachaPoint;
        }

        /**
         * @param $ceiling_id
         * @param $exchange_id
         * @return array交換するアイテムデーター
         */
        public static function getExchangeItemData($ceiling_id, $exchange_id)
        {
            $exchangeItemData = GachaCeilingPointExchange::getByCeilingIdAndExchnageId($ceiling_id, $exchange_id);
            return $exchangeItemData;
        }

        /**
         * @param $ceiling_id
         * @return array 交換可能なリストもとにリスポンス作るか
         */
        public static function getExchangeListByCeilingId($ceiling_id)
        {
            $gachaCeilingPointExchangeList = GachaCeilingPointExchange::getByCeilingId($ceiling_id);
            return $gachaCeilingPointExchangeList;
        }

        /**
         * @param $gacha_id
         */
        private static function _gachaCeilingPointScheduleData($gacha_id)
        {
            $gachaCeilingScheduleData = '';

            $gachaCeilingScheduleData = GachaCeilingSchedule::getGachaCeilingScheduleDataByGachaId($gacha_id);

            return $gachaCeilingScheduleData;
        }

}
