<?php
/**
 * プレイヤクエスト のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerQuestListResponse;
use App\Models\Player;
use App\Models\PlayerQuest;
use App\Models\BaseGameModel;
use App\Services\MissionChecker\MissionCheckerQuestUpdate;
use App\Utils\DebugUtil;

/**
 * プレイヤクエスト のサービス
 *
 */
class PlayerQuestService extends BaseService
{
	/**
	 * 一覧
	 *
	 * @param PlayerQuestListRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        $playerQuestList = PlayerQuest::getByPlayerId(
            $request->player_id, $request->quest_category,
            $request->flags, $now
        );

		$body = [
			'player_quest_list' =>
                PlayerQuestListResponse::make($playerQuestList, $now),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 更新
	 *
	 * @param PlayerQuestUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function update($request)
	{
		// DebugUtil::e_log('PQS_update', 'request', $request->all());
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerQuestUpdate::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤ取得
		$player = Player::find_($request->player_id);

        $questService = QuestService::make($request->quest_category);

        $quest = $questService->getQuest(
            $request->chapter_id,
            $request->quest_id
        );

        // クエストの日付をチェックする
        $questService->checkStartEndDay($quest, $now);

        // クエストタイプをチェックする
        if ($quest->quest_type != QuestService::QUEST_TYPE_STORY)
        {
            throw \App\Exceptions\ParamException::make(
                'specified quest\'s quest_type is invalid: '
                . $quest->quest_type
            );
        }

		{
			BaseGameModel::beginTransaction();

            // PlayerQuest の準備

            $playerQuest = $questService->getPlayerQuest(
                $request->player_id,
                $request->chapter_id,
                $request->quest_id
            );

            // 無ければ新規登録 (普通は無い)

            $questService->preparePlayerQuest(
                $playerQuest,
                $request->player_id,
                $request->chapter_id,
                $request->quest_id
            );

            // クリア済みにして保存

            if ($playerQuest->clear_flag != QuestService::CLEAR_FLAG_YES)
            {
                $playerQuest->clear_flag = QuestService::CLEAR_FLAG_YES;
                $playerQuest->cleared_at = $now;
            }

            // クリア数を増加する
            ++ $playerQuest->clear_count;

            $playerQuest->save();

            // クエストミッションの達成チェック

            $missionChecker->addData(
                'player', $player
            );
            $missionChecker->addData(
                'playerQuest', $playerQuest
            );

            // 絶対値になったため、クリアしたクエスト以外の判定も必要だろう //

            $playerQuests = PlayerQuest::getByPlayerId(
                $player->id, PlayerQuest::QUEST_CATEGORY_ALL, 0, $now
            );
            $missionChecker->addData(
                'playerQuests', $playerQuests
            );

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

        // $playerQuestList = PlayerQuest::getByPlayerId(
        // $request->player_id, $request->quest_category
        // );

		$body = [
			// 'player_quest_list' =>
            // PlayerQuestListResponse::make($playerQuestList, $now),
		];
		$response->body = $body;

		return $response;
	}
}
