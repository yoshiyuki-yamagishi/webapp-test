<?php
/**
 * ログイン のサービス
 *
 */

namespace App\Services;

use App\Http\Requests\AuthenticatedRequest;
use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerResponse;
use App\Models\PlayerCommon;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerBattle;
use App\Models\PlayerLogin;

/**
 * ログイン のサービス
 *
 */
class LoginService extends BaseService
{
	/**
	 * ログイン
	 *
	 * @param LoginRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function index($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();
		// プレイヤ共通テーブル (共通DB) からプレイヤ情報を取得
		$playerCommon = PlayerCommon::getByPlayerId_($request->player_id);

		// セッション開始
		SessionService::start(
            $playerCommon->unique_id,
            SessionService::SS_LOGIN,
            $playerCommon->db_no
        );

        // プレイヤ取得
        $player = Player::find_($request->player_id);

		// セッションから認証コード取得
		$authCode = SessionService::getAuthCode();

		SessionService::end();

        {
            BaseGameModel::beginTransaction();
            $playerLogin = PlayerLogin::getByPlayerIdToday($player->id, $now);
            if (!isset($playerLogin)) {
                $playerLogin = PlayerLogin::register($player->id);
            } else {
                PlayerLogin::countUp($playerLogin->id, $playerLogin->count);
            }
            BaseGameModel::commit();
        }

		$body = [
            'player_disp_id' => $playerCommon->player_disp_id,
			'player' => PlayerResponse::make($player),
			'auth_code' => $authCode
		];
		$response->body = $body;

		return $response;
	}
	/**
	 * 認証コード更新
	 *
	 * @param LoginUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function update($request)
	{
        $response = self::getResponse($request);

		// プレイヤ共通テーブル (共通DB) からプレイヤ情報を取得
		$playerCommon = PlayerCommon::getByPlayerId_($request->player_id);

		// セッション開始
		SessionService::start(
            $playerCommon->unique_id,
            SessionService::SS_UPDATE,
            $playerCommon->db_no
        );

		// セッションから認証コード取得
		$authCode = SessionService::getAuthCode();

		SessionService::end();

		$body = [
			'auth_code' => $authCode,
		];
		$response->body = $body;

		return $response;
    }


    /**
     * 中断バトルのコード取得
     * @param AuthenticatedRequest $request
     * @return mixed
     */
    public static function getBattleCode($request)
    {
        $response = self::getResponse($request);

        // バトルコードの確認
        $playerBattle = PlayerBattle::getActive($request->player_id);
        $battleCode = null;
        if (PlayerBattle::isValidBattleCode($playerBattle)){
            $battleCode = $playerBattle->battle_code;
        }

        $body = [
            'battle_code' => $battleCode,
        ];
        $response->body = $body;

        return $response;
    }
}
