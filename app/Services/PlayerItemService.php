<?php
/**
 * プレイヤアイテム のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerItemListResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerData;
use App\Models\PlayerEventItem;
use App\Models\PlayerItem;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Item;
use App\Utils\DebugUtil;

/**
 * プレイヤアイテム のサービス
 *
 */
class PlayerItemService extends BaseService
{
	/**
	 * 一覧
	 *
	 * @param PlayerItemListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);

        $playerItemList = PlayerItem::getByPlayerId(
            $request->player_id, $request->item_category
        );

		$body = [
			'player_item_list' => PlayerItemListResponse::make($playerItemList),
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * 売却
	 *
	 * @param PlayerItemSellRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function sell($request)
	{
        $response = self::getResponse($request);

		// プレイヤ取得

		$player = Player::find_($request->player_id);

		// 売却するプレイヤアイテムを取得

		$playerItem = PlayerItem::find_($request->player_item_id);

        // 個数チェック

		if ($playerItem->num < $request->num)
		{
            throw \App\Exceptions\DataException::makeNotEnough(
                'player_item', 'id', $request->player_item_id,
                $request->num, $playerItem->num
            );
		}

        // アイテムを取得

        $item = Item::getOne_($playerItem->item_id);

		{
			BaseGameModel::beginTransaction();

			// p$ 増加

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemId = Item::ID_P_DOLLAR;
            $param->count = $item->sale_money * $request->num;
            $param->srcType = SrcType::SELL;
            $param->srcId = 0;

            PlayerService::giveOrPay($param);
            $log = $param->logs[0];

            // アイテムを減らす

            $_param = new GiveOrPayParam();
            $_param->playerId = $request->player_id;
            $_param->itemId = $playerItem->item_id;
            $_param->count = - $request->num;
            $_param->srcType = SrcType::SELL;
            $_param->srcId = $log->id;

            PlayerService::giveOrPay($_param);

			BaseGameModel::commit();
		}

        // プレイヤアイテム更新

		$playerItem = PlayerItem::find_($request->player_item_id);
        $playerItemList = [$playerItem];

		$body = [
			'player_item_list' =>
                PlayerItemListResponse::make($playerItemList),
			'platinum_dollar_num' =>
                Player::getPDollar($player->id)
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 欠片パウダー精製
	 *
	 * @param PlayerItemPowderPurifyRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function powderPurify($request)
	{
        // DebugUtil::e_log('powderPurify', 'request', $request->all());
        $response = self::getResponse($request);
        $playerId = $request->player_id;

        // 定数マスタ取得

        $purifyCs = Constant::getRange(
            Constant::POWDER_PURIFY_BEGIN,
            Constant::POWDER_PURIFY_END
        );

        // アイテム ID を取得し、ついでに所持数が足りているかチェックする

        $useItems = $request->player_item_list;
        static::setupUseItems($useItems);

        // 取得欠片パウダー数を計算する

        $getNum = 0;
        foreach ($useItems as $useItem)
        {
            $characterId = Item::characterIdFrom($useItem['item_id']);
            $character = CharacterBase::getOne_($characterId);

            $rarity0 = $character->character_initial_rarity;
            if ($rarity0 < 1 || $rarity0 > count($purifyCs))
            {
                throw \App\Exceptions\MasterException::makeInvalid(
                    'character_base', 'character_initial_rarity', $rarity0
                );
            }

            $getNum1 = $purifyCs[$rarity0 - 1]->value1;
            $getNum += $getNum1 * $useItem['item_num'];

            // DebugUtil::e_log('powderPurify', 'character', $character);
            // DebugUtil::e_log('powderPurify', 'rarity0', $rarity0);
            // DebugUtil::e_log('powderPurify', 'getNum1', $getNum1);
        }

        if ($getNum != $request->powder)
        {
            throw \App\Exceptions\ParamException::make(
                'powder is too many / few need: ' . $getNum
            );
        }

        // データベースの更新

        $itemIds = [];
		{
			BaseGameModel::beginTransaction();

            // 欠片パウダーを増加する
            $log = null;
            {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemId = Item::ID_POWDER;
                $param->count = $getNum;
                $param->srcType = SrcType::PURIFY;
                PlayerService::giveOrPay($param);
                $log = $param->logs[0];
            }

            // アイテムの消費

            foreach ($useItems as $useItem)
            {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemId = $useItem['item_id'];
                $param->count = - $useItem['item_num'];
                $param->srcType = SrcType::PURIFY;
                $param->srcId = $log->id;
                PlayerService::giveOrPay($param);

                $itemIds[] = $useItem['item_id'];
            }

			BaseGameModel::commit();
        }

		$playerItemList = PlayerItem::getByItemIds($playerId, $itemIds);

		$body = [
            'powder' =>
                Player::getPowder($playerId),
			'player_item_list' =>
                PlayerItemListResponse::make($playerItemList),
		];
		$response->body = $body;
		return $response;
    }

	/**
	 * オーブ合成
	 *
	 * @param PlayerItemOrbSynthesizeRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function orbSynthesize($request)
	{
        // DebugUtil::e_log('orbSynthesize', 'request', $request->all());
        $response = self::getResponse($request);
        $playerId = $request->player_id;

		$player = Player::find_($playerId);

        // アイテム ID を取得し、ついでに所持数が足りているかチェックする

        $useItems = $request->player_item_list;
        static::setupUseItems($useItems);

        // 消費前の個数を保存しておく

        $useItemsOrg = PlayerItemService::cloneItems($useItems);

        // 合成

        $synthesized = OrbService::synthesize(
            $price, $useItems, [$request->item_id => $request->count]
        );

        // 合成費用が正しいかチェックする //

        if ($price != $request->platinum_dollar)
        {
            throw \App\Exceptions\ParamException::make(
                'platinum_dollar is too many / few: '
                . $price . ' != ' . $request->platinum_dollar
            );
        }

        // 消費して 0 になったかチェックする

        PlayerItemService::checkUseItemsZero($useItems);

        {
			BaseGameModel::beginTransaction();

            // アイテムを増やす //
            $log = null;
            {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemId = $request->item_id;
                $param->count = $request->count;
                $param->srcType = SrcType::ORB_SYNTHESIZE;

                PlayerService::giveOrPay($param);
                $log = $param->logs[0];
            }

            // P$ を消費する //
            {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemId = Item::ID_P_DOLLAR;
                $param->count = - $request->platinum_dollar;
                $param->srcType = SrcType::ORB_SYNTHESIZE;
                $param->srcId = $log->id;

                PlayerService::giveOrPay($param);
            }

            // アイテムを消費する //

            foreach ($useItemsOrg as $useItem)
            {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemId = $useItem['item_id'];
                $param->count = - $useItem['item_num'];
                $param->srcType = SrcType::ORB_SYNTHESIZE;
                $param->srcId = $log->id;

                PlayerService::giveOrPay($param);
            }

			BaseGameModel::commit();
		}

        $playerItemList = PlayerItem::getByItemIds(
            $playerId, [$request->item_id]
        );

		$body = [
			'player_item_list' =>
                PlayerItemListResponse::make($playerItemList),
            'platinum_dollar' =>
                Player::getPDollar($playerId)
		];
		$response->body = $body;
		return $response;
    }

	/**
	 * アイテム個数取得
	 *
	 * @param object $param 付与/消費パラメータ
	 * @return integer 個数
	 */
	public static function getCount($param)
    {
        assert($param->itemType == Item::TYPE_ITEM);

		$response = ApiResponse::getInstance();
        $now = $response->currentDateDB();

        $itemCategory = Item::categoryFromId($param->itemId);
        switch ($itemCategory)
        {
        case Item::CATEGORY_FREE_BLUE_CRYSTAL:
            return PlayerBlueCrystal::getFreeNum($param->playerId, $now);
        case Item::CATEGORY_TICKET:
        case Item::CATEGORY_EXP:
        case Item::CATEGORY_ORB:
        case Item::CATEGORY_SKIP_TICKET:
        case Item::CATEGORY_FRAGMENT:
        case Item::CATEGORY_AL_RECOVER:
            break;
        case Item::CATEGORY_EVENT:
            return Player::getEventItemCount($param->playerId, $param->itemId);
        case Item::CATEGORY_FRIEND_POINT:
            return Player::getFriendPoint($param->playerId);
        case Item::CATEGORY_BLUE_CRYSTAL:
            return PlayerBlueCrystal::getChargedNum($param->playerId, $now);
        case Item::CATEGORY_P_DOLLAR:
            return Player::getPDollar($param->playerId);
        case Item::CATEGORY_ELEMENT:
            return Player::getElement($param->playerId);
        case Item::CATEGORY_POWDER:
            return Player::getPowder($param->playerId);
        default:
            throw \App\Exceptions\MasterException::makeInvalid(
                '', 'item_id', $param->itemId
            );
        }

        $playerItem = getOne($param->playerId, $param->itemId);
        if (empty($playerItem))
            return 0;

        return $playerItem->num;
    }

	/**
	 * アイテム付与/消費
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPay($param)
    {
        if ($param->count == 0)
            return;

		$playerItem = PlayerItem::getOne($param->playerId, $param->itemId);

		if (!isset($playerItem))
		{
            if ($param->count < 0)
            {
                throw \App\Exceptions\DataException::makeNotEnough(
                    'player_item', 'id', $param->itemId,
                    0, - $param->count
                );
            }

            if (!$param->onlyCheck)
            {
                $playerItem = new PlayerItem();
                $playerItem->player_id = $param->playerId;
                $playerItem->item_id = $param->itemId;
            }
		}
		else
		{
            if ($param->count < 0)
            {
                if ($playerItem->num < - $param->count)
                {
                    throw \App\Exceptions\DataException::makeNotEnough(
                        'player_item', 'id', $param->itemId,
                        $playerItem->num, - $param->count
                    );
                }
            }
            else
            {
                if ($playerItem->num >= PlayerItem::MAX_NUM)
                {
                    $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_OVER;
                    throw \App\Exceptions\OverflowException::makeOverflow(
                        'player_item', 'id', $param->itemId,
                        Item::TYPE_ITEM, $param->itemId, $param->count,
                        $playerItem->num
                    );
                }
            }
		}

        if (!$param->onlyCheck)
        {
            $playerItem->num += $param->count;
            $playerItem->save();
            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_TAKE;

            // 付与/消費ログ

            $log = LogService::playerItemGiveOrPay(
                $playerItem, $param->itemId, $param->count, $playerItem->num,
                $param->srcType, $param->srcId
            );
            $param->logs[] = $log;
        }
    }

    /**
     * イベントアイテム付与/消費
     *
     * @param object $param 付与/消費パラメータ
     */
    public static function giveOrPayEventItem($param)
    {
        if ($param->count == 0)
            return;

        $playerEventItem = PlayerEventItem::getOne($param->playerId, $param->chapterId, $param->itemId);

        if (!isset($playerEventItem))
        {
            if ($param->count < 0)
            {
                throw \App\Exceptions\DataException::makeNotEnough(
                    'player_item', 'id', $param->itemId,
                    0, - $param->count
                );
            }

            if (!$param->onlyCheck)
            {
                $playerEventItem = new PlayerEventItem();
                $playerEventItem->player_id = $param->playerId;
                $playerEventItem->event_id = $param->chapterId;
                $playerEventItem->item_id = $param->itemId;
            }
        } else {
            if ($param->count < 0)
            {
                if ($playerEventItem->num < - $param->count)
                {
                    throw \App\Exceptions\DataException::makeNotEnough(
                        'player_item', 'id', $param->itemId,
                        $playerEventItem->num, - $param->count
                    );
                }
            }
        }

        if (!$param->onlyCheck)
        {
            $playerEventItem->num += $param->count;
            if($param->count > 0){
                $playerEventItem->total_num += $param->count;
            }else{
                $playerEventItem->total_num += 0;
            }
            $playerEventItem->save();
            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_TAKE;

            // 付与/消費ログ
            $log = LogService::playerItemGiveOrPay(
                $playerEventItem, $param->itemId, $param->count, $playerEventItem->num,
                $param->srcType, $param->srcId
            );
            $param->logs[] = $log;
        }
    }

	/**
	 * アイテム消費リストの準備
	 *
	 * @param 配列 $items 修正元のアイテムリスト
	 */
    public static function setupUseItems(&$items)
    {
        foreach ($items as &$item)
        {
            $playerItem = PlayerItem::find(
                $item['player_item_id']
            );
            if (empty($playerItem))
            {
                throw \App\Exceptions\DataException::makeNotEnough(
                    'player_item', 'id', $item['player_item_id'],
                    0, $item['item_num']
                );
            }
            if ($playerItem->num < $item['item_num'])
            {
                throw \App\Exceptions\DataException::makeNotEnough(
                    'player_item', 'id', $item['player_item_id'],
                    $playerItem->num, $item['item_num']
                );
            }

            $item['item_id'] = $playerItem->item_id;
            // $item['num'] = $playerItem->num;
        }
    }

	/**
	 * アイテム消費リストをクローンする
	 *
	 * @param 配列 $items コピー元のアイテムリスト
	 */
    public static function cloneItems($items)
    {
        $ret = [];
        foreach ($items as $item)
        {
            $ret[] = $item;
        }
        return $ret;
    }

	/**
	 * アイテム消費リスト、すべて空になったかチェックする
	 *
	 * @param 配列 $items 修正元のアイテムリスト
	 */
    public static function checkUseItemsZero($items)
    {
        foreach ($items as $item)
        {
            if ($item['item_num'] != 0)
            {
                throw \App\Exceptions\ParamException::make(
                    'player_item_list is too many / few'
                );
            }
        }
    }

}
