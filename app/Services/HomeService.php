<?php
/**
 * ホーム のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\BannerListResponse;
use App\Http\Responses\HomeResponse;
use App\Http\Responses\LoginBonusResponse;
use App\Http\Responses\LoginBonusListResponse;
use App\Http\Responses\PlayerItemListResponse;
use App\Http\Responses\PlayerResponse;
use App\Models\BaseGameModel;
use App\Models\DirectPresent;
use App\Models\Player;
use App\Models\PlayerCommonCache;
use App\Models\PlayerDirectPresent;
use App\Models\PlayerFriend;
use App\Models\PlayerItem;
use App\Models\PlayerLogin;
use App\Models\PlayerLoginBonus;
use App\Models\PlayerLoginPoint;
use App\Models\PlayerMission;
use App\Models\PlayerPresent;
use App\Models\PlayerProductBuy;
use App\Models\PlayerProductBuyItem;
use App\Models\PlayerQuest;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\HomeBanner;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\LoginBonus;
use App\Models\MasterModels\LoginBonusReward;
use App\Models\MasterModels\PlayerLevel;
use App\Services\KpiLogger\LoginLogger;
use App\Services\MissionChecker\MissionCheckerHome;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * ホーム のサービス
 *
 */
class HomeService extends BaseService
{
	/**
	 * ホーム
	 *
	 * @param HomeRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function index($request)
	{

        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerHome::getInstance();
        $missionChecker->init($request->player_id, $now);

        $player = Player::find_($request->player_id);
        $lastLoginAt = $player->last_login_at; // 前回ログイン日時を保存

        $continueFlag = DateTimeUtil::checkContinue($lastLoginAt, $now);


        // 直接付与は、CommonDB を扱うので別トランザクションとする //
        // TODO: テスト中は高速化のコードをはずす
        // if ($continueFlag > 0)
        {
            // 高速化のため、1 日 1 回しかチェックしない
            // すぐに、ゲットするには、再起動などが必要
            static::_giveDirectPresent(
                $player->id, $now
            );
        }


		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

            //------------------------------
            // ログイン日数カウント
            //------------------------------
			if ($continueFlag > 0)
			{
				$player->login_days += 1;
			}

			//------------------------------
			// 最終ログイン日時更新
			//------------------------------
			$player->last_login_at = $now;

			//------------------------------
			// ログインログ取得
			//------------------------------
			$playerLogin = PlayerLogin::getByPlayerIdToday($request->player_id,$now);

			//------------------------------
			// AL回復処理(自然回復)
			//------------------------------
			$player->updateAl($now, false);
			$player->save(); // last_login_at が必ず変わる

			//------------------------------
			// デイリーパック取得処理
			//------------------------------
			// if ($continueFlag > 0) // 高速化 (同日なら処理済み)
            {
                static::_processDailyPack(
                    $request->player_id, $lastLoginAt, $now
                );
            }

			//------------------------------
			// ログインボーナス取得処理
			//------------------------------

            $loginBonusList = LoginBonus::getAll($now);

            $_loginRewardList = []; // 全、受け取りリスト
            $_newLoginRewardList = []; // 今回、受け取りリスト

			foreach ($loginBonusList as $loginBonus)
			{
				$loginBonusId = $loginBonus->id;

                $rewardList = LoginBonusReward::getAll($loginBonusId);
                if (empty($rewardList))
                    continue;

                $maxCount = end($rewardList)->count;

                // 受け取り履歴から、ボーナスごとの、受け取り回数を取得する

                $latest = PlayerLoginBonus::getLatest(
                    $request->player_id, $loginBonusId
                );

                $nextPlayerCount = 1; // 最初は 1

                if (isset($latest))
                {
                    $nextPlayerCount = $latest->player_count + 1;

                    // 本日に受け取り済みかチェックする
                    $days = DateTimeUtil::checkContinue(
                        $latest->created_at, $now
                    );
                    if ($days < 1)
                    {
                        // 受け取り数は増えない
                        LoginBonusListResponse::appendByCount(
                            $_loginRewardList,
                            $loginBonus, $rewardList, $latest->count
                        );
                        continue;
                    }
                }

                // 次に受けとる count を計算する

				if ($loginBonus->loop == LoginBonus::LOOP_JUDGE_YES)
                {
                    $nextCount = ($nextPlayerCount - 1) % $maxCount;
                    ++ $nextCount; // 1-base に戻す
                }
                else
                {
                    $nextCount = $nextPlayerCount;
                }

                if ($nextCount > $maxCount)
                {
                    // 最後まで受けとり済み
                    LoginBonusListResponse::appendByCount(
                        $_loginRewardList,
                        $loginBonus, $rewardList, $maxCount
                    );
                    continue;
                }

                // 次に受け取る、ログインボーナスがある場合の処理 //

                $newRewardList = LoginBonusReward::filterOnlyCount(
                    $rewardList, $nextCount
                );

				if (empty($newRewardList))
                {
                    // マスターデーターが不正、
                    // count がきちんと連番になってない !

                    throw \App\Exceptions\MasterException::makeNotFound(
                        'login_bonus',
                        ['login_bonus_id', 'count'],
                        [$loginBonusId, $nextCount]
                    );
                }

                // ログインボーナス付与
                self::_giveLoginBonus(
                    $request->player_id, $loginBonus, $newRewardList,
                    $nextPlayerCount, $now
                );

                // 全取得リストに追加
                // 今回取得のものは、取得済みとしない !
                LoginBonusListResponse::appendByCount(
                    $_loginRewardList,
                    $loginBonus, $rewardList, $nextCount - 1
                );

                // 今回取得リストに追加
                LoginBonusListResponse::append(
                    $_newLoginRewardList,
                    $loginBonus, $newRewardList,
                    LoginBonusResponse::TAKE
                );
            }

            // ログインポイント付与
            static::_giveLoginPoint(
                $playerLogin, PlayerLoginPoint::TYPE_FRIEND, $now
            );

            // ホームミッション更新

            $missionChecker->addData('player', $player);

            $missionChecker->addData('loginDays', $player->login_days);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

        if ($continueFlag > 0)
        {
            // 最終ログイン日時を、共通 DB の方にキャッシュ
            // 毎回更新してもよいが、高速化のため、1 日 1 回程度にする

            // 1 SQL なので、トランザクション不要
            // 失敗しても、さほど、問題無し
            PlayerCommonCache::updateCache($player);
        }

        //------------------------------
        // レスポンスの生成
        //------------------------------

		// $playerItemList = PlayerItem::getByPlayerId($request->player_id);

        $takablePresentNum = PlayerPresent::getTakableCount(
            $request->player_id, $now
        );
        $takableMissionNum = PlayerMission::getTakableCount(
            $request->player_id, $now
        );

        // クエストクリア状況の取得

        $questFlag = 0;
        {
            $first = QuestService::QUEST_CATEGORY_FIRST;
            $last = QuestService::QUEST_CATEGORY_LAST;
            for ($i = $first; $i <= $last; ++ $i)
            {
                if (PlayerQuest::isNotCleared($request->player_id, $i, $now))
                    $questFlag |= HomeResponse::QUEST_FLAG_EXIST;
            }
        }

        // 新しいガチャフラグを設定する

        $gachaFlag = HomeResponse::GACHA_FLAG_NONE;
        if (Gacha::isNew($now))
            $gachaFlag = HomeResponse::GACHA_FLAG_EXIST;

        //------------------------------
        // KPIログ登録
        //------------------------------
/*        $logger = new LoginLogger();
        $logger->register($request->player_id, $player->player_lv, $now);*/

        // ホームバナー情報取得
        $homeBanner = HomeBanner::getViewList($now);

		$body = [
			'player' =>
                PlayerResponse::make($player),
			// 'player_item_list' =>
            // PlayerItemListResponse::make($playerItemList),
			'new_login_bonus_list' =>
                $_newLoginRewardList,
			'login_bonus_list' =>
                $_loginRewardList,
            'takable_present_num' =>
                $takablePresentNum,
            'takable_mission_num' =>
                $takableMissionNum,
            'quest_flag' =>
                $questFlag,
            'gacha_flag' =>
                $gachaFlag,
            'banner_list' =>
                BannerListResponse::make($homeBanner[HomeBanner::BANNER_LIST_TYPE_HOME]),
            'force_banner_list' =>
                BannerListResponse::make($homeBanner[HomeBanner::BANNER_LIST_TYPE_FORCE])
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * デイリーパック処理
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $lastLoginAt 前回ログイン日時
	 * @param string $now 処理日時
	 * @return なし
	 */
	private static function _processDailyPack(
        $playerId, $lastLoginAt, $now
    )
    {
        // 付与すべきデイリーパックがあれば、プレゼントボックスに入れる

        $playerBuys = PlayerProductBuy::getDaily($playerId);

        if (empty($playerBuys))
            return;

        $_now = new \DateTime($now);

        $finished = true;
        $given = false;
        foreach ($playerBuys as $playerBuy)
        {
            // 期限切れのチェック

            $giveItems = true;

            $endAt = $playerBuy->dailyEndDate();
            if ($_now >= $endAt)
            {
                assert($finished === true);
                $giveItems = false;
            }

            // 本日に受け取り済みかチェックする

            if ($giveItems)
            {
                $days = DateTimeUtil::checkContinue(
                    $playerBuy->granted_at, $now
                );
                if ($days < 1)
                {
                    $giveItems = false;
                    $finished = false;
                }
            }

            if ($giveItems)
            {
                // プレゼントボックスに入れる処理

                $playerBuyItems = PlayerProductBuyItem::getDaily(
                    $playerBuy->id
                );

                foreach ($playerBuyItems as $playerBuyItem)
                {
                    ++ $playerBuyItem->grant_progress; // msg に使用する

                    PlayerPresent::registerDaily(
                        $playerBuy, $playerBuyItem, $now
                    );

                    if (!$playerBuyItem->grantedAll())
                        $finished = false;

                    $playerBuyItem->save();
                    $given = true;
                }
            }

            // プレイヤ製品購入の状態更新

            if ($finished)
                $playerBuy->state = PlayerProductBuy::STATE_FINISHED_GRANTED;
            if ($given)
                $playerBuy->granted_at = $now;
            if ($finished || $given)
                $playerBuy->save();
        }

    }

	/**
	 * ログインボーナス付与
	 *
	 * @param integer $playerId プレイヤID
	 * @param LoginBonus $loginBonus ログインボーナス
	 * @param array $rewardList ログインボーナス報酬リスト
	 * @param integer $playerCount プレイヤログイン回数
	 * @param string $now 処理日時
	 * @return なし
	 */
	private static function _giveLoginBonus(
        $playerId, $loginBonus, $rewardList, $playerCount, $now
    )
	{
        //------------------------------
        // 受け取ったアイテムをプレゼントボックスに追加する
        //------------------------------

        foreach ($rewardList as $reward)
        {
            $playerLoginBonus = PlayerLoginBonus::regist(
                $playerId, $reward, $playerCount, $now
            );
            PlayerPresent::registLoginBonus(
                $playerLoginBonus, $loginBonus, $reward, $now
            );
        }
	}

	/**
	 * ログインポイントを付与する
	 *
	 * @param PlayerLogin $playerLogin プレイヤログイン
	 * @param integer $type ポイント種別
	 * @param string $now 処理日時
	 * @return なし
	 *
	 */
	private static function _giveLoginPoint($playerLogin, $type, $now)
	{
        $playerId = $playerLogin->player_id;
        if (PlayerLoginPoint::alreadyGive($playerId, $type, $now))
            return;

        $_now = new \DateTime($now);

        $point = 0;
        $msg = '';
        switch ($type)
        {
        case PlayerLoginPoint::TYPE_FRIEND:
            $point = PlayerFriend::calcLoginPoint($playerId);
            $msg =
                 date_format($_now , 'm') . "月" .
                 date_format($_now , 'd') . "日分のフレンドポイントです\n" .
                 "※フレンド数が増えると、もらえるフレンドポイントが増加します";
            break;
        default:
            throw \App\Exceptions\UnknownException::make(
                'unknown login bonus point type: ' . $type
            );
            break;
        }

        if ($point > 0)
        {
            // 受け取り期限
            $expiredAt = PlayerPresent::defaultExpiredAt();

            PlayerPresent::regist(
                $playerId,
                Item::TYPE_ITEM,
                Item::ID_FRIEND_POINT,
                $point,
                SrcType::LOGIN_POINT,
                $playerLogin->id,
                $msg,
                $expiredAt
            );
        }

        // ポイント 0 でも履歴は残す
        // (フォローした場合、受け取りは明日から)

        $loginPoint = new PlayerLoginPoint();
        $loginPoint->player_id = $playerId;
        $loginPoint->type = $type;
        $loginPoint->point = $point;
        $loginPoint->created_at = $now; // 進んで、次の日だとよくない
        $loginPoint->save();
    }

	/**
	 * 直接付与アイテムをプレゼントボックスに入れる
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now 処理日時
	 * @return なし
	 *
	 */
	private static function _giveDirectPresent($playerId, $now)
	{
        // 1 プレイヤがそんなにたくさん直接付与されることはない前提 //
        $directIds = PlayerDirectPresent::getDirectPresentIds($playerId);

        // 直接付与検索

        $presents0 = DirectPresent::getByPlayerId(
            $playerId, $directIds, $now
        );
        if (empty($presents0))
            return;

        // フィルター実行
        $_now = new \DateTime($now);
        $presents = [];
        foreach ($presents0 as $present)
        {
            $skip = false;
            switch ($present->type)
            {
            case DirectPresent::TYPE_ALL:
            case DirectPresent::TYPE_PLAYER:
                // チェック済み
                break;
            case DirectPresent::TYPE_LOGIN:
                $isLogin = PlayerLogin::isLogin(
                    $playerId,
                    $present->date1,
                    $present->date2,
                    $now
                );
                if (!$isLogin)
                    $skip = true;
                break;
            default:
                throw \App\Exceptions\DataException::makeInvalid(
                    'direct_present', 'type', $present->type
                );
            }

            if (!$skip)
                $presents[] = $present;
        }
        if (empty($presents))
            return;

        foreach ($presents as $present)
        {
            $days = $present->takable_days;

            $expiredAt = null;
            if ($days > 0)
            {
                $period = $present->takable_days . " days";

                $_expiredAt = new \DateTime($now);
                $_expiredAt->add(
                    \DateInterval::createFromDateString($period)
                );
                $expiredAt = DateTimeUtil::formatDB($_expiredAt);
            }

            // トランザクション開始
            BaseGameModel::beginTransaction();

            $playerPresent = new PlayerDirectPresent();
            $playerPresent->player_id = $playerId;
            $playerPresent->direct_present_id = $present->id;
            $playerPresent->save();

            PlayerPresent::regist(
                $playerId,
                $present->item_type,
                $present->item_id,
                $present->item_num,
                SrcType::DIRECT,
                $playerPresent->id,
                $present->message,
                $expiredAt
            );

            // コミット
            BaseGameModel::commit();
        }
    }


}
