<?php
/**
 * ガチャ のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\GachaListResponse;
use App\Http\Responses\GachaResultListResponse;
use App\Http\Responses\PlayerItemResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerCharacter;
use App\Models\PlayerData;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\PlayerPresent;
use App\Models\PlayerGacha;
use App\Models\PlayerGachaResult;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\GachaName;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\GachaLot;
use App\Models\MasterModels\GachaGroup;
use App\Models\PlayerBlueCrystal;
use App\Services\KpiLogger\GachaItemLogger;
use App\Services\KpiLogger\GachaLogger;
use App\Services\MissionChecker\MissionCheckerGacha;
use App\Utils\DateTimeUtil;

/**
 * ガチャ のサービス
 *
 */
class GachaService extends BaseService
{
	const GACHA_COUNT_SINGLE	= 1;	// 単発ガチャ
	const GACHA_COUNT_TEN		= 2;	// １０連ガチャ

	// 有償のみか否か
	const COMPENSATION_YES		= 1;	// 有償のみ使用
	const COMPENSATION_NO		= 2;	// 有償無償問わず

	/**
	 * ガチャ一覧
	 *
	 * @param GachaListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

		$gachaList = Gacha::getAll($now);

		$body = [
			'gacha_list' => GachaListResponse::make(
                $request->player_id, $gachaList, $now
            )
		];

		$response->body = $body;
		return $response;
	}


	/**
	 * 実行
	 *
	 * @param GachaExecRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function exec($request)
	{
        return static::execImpl($request);
	}

	/**
	 * 引き直し
	 *
	 * @param GachaRetryRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function retry($request)
	{
        return static::execImpl($request);
	}

	/**
	 * 実行
	 *
	 * @param GachaCommitRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function commit($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerGacha::getInstance();
        $missionChecker->init($request->player_id, $now);

        $playerGacha = PlayerGacha::getForRetryCommit(
            $request->player_gacha_id, $request->player_id
        );

        $resultList = PlayerGachaResult::getByPlayerGachaId($playerGacha->id);
        if (empty($resultList))
        {
            throw \App\Exceptions\DataException::make(
                'player_gacha_result is empty. player_gacha_id: '
                . $playerGacha->id
            );
        }

		// プレイヤ取得

		$player = Player::find_($request->player_id);

		// ガチャを取得

		$gacha = Gacha::getOne($playerGacha->gacha_id);
		if (!isset($gacha))
		{
            throw \App\Exceptions\MasterException::makeNotFound(
                'gacha', 'gacha_id', $request->gacha_id
            );
		}

        // 回数制限 //

        $limitCount = PlayerGacha::checkLimitCount(
            $request->player_id, $gacha, $playerGacha->created_at
        );

        // 消費数の計算 //

        $realGachaCount = 0;
        $payCount = 0;
        $isConfirm = false;
        $nowPlayerGachaCeilingPoint = 0;
        static::calcGachaParams(
            $realGachaCount,
            $payCount,
            $isConfirm,
            $playerGacha->gacha_count,
            $gacha
        );

        // 実処理
		{
			BaseGameModel::beginTransaction();

            // アイテム付与

            static::giveResults($gacha, $playerGacha, $resultList);

            // アイテム消費

            $param = new GiveOrPayParam();
            $param->player = $player;
            $param->playerId = $player->id;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = $gacha->item_id;
            $param->count = - $payCount;
            $param->srcType = SrcType::GACHA;
            $param->srcId = $playerGacha->id;

            PlayerService::giveOrPay($param);

            // アイテム付与テーブル保存

            static::saveResults($resultList);

            // プレイヤガチャ更新

            $playerGacha->taked_at = $now;
            $playerGacha->save();

            // ミッション達成チェック

            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerGacha', $playerGacha);
            $missionChecker->addData('playerGachaResultList', $resultList);
            $missionChecker->addData('gacha', $gacha);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);


            //ガチャポイント付与 player $gacha_point + $gacha_count
            $nowPlayerGachaCeilingPoint = GachaCeilingPointService::saveGachaPoint(
                $request->player_id, $playerGacha->gacha_id, $now, $playerGacha->gacha_count
            );

            BaseGameModel::commit();
        }

        return static::makeResponse(
            $request, $playerGacha, $resultList, $player,
            $gacha->item_id, $payCount,
            false, false, $nowPlayerGachaCeilingPoint
        );
    }

	/**
	 * 実行処理
	 *
	 * @param GachaExecRequest/GachaRetryRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function execImpl($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();
        $execDate = $now;

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerGacha::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤ取得

		$player = Player::find_($request->player_id);

        $retry = ($request instanceof \App\Http\Requests\GachaRetryRequest);

        $playerGacha = null;
        $gachaId = null;
        $gachaCount = 0;
        if ($retry)
        {
            $playerGacha = PlayerGacha::getForRetryCommit(
                $request->player_gacha_id, $request->player_id
            );
            $gachaId = $playerGacha->gacha_id;
            $gachaCount = $playerGacha->gacha_count;
            $execDate = $playerGacha->created_at; // 最初の日付
        }
        else
        {
            $gachaId = $request->gacha_id;
            $gachaCount = $request->gacha_count;
        }

		// ガチャを取得

		$gacha = Gacha::getOne($gachaId);
		if (!isset($gacha))
		{
            throw \App\Exceptions\MasterException::makeNotFound(
                'gacha', 'gacha_id', $gachaId
            );
		}

        $canLoop = Gacha::canLoop($gacha);

        // 回数制限 //

        $limitCount = PlayerGacha::checkLimitCount(
            $request->player_id, $gacha, $execDate
        );

        // 消費数の計算 //

        $realGachaCount = 0;
        $payCount = 0;
        $isConfirm = false;
        $nowPlayerGachaCeilingPoint = 0;
        static::calcGachaParams(
            $realGachaCount,
            $payCount,
            $isConfirm,
            $gachaCount,
            $gacha
        );

		// 消費アイテムが足りているかチェック

		PlayerService::canPay(
            $player->id, Item::TYPE_ITEM, $gacha->item_id, $payCount
        );

		// ガチャロット取得

		$gachaLotList = GachaLot::getAll($gacha->gacha_lot_id);

        $confirmLotList = [];
        if ($gacha->confirm > 0 && $isConfirm)
            $confirmLotList = GachaLot::getAll($gacha->confirm);

        // 10000 決め打ちにすると高速化できるが・・・

        $totalRate = GachaLot::calcRateSum($gachaLotList);

        $confirmTotalRate = 0;
        if (count($confirmLotList) > 0)
            $confirmTotalRate = GachaLot::calcRateSum($confirmLotList);


		$resultList	= [];
		{
			BaseGameModel::beginTransaction();

			// ガチャ履歴追加、修正

            if (empty($playerGacha))
            {
                $playerGacha = PlayerGacha::regist(
                    $request->player_id,
                    $gacha,
                    $request->gacha_count,
                    true
                );
            }
            else
            {
                ++ $playerGacha->loop_count;
                $playerGacha->save();
            }

            // ガチャ結果削除

            if ($retry)
            {
                PlayerGachaResult::deleteByPlayerGachaId($playerGacha->id);
            }

            // ガチャ結果計算 (確定枠)

            if ($confirmTotalRate > 0)
            {
                -- $realGachaCount;

                $resultList[] = static::calcResult(
                    $playerGacha, $confirmLotList, $confirmTotalRate,
                    PlayerGachaResult::FLAG_CONFIRMED
                );
            }

            // ガチャ結果計算

			for ($i = 0; $i < $realGachaCount; ++ $i)
			{
                $resultList[] = static::calcResult(
                    $playerGacha, $gachaLotList, $totalRate,
                    PlayerGachaResult::FLAG_NONE
                );
			}

            // 新規フラグ設定

            static::setNewFlags($player->id, $resultList);
            // アイテム付与 (ループ不可の場合)

            if (!$canLoop)
            {
                static::giveResults($gacha, $playerGacha, $resultList);

                // アイテム消費
                $param = new GiveOrPayParam();
                $param->player = $player;
                $param->playerId = $player->id;
                $param->itemType = Item::TYPE_ITEM;
                $param->itemId = $gacha->item_id;
                $param->count = - $payCount;
                $param->srcType = SrcType::GACHA;
                $param->srcId = $playerGacha->id;

                PlayerService::giveOrPay($param);
            }
            else
            {
                // 付与フラグを一部設定する
                static::simulateResults($gacha, $playerGacha, $resultList);
            }

            // アイテム付与テーブル保存
            static::saveResults($resultList);
            // プレイヤガチャ更新

            if (!$canLoop)
            {
                $playerGacha->taked_at = $now;
                $playerGacha->save();
            }

            // ミッション達成チェック

            if (!$canLoop)
            {
                $missionChecker->addData('player', $player);
                $missionChecker->addData('playerGacha', $playerGacha);
                $missionChecker->addData('playerGachaResultList', $resultList);
                $missionChecker->addData('gacha', $gacha);

                $achiveMissionList = [];
                $missionChecker->updateMission($achiveMissionList, false);
            }
            if(!$canLoop)
            {
                //ガチャポイント付与 player $gacha_point + $gacha_count
                $nowPlayerGachaCeilingPoint = GachaCeilingPointService::saveGachaPoint(
                    $request->player_id, $playerGacha->gacha_id, $now, $playerGacha->gacha_count
                );
            }
			BaseGameModel::commit();
		}

        //------------------------------
        // KPIログ登録
        //------------------------------
        /*$logger = new GachaLogger();
        $logger->register($request->player_id, $player->player_lv, $playerGacha, $gachaId, $gachaCount, $now);

        $gachaItemLogger = new GachaItemLogger();
        $gachaItemLogger->addData($resultList, $gacha, $now);*/

        return static::makeResponse(
            $request, $playerGacha, $resultList, $player,
            $gacha->item_id, $payCount,
            $canLoop, true , $nowPlayerGachaCeilingPoint
        );
	}


	/**
	 * ガチャパラメーター計算処理
	 *
	 */
	public static function calcGachaParams(
        &$realGachaCount,
        &$payCount,
        &$isConfirm,
        $gachaCount,
        $gacha
    )
	{
        $realGachaCount = $gachaCount;
		$payCount = 0;
        $isConfirm = false;

		if ($gachaCount < 1)
		{
            throw \App\Exceptions\ParamException::make(
                'gacha_count < 1'
            );
		}
		if ($gachaCount > 10)
		{
            throw \App\Exceptions\ParamException::make(
                'gacha_count > 10'
            );
		}

		if ($gachaCount == 1)
        {
			$payCount = $gacha->single_item;
            $realGachaCount = $gacha->single_item_get_count;
        }
		else if ($gachaCount == 10 && $gacha->ten_item > 0)
        {
            $isConfirm = true; // 確定枠あり
			$payCount = $gacha->ten_item;
            $realGachaCount = $gacha->ten_item_get_count;
        }
		else
        {
			$payCount = $gacha->single_item * $gachaCount;
        }
    }

	public static function calcResult(
        $playerGacha, $gachaLotList, $totalRate, $resultFlag
    )
	{
        // 確率でガチャグループ ID を決める

        $resultLot = null;
        {
            $r = mt_rand(1, $totalRate);

            $rateSum = 0;
            foreach ($gachaLotList as $gachaLot)
            {
                $rateSum += $gachaLot->rate;
                if ($r <= $rateSum)
                {
                    $resultLot = $gachaLot;
                    break;
                }
            }
        }

        assert(isset($resultLot));

        // ガチャグループを取得

        $gachaGroupList = GachaGroup::getAll(
            $resultLot->gacha_group_id
        );

        // 個数チェック

        $groupCount = count($gachaGroupList);
        if ($groupCount <= 0)
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'gacha_group_list', 'gacha_group_id',
                $resultLot->gacha_group_id
            );
        }

        // 確率で出るアイテムを決定する

        $groupIndex = mt_rand(0, $groupCount - 1);
        $resultGroup = $gachaGroupList[$groupIndex];

        // ガチャ結果生成

        return PlayerGachaResult::make(
            $playerGacha,
            $resultGroup,
            $resultFlag
        );
    }

	public static function giveResult($gacha, $playerGacha, $result)
    {
        // アイテム付与

        $param = new GiveOrPayParam();
        $param->playerId = $playerGacha->player_id;
        $param->itemType = $result->item_type;
        $param->itemId = $result->item_id;
        $param->count = $result->item_count;
        $param->srcType = SrcType::GACHA;
        $param->srcId = $playerGacha->id;
        try
        {
            PlayerService::giveOrPay($param);
            $result->take_flag = $param->takeFlag;
            $result->after_repeat = $param->repeat;
        }
        catch (\App\Exceptions\OverflowException $e)
        {
            $result->take_flag = $param->takeFlag;
            $result->after_repeat = $param->repeat;
            // メッセージを計算

            $gachaName = GachaName::getOne_($gacha->gacha_name_id);
            $msg = $gachaName->dictionary_ja . 'で獲得したアイテムです';

            // 蒼の結晶ガチャの場合は無期限

            $expiredAt = null;
            if ($gacha->gacha_type == Gacha::TYPE_FRIEND_POINT)
            {
                $expiredAt = PlayerPresent::defaultExpiredAt();
            }

            // プレゼントボックスに入れる処理

            PlayerPresent::regist(
                $playerGacha->player_id,
                $e->itemType,
                $e->item,
                $e->count,
                SrcType::GACHA,
                $playerGacha->id,
                $msg,
                $expiredAt
            );
        }

        return $result;
    }

	public static function giveResults($gacha, $playerGacha, $resultList)
    {
        foreach ($resultList as $result)
        {
            static::giveResult($gacha, $playerGacha, $result);
        }
    }

	public static function simulateResults($gacha, $playerGacha, $resultList)
    {
        // キャラクター受け取りか、欠片変換になるかを推定する //

        $newCharaIds = [];
        $newFlg = PlayerGachaResult::FLAG_NEW;

        foreach ($resultList as $result)
        {

            if ($result->item_type != Item::TYPE_CHARACTER)
                continue;

            if (($result->gacha_result_flag & $newFlg) == 0)
            {
                // 欠片受け取り
                $result->take_flag = GiveOrPayParam::TAKE_FLAG_CONVERT;
                continue;
            }

            if (in_array($result->item_id, $newCharaIds))
            {
                // 欠片受け取り (新規キャラが重複した)
                $result->take_flag = GiveOrPayParam::TAKE_FLAG_CONVERT;
                continue;
            }

            $result->take_flag = GiveOrPayParam::TAKE_FLAG_CHARACTER;
            $newCharaIds[] = $result->item_id; // 新規入手キャラが被るかも
        }
    }

	public static function saveResults($resultList)
    {
        foreach ($resultList as $result)
        {
            $result->save();
        }
    }

	public static function calcEffectTableId($resultList)
    {
        // 結果から、ガチャ演出のタイプを計算する
        //
        // キャラSS以上/魔道書★5を含む100
        // キャラS以上,魔道書★4を含む200
        // キャラS以上,魔道書★4を含まない300

        $effectTableId = 300;
        foreach ($resultList as $result)
        {
            switch ($result->item_type)
            {
            case Item::TYPE_CHARACTER:
                $character = CharacterBase::getOne_($result->item_id);
                $rarity = $character->character_initial_rarity;
                if ($rarity >= 4)
                    $effectTableId = min($effectTableId, 100);
                else if ($rarity >= 3)
                    $effectTableId = min($effectTableId, 200);
                break;
            case Item::TYPE_GRIMOIRE:
                $grimoire = Grimoire::getOne_($result->item_id);
                $rarity = $grimoire->grimoire_initial_rarity;
                if ($rarity >= 5)
                    $effectTableId = min($effectTableId, 100);
                else if ($rarity >= 4)
                    $effectTableId = min($effectTableId, 200);
                break;
            }
        }

        return $effectTableId;
    }

	public static function makeResponse(
        $request, $playerGacha, $resultList, $player,
        $payItemId, $payCount, $canLoop, $exec , $nowPlayerGachaCeilingPoint = 0
    )
    {
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        if ($exec)
            $effectTableId = static::calcEffectTableId($resultList);
        else
            $effectTableId = 0;

        $item = PlayerItem::getOne($player->id, $payItemId);

		$body = [
            'player_gacha_id' =>
                $canLoop ? $playerGacha->id : 0,
			'gacha_result_list' =>
                GachaResultListResponse::make($resultList),
            'effect_table_id' =>
                $effectTableId,
            'pay_item_id' =>
                $payItemId,
            'pay_item_count' =>
                $payCount,
            'pay_item' =>
                PlayerItemResponse::make($item),
			'free_blue_crystal_num' =>
                $player->freeBlueCrystalNum($now),
			'blue_crystal_num' =>
                $player->chargedBlueCrystalNum($now),
			'friend_point' =>
                $player->friend_point,
            'player_gacha_ceiling_point' =>
                $nowPlayerGachaCeilingPoint,
		];

		$response->body = $body;
		return $response;
    }

	public static function setNewFlags($playerId, &$resultList)
    {
        $characterIds = [];
        $grimoireIds = [];
        foreach ($resultList as $result)
        {
            switch ($result->item_type)
            {
            case Item::TYPE_CHARACTER:
                $characterIds[] = $result->item_id;
                break;
            case Item::TYPE_GRIMOIRE:
                $grimoireIds[] = $result->item_id;
                break;
            case Item::TYPE_ITEM:
                break;
            default:
                // スルーでも問題無し
                assert(false);
                break;
            }
        }

        if (!empty($characterIds))
        {
            $playerCharacters = PlayerCharacter::getByCharacterIds(
                $playerId, $characterIds
            );
        }
        if (!empty($grimoireIds))
        {
            // 削除済みの魔道書も NEW に関係する
            $playerGrimoires = PlayerGrimoire::getByGrimoireIds(
                $playerId, $grimoireIds, NULL
            );
        }

        // フラグの設定 //

        $flg = PlayerGachaResult::FLAG_NEW;

        foreach ($resultList as &$result)
        {
            switch ($result->item_type)
            {
            case Item::TYPE_CHARACTER:
                $count = $playerCharacters->where(
                    'character_id', $result->item_id
                )->count();
                if ($count <= 0)
                    $result->gacha_result_flag |= $flg;
                break;
            case Item::TYPE_GRIMOIRE:
                $count = $playerGrimoires->where(
                    'grimoire_id', $result->item_id
                )->count();
                if ($count <= 0)
                    $result->gacha_result_flag |= $flg;
                break;
            case Item::TYPE_ITEM:
                break;
            default:
                // スルーでも問題無し
                assert(false);
                break;
            }
        }
    }

}
