<?php

namespace App\Models;



/**
 * player_battle_reward:プレイヤバトル報酬 のモデル
 *
 */
class PlayerBattleReward extends BaseGameModel
{
	protected $table = 'player_battle_reward';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 登録
	 *
	 * @param string $playerBattleId プレイヤバトルID
	 * @param integer  $skipNo スキップ番号
	 * @param integer $rewardType 報酬種別
	 * @param integer $rewardList 報酬リスト
	 */
	public static function registAll(
        $playerBattleId, $skipNo, $rewardType, $rewardList
    )
	{
        $models = [];
        
        foreach ($rewardList as $reward)
        {
            $model = new self();
            $model->player_battle_id = $playerBattleId;
            $model->skip_no = $skipNo;
            $model->reward_type = $rewardType;
            $model->frame_count = $reward['frame_count'];
            $model->item_type = $reward['item_type'];
            $model->item_id = $reward['item_id'];
            $model->item_num = $reward['item_num'];
            $model->save();
            
            $models[] = $model;
        }
        
        return $models;
	}
    
	/**
	 * プレイヤバトルIDより取得
	 *
	 * @param string $playerBattleId プレイヤバトルID
	 * @param string $rewardType 報酬種別
	 * @return array 本クラスの配列
	 */
	public static function getByBattleId($playerBattleId, $rewardType)
	{
		$model =
			self::where('player_battle_id', $playerBattleId)
				->where('reward_type', $rewardType)
				->get();

		return $model;
	}
}
