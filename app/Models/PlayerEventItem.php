<?php

    namespace App\Models;

    /**
     * player_event_status:プレイヤイベントステータス のモデル
     *
     */
    class PlayerEventItem extends BaseGameModel{

        protected $table = 'player_event_item';
        protected $primaryKey = 'id';

        /**
         * 取得
         *
         * @param integer $playerId プレイヤID
         * @param integer $itemId アイテムID
         * @return self 本クラス
         */
        public static function getOne($playerId, $itemId, $eventId)
        {
            $model =
                self::where('player_id', $playerId)
                    ->where('event_id', $eventId)
                    ->where('item_id', $itemId)
                    ->first();
            return $model;
        }

        /**
         * 新規登録
         * @param $playerId
         * @return PlayerEventStatus
         */
        public static function insert($playerId, $eventId, $itemId)
        {
            $model = new self();
            $model->player_id = $playerId;
            $model->event_id = $eventId;
            $model->item_id = $itemId;
            $model->num = 0;
            $model->total_num = 0;
            $model->save();

            return $model;
        }

        /**
         * 新規登録
         * @param $playerId
         * @return PlayerEventStatus
         */
        public static function updateNum($id, $itemNum)
        {
            $model = new self();
            $model->id = $id;
            $model->item_num = $itemNum;
            $model->save();

            return $model;
        }


        /**
         * イベントアイテムリスト取得（プレイヤーIDとイベントID）
         * @param $playerId
         * @param $eventId
         * @return
         */
        public static function getListByPlayerIdAndEventId($playerId, $eventId){

            $model = self::where('player_id', $playerId)
                    ->where('event_id', $eventId)
                    ->get();

            return $model;
        }


    }
