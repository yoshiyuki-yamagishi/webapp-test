<?php

namespace App\Models;

/**
 * player_battle_character:プレイヤバトルキャラクタ のモデル
 *
 */
class PlayerBattleCharacter extends BaseGameModel
{
	protected $table = 'player_battle_character';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 登録
	 *
	 * @param string $playerBattleId プレイヤバトルID
	 * @param array $characterList キャラクタリスト
	 */
	public static function register(
        $playerBattle, $characterList
    )
	{
        // あらかじめ、SQL 1 回でオーブの item_id を得る //

        $orbPids = [];
        foreach ($characterList as $chara)
        {
            for ($j = 0; $j < PlayerCharacter::MAX_ORB_EQUIP; ++ $j)
            {
                $prop = 'player_orb_id_' . ($j + 1);
                if (empty($chara->$prop))
                    continue;
                if (in_array($chara->$prop, $orbPids))
                    continue;

                $orbPids[] = $chara->$prop;
            }
        }
        // DebugUtil::e_log('PBC', 'orbPids', $orbPids);

        $orbs = PlayerItem::getByIds($orbPids);
        // DebugUtil::e_log('PBC', 'orbs', $orbs);

        // 登録 //

        $models = [];

        $count = count($characterList);
        for ($i = 0; $i < $count; ++ $i)
        {
            $chara = $characterList[$i];

            if (empty($chara))
                continue;

            $model = new self();
            $model->player_battle_id = $playerBattle->id;
            $model->position = ($i + 1);
            $model->character_id = $chara->character_id;
            $model->spine = $chara->spine; // 強さには関係ない
            $model->evolve = $chara->evolve;
            $model->repeat = $chara->repeat;
            $model->grade = $chara->grade;
            $model->character_lv = $chara->character_lv;

            for ($j = 0; $j < PlayerCharacter::MAX_ORB_EQUIP; ++ $j)
        {
            $prop = 'player_orb_id_' . ($j + 1);

            if (empty($chara->$prop))
                continue;

            $dstProp = 'orb_id_' . ($j + 1);
            $orb = $orbs->find($chara->$prop);
            $model->$dstProp = $orb->item_id;
        }

            $model->active_skill_1_lv = $chara->active_skill_1_lv;
            $model->active_skill_2_lv = $chara->active_skill_2_lv;
            $model->save();

            $models[] = $model;
        }

        return $models;
	}

}
