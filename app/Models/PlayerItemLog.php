<?php

namespace App\Models;
use App\Utils\DateTimeUtil;

/**
 * player_item_log:プレイヤアイテムログ のモデル
 *
 */
class PlayerItemLog extends BaseGameLogModel
{
    public static $target_id_name = 'player_item_id';
    
	protected $table = 'player_item_log';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const TYPE_GIVE = 1;
    const TYPE_PAY = 2;

	/**
	 * アイテムログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}
}
