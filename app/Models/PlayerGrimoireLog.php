<?php

namespace App\Models;
use App\Models\MasterModels\Grimoire;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use Illuminate\Support\Facades\DB;

/**
 * player_grimoire_log:プレイヤ魔道書ログ のモデル
 *
 */
class PlayerGrimoireLog extends BaseGameLogModel
{
    public static $target_id_name = 'player_grimoire_id';
    
	protected $table = 'player_grimoire_log';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const TYPE_GIVE = 1;
    const TYPE_PAY = 2;
    const TYPE_AWAKE = 3;
    const TYPE_SLOT_RELEASE = 4;
    const TYPE_SLOT_EQUIP = 5;

	/**
	 * 覚醒ログを登録
     *
	 * @param object $playerGrimoire プレイヤ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerAwake(
        $playerGrimoire, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $playerGrimoire->player_id;
        $model->player_grimoire_id = $playerGrimoire->id;
        $model->type = self::TYPE_AWAKE;
        $model->value1 = 1;
        $model->value2 = $playerGrimoire->awake;
        $model->value3 = $playerGrimoire->grimoire_id;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}


	/**
	 * 魔道書ログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}

	/**
	 * 魔道書覚醒回数ログ取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array (grimoire_id, awake_count)
	 */
	public static function getAwakeCounts($playerId)
	{
        $query = self::where('player_id', $playerId)
               ->where('type', self::TYPE_AWAKE)
               ->groupBy('value3')
               ->orderBy('value3', 'asc')
               ->select(
                   'value3 as grimoire_id',
                   DB::raw('sum(value1) as awake_count')
               );
        
		$model = $query->get();
		// DebugUtil::e_log('PGL', 'getAwakeCounts', $model);
		return $model;
	}

	/**
	 * 魔道書レアリティ別覚醒回数ログ計算
	 *
	 * @param array $awakeCounts 魔道書覚醒回数ログ
	 * @return array (rarity, awake_count)
	 */
	public static function calcRarityAwakeCounts($awakeCounts)
	{
        $ret = [];
        
        foreach ($awakeCounts as $awakeCount)
        {
            if ($awakeCount->grimoire_id <= 0)
            {
                // コードが古い場合に出力されるデータ //
                continue;
            }
            
            $grimoire = Grimoire::getOne_($awakeCount->grimoire_id);
            $rarity = $grimoire->grimoire_initial_rarity;

            if (!array_key_exists($rarity, $ret))
                $ret[$rarity] = 0;
            
            $ret[$rarity] += $awakeCount->awake_count;
        }

		// DebugUtil::e_log('PGL', 'calcRarityAwakeCounts', $ret);
        return $ret;
    }
    
}
