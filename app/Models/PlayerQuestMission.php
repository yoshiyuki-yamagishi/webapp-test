<?php

namespace App\Models;
use App\Utils\DateTimeUtil;
use App\Models\MasterModels\Mission;

/**
 * player_quest_mission:プレイヤクエストミッション のモデル
 *
 */
class PlayerQuestMission extends BaseGameModel
{
	protected $table = 'player_quest_mission';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
      protected $fillable = [
      //'id',
      */

	/**
	 * プレイヤクエストミッション一覧を取得
	 *
	 * @param string $playerQuestId　プレイヤクエストID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerQuestId($playerQuestId, $now)
	{
		$model =
               self::where('player_quest_id', $playerQuestId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->get();

		return $model;
	}

	/**
	 * 未報告のミッション一覧を取得する
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getUnreported($playerId, $now)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw('progress_count >= count')
               ->where('report_flag', 0)
               ->whereNull('taked_at')
               ->get();

		return $model;
    }
    

	/**
	 * 報酬取得可能なプレイヤミッション一覧を取得
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getTakable($playerId, $now)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw('progress_count >= count')
               ->whereNull('taked_at')
               ->get();

		return $model;
    }

	public function setReported()
	{
        $this->report_flag = PlayerMission::REPORT_FLAG_DONE;
    }
    
}
