<?php

    namespace App\Models;

    /**
     * player_event_status:プレイヤイベントステータス のモデル
     *
     */
    class PlayerEventStatus extends BaseGameModel{

        protected $table = 'player_event_status';
        protected $primaryKey = 'id';

        /**
         * 新規登録
         * @param $playerId
         * @param $eventId
         * @return PlayerEventStatus
         */
        public static function insert($playerId, $eventId)
        {
            $model = new self();
            $model->player_id = $playerId;
            $model->event_id = $eventId;
            $model->point = 0; //初回作成時は0
            $model->clear_percent = 0; //初回作成時は0
            $model->save();

            return $model;
        }

        /**
         * 1件取得（プレイヤーIDとイベントID）
         * @param $playerId
         * @param $eventId
         * @return
         */
        public static function getOneByPlayerIdAndEventId($playerId, $eventId){

            $model = self::where('player_id', $playerId)
                    ->where('event_id', $eventId)
                    ->first();
            return $model;
        }

        /**
         * クリアパーセント更新
         * @param $id
         * @param $clearPercent
         * @return
         */
        public static function updateClearPercent($id, $clearPercent)
        {
            self::where('id',$id)
                ->update(['clear_percent' => $clearPercent]);
            return;
        }

    }
