<?php

namespace App\Models;
use App\Utils\DebugUtil;

/**
 * player_common:プレイヤ共通 のモデル
 *
 */
class PlayerCommon extends BaseCommonModel
{
    const OS_VERSION_LEN = 128;
    const MODEL_NAME_LEN = 128;

	protected $table = 'player_common';
	protected $primaryKey = 'id';
	public $incrementing = true;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	// 有効フラグ
	const VALID_FLAG_NO		= 0;	// 無効
	const VALID_FLAG_YES	= 1;	// 有効

	const PREFIX_PASSWORD	= '50aNkf7W';

    const PLAYER_ID_CHARS = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
    const PLAYER_ID_CHARS_COUNT = 24; // 実行速度のために決め打ち

	/**
	 * プレイヤ共通を登録する
	 *
	 * @param string $uniqueId 端末固有ID
	 * @param integer $osType OS 種別
	 * @param string $osVersion OS バージョン
	 * @param string $modelName 機種名
	 * @return self 本クラス
	 */
	public static function register($uniqueId, $osType, $osVersion, $modelName)
	{
		$playerId = self::createPlayerId();
        $playerDispId = self::createPlayerDispId();
		$dbNo = self::createDbNo($playerId);

		$playerCommon = new self();
		$playerCommon->player_id	= $playerId;
        $playerCommon->player_disp_id = $playerDispId;
		$playerCommon->unique_id	= $uniqueId;
		$playerCommon->os_type	= $osType;
		$playerCommon->os_version	= $osVersion;
		$playerCommon->model_name	= $modelName;
		$playerCommon->db_no		= $dbNo;
		$playerCommon->valid_flag	= self::VALID_FLAG_YES;
		$playerCommon->save();

		return $playerCommon;
	}

	/**
	 * 引継ぎコードとパスワードを登録する
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $passCode 引継ぎコード
	 * @param string $password パスワード
	 * @return self 本クラス
	 */
	public static function registPasscode($playerId, $passcode, $password)
	{
		$model = self::getByPlayerId($playerId);
		if (!isset($model))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_common', 'player_id', $playerId
            );
		}

		$model->passcode = $passcode;
		$model->password = self::_encryptPassword($password);
        $model->valid_flag = PlayerCommon::VALID_FLAG_YES;
        $model->save();

		return $model;
	}

    /**
     * 引き続きデータ処理
     *
     * @param $id
     * @param $uniqueId
     * @param $osType
     * @param $osVersion
     * @param $modelName
     * @throws \App\Exceptions\ApiException
     */
	public static function transitData($id, $uniqueId, $osType, $osVersion, $modelName)
    {
        $model = self::find_($id);

        $model->unique_id   = $uniqueId;
        $model->os_type     = $osType;
        $model->os_version  = $osVersion;
        $model->model_name  = $modelName;
        $model->passcode	= null;
        $model->password	= null;

        $model->save();
    }

	/**
	 * プレイヤIDから取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return self 本クラス
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('valid_flag', self::VALID_FLAG_YES)
               ->first();
		return $model;
	}

	/**
	 * プレイヤIDから取得 (例外スロー)
	 *
	 * @param integer $playerId プレイヤID
	 * @return self 本クラス
	 */
	public static function getByPlayerId_($playerId)
	{
        $model = self::getByPlayerId($playerId);
        if (empty($model))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_common', 'player_id', $playerId
            );
        }

		return $model;
	}

	/**
	 * アカウント連携トークンからから取得
	 *
	 * @param integer $osType OS種別
	 * @param string $accountToken アカウントトークン
	 * @return self 本クラス
	 */
    public static function getByAccountToken($osType, $accountToken)
	{
		$model =
			self::where('os_type', $osType)
				->where('account_token', $accountToken)
				->where('valid_flag', self::VALID_FLAG_YES)
				->first();

		return $model;
	}

	/**
	 * 引継ぎコードとパスワードから取得
	 *
	 * @param string $passcode 引継ぎコード
	 * @param string $password パスワード
	 * @return self 本クラス
	 */
	public static function getByPasscodeAndPassword($passcode, $password)
	{
		$encryptPassword = self::_encryptPassword($password);

		$model =
			self::where('passcode', $passcode)
				->where('password', $encryptPassword)
				->where('valid_flag', self::VALID_FLAG_YES)
				->first();

		return $model;
	}

    /**
     * passcode使用チェック
     *
     * @param $passcode
     * @return boolean
     */
	public static function isUsePasscode($passcode)
    {
        return self::where('passcode', $passcode)->exists();
    }

	/**
	 * プレイヤIDを作成する。
	 *
	 * @return integer プレイヤID
	 */
	public static function createPlayerId()
	{
		while(true)
		{
			$playerId = random_int(100000000, 999999999);
			$model =
				self::where('player_id', $playerId)
					->first();

			if (!isset($model))
			{
				return $playerId;
			}
		}
	}

	/**
	 * 表示用プレイヤIDを作成する。
	 *
	 * @return string 表示用プレイヤID
	 */
	public static function createPlayerDispId()
	{
		while (true)
		{
            $id = '';

            // 4 桁のアルファベット

            for ($i = 0; $i < 4; ++ $i)
            {
                $x = random_int(0, self::PLAYER_ID_CHARS_COUNT - 1);
                $id .= self::PLAYER_ID_CHARS[$x];
            }
            if ($id == 'AAAA')
            {
                continue; // テスト用に確保
            }

            // 4 桁の数字

            $x = random_int(0, 9999);
            $id .= sprintf("%04d", $x);

			$model =
				self::where('player_disp_id', $id)
					->first();

			if (!isset($model))
			{
				return $id;
			}
		}
	}

	/**
	 * Game DB番号を作成する。
	 * 1始まりとする
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer DB番号
	 */
	public static function createDbNo($playerId)
	{
		$dbGameNum = env('DB_GAME_NUM');

		// プレイヤIDの下一桁から、Game DB番号を決定する
		$tmp = (integer) substr($playerId, -1);

		$ret = 1;
		if ($dbGameNum > 0)
		{
			$ret = ($tmp % $dbGameNum) + 1;
		}

		return $ret;
	}

	/**
	 * パスワードを暗号化する
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $password パスワード
	 * @return string 暗号化したパスワード
	 */
	private static function _encryptPassword($password)
	{
		$str = md5(self::PREFIX_PASSWORD . $password);
		return $str;
	}

    /**
     * os_version の整形
     *
     * @param string $value
     * @return void
     */
    public function setOsVersionAttribute($value)
    {
        $this->attributes['os_version'] = mb_substr(
            $value , 0, self::OS_VERSION_LEN
        );
    }

    /**
     * model_name の整形
     *
     * @param string $value
     * @return void
     */
    public function setModelNameAttribute($value)
    {
        $this->attributes['model_name'] = mb_substr(
            $value , 0, self::MODEL_NAME_LEN
        );
    }

}
