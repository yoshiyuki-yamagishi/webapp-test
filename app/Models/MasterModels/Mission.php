<?php

namespace App\Models\MasterModels;
use App\Config\Constants;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;
use \DateTime;

/**
 * mission:ミッション のモデル
 *
 */
class Mission extends BaseMasterModel
{
	protected $table = 'mission';
	protected $primaryKey = 'id';

    const TYPE_DAILY = 1; // 1：デイリー
    const TYPE_NORMAL = 2; // 2：ノーマル
    const TYPE_EVENT = 3; // 3：イベント
    const TYPE_QUEST = 4; // 4：クエスト達成ミッション
    const TYPE_BEGINNER = 5; // 5：初心者

    const TYPE_LIST = [
        self::TYPE_DAILY,
        self::TYPE_NORMAL,
        self::TYPE_EVENT,
        self::TYPE_QUEST,
        self::TYPE_BEGINNER
    ];

    const CLEAR_TYPE_NONE = 0; // チェック不要
    const CLEAR_TYPE_REPORT = 1; // 報告済みになってからクリア可能になる

    // TODO: 完全なリストではない (イベント関連)
    const ST_QUEST_CLEAR = 1; // クエストクリア
    const ST_GACHA_EXEC = 2; // ガチャを回す
    const ST_PLAYER_LV = 3; // プレイヤーレベル到達
    const ST_CHARACTER_GET = 4; // キャラ獲得
    const ST_CHARACTER_EVOLVE = 5; // キャラ進化
    const ST_CHARACTER_REINFORCE = 6; // キャラ強化
    const ST_QUEST_WITH = 7; // キャラを失わずにクエストクリア TODO:未実装
    const ST_CHARACTER_ORB_EQUIP = 8; // オーブ装備 TODO:未実装
    const ST_GRIMOIRE_GET = 9; // 魔道書入手
    const ST_GRIMOIRE_REINFORCE = 10; // 魔道書強化 TODO:未実装
    const ST_SHOP_BUY = 11; // ショップで購入 TODO:未実装
    const ST_MISSION_CLEAR = 12; // 特定ミッションクリア
    const ST_PARTY_UPDATE = 13; // 編成する
    const ST_CHARACTER_GRADE = 14; // キャラグレードアップ TODO:未実装
    const ST_CHARACTER_SKILL_REINFORCE = 15; // アクティブスキル強化
    const ST_GRIMOIRE_AWAKE = 16; // 魔道書覚醒
    const ST_PLAYER_UPDATE = 17; // プロフィール編集
    const ST_LOGIN_DAYS = 18; // ログイン日数count
    const ST_STORY_QUEST_CLEAR = 19; // ストーリークエスト
    const ST_CHARACTER_QUEST_CLEAR = 20; // キャラクタークエスト
    // const ST_ACTIVE_SKILL = 21; // アクティブスキル使用 (廃止)
    const ST_NOT_DEAD = 22; // 〇体倒れずにクリア
    const ST_ACTIVE_SKILL = 23; // アクティブスキル〇回使用してクリア
    const ST_NO_ACTIVE_SKILL = 24; // アクティブスキルを使用せずクリア
    const ST_OD_USE = 25; // ODを使用してクリア
    const ST_DD_USE = 26; // DDを使用してクリア
    const ST_AH_USE = 27; // AHを使用してクリア
    const ST_OD_FINISH = 28; // ODでとどめを刺してクリア
    const ST_DD_FINISH = 29; // DDでとどめを刺してクリア
    const ST_AH_FINISH = 30; // AHでとどめを刺してクリア
    const ST_COMBO_COUNT = 31; // コンボ〇回以上で攻撃
    const ST_FEW_CLEAR = 32; // 〇人以下の編成でクリア
    const ST_ELEMENT_CLEAR = 33; // 〇属性のキャラクターを編成してクリア
    const ST_ELEMENT_FINISH = 34; // 〇属性のキャラクターでとどめを刺してクリア
    const ST_CLEAR = 35; // クリアするだけ
    const ST_EVENT_QUEST_CLEAR_COUNT_ALL =36;   // イベントクエスト〇回クリア（イベントIDでクエストリスト取得）
    const ST_EVENT_QUEST_CLEAR_COUNT_ONE = 37;  // 指定イベントクエスト〇回クリア(指定したイベントのクエストＩＤのみ)
    const ST_EVENT_QUEST_EVENT_ITEM_COUNT = 38; // アイテム取得数
    const ST_EVENT_QUEST_CLEAR_PERCENT = 39;    // クリアパーセント
	/**
	 * ミッションリストを取得
	 *
	 * @param string $now セレクトに使用する日付
	 * @param array $missionTypes mission_type の配列
	 * @return array ミッションのリスト
	 */
	public static function getAll($now, $missionTypes)
	{
        $_now = new DateTime($now);

        $_this = new self();
        $_all = self::_getAll($_this->table);

        $all = [];
        foreach ($_all as $item)
        {
            $startDay = new DateTime($item->start_day);
            $endDay = new DateTime($item->end_day);

            if ($startDay > $_now)
                continue;
            if ($endDay < $now)
                continue;
            if (isset($missionTypes) &&
                !in_array($item->mission_type, $missionTypes))
                continue;

            $all[] = $item;
        }

        // mission_type, idでソート
        if (count($all) > 0) SortUtil::classUSort($all, "mission_type", "id");

        return $all;
    }

    /**
     * IDをキーにしたリストを取得
     *
     * @param string $now セレクトに使用する日付
     * @param array $missionTypes mission_type の配列
     * @return array ミッションのリスト
     */
    public static function getAllByKeyId($now, $missionTypes=[])
    {
        // Typesが配列じゃない場合、強制的に配列にしておく
        if (!is_array($missionTypes)) {
            $tmpArr = [$missionTypes];
            $missionTypes = $tmpArr;
        }
        if (count($missionTypes) <= 0) $missionTypes = self::TYPE_LIST;
        $list = self::getAll($now, $missionTypes);

        // IDがキーの配列に変換
        $allById = array_combine(array_column($list, "id"), $list);

        return $allById;
    }

	/**
	 * ミッションリストを取得
	 *
	 * @param string $now セレクトに使用する日付
	 * @return array ミッションのリスト
	 */
	public static function getDailyNormal($now)
	{
        // TODO: 初心者は廃止された模様
        return self::getAll(
            $now, [self::TYPE_DAILY, self::TYPE_NORMAL, self::TYPE_BEGINNER, Constants::MISSION_TYPE_EVENT]
        );
    }

    /**
     * リリーストリガーがないものを取得
     * @param $now
     * @return mixed
     */
    public static function getPrepareOpenMissionList($now) {
        $modelDatas = [];
        $_model = self::getAll(
	      $now, [self::TYPE_NORMAL, self::TYPE_EVENT]
        );
        foreach($_model as $model){
            $release_triggers = StrUtil::decodeSimpleCsv( $model->release_trigger);
            $triggerDataCount = count($release_triggers);
            if ( $triggerDataCount <= 0 ) {
                //DebugUtil::e_log('yakisoba3', 'hoimi', $model->release_trigger);
                $modelDatas[] = $model;
            }
        }
	    return $modelDatas;
    }

    /**
     * ミッションリストを取得
     *
     * @param string $now セレクトに使用する日付
     * @return array ミッションのリスト
     */
    public static function getEventMissionList($now)
    {
        // TODO: 初心者は廃止された模様
        return self::getAll(
            $now, [Constants::MISSION_TYPE_EVENT]
        );
    }

	/**
	 * ミッションリストを取得
	 *
	 * @param string $now セレクトに使用する日付
	 * @param array $missionIds ミッション ID のリスト
	 * @return array ミッションのリスト
	 */
	public static function getQuest($now, $missionIds)
	{
        $_now = new DateTime($now);

        $_this = new self();
        $all = [];

        foreach ($missionIds as $missionId)
        {
            if ($missionId <= 0)
            {
                // ミッション無し //
                $all[] = null;
                continue;
            }

            $one = self::getOne_($missionId);

            $all[] = $one;
        }

        return $all;
    }

	/**
	 * デイリーミッションかどうか判定する
	 *
	 * @param object $mission ミッション
	 * @return デイリーの場合 true
	 */
    public static function isDaily($mission)
    {
        return $mission->mission_type == Mission::TYPE_DAILY;
    }

	/**
	 * カウント系ミッションかどうか判定する
	 *
	 * @param object $mission ミッション
	 * @return カウント系の場合 true
	 */
    public static function isCountType($mission)
    {
        switch ($mission->mission_success_type)
        {
        case self::ST_QUEST_CLEAR:
        case self::ST_GACHA_EXEC:
        case self::ST_CHARACTER_REINFORCE:
            return true;
        }
        return false;
    }

    public static function isQuestLike($mission)
    {
        switch ($mission->mission_type)
        {
        case self::TYPE_EVENT:
        case self::TYPE_QUEST:
            return true;
        }
        return false;
    }

    public static function missionTypeName($missionType)
    {
        switch ($missionType)
        {
        case self::TYPE_DAILY:
            return 'デイリー';
        case self::TYPE_NORMAL:
            return 'ノーマル';
        case self::TYPE_EVENT:
            return 'イベント';
        case self::TYPE_QUEST:
            return 'クエスト';
        case self::TYPE_BEGINNER:
            return '初心者';
        }

        throw \App\Exceptions\MasterException::makeNotFound(
            '', 'mission_type', $missionType
        );
    }

}

