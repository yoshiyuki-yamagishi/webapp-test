<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;


/**
 * rarity_coefficient:レアリティ係数 のモデル
 *
 */
class RarityCoefficient extends BaseMasterModel
{
	protected $table = 'rarity_coefficient';
	protected $primaryKey = 'rarity';
    
}
