<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;


/**
 * character:キャラクター のモデル
 *
 */
class Character extends BaseMasterModel
{
	protected $table = 'character';
	protected $primaryKey = 'id';

    const MAX_RARITY = 7;

    // TODO: Constが入ったら移動させなさい
    const INITIAL_RARITY_MIN = 1;
    const INITIAL_RARITY_MAX = 4;
    const EVOLVE_PARAM_MIN = 0;
    const EVOLVE_PARAM_MAX = 6;

    // TODO:マスタ移行を考えなさい
    // [初期レア, 進化度] -> スパイン段階
    const SPINE_LV_TABLE = [
        1 => [ 0, 0, 0, 0, 1, 2, 2 ], // 初期レア 1
        2 => [ 0, 0, 0, 1, 2, 2, 2 ], // 初期レア 2
        3 => [ 0, 0, 1, 2, 2, 2, 2 ], // 初期レア 3
        4 => [ 0, 1, 2, 2, 2, 2, 2 ], // 初期レア 4
    ];
    // [初期レア, 進化度] -> イラスト段階
    const ILLUST_LV_TABLE = [
        1 => [ 0, 0, 0, 0, 1, 2, 3 ], // 初期レア 1
        2 => [ 0, 0, 0, 1, 2, 3, 3 ], // 初期レア 2
        3 => [ 0, 0, 1, 2, 3, 3, 3 ], // 初期レア 3
        4 => [ 0, 1, 2, 3, 3, 3, 3 ], // 初期レア 4
    ];

	/**
	 * キャラクタIDからシリーズIDを取得
	 */
	public static function seriesIdFrom($characterId)
	{
        return intdiv($characterId, 100);
	}

	/**
	 * キャラクタIDから欠片IDを取得
	 */
    public static function fragmentIdFrom($characterId)
    {
        return Item::CATEGORY_FRAGMENT * Item::CATEGORY_RANGE
            + $characterId;
    }

	/**
	 * キャラクター段階を計算する
	 * @param $table 対応表
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer キャラクター段階
	 */
    public static function calcReleaseLv($lvTable, $initialRarity, $evolve)
    {
        if ($initialRarity < self::INITIAL_RARITY_MIN || $initialRarity > self::INITIAL_RARITY_MAX)
        {
            throw \App\Exceptions\MasterException::makeInvalid(
                'character_base', 'character_initial_rarity', $initialRarity
            );
        }
        if ($evolve < self::EVOLVE_PARAM_MIN && $evolve > self::EVOLVE_PARAM_MAX)
        {
            throw \App\Exceptions\DataException::makeInvalid(
                'player_character', 'evolve', $evolve
            );
        }

        return $lvTable[$initialRarity][$evolve];
    }

	/**
	 * スパイン開放段階を計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer スパイン開放段階(0-)
	 */
    public static function calcSpineLv($initialRarity, $evolve)
    {
        return self::calcReleaseLv(
            self::SPINE_LV_TABLE, $initialRarity, $evolve
        );
    }

	/**
	 * イラスト開放段階を計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer イラスト開放段階(0-)
	 */
    public static function calcIllustLv($initialRarity, $evolve)
    {
        return self::calcReleaseLv(
            self::ILLUST_LV_TABLE, $initialRarity, $evolve
        );
    }

	/**
	 * 進化数からスパインIDを計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer スパインID
	 */
    public static function calcSpineIdForEvolve($characterId, $initialRarity, $evolve)
    {
        return self::calcSpineId($characterId, self::calcSpineLv($initialRarity, $evolve));
    }

    /**
     * スパインIDを計算する
     * @param $characterId
     * @param $spine
     * @return integer スパインID
     *
     */
    public static function calcSpineId($characterId, $spine)
    {
        return intval("{$characterId}{$spine}");
    }

    /**
	 * 進化数からイラストIDを計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer イラストID
	 */
    public static function calcIllustIdEvolve($characterId, $initialRarity, $evolve)
    {
        return self::calcIllustId($characterId, self::calcIllustLv($initialRarity, $evolve));
    }

    /**
     * イラストIDを計算する
     * @param $characterId
     * @param $illustNum
     * @return integer イラストID
     */
    public static function calcIllustId($characterId, $illustNum)
    {
        return intval("{$characterId}{$illustNum}");
    }
}
