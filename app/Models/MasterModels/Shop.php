<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * shop:ショップのモデル
 *
 */
class Shop extends BaseMasterModel
{
	protected $table = 'shop';
	protected $primaryKey = 'id';

	/**
	 * ショップ一覧取得
	 *
	 * @param integer $category ショップカテゴリー
	 * @return array ショップリスト
	 */
	public static function getByCategory($now, $category = null)
	{
        $_this = new self();
        $all = self::_getAll(
            $_this->table
        );

        $_now = new \DateTime($now);
        
        $ret = [];
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            $endDay = new \DateTime($item->end_day);

            if ($startDay > $_now)
                continue;
            if ($endDay < $now)
                continue;

            if (isset($category) && $category > 0 &&
                $category != $item->shop_category)
                continue;
            
            $ret[] = $item;
        }
        
        usort($ret, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->shop_category, $b->shop_category);
            if ($cmp != 0)
                return $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        return $ret;
	}

}
