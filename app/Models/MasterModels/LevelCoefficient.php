<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;


/**
 * level_coefficient:レベル係数 のモデル
 *
 */
class LevelCoefficient extends BaseMasterModel
{
	protected $table = 'level_coefficient';
	protected $primaryKey = 'level';
    
}
