<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * login_bonus_reward:ログインボーナス のモデル
 *
 */
class LoginBonusReward extends BaseMasterModel
{
	protected $table = 'login_bonus_reward';
	protected $primaryKey = 'id';

	/**
	 * ログインボーナス報酬を取得
	 *
	 * @return array ログインボーナス報酬の配列
	 */
	public static function getAll($loginBonusId)
	{
        $_this = new self();
        $all = self::_getAllEx(
            $_this->table, ['login_bonus_id'], [$loginBonusId]
        );

        usort($all, function ($a, $b) {
            return SortUtil::val_cmp($a->count, $b->count);
        });

        return $all;
	}

	/**
	 * ログインボーナス報酬を取得
	 *
	 * @return array ログインボーナス報酬の配列
	 */
	public static function getAllCount()
	{
        // TODO: count で groupBy
        
        $_this = new self();
        $all = self::_getAll(
            $_this->table
        );

        usort($all, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->login_bonus_id, $b->login_bonus_id);
            if ($cmp != 0)
                return $cmp;
            return SortUtil::val_cmp($a->count, $b->count);
        });

        return $all;
	}

	/**
	 * ログインボーナス報酬を取得
	 *
	 * @param integer $loginBonusId ログインボーナスID
	 * @param integer $count ログインボーナスの何回目か？
	 * @return array ログインボーナス報酬の配列
	 */
	public static function getByCount($loginBonusId, $count)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['login_bonus_id', 'count'], [$loginBonusId, $count]
        );
    }

	/**
	 * 指定の番号のログインボーナス報酬を抽出
	 *
	 * @param array ログインボーナス報酬の配列
	 * @param integer $count ログインボーナスの何回目か？
	 * @return array ログインボーナス報酬の配列
	 */
	public static function filterOnlyCount($list, $count)
	{
        $ret = [];

        foreach ($list as $item)
        {
            if ($item->count == $count)
                $ret[] = $item;
        }

        return $ret;
    }
    
}
