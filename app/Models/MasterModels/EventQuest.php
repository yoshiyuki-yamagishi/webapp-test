<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * event_quest:イベントクエスト のモデル
 *
 */
class EventQuest extends BaseMasterModel
{
	protected $table = 'event_quest';
    protected $primaryKey = 'id';

	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}
    
}
