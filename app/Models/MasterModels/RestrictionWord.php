<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use App\Utils\SortUtil;

/**
 * restriction_word:使用不可文言 のモデル
 *
 */
class RestrictionWord extends BaseMasterModel
{
	protected $table = 'restriction_word';
	protected $primaryKey = 'id';

	/**
	 * 一覧を取得
	 *
	 * @return array 使用不可文言の配列
	 */
    public static function getAll()
    {
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
    }
    
}
