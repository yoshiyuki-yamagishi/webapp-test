<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * character_exp_table:キャラクターレベル のモデル
 *
 */
class CharacterExpTable extends BaseMasterModel
{
	protected $table = 'character_exp_table';
	protected $primaryKey = ['id', 'lv'];

	/**
	 * 全取得
	 *
	 * @param integer $id キャラクタ経験値テーブルID
	 * @return array App/Models/Thrift/CharacterExpTable 経験値テーブルのリスト
	 */
	public static function getAll($id)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['id'], [$id]
        );
	}
    
	/**
	 * キャラクターの経験値から現在のレベルを取得
	 *
	 * @param object $next 次のキャラクターの経験値を返す
	 * @param integer $id キャラクタ経験値テーブルID
	 * @param integer $experience 経験値
	 * @return object キャラクターの経験値
	 */
	public static function getByExperience(&$next, $id, $experience)
	{
        $next = null;
        $list = self::getAll($id);

        usort($list, function ($a, $b) {
            return SortUtil::val_cmp($a->lv, $b->lv);
        });

        $count = count($list);
        if ($count <= 0)
        {
            throw \App\Exceptions\MasterException::make(
                'character_exp_table is empty: ' . $id
            );
        }
        
        for ($i = 0; $i < $count; ++ $i)
        {
            if ($list[$i]->exp_character > $experience)
                break;
        }

        if ($i < $count)
            $next = $list[$i];
        
        return $list[$i - 1];
	}

	/**
	 * 該当の行から指定の行数を取得する
	 *
	 * @param integer $id キャラクタ経験値テーブルID
	 * @param integer $lv レベル
	 * @param integer $count 個数
	 * @return array キャラクター経験値テーブルの配列
	 */
	public static function getLevels($id, $lv, $count)
	{
        $list = self::getAll($id);

        usort($list, function ($a, $b) {
            return SortUtil::val_cmp($a->lv, $b->lv);
        });

        $ret = [];
        foreach ($list as $item)
        {
            if ($item->lv < $lv)
                continue;

            $ret[] = $item;
            -- $count;
            
            if ($count == 0)
                break;
        }

        if (empty($ret))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'character_exp_table', ['id', 'lv'], [$id, $lv]
            );
        }

        return $ret;
	}
    
}
