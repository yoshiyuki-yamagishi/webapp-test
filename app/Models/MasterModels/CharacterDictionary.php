<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * character_dictionary:キャラクターテキストのモデル
 *
 */
class CharacterDictionary extends BaseMasterModel
{
	protected $table = 'chara_enemy_dictionary';
	protected $primaryKey = 'id';

}
