<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * login_bonus_dictionary:ログインボーナステキスト のモデル
 *
 */
class LoginBonusDictionary extends BaseMasterModel
{
	protected $table = 'login_bonus_dictionary';
	protected $primaryKey = 'id';

}
