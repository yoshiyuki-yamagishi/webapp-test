<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * event_dictionary:イベントクエストテキストのモデル
 *
 */
class EventQuestDictionary extends BaseMasterModel
{
	protected $table = 'event_dictionary';
	protected $primaryKey = 'id';

}
