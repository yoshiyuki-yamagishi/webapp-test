<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use App\Utils\SortUtil;

/**
 * restriction_word:状態異常耐性 のモデル
 *
 */
class ResistAbnormal extends BaseMasterModel
{
	protected $table = 'resist_abnormal';
	protected $primaryKey = 'id';

	/**
	 * 一覧を取得
	 *
	 * @return array 状態異常耐性の配列
	 */
    public static function getAll()
    {
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
    }
    
}
