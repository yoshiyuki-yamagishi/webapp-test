<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * player_level:プレイヤレベル のモデル
 *
 */
class PlayerLevel extends BaseMasterModel
{
	protected $table = 'player_level';
	protected $primaryKey = 'lv';

	/**
	 * 全行取得
	 *
	 * @return array プレイヤレベルテーブルの配列
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}

	/**
	 * プレイヤーの経験値から現在のレベルを取得
	 *
	 * @param object $next 次のプレイヤレベルを返す
	 * @param integer $experience 経験値
	 * @return object プレイヤレベル
	 */
	public static function getByExperience(&$next, $experience)
	{
        $next = null;
        $list = self::getAll();

        usort($list, function ($a, $b) {
            return SortUtil::val_cmp($a->lv, $b->lv);
        });

        $count = count($list);
        if ($count <= 0)
        {
            throw \App\Exceptions\MasterException::make(
                'player_level is empty'
            );
        }
        
        for ($i = 0; $i < $count; ++ $i)
        {
            if ($list[$i]->exp_player > $experience)
                break;
        }

        if ($i < $count)
            $next = $list[$i];
        
        return $list[$i - 1];
	}
    
	/**
	 * 該当の行から指定の行数を取得する
	 *
	 * @param integer $lv レベル
	 * @param integer $count 個数
	 * @return array プレイヤレベルの配列
	 */
	public static function getLevels($lv, $count)
	{
        $list = self::getAll();

        usort($list, function ($a, $b) {
            return SortUtil::val_cmp($a->lv, $b->lv);
        });

        $ret = [];
        foreach ($list as $item)
        {
            if ($item->lv < $lv)
                continue;

            $ret[] = $item;
            -- $count;
            
            if ($count == 0)
                break;
        }

        if (empty($ret))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'player_level', 'lv', $lv
            );
        }

        return $ret;
	}

	/**
	 * 経験値上限カット処理
	 *
	 * @param integer $experience プレイヤ経験値
	 * @return boolean true: 変化あり
	 */
	public static function cutExperience(&$experience)
    {
        $levelLimit = Constant::userLevelLimit();
        $model = static::getOne_($levelLimit->value1);
        if ($experience <= $model->exp_player)
            return false;

        $experience = $model->exp_player;
        return true;
    }
    
}
