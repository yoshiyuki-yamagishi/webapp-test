<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * gacha_name:ガチャテキスト のモデル
 *
 */
class GachaName extends BaseMasterModel
{
	protected $table = 'gacha_name';
	protected $primaryKey = 'id';

}
