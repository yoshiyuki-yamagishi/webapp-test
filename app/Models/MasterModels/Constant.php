<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;
use App\Utils\SortUtil;

/**
 * constant:汎用定数 のモデル
 *
 */
class Constant extends BaseMasterModel
{
	protected $table = 'constant';
	protected $primaryKey = 'id';

    const SKILL_UP_COST_BEGIN = 1001;
    const SKILL_UP_COST_END = 1009;
    const CHARACTER_RARITY_UP_COST_BEGIN = 2001;
    const CHARACTER_RARITY_UP_COST_END = 2006;
    const GRADE_CURRENT_LIMIT = 3001;
    const GRADE_UPPER_LIMIT = 3002;
    const CHARACTER_RARITY_LIMIT = 3003;
    const GRIMOIRE_AWAKE_LIMIT = 3004;
    const GRIMOIRE_INCREASE_UNIT = 4001;
    const GRIMOIRE_INCREASE_LIMIT = 4002;
    const POWDER_PURIFY_BEGIN = 5001;
    const POWDER_PURIFY_END = 5004;
    const AL_RECOVER_TIME = 7001;
    const AL_RECOVER_CRYSTAL = 7002;
    const USER_AL_LIMIT = 7003;
    const USER_LEVEL_LIMIT = 7101;
    const USER_NAME_LENGTH = 7102;
    const USER_MESSAGE_LENGTH = 7103;
    const USER_PASSWORD_LENGTH = 7104;
    const FRIEND_POINT_DAILY_MUTUAL = 7201;
    const FRIEND_POINT_DAILY_FOLLOWER = 7202;
    const FRIEND_POINT_DAILY_FOLLOWED = 7203;
    const FRIEND_POINT_DAILY_LIMIT = 7204;
    const FRIEND_RECOMMEND_LEVEL = 7221;
    const FRIEND_RECOMMEND_LOGIN = 7222;
    const BATTLE_CONTINUE_PRICE = 9001;

    // レアリティコスト関連ベースID（レアリティを加算すると該当IDになる）
    const CHARACTER_RARITY_UP_COST_BASE_ID = 2000;

    /**
	 * 全行取得
	 *
	 * @return array 汎用定数テーブルの配列
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}

	/**
	 * 指定範囲取得
	 *
	 * @param integer $minId 最小ID
	 * @param integer $maxId 最大ID
	 * @return array 汎用定数テーブルの配列
	 */
	public static function getRange($minId, $maxId)
	{
        $list = self::getAll();

        usort($list, function ($a, $b) {
            return SortUtil::val_cmp($a->id, $b->id);
        });

        $ret = [];
        foreach ($list as $item)
        {
            if ($item->id >= $minId && $item->id <= $maxId)
                $ret[] = $item;
        }

        $retCount = count($ret);

        if ($retCount != $maxId - $minId + 1)
        {
            throw \App\Exceptions\MasterException::make(
                'constant table is not successive from: '
                . $minId . ' to: ' . $maxId
            );
        }

        return $ret;
    }

	/**
	 * キャラクタースキル強化コスト一覧取得
	 *
	 * @return array 汎用定数テーブルの配列
	 */
    public static function skillUpConsts()
    {
        $model = self::getRange(
            self::SKILL_UP_COST_BEGIN,
            self::SKILL_UP_COST_END
        );
        if (empty($model))
        {
            throw \App\Exceptions\MasterException::make(
                'skill up costs is empty, from: '
                . self::SKILL_UP_COST_BEGIN
            );
        }
        return $model;
    }

	/**
	 * キャラクター進化コスト一覧取得
	 *
	 * @return array 汎用定数テーブルの配列
	 */
    public static function characterRarityUpConstModel($rarity)
    {
        $list = self::getRange(
            self::CHARACTER_RARITY_UP_COST_BEGIN,
            self::CHARACTER_RARITY_UP_COST_END
        );
        if (empty($list))
        {
            throw \App\Exceptions\MasterException::make(
                'character rarity up costs is empty, from: '
                . self::CHARACTER_RARITY_UP_COST_BEGIN
            );
        }
        // IDがキーの配列に変換
        $allById = array_combine(array_column($list, "id"), $list);

        if (!array_key_exists(self::CHARACTER_RARITY_UP_COST_BASE_ID+$rarity, $allById))
        {
            throw \App\Exceptions\MasterException::make(
                '該当レアリティのコストデータがありません: ID->'
                . (self::CHARACTER_RARITY_UP_COST_BASE_ID+$rarity) .
                ' Rarity->'. $rarity
            );
        }

        return $allById[self::CHARACTER_RARITY_UP_COST_BASE_ID+$rarity];
    }

	/**
	 * 魔道書覚醒の上限取得
	 *
	 * @return array 汎用定数テーブル
	 */
    public static function grimoireAwakeLimit()
    {
        return self::getOne_(self::GRIMOIRE_AWAKE_LIMIT);
    }


	/**
	 * グレードの現在の上限取得
	 *
	 * @return array 汎用定数テーブル
	 */
    public static function gradeCurrentLimit()
    {
        return self::getOne_(self::GRADE_CURRENT_LIMIT);
    }

	/**
	 * プレイヤレベルの上限取得
	 *
	 * @return array 汎用定数テーブル
	 */
    public static function userLevelLimit()
    {
        return self::getOne_(self::USER_LEVEL_LIMIT);
    }

}
