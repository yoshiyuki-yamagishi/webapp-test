<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * home_banner:ホームバナー のモデル
 *
 */
class HomeBanner extends BaseMasterModel
{
	protected $table = 'home_banner';
	protected $primaryKey = ['id'];

	// バナータイプ
    const BANNER_TYPE_MAIN = 1;
    const BANNER_TYPE_ROTATE = 2;
    const BANNER_TYPE_ONCE_FORCE = 3;
    const BANNER_TYPE_ALWAYS_FORCE = 4;

	// バナーリストタイプ
	const BANNER_LIST_TYPE_HOME = 0;    // ホーム画面内バナー
	const BANNER_LIST_TYPE_FORCE = 1;   // 強制表示バナー

	/**
	 * 全取得
	 *
	 * @param string $now 現在時刻
	 * @return array App/Models/Thrift/HomeBanner ホームバナー
	 */
	public static function getAll($now)
	{
        $_this = new self();
        $all = self::_getAll($_this->table);

        usort($all, function ($a, $b)
        {
            $cmp = SortUtil::val_cmp($a->banner_type, $b->banner_type);
            if ($cmp != 0)
                return $cmp;
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        $ret = [];
        $_now = new \DateTime($now);
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            $endDay = new \DateTime($item->end_day);

            if ($startDay > $_now)
                continue;
            if ($endDay < $now)
                continue;

            $ret[] = $item;
        }

        return $ret;
	}

    /**
     * 各タイプ毎にpriority、idでソートし、ホーム、強制で分けて配列で返す
     * @param $now
     * @return array
     * @throws \Exception
     */
	public static function getViewList($now)
    {
        $_this = new self();
        $all = self::_getAll($_this->table);

        // バナータイプをキーの配列に入れる
        $setList = [
            self::BANNER_TYPE_MAIN => [],
            self::BANNER_TYPE_ROTATE => [],
            self::BANNER_TYPE_ONCE_FORCE => [],
            self::BANNER_TYPE_ALWAYS_FORCE => []
        ];

        // 期間内のデータを各タイプ毎にセット
        $_now = new \DateTime($now);
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            $endDay = new \DateTime($item->end_day);

            if ($startDay < $_now && $_now < $endDay)
            {
                $setList[$item->banner_type][] = $item;
            }
        }

        // priority, id でソートする
        if (count($setList[self::BANNER_TYPE_MAIN]))
            SortUtil::classUSort($setList[self::BANNER_TYPE_MAIN], "priority", "id");
        if (count($setList[self::BANNER_TYPE_ROTATE]))
            SortUtil::classUSort($setList[self::BANNER_TYPE_ROTATE], "priority", "id");
        if (count($setList[self::BANNER_TYPE_ONCE_FORCE]))
            SortUtil::classUSort($setList[self::BANNER_TYPE_ONCE_FORCE], "priority", "id");
        if (count($setList[self::BANNER_TYPE_ALWAYS_FORCE]))
            SortUtil::classUSort($setList[self::BANNER_TYPE_ALWAYS_FORCE], "priority", "id");

        // ホーム画面用リストと強制表示用リストにして返す
        return [
            self::BANNER_LIST_TYPE_HOME => array_merge($setList[self::BANNER_TYPE_MAIN], $setList[self::BANNER_TYPE_ROTATE]),
            self::BANNER_LIST_TYPE_FORCE => array_merge($setList[self::BANNER_TYPE_ONCE_FORCE], $setList[self::BANNER_TYPE_ALWAYS_FORCE])
        ];
    }

}
