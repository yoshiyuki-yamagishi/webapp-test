<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * story_quest_battle:ストーリクエスト バトル のモデル
 *
 */
class StoryQuestBattle extends BaseMasterModel
{
	protected $table = 'story_quest_battle';
    protected $primaryKey = ['id','wave','position'];

	/**
	 * 全取得
	 *
	 * @param integer $battleId バトルID
	 * @return array App/Models/Thrift/StoryQuestBattle バトルのリスト
	 */
	public static function getAll($battleId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['id'], [$battleId]
        );
	}
    
}
