<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * event_quest_battle: イベントクエスト バトル のモデル
 *
 */
class EventQuestBattle extends BaseMasterModel
{
	protected $table = 'event_quest_battle';
    protected $primaryKey = ['id','wave','position'];

	/**
	 * 全取得
	 *
	 * @param integer $battleId バトルID
	 * @return array App/Models/Thrift/EventQuestBattle バトルのリスト
	 */
	public static function getAll($battleId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['id'], [$battleId]
        );
	}
    
}
