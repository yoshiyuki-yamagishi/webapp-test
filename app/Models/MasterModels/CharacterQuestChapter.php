<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * character_quest_chapter:ストーリクエスト章 のモデル
 *
 */
class CharacterQuestChapter extends BaseMasterModel
{
	protected $table = 'character_quest_chapter';
    protected $primaryKey = 'id';

    /**
	 * モデル取得
	 *
	 * @param object $characterId キャラクターID
	 * @return object App/Models/Thrift/??? モデル
	 */
	public static function getByCharacterId($characterId)
	{
        $_this = new static();
        return self::_getOne_(
            $_this->table,
            'character_base_id',
            $characterId
        );
    }

}
