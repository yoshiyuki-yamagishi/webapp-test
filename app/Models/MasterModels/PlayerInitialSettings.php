<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * player_level:プレイヤレベル のモデル
 *
 */
class PlayerInitialSettings extends BaseMasterModel
{
	protected $table = 'player_initial_settings';
	protected $primaryKey = 'id';

	/**
	 * 生年月日の DB 表現を返す
	 *
	 * @param integer $dateNum 日付を表す整数
	 * @return string DB に設定する日付
	 */
	public static function getBirthDb($dateNum)
	{
        if ($dateNum <= 0)
            return null;

        $month = $dateNum / 100;
        $day = $dateNum % 100;

        // うるう年ありで、全日付を容認する -> 2020 年でやる //
        if (!checkdate($month, $day, 2020))
        {
            throw \App\Exceptions\MasterException::makeInvalid(
                'player_initial_settings', 'birthday', $dateNum
            );
        }
        
        return sprintf('2020-%02d-%02d', $month, $day);
    }
    
}
