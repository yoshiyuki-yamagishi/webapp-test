<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * story_quest_chapter:ストーリクエスト章 のモデル
 *
 */
class StoryQuestChapter extends BaseMasterModel
{
	protected $table = 'story_quest_chapter';
    protected $primaryKey = 'id';

	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}
    
}
