<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * shop_dictionary:ショップテキストのモデル
 *
 */
class ShopDictionary extends BaseMasterModel
{
	protected $table = 'shop_dictionary';
	protected $primaryKey = 'id';

}
