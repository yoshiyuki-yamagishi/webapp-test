<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;



/**
 * gacha_lot:ガチャロット のモデル
 *
 */
class GachaLot extends BaseMasterModel
{
	protected $table = 'gacha_lot';
	protected $primaryKey = ['id', 'group_id'];

	/**
	 * 全取得
	 *
	 * @param integer $gachaLotId ガチャロットID
	 * @return array App/Models/Thrift/GachaLot ガチャロット
	 */
	public static function getAll($gachaLotId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['id'], [$gachaLotId]
        );
	}

    public static function calcRateSum($gachaLotList)
    {
        $sum = 0;
        foreach ($gachaLotList as $gachaLot)
        {
            $sum += $gachaLot->rate;
        }
        return $sum;
    }
}
