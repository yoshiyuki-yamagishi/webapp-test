<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;


/**
 * character_base:キャラクターパラメータ のモデル
 *
 */
class CharacterBase extends BaseMasterModel
{
    const MAX_PASSIVE_COUNT = 10;
    
	protected $table = 'character_base';
	protected $primaryKey = 'id';
    
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}

	/**
	 * キャラクター用の欠片 ID かチェックする
	 *
	 * @param object $characterBase キャラクターベース
	 * @param integer $itemId アイテムID
	 * @return なし
	 */
	public static function checkFragmentId($characterBase, $itemId)
	{
        $cat = Item::categoryFromId($itemId);
        if ($cat != Item::CATEGORY_FRAGMENT)
        {
            throw \App\Exceptions\ParamException::make(
                'item_id is not fragment: ' . $itemId
            );
        }

        $generalPrefix = intdiv($itemId, Item::GENERAL_PIECE_MOD);
        if ($generalPrefix == Item::GENERAL_PIECE_MOD_ID)
        {
            // 汎用欠片
            $element = $itemId % Item::GENERAL_PIECE_MOD;
            if ($element != $characterBase->element)
            {
                throw \App\Exceptions\ParamException::make(
                    'character.element != fragment\'s element: ' .
                    $characterBase->element . ' != ' . $element
                );
            }
            return; // 問題なし
        }

        $fragmentId = Character::fragmentIdFrom($characterBase->id);

        if ($itemId != $fragmentId)
        {
            throw \App\Exceptions\GameException::make(
                'item_id: ' . $itemId .
                ' is not for character: '. $characterBase->id
            );
        }
    }
    
}
