<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;

/**
 * story_quest_quest:ストーリクエストクエスト のモデル
 *
 */
class StoryQuestQuest extends BaseMasterModel
{
	protected $table = 'story_quest_quest';
    protected $primaryKey = ['id', 'story_quest_chapter_id'];
    
	public static function getByChapterId($chapterId)
	{
        $_this = new self();
        return self::_getAllEx(
			$_this->table,
			['story_quest_chapter_id'],
			[$chapterId]
        );
	}

	/**
	 * 取得
	 *
	 * @return App/Models/Thrift/StoryQuestQuest クエストの配列
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
			$_this->table
        );
	}

	public static function getByFirstRewardId($rewardId)
	{
        $_this = new self();
        return self::_getAllEx(
			$_this->table,
			['first_reward_id'],
			[$rewardId]
        );
	}

	public static function getByFixRewardId($rewardId)
	{
        $_this = new self();
        return self::_getAllEx(
			$_this->table,
			['fix_reward_id'],
			[$rewardId]
        );
	}

	public static function getByDropRewardId($rewardId)
	{
        $_this = new self();
        return self::_getAllEx(
			$_this->table,
			['drop_reward_id'],
			[$rewardId]
        );
	}
    
}
