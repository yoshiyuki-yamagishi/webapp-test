<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * drop_reward: ドロップ報酬 のモデル
 *
 */
class DropReward extends BaseMasterModel
{
	protected $table = 'drop_reward';
	protected $primaryKey = 'id';

	/**
	 * 全取得
	 *
	 * @param integer $dropRewardId ドロップ報酬ID
	 * @param integer $dropBoxType 宝箱の種類
	 * @return array App/Models/Thrift/DropReward ドロップ報酬のリスト
	 */
	public static function getAll($dropRewardId, $dropBoxType)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table,
            ['id', 'drop_box_type'],
            [$dropRewardId, $dropBoxType]
        );
	}

	/**
	 * 全取得
	 *
	 * @param integer $dropRewardId ドロップ報酬ID
	 * @return array App/Models/Thrift/DropReward ドロップ報酬のリスト
	 */
	public static function getById($dropRewardId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table,
            ['id'],
            [$dropRewardId]
        );
	}

	/**
	 * 全取得
	 *
	 * @param integer $dropItemId ドロップアイテムID
	 * @return array App/Models/Thrift/DropReward ドロップ報酬のリスト
	 */
	public static function getByItemId($dropItemId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table,
            ['drop_item_id'],
            [$dropItemId]
        );
	}



	
    
}
