<?php

namespace App\Models;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ProductShop;
use App\Models\MasterModels\ProductContents;
use App\Utils\DateTimeUtil;

/**
 * player_product_buy_item:プレイヤ製品購入アイテム のモデル
 *
 */
class PlayerProductBuyItem extends BaseGameModel
{
	protected $table = 'player_product_buy_item';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 登録
	 *
	 * @param object $playerBuy プレイヤ製品購入モデル
	 * @param object $product ProductShop マスタデータ
	 * @param object $content ProductContents マスタデータ
	 * @return self 本クラス
	 */
	public static function register(
        $playerBuy, $product, $content
    )
	{
        $daily = ProductContents::isDaily($content);
        if ($daily)
        {
            // デイリーの場合のデータ整合性チェック
            ProductShop::checkDaily($product);
        }
        
        $grantCount = $product->grant_count;
        if ($grantCount < 0 || ! $daily)
            $grantCount = 1; // 負の場合、デイリーじゃない場合
        
        $model = new PlayerProductBuyItem();
        $model->player_product_buy_id = $playerBuy->id;
        $model->item_type = Item::TYPE_ITEM;
        $model->item_id = $content->item_id;
        $model->item_count = $content->item_count;
        $model->grant_type = $content->grant_type;
        $model->grant_progress = 1;
        $model->grant_count = $grantCount;
        $model->save();
		return $model;
	}

    /**
	 * 取得
	 *
	 * @param integer $playerProductBuyId プレイヤ製品購入ID
	 * @return array 本クラスの配列
	 */
	public static function getDaily($playerProductBuyId)
    {
        $model =
               self::where('player_product_buy_id', $playerProductBuyId)
               ->whereRaw('grant_progress < grant_count')
               ->where('grant_type', ProductContents::GRANT_TYPE_DAILY)
               ->orderBy('created_at', 'asc') // 古いのから処理しちゃう?
               ->get();
        return $model;
    }

    /**
	 * 取得
	 *
	 * @return boolean true: 全部配布済み
	 */
    public function grantedAll()
    {
        return $this->grant_progress >= $this->grant_count;
    }

}
