<?php

namespace App\Models;
use App\Http\Requests\PlayerQuestListRequest;
use App\Services\QuestService;
use App\Utils\DebugUtil;
use App\Models\CacheModels\CharacterQuestLastIds;
use App\Models\CacheModels\StoryQuestLastIds;
use App\Models\CacheModels\FutureEventQuestQuest;

/**
 * player_quest:プレイヤクエスト のモデル
 *
 */
class PlayerQuest extends BaseGameModel
{
	protected $table = 'player_quest';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	const QUEST_CATEGORY_ALL = 0; // 全て
	const QUEST_CATEGORY_STORY = 1; // ストーリ
	const QUEST_CATEGORY_CHARACTER = 2; // キャラクタ
	const QUEST_CATEGORY_EVENT = 3; // イベント

	const MISSION_FLAG_NO = 0; // ミッション未達成
	const MISSION_FLAG_YES = 1; // ミッション達成

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */



	/**
	 * クエスト取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $questCategory クエスト種類
	 * @param integer $chapterId チャプタID
	 * @param integer $questId クエストID
	 * @return self 本クラス
	 */
	public static function getOne(
        $playerId, $questCategory, $chapterId, $questId
    )
	{
		$model =
			self::where('player_id', $playerId)
				->where('quest_category', $questCategory)
				->where('chapter_id', $chapterId)
				->where('quest_id', $questId)
				->first();

		return $model;
	}

	/**
	 * クエスト取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $stageId ステージID
	 * @return self 本クラス
	 */
	public static function getByStageId($playerId, $stageId)
	{
		$model =
			self::where('player_id', $playerId)
				->where('stage_id', $stageId)
				->first();

		return $model;
	}

	/**
	 * クエスト一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $questCategory カテゴリ
	 * @param integer $flags 一覧フラグ
	 * @param string $now 現在DB日時
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId(
        $playerId, $questCategory, $flags, $now
    )
	{
        $needClosed = (
            $flags & PlayerQuestListRequest::FLAG_NEED_CLOSED_EVENT
        ) != 0;

		$q = self::where('player_id', $playerId);

        switch ($questCategory)
        {
        case self::QUEST_CATEGORY_ALL:
            break;
        case self::QUEST_CATEGORY_STORY:
        case self::QUEST_CATEGORY_CHARACTER:
        case self::QUEST_CATEGORY_EVENT:
            $q->where('quest_category', $questCategory);
            break;
        default:
            assert(false);
            return false;
        }

        if (!$needClosed)
        {
            $q->whereRaw(
                '(close_day is null or close_day >= ?)',
                [$now]
            );
        }

        $q->orderBy('quest_category', 'asc')
          ->orderBy('chapter_id', 'asc')
          ->orderBy('quest_id', 'asc');

        $model = $q->get();
		return $model;
	}

    /**
     * クエスト一覧取得()
     *
     * @param integer $playerId プレイヤID
     * @param integer $questCategory カテゴリ
     * @param integer $flags 一覧フラグ
     * @param string $now 現在DB日時
     * @return array 本クラスの配列
     */
    public static function getByPlayerIdAndChapterId($playerId, $chapterId){

        $q = self::where('player_id', $playerId)
             ->where('chapter_id',$chapterId)
             ->orderBy('quest_id', 'asc');

        $model = $q->get();
        return $model;
    }


	/**
	 * 未クリアのものがあるか判定する
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $questCategory クエスト種別
	 * @param string $now 現在日付
	 * @return boolean true:未クリアのものがある
	 */
	public static function isNotCleared($playerId, $questCategory, $now)
    {
        // イベントでは、開催中のイベント ID を動的に計算する必要がある //

        $catSign = '';
        switch ($questCategory)
        {
        case QuestService::QUEST_CATEGORY_STORY:
            $catSign = '[St]';
            $ids = StoryQuestLastIds::get();
            break;
        case QuestService::QUEST_CATEGORY_CHARACTER:
            $catSign = '[Ch]';
            $ids = CharacterQuestLastIds::getActiveIds($playerId);
            break;
        case QuestService::QUEST_CATEGORY_EVENT:
            $catSign = '[Ev]';
            $ids = FutureEventQuestQuest::getActiveIds();
            break;
        default:
            assert(false);
            break;
        }

		$clearQuestCount = self::where('player_id', $playerId)
                    ->where('quest_category', $questCategory)
                    ->where('clear_flag', QuestService::CLEAR_FLAG_YES)
                    ->whereIn('quest_id', $ids)
                    ->count();

        // DebugUtil::e_log('PQ', $catSign . ' ids', $ids);
        // DebugUtil::e_log('PQ', 'clearQuestCount', $clearQuestCount);
        // DebugUtil::e_log('PQ', 'lastIdsCount', count($ids));
        return $clearQuestCount < count($ids);
    }


	/**
	 * 有効なクエスト種類かチェックする
	 *
	 * @param integer $questCategory クエスト種類
	 * @return boolean true:有効、false:無効
	 */
	public static function isCategory($questCategory)
	{
        if (!is_numeric($questCategory))
            return false;

        return
            $questCategory >= self::QUEST_CATEGORY_STORY &&
            $questCategory <= self::QUEST_CATEGORY_CHARACTER;
    }

	/**
	 * 有効なクエスト種類かチェックする (ALL 含む)
	 *
	 * @param integer $questCategory クエスト種類
	 * @return boolean true:有効、false:無効
	 */
	public static function isCategoryFlag($questCategory)
	{
        if (!is_numeric($questCategory))
            return false;

        return
            $questCategory >= self::QUEST_CATEGORY_ALL &&
            $questCategory <= self::QUEST_CATEGORY_CHARACTER;
    }

	/**
	 * ミッションクリア済みか返す
	 *
	 * @param integer $i ミッション番号 (1, 2, 3)
	 * @return boolean true:クリア済み
	 */
    public function isMissionClear($i)
    {
        $propName = sprintf('mission_flag_%d', $i);
        return $this->$propName != self::MISSION_FLAG_NO;
    }

	/**
	 * ミッションクリアフラグを設定する
	 *
	 * @param integer $i ミッション番号 (1, 2, 3)
	 * @param integer $missionFlag MISSION_FLAG_YES / MISSION_FLAG_NO
	 */
    public function setMissionFlag($i, $missionFlag = self::MISSION_FLAG_YES)
    {
        $propName = sprintf('mission_flag_%d', $i);
        $this->$propName = $missionFlag;
    }

	/**
	 * 日毎のクリア回数が必要か返す
	 *
	 * @return boolean true: 日毎のクリア回数が必要
	 */
    public function needDailyClearCount()
    {
        switch ($this->quest_category)
        {
        case QuestService::QUEST_CATEGORY_CHARACTER:
            return true;
        case QuestService::QUEST_CATEGORY_EVENT:
            // 現在の仕様では、デイリークエストのみ true
            // イベントについては不要だが、マスタデータ的に判定困難
            return true;
        }
        return false;
    }

}
