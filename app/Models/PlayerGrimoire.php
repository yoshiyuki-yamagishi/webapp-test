<?php

namespace App\Models;



use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * player_grimoire:プレイヤ魔道書 のモデル
 *
 */
class PlayerGrimoire extends BaseGameModel
{
	protected $table = 'player_grimoire';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const MAX_SLOT_EQUIP = 3; // 最大 3 枠

	const SLOT_RELEASE_FLAG_NO	= 0;	// 未解放
	const SLOT_RELEASE_FLAG_YES	= 1;	// 解放済み

	// ロックフラグ
	const LOCK_FLAG_NO			= 0;	// アンロック
	const LOCK_FLAG_YES			= 1;	// ロック

	// 削除フラグ
	const DELETE_FLAG_NO		= 0;	// 使用可能
	const DELETE_FLAG_YES		= 1;	// 売却や覚醒等で使用不可能

	/**
	 * プレイヤ魔道書取得
	 *
	 * @param integer $id プレイヤ魔道書ID
	 * @return self 本クラス
	 */
	public static function getOne($id)
	{
		$model =
			self::where('id', $id)
				->where('delete_flag', self::DELETE_FLAG_NO)
				->first();

		return $model;
	}

	/**
	 * プレイヤ魔道書取得 (例外スロー)
	 *
	 * @param integer $id プレイヤ魔道書ID
	 * @return self 本クラス
	 */
	public static function getOne_($id)
	{
        $model = self::getOne($id);
        if (empty($model))
        {
            $_this = new static();
            throw \App\Exceptions\DataException::makeNotFound(
                $_this->table, $_this->primaryKey, $id
            );
        }
		return $model;
	}
    
	/**
	 * プレイヤ魔道書一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
			self::where('player_id', $playerId)
				->where('delete_flag', self::DELETE_FLAG_NO)
				->get();

		return $model;
	}

	/**
	 * プレイヤ魔道書の所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer 魔道書の所持数
	 */
	public static function countByPlayerId($playerId)
	{
		$count =
			self::where('player_id', $playerId)
				->where('delete_flag', self::DELETE_FLAG_NO)
				->count();

		return $count;
	}

	/**
	 * 魔道書の所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $grimoireId 魔道書ID
	 * @return integer 魔道書の所持数
	 */
	public static function countGrimoire($playerId, $grimoireId)
	{
		$count =
               self::where('player_id', $playerId)
               ->where('grimoire_id', $grimoireId)
               ->where('delete_flag', self::DELETE_FLAG_NO)
               ->count();
		return $count;
	}

	/**
	 * プレイヤ魔道書の取得を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param boolean $countDeleted true: 削除済みのものも数える
	 * @param string $start 開始日付
	 * @return integer 魔道書の所持数
	 */
	public static function countAcquired(
        $playerId, $countDeleted = true, $start = null
    )
	{
        $query = self::where('player_id', $playerId);

        if (!$countDeleted)
            $query->where('delete_flag', self::DELETE_FLAG_NO);
        if (isset($start))
            $query->where('acquired_at', '>=', $start);
        
		$count = $query->count();
		return $count;
	}

	/**
	 * プレイヤ魔道書一覧を取得
	 *
	 * @param array $ids プレイヤ魔道書IDの配列
	 * @return array 本クラスの配列
	 */
	public static function getByIds($ids)
	{
		$model =
			self::whereIn('id', $ids)
				->get();

		return $model;
	}

	/**
	 * プレイヤ魔道書一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param array $grimoireIds 魔道書IDの配列
	 * @param integer $deleteFlag 削除フラグ
	 * @return array 本クラスの配列
	 */
	public static function getByGrimoireIds(
        $playerId, $grimoireIds, $deleteFlag = self::DELETE_FLAG_NO
    )
	{
        $q = self::where('player_id', $playerId);
        if (is_array($grimoireIds))
        {
            $q->whereIn('grimoire_id', $grimoireIds);
        }
        if (!is_null($deleteFlag))
        {
            $q->where('delete_flag', $deleteFlag);
        }
        
        $q->orderBy('grimoire_id', 'asc');
		// DebugUtil::e_log('PG', 'getByGrimoireIds', $q->toSql());
        return $q->get();
	}
    
	/**
	 * パーティのプレイヤ魔道書一覧を取得
	 *
	 * @param array $playerParty プレイヤパーティ
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerParty($playerParty)
	{
        $ids = [];
        for ($i = 1; $i <= PlayerParty::MAX_POSITIONS; ++ $i)
        {
            $colName = 'player_grimoire_id_' . $i;
            $pid = $playerParty->$colName;
            
            if (isset($pid))
                $ids[] = $pid;
        }

        $_models = self::getByIds($ids);

        $models = [];
        for ($i = 1; $i <= PlayerParty::MAX_POSITIONS; ++ $i)
        {
            $colName = 'player_grimoire_id_' . $i;
            $pid = $playerParty->$colName;
            
            if (empty($pid))
            {
                $models[$i - 1] = null;
                continue;
            }
            
            $models[$i - 1] = $_models->find($pid);
            
            if (empty($models[$i - 1]))
            {
                $_this = new static();
                throw \App\Exceptions\DataException::make(
                    'player_party grimoire not found: ' . $pid
                );
            }
        }
        
        return $models;
	}

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $grimoireId 魔道書ID
	 * @return self
	 */
	public static function regist($playerId, $grimoireId)
	{
		$now = DateTimeUtil::getNOW();
		$model = new self();
		$model->player_id	= $playerId;
		$model->grimoire_id	= $grimoireId;
		$model->awake		= 0;
		$model->slot_1_flag	= self::SLOT_RELEASE_FLAG_NO;
		$model->slot_2_flag = self::SLOT_RELEASE_FLAG_NO;
		$model->slot_3_flag = self::SLOT_RELEASE_FLAG_NO;
		$model->lock_flag	= self::LOCK_FLAG_NO;
		$model->delete_flag	= self::DELETE_FLAG_NO;
		$model->acquired_at	= $now;
		$model->save();
        return $model;
	}

	/**
	 * ロックされているか？
	 *
	 * @return boolean true: ロック, false: ロックされていない
	 */
	public function isLocked()
    {
        return $this->lock_flag == self::LOCK_FLAG_YES;
	}

	/**
	 * ロック/アンロック する
	 *
	 * @param array $ids プレイヤ魔道書IDの配列
	 * @param integer $lockFlag ロックフラグ
	 *
	 */
	public static function lockByIds($ids, $lockFlag)
    {
        self::whereIn('id', $ids)->update(
            ['lock_flag' => $lockFlag]
        );
	}

	/**
	 * スロットが開放されているか？
	 *
	 * @param integer $slotNo スロット番号
	 * @return boolean true: 開放済み, false: 未開放
	 */
	public function isSlotReleased($slotNo)
    {
        $slot = sprintf('slot_%d_flag', $slotNo);
        return $this->$slot == self::SLOT_RELEASE_FLAG_YES;
	}
    
	/**
	 * スロット開放状態を設定する
	 * 
	 * @param integer $slotNo スロット番号
	 * @param integer $flag 開放フラグ
	 */
	public function setSlotReleased(
        $slotNo, $flag = self::SLOT_RELEASE_FLAG_YES
    )
    {
        $slot = sprintf('slot_%d_flag', $slotNo);
        $this->$slot = $flag;
	}

	/**
	 * スロットに装備する
	 * 
	 * @param integer $slotNo スロット番号
	 * @param integer $playerItemId プレイヤアイテムID
	 */
	public function setSlotEquip($slotNo, $playerItemId)
    {
        $slot = sprintf('slot_%d', $slotNo);
        $this->$slot = $playerItemId;
	}
}
