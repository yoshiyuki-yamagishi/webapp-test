<?php

namespace App\Models;

use App\Models\MasterModels\Constant;
use App\Models\MasterModels\PlayerLevel;
use App\Utils\DateTimeUtil;
use App\Utils\MathUtil;
use App\Utils\DebugUtil;

/**
 * player:プレイヤ のモデル
 *
 */
class Player extends BaseGameModel
{
// 	protected $connection = 'mysql_01';
	protected $table = 'player';
	protected $primaryKey = 'id';
	public $incrementing = false;
// 	public $timestamps = false;


	private $_playerData = null;

/*
	protected $table = 'Business';
	protected $primaryKey = 'id';
	//public $incrementing = false;
	//public $timestamps = false;

	protected $fillable = [
		//'id',
	];
*/

	const GENDER_MALE	= 1;	// 男性
	const GENDER_FEMALE	= 2;	// 女性

	public function playerBattle()
	{
		return $this->hasMany('App\Models\PlayerBattle');
	}

	public function playerBlueCrystal()
	{
		return $this->hasMany('App\Models\PlayerBlueCrystal');
	}

	public function playerCharacter()
	{
		return $this->hasMany('App\Models\PlayerCharacter');
	}

	public function playerData()
	{
		return $this->hasMany('App\Models\PlayerData', 'player_id');
	}

	public function playerFollow()
	{
		return $this->hasMany('App\Models\PlayerFollow');
	}

	public function playerGrimoire()
	{
		return $this->hasMany('App\Models\PlayerGrimoire');
	}

	public function playerItem()
	{
		return $this->hasMany('App\Models\PlayerItem');
	}

	public function playerParty()
	{
		return $this->hasMany('App\Models\PlayerParty');
	}

	public function playerPresent()
	{
		return $this->hasMany('App\Models\PlayerPresent');
	}

	public function playerQuest()
	{
		return $this->hasMany('App\Models\PlayerQuest');
	}

	/**
	 * プレイヤの所持している有料蒼の結晶数を取得する
	 *
	 * @return integer 有料蒼の結晶数
	 */
	public function chargedBlueCrystalNum($now)
	{
        return PlayerBlueCrystal::getChargedNum($this->id, $now);
	}

	/**
	 * プレイヤの所持している無料蒼の結晶数を取得する
	 *
	 * @return integer 無料蒼の結晶数
	 */
	public function freeBlueCrystalNum($now)
	{
        return PlayerBlueCrystal::getFreeNum($this->id, $now);
	}

	/**
	 * プレイヤが所持しているポイント数を取得する
	 *
	 * @return integer ポイント数
	 */
	public static function getPlayerPoint($id, $colName)
	{
        return (integer)self::where('id', $id)->sum($colName);
    }

	/**
	 * プレイヤが所持しているP$数を取得する
	 *
	 * @return integer P$数
	 */
	public static function getPDollar($id)
	{
        return self::getPlayerPoint($id, 'platinum_dollar');
	}

	/**
	 * プレイヤが所持している欠片パウダー数を取得する
	 *
	 * @return integer 欠片パウダー数
	 */
	public static function getPowder($id)
	{
        return self::getPlayerPoint($id, 'powder');
	}

	/**
	 * プレイヤが所持している魔素数を取得する
	 *
	 * @return integer 魔素数
	 */
	public static function getElement($id)
	{
        return self::getPlayerPoint($id, 'magic_num');
	}

	/**
	 * プレイヤが所持しているフレンドポイントを取得する
	 *
	 * @return integer フレンドポイント
	 */
	public static function getFriendPoint($id)
	{
        return self::getPlayerPoint($id, 'friend_point');
	}

    /**
     * プレイヤが所持しているイベントアイテムを取得する
     *
     * @return integer フレンドポイント
     */
    public static function getEventItemCount($playerId, $itemId)
    {
        $playerEventItemData = PlayerEventItem::getOne($playerId, $itemId);
        return $playerEventItemData->item_num;
    }


	/**
	 * プレイヤデータ種別からプレイヤデータを取得
	 *
	 * @param integer $playerDataType プレイヤデータ
	 * @return \App\Models\PlayerData
	 */
	private function _getPlayerDataByPlayerDataType($playerDataType)
	{
		$ret = null;

		if (!isset($this->_playerData))
		{
			$this->_playerData = $this->playerData;
		}

		foreach ($this->_playerData as $data)
		{
			if ($data->type == $playerDataType)
			{
				$ret = $data;
				break;
			}
		}
		return $ret;
	}

	/**
	 * AL 更新
	 *
	 * @param string $now 現在日付
	 * @param boolean $save save() する場合 true
	 * @return boolean 更新したか？
	 */
	public function updateAl($now, $save = true)
    {
        $alRecover = Constant::getOne_(Constant::AL_RECOVER_TIME);
        $IT = $alRecover->value1;
        // DebugUtil::e_log('updateAl', 'IT', $IT);
        
        $diff = DateTimeUtil::diffSeconds($now, $this->al_recovery_at);

        if ($this->al >= $this->max_al)
        {
            // 全快の場合、回復予定時刻が前なら、何でもいい
            
            if ($diff <= 0)
                return false;

            // ココには来ないはずだが、
            // 将来に設定されている場合は、今にする
            
            $this->al_recovery_at = $now;

            if ($save)
                $this->save();
            
            return true; // al は変わっていないけど・・・
        }
        
        assert($this->al < $this->max_al);
        
        if ($diff <= 0)
        {
            // 全快した //

            $this->al = $this->max_al;
            
            if ($save)
                $this->save();
            return true;
        }

        $currentAl = $this->al;
        
        // 現在のスタミナを計算する //
        
        $this->al = $this->max_al - MathUtil::intdiv_ceil($diff, $IT);

        // DebugUtil::e_log('Player', 'al', $this->al);
        // DebugUtil::e_log('Player', 'max_al', $this->max_al);
        // DebugUtil::e_log('Player', 'diff', $diff);
        // DebugUtil::e_log('Player', 'now', $now);
        // DebugUtil::e_log('Player', 'recovery_at', $this->al_recovery_at);

        if ($this->al < 0)
            $this->al = 0;
        
        if ($currentAl == $this->al)
            return false;
        
        if ($save)
            $this->save();
        
        return true;
    }

	/**
	 * AL 回復
	 *
	 * @param string $now 現在日付
	 * @param integer $count 増加スタミナ数
	 * @param boolean $save save() する場合 true
	 * @return boolean 更新したか？
	 */
	public function giveAl($now, $count, $checkMax = true, $save = true)
    {
        if ($count <= 0)
            return false;
        
        if ($checkMax && $this->al >= $this->max_al)
        {
            throw \App\Exceptions\OverflowException::makeOverflow(
                '', '', 'al',
                0, 0, $count,
                $this->al
            );
        }

        // AL 増加
        
        $this->al += $count;

        // 付与分だけ、全快予定時刻を戻す
        
        $alRecover = Constant::getOne_(Constant::AL_RECOVER_TIME);
        $IT = $alRecover->value1;
        $this->al_recovery_at = DateTimeUtil::addSecondsToDate(
            $this->al_recovery_at, - $IT * $count
        );
        
        if ($save)
            $this->save();
            
        return true;
    }

	/**
	 * AL 消費
	 *
	 * @param string $now 現在日付
	 * @param integer $count 消費スタミナ数
	 * @param boolean $save save() する場合 true
	 * @return boolean 更新したか？
	 */
	public function useAl($now, $count, $save = true)
    {
        assert($count >= 0);

        $this->updateAl($now, $save);

        if ($count == 0)
            return false;

        if ($this->al < $count)
        {
            throw \App\Exceptions\DataException::makeNotEnough(
                'player', '', 'al',
                $count, $this->al
            );
        }

        $alRecover = Constant::getOne_(Constant::AL_RECOVER_TIME);
        $IT = $alRecover->value1;
        $diff = DateTimeUtil::diffSeconds($now, $this->al_recovery_at);
        if ($diff <= 0)
        {
            // 全快状態 //

            $this->al -= $count;
            
            if ($this->al < $this->max_al)
            {
                // max よりも小さくなった場合に、全快予定時刻を更新する //

                $this->al_recovery_at = DateTimeUtil::addSecondsToDate(
                    $now, $IT * ($this->max_al - $this->al)
                );
            }
            
            if ($save)
                $this->save();
            
            return true;
        }

        // 全快で無い場合 //

        $this->al -= $count; // 消費

        // 消費分だけ、全快予定時刻を進める
        
        $this->al_recovery_at = DateTimeUtil::addSecondsToDate(
            $this->al_recovery_at, $IT * $count
        );
        
        if ($save)
            $this->save();
        
        return true;
    }

	/**
	 * 経験値からレベルを更新する
	 *
	 * @return array 本クラスの配列
	 * @return boolean 
	 */
	public function updateLevel($now, $save = true)
	{
        $exp = $this->experience;
        
        $isLast = false;
        $next = null;
        $playerLevel = PlayerLevel::getByExperience($next, $exp);
        if ($playerLevel->lv <= $this->player_lv)
        {
            // 普通に、経験値不足
            return false;
        }

        // AL 回復量を計算する
        //
        // レベルアップ時のスタミナ回復は
        // 新しいレベルの最大値分増える

        $recover = 0;
        for ($i = $this->player_lv + 1; $i < $playerLevel->lv; ++ $i)
        {
            // ココは、2 レベル以上、一気にあがった場合にくる //
            $_lv = PlayerLevel::getOne($i);
            $recover += $_lv->max_al;
        }
        $recover += $playerLevel->max_al; // 新レベルは取得済み
        // DebugUtil::e_log('updateLevel', 'recover', $recover);

        // レベルの更新
        
		$this->player_lv = $playerLevel->lv; // レベル更新

        // 最大ALの更新
        
        if ($this->max_al < $playerLevel->max_al) // 本来いらない判定
        {
            // テスト用に、増える場合のみ更新する
            $this->max_al = $playerLevel->max_al;
        }

        $this->giveAl($now, $recover, false, $save);
        return true;
    }
    
}
