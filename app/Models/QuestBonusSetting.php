<?php

namespace App\Models;

/**
 * quest_bonus_settin:ボーナス設定 のモデル
 *
 */
class QuestBonusSetting extends BaseCommonModel
{
	protected $table = 'quest_bonus_setting';
	protected $primaryKey = 'id';
	public $incrementing = true;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    /**
     *
     * 1件取得（questidとquestTypeと開催チェック）
     * @param $questId
     * @param $questCatgory
     * @param $now
     * @return self 本クラス
     */
	public static function getOneByChapterIdAndQuestType($chapterId,$questCatgory, $bonusType,$now)
	{

        return
            self::where('chapter_id',$chapterId)
                ->where('quest_category',$questCatgory)
                ->where('bonus_type',$bonusType)
                ->where('start_date', '<=', $now)
                ->where('end_date', '>=', $now)
                ->first();
	}

}
