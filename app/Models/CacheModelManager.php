<?php

namespace App\Models;
use App\Models\CacheModels\StoryQuestLastIds;
use App\Models\CacheModels\CharacterQuestLastIds;
use App\Models\CacheModels\FutureEventQuestQuest;
use App\Models\CacheModel;
use App\Utils\DebugUtil;

/**
 * Cacheモデルの管理クラス
 *
 */
class CacheModelManager
{
	/** キャッシュ用のタグ */
    private static $TAGS = ['cache'];

    // シングルトン //

    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton))
        {
            self::$singleton = new CacheModelManager();
        }
        return self::$singleton;
    }

    // インスタンスデータ //

    private $cacheData = [];

	/**
	 * データを用意する
     *
	 */
    public function reload()
    {
        // 加工済みデータを用意する //

        StoryQuestLastIds::prepare();
        CharacterQuestLastIds::prepare();
        FutureEventQuestQuest::prepare();
    }

	/**
	 * キャッシュに保存
     *
	 * @param string $valueName 保存先のキー
	 * @param object $values 保存する値
	 */
    public function setData($valueName, $values)
    {
        $this->cacheData[$valueName] = $values; // メモリ
        CacheModel::set(static::$TAGS, $valueName, $values); // キャッシュ
    }

	/**
	 * キャッシュから取得
     *
	 * @param string $valueName 保存先のキー
	 */
    public function getData($valueName)
    {
        MasterModelManager::getInstance()->prepare();

        // メモリー内にあれば、それを使用した方が速い

        if (array_key_exists($valueName, $this->cacheData))
            return $this->cacheData[$valueName];

        if (CacheModel::has(static::$TAGS, $valueName))
        {
            $values = CacheModel::get(static::$TAGS, $valueName);

            $this->cacheData[$valueName] = $values;

            // $dbg = $valueName . ' (' . count($values) . ')';
            // DebugUtil::e_log('CMM_Cache', 'hit', $dbg);

            return $values;
        }

        assert(false);
        return [];
    }

}
