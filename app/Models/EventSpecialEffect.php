<?php

namespace App\Models;

use App\Models\BaseCommonModel;

/**
 * event_special_effect:イベント特攻データ のモデル
 *
 */
class EventSpecialEffect extends BaseCommonModel
{
	protected $table = 'event_special_effect';
    protected $primaryKey = ['id'];

	/**
	 * イベント毎取得
	 *
	 * @return App/Models/Thrift/EventQuestQuest クエストの配列
	 */
	public static function getByChapterIdAndBonusType($chapterId, $bonusType)
	{
        $model =
            self::where('event_id', $chapterId)
                ->where('effect_type', $bonusType)
                ->get();
        return $model;

	}

}
