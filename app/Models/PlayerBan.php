<?php

namespace App\Models;
use App\Utils\DebugUtil;

/**
 * player_ban:プレイヤバン のモデル
 *
 */
class PlayerBan extends BaseGameModel
{
    const STATUS_NONE = 0;
    const STATUS_WARN = 1;
    const STATUS_BAN = 2;
    
	protected $table = 'player_ban';
	protected $primaryKey = 'id';

	/**
	 * 現在有効なプレイヤバン取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getOne($playerId, $now)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('start_at', '<=', $now)
               ->where('end_at', '>=', $now)
               ->where('delete_flag', 0)
               ->orderBy('ban_status', 'dsc') // 大きいほど、重大
               ->orderBy('id', 'dsc') // 後に登録の方を前に
               ->first();

		return $model;
	}
    
	/**
	 * プレイヤバン一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}
}
