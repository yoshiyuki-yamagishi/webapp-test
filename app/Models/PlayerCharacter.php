<?php

namespace App\Models;
use App\Utils\DateTimeUtil;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterExpTable;
use App\Models\MasterModels\Gradeup;

/**
 * player_character:プレイヤキャラクタ のモデル
 *
 */
class PlayerCharacter extends BaseGameModel
{
	protected $table = 'player_character';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const MAX_ORB_EQUIP = 6; // 最大 6 枠

	// 属性(キャラIDの2桁目)
	const ATTRIBUTE_FIRE	= 1;	// 火属性
	const ATTRIBUTE_WATER	= 2;	// 水属性
	const ATTRIBUTE_EARTH	= 3;	// 土属性
	const ATTRIBUTE_WIND	= 4;	// 風属性
	const ATTRIBUTE_LIGHT	= 5;	// 光属性
	const ATTRIBUTE_DARK	= 6;	// 闇属性
	const ATTRIBUTE_PALE	= 7;	// 蒼属性
	const ATTRIBUTE_NO		= 8;	// 無属性
	const ATTRIBUTE_OTHER	= 9;	// その他(ADV用)

	/**
	 * キャラクタIDからキャラクタの属性を取得
	 *  ※キャラクタIDの2桁目が属性を意味している
	 *
	 */
	public static function getAttributeByCharacterId($characterId)
	{
		$attribute = substr($characterId, 1, 1);
		return $attribute;
	}

	/**
	 * 取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $characterId キャラクタID
	 * @return self 本クラス
	 */
	public static function getOne($playerId, $characterId)
	{
		$model =
			self::where('player_id', $playerId)
				->where('character_id', $characterId)
				->first();

		return $model;
	}

	/**
	 * プレイヤキャラクタ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}

	/**
	 * プレイヤキャラクタの所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer キャラクタの所持数
	 */
	public static function countByPlayerId($playerId)
	{
		$count =
			self::where('player_id', $playerId)
				->count();

		return $count;
	}

	/**
	 * プレイヤキャラクタスキル強化数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer キャラクタの所持数
	 */
	public static function countSkillReinforce($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->selectRaw('sum(active_skill_1_lv + active_skill_2_lv - 2) as reinforceCount')->first();

        if (empty($model))
            $count = 0;
        else
            $count = (int)$model->reinforceCount;

        return $count;
    }

	/**
	 * プレイヤキャラクタオーブ装備数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer オーブ装備数
	 */
	public static function countOrbEquip($playerId)
	{
        $rawCol =
                'sum((grade-1)*6' .
                '+if(player_orb_id_1 is NULL,0,1)' .
                '+if(player_orb_id_2 is NULL,0,1)' .
                '+if(player_orb_id_3 is NULL,0,1)' .
                '+if(player_orb_id_4 is NULL,0,1)' .
                '+if(player_orb_id_5 is NULL,0,1)' .
                '+if(player_orb_id_6 is NULL,0,1)' .
                ') as orbEquipCount';

		$model =
               self::where('player_id', $playerId)
               ->selectRaw($rawCol)->first();

        if (empty($model))
            $count = 0;
        else
            $count = (int)$model->orbEquipCount;

        return $count;
    }

	/**
	 * プレイヤキャラクタ一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param array $characterIds キャラクタIDの配列
	 * @return array 本クラスの配列
	 */
	public static function getByCharacterIds($playerId, $characterIds)
	{
        $q = self::where('player_id', $playerId);
        if (is_array($characterIds))
        {
            $q->whereIn('character_id', $characterIds);
        }
        $q->orderBy('character_id', 'asc');
        return $q->get();
	}

	/**
	 * プレイヤキャラクタ一覧を取得
	 *
	 * @param array $ids プレイヤキャラクタIDの配列
	 * @return array 本クラスの配列
	 */
	public static function getByIds($ids)
	{
		$model =
			self::whereIn('id', $ids)
				->get();

		return $model;
	}

	/**
	 * パーティのプレイヤキャラクタ一覧を取得
	 *
	 * @param array $playerParty プレイヤパーティ
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerParty($playerParty)
	{
        $ids = [];
        for ($i = 1; $i <= PlayerParty::MAX_POSITIONS; ++ $i)
        {
            $colName = 'player_character_id_' . $i;
            $pid = $playerParty->$colName;

            if (isset($pid))
                $ids[] = $pid;
        }

        $_models = self::getByIds($ids);

        $models = [];
        for ($i = 1; $i <= PlayerParty::MAX_POSITIONS; ++ $i)
        {
            $colName = 'player_character_id_' . $i;
            $pid = $playerParty->$colName;

            if (empty($pid))
            {
                $models[$i - 1] = null;
                continue;
            }

            $models[$i - 1] = $_models->find($pid);

            if (empty($models[$i - 1]))
            {
                $_this = new static();
                throw \App\Exceptions\DataException::make(
                    'player_party character not found: ' . $pid
                );
            }
        }

        return $models;
	}

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $characterId キャラクタID
	 * @param integer $characterLv キャラクタレベル
	 * @param integer $experience 経験値
	 * @return self
	 */
	public static function regist(
        $playerId, $characterId, $evolve = 0, $characterLv = 1, $experience = 0, $repeat = 0
    )
	{
		$now = DateTimeUtil::getNOW();

		$model = new self();
		$model->player_id = $playerId;
		$model->character_id = $characterId;
		$model->evolve = $evolve;
		$model->character_lv = $characterLv;
		$model->experience = $experience;
        $model->repeat = $repeat;
		$model->player_orb_id_1 = null;
		$model->player_orb_id_2 = null;
		$model->player_orb_id_3 = null;
		$model->player_orb_id_4 = null;
		$model->player_orb_id_5 = null;
		$model->player_orb_id_6 = null;
		$model->active_skill_1_lv = 1;
		$model->active_skill_2_lv = 1;
		$model->acquired_at = $now;
		$model->save();

		return $model;
	}

    /**
     * キャラクター重複ボーナス更新
     * @param $id
     * @param $repeat
     * @return PlayerCharacter
     */
    public static function updateRepeatCount(
        $id , $repeat
    )
    {
        $model = new self();
        $model->where('id',$id)
            ->update(['repeat' => $repeat]);
        return $model;
    }

	/**
	 * 経験値からレベルを更新する
	 *
	 * @param integer $playerLv プレイヤLV
	 * @return true: 更新あり
	 */
    public function updateLevel($playerLv)
    {
        // キャラクタレベル取得
        $charaId = $this->character_id;

        $charaBase = CharacterBase::getOne($charaId);

        $nextLevel = null;

        $level = CharacterExpTable::getByExperience(
            $nextLevel,
            $charaBase->character_exp_table_id,
            $this->experience
        );

        $newLv = $level->lv;
        $levelupLimitFlag = false;
        // キャラクターレベルの上限はプレイヤレベル
        if ($newLv > $playerLv) {
            $newLv = $playerLv;
            $levelupLimitFlag = true;
        }
        //レベルキャップで補正されたら経験値も補正する
        if($levelupLimitFlag)
        {
            //補正されたレベルから次のレベルまでの経験値テーブルデーター
            $nextCharacterExpTableData = CharacterExpTable::getLevels($charaBase->character_exp_table_id, $newLv + 1, 1);
            $nextCharacterExpTable = array_first($nextCharacterExpTableData);
            $requireNextLevelUpExp = $nextCharacterExpTable->exp_character;

            //経験値はすでに更新されている
            $nowPlayerCharacterExp = $this->experience;

            //現在経験値が次のレベルアップ経験値を超える場合経験値も補正
            if($nowPlayerCharacterExp >= $requireNextLevelUpExp)
            {
                $this->experience = $requireNextLevelUpExp - 1;
            }
        }

        // レベルは下げない (現在のところ、LV 低下ログは無いため落ちる)
        if ($newLv <= $this->character_lv)
            return false;

        // プレイヤキャラクタのレベルを更新
        $this->character_lv = $newLv;
        return true;
    }

	/**
	 * パラメーターを計算する
	 *
	 * @param integer $baseValue 基本値
	 * @param array $coeffs 倍率
	 * @return integer パラメーター
	 */
    public static function calcParam($baseValue, $coeffs)
    {
        $value = floatval($baseValue); // 一応、変換
        foreach ($coeffs as $coeff)
            $value *= $coeff;

        return (int)round($value);
    }

	/**
	 * グレードアップによるパラメーター上昇値を計算する
	 *
	 * @param integer $gradupId グレードアップID
	 * @return list パラメーターの連想配列
	 */
    public function calcGradeupParams($gradeupId)
    {
        $ret = [
            'hp' => 0,
            'atk' => 0,
            'def' => 0,
        ];

        // オーブ装備取得 (マスタ)

        $gradeups = Gradeup::getAll($gradeupId);

        // 全装備状態で初期化する

        $equipFlags = array_pad([], self::MAX_ORB_EQUIP, true);

        for ($i = 1; $i <= $this->grade; ++ $i)
        {
            if ($gradeups[$i - 1]->gradeup_stage != $i)
            {
                throw \App\Exceptions\MasterException::make(
                    'gradeup table is invalid: ' . $gradeupId
                );
            }

            if ($i == $this->grade)
            {
                // 最後は、装備しているもののみで、パラメータ上昇する

                for ($j = 1; $j <= self::MAX_ORB_EQUIP; ++ $j)
                {
                    $pcOrbProp = 'player_orb_id_' . $j;
                    $equipFlags[$j - 1] = !empty($this->$pcOrbProp);
                }
            }

            for ($j = 1; $j <= self::MAX_ORB_EQUIP; ++ $j)
            {
                if (!$equipFlags[$j - 1])
                    continue;

                $upTypeProp = 'up_type' . $j;
                $upValueProp = 'up_value' . $j;

                $upType = $gradeups[$i - 1]->$upTypeProp;
                $upValue = $gradeups[$i - 1]->$upValueProp;

                $key = Gradeup::upTypeToKey($upType);
                $ret[$key] += $upValue;
            }
        }

        return $ret;
    }

}
