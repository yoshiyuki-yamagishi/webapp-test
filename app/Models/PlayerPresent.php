<?php

namespace App\Models;

use App\Config\Constants;
use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\LoginBonusDictionary;
use App\Services\GiveOrPayParam;
use App\Services\SrcType;
use App\Utils\DateTimeUtil;

/**
 * player_present:プレイヤプレゼント のモデル
 *
 */
class PlayerPresent extends BaseGameModel
{
	protected $table = 'player_present';
	protected $primaryKey = 'id';

	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	// 受取フラグ
	const TAKE_FLAG_NO = 0;	// 未受取
	const TAKE_FLAG_YES = 1;	// 受取済み

    const DEFAULT_PERIOD_DAYS = 30;

	/**
	 * 取得
	 *
	 * @param integer $playerPresentId プレイヤプレゼントID
	 * @param boolean $takeFlag 受取フラグ
	 * @return self 本クラス
	 */
	public static function getOne($playerPresentId, $takeFlag = null)
	{
		$model = self::where('id', $playerPresentId);

		if (isset($takeFlag))
		{
			$model->where('take_flag', $takeFlag);
		}

		return $model->first();
	}

	/**
	 * プレイヤプレゼント取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now 期限切れをチェック用の日付
	 * @param string $takeFlag 受け取りフラグ
	 * @param string $count 件数
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId(
        $playerId, $now, $takeFlag, $count
    )
	{
        $taken = ($takeFlag == self::TAKE_FLAG_YES);

        $q = self::where('player_id', $playerId)
           ->where('take_flag', $takeFlag);

        // 取得済みリストには、expired_at は関係無い
        if (!$taken){
            $q->where(function($q) use($now){
                $q->where('expired_at', '>=', $now)
                  ->orWhereNull('expired_at');
            });
        }

		if (isset($count) && $count > 0)
			$q->limit($count);

        if ($taken)
            $q->orderBy('taked_at', 'desc');
        else
            $q->orderBy('created_at', 'desc');

        $model = $q->get();
		return $model;
	}

    /**
     * プレイヤプレゼント取得(type絞り)
     *
     * @param integer $playerId プレイヤID
     * @param string $now 期限切れをチェック用の日付
     * @param string $takeFlag 受け取りフラグ
     * @param string $count 件数
     * @return array 本クラスの配列
     */
    public static function getByPlayerIdAndItemType(
        $playerId, $now, $takeFlag, $itemType,$count
    )
    {

        $q = self::where('player_id', $playerId)
            ->where('take_flag', $takeFlag)
            ->where('item_type', $itemType);

        // 取得済みリストには、expired_at は関係無い
        $q->where(function($q) use($now){
            $q->where('expired_at', '>=', $now)
              ->orWhereNull('expired_at');
        });

        if (isset($count) && $count > 0)
            $q->limit($count);

        $q->orderBy('created_at', 'desc');

        $model = $q->get();
        return $model;
    }

    /**
     * プレイヤプレゼント取得(その他用)
     *
     * @param integer $playerId プレイヤID
     * @param string $now 期限切れをチェック用の日付
     * @param string $takeFlag 受け取りフラグ
     * @param string $count 件数
     * @return array 本クラスの配列
     */
    public static function getByPlayerIdForOther(
        $playerId, $now, $takeFlag, $count
    )
    {

        $q = self::where('player_id', $playerId)
            ->where('take_flag', $takeFlag)
            ->where('item_type', '>', Constants::ITEM_TYPE_ITEM);

        // 取得済みリストには、expired_at は関係無い
        $q->where(function($q) use($now){
            $q->where('expired_at', '>=', $now)
                ->orWhereNull('expired_at');
        });

        if (isset($count) && $count > 0)
            $q->limit($count);

        $q->orderBy('created_at', 'desc');

        $model = $q->get();
        return $model;
    }

	/**
	 * 取得可能なプレイヤプレゼントの数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now 期限切れをチェック用の日付
	 * @return array 本クラスの配列
	 */
	public static function getTakableCount($playerId, $now)
	{

        $q = self::where('player_id', $playerId);
        $q->where('take_flag', self::TAKE_FLAG_NO);
        $q->where(function($q) use($now){
            $q->where('expired_at', '>=', $now)
              ->orWhereNull('expired_at');
        });

        $model = $q->count();
        return $model;

}

		/**
	 * 取得可能なプレイヤプレゼントを取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now 期限切れをチェック用の日付
	 * @return array 本クラスの配列
	 */
	public static function get($playerId, $now)
	{
        return
            self::where('player_id', $playerId)
            ->where('take_flag', self::TAKE_FLAG_NO)
            ->where('expired_at', '>=', $now)
            ->get();
	}


	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $itemType 種別
	 * @param integer $itemId 汎用ID
	 * @param integer $itemNum プレゼント数
	 * @param integer $srcType 取得元種別
	 * @param integer $srcId 取得元ID
	 * @param string $message メッセージ
	 * @param string $pic 画像
	 * @param integer $takeFlag 受取フラグ
	 * @param string $takedAt 受取日時(Y-m-d H:i:s形式)
	 * @param string $expiredAt 受取期限(Y-m-d H:i:s形式)
	 * @return self
	 */
	public static function regist(
        $playerId,
        $itemType,
        $itemId,
        $itemNum,
        $srcType,
        $srcId,
        $message,
        $expiredAt = null,
        $takeFlag = self::TAKE_FLAG_NO,
        $takedAt = null
    )
	{
		$model = new self();
		$model->player_id = $playerId;
		$model->item_type = $itemType;
		$model->item_id = $itemId;
		$model->item_num = $itemNum;
		$model->src_type = $srcType;
		$model->src_id = $srcId;
		$model->message = $message;
		$model->take_flag = $takeFlag;
		$model->taked_at = $takedAt;
		$model->expired_at = $expiredAt;
		$model->save();
        return $model;
	}

	/**
	 * 登録
	 *
	 * @param string $playerLoginBonus プレイヤログインボーナス
	 * @param array $loginBonus ログインボーナス
	 * @param string $now 獲得日付
	 * @return self
	 */
	public static function registLoginBonus(
        $playerLoginBonus, $loginBonus, $rewardItem, $now
    )
	{
        $loginBonusName = LoginBonusDictionary::getOne(
            $loginBonus->login_bonus_description
        );
        if (empty($loginBonusName))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'login_bonus_dictionary', 'id',
                $loginBonus->login_bonus_description
            );
        }

        $msg = $loginBonusName->dictionary_ja . $rewardItem->count
             . '日目のボーナスです';

        return self::regist(
            $playerLoginBonus->player_id,
            $rewardItem->remuneration_item_type,
            $rewardItem->remuneration,
            $rewardItem->number,
            SrcType::LOGIN_BONUS,
            $playerLoginBonus->id,
            $msg,
            self::defaultExpiredAt()
        );
    }

	/**
	 * 蒼の結晶、デイリーパック 登録
	 *
	 * @param PlayerProductBuy $playerBuy プレイヤ製品購入
	 * @param PlayerProductBuyItem $playerBuyItem プレイヤ製品購入アイテム
	 * @param string $now 獲得日付
	 * @return self
	 */
	public static function registerDaily(
        $playerBuy, $playerBuyItem, $now
    )
	{
        $msg = '蒼の結晶デイリーパック'
             . $playerBuyItem->grant_progress . '/'
             . $playerBuyItem->grant_count . '回目の獲得アイテムです';

        return self::regist(
            $playerBuy->player_id,
            $playerBuyItem->item_type,
            $playerBuyItem->item_id,
            $playerBuyItem->item_count * $playerBuy->num,
            SrcType::DAILY_PACK,
            $playerBuy->id,
            $msg
        );
    }

	/**
	 * 既定の受取期限を返す
	 *
	 * @return string 受取期限 (DB 形式)
	 */
	public static function defaultExpiredAt()
    {
        $period = self::DEFAULT_PERIOD_DAYS . " days";

        $expiredAt = clone ApiResponse::getInstance()->currentDate;
        $expiredAt->add(
            \DateInterval::createFromDateString($period)
        );

        return DateTimeUtil::formatDB($expiredAt);
    }

}
