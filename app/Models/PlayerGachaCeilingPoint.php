<?php
/**
 * プレイヤーのガチャポイントモデル
 */

namespace App\Models;


class PlayerGachaCeilingPoint extends BaseGameModel
{
    protected $table = 'player_gacha_ceiling_point';
    protected $primaryKey = 'id';

    /**
     * @param $player_id
     * @param $ceiling_id
     * @return self ユーザーのガチャポイントデーター
     */
    public static function getByPlayerIdAndCeilingId($player_id, $ceiling_id)
    {
        $model = self::where('player_id', $player_id)
                ->where('ceiling_id', $ceiling_id)
                ->first();
        return $model;
    }

    /**
     * ユーザーガチャポイントデーター作成
     * @param $player_id
     * @param $ceiling_id
     * @param self ポイントデーター
     */
    public static function regist($player_id, $ceiling_id)
    {

            $model = self::getByPlayerIdAndCeilingId($player_id, $ceiling_id);
            //データー無しは作成
            if(!isset($model)){
                $model = new self();
                $model->player_id = $player_id;
                $model->ceiling_id = $ceiling_id;
                $model->gacha_point = 0;
                $model->save();
            }
            return $model;
    }

    /**
     * ガチャポイント更新
     * @param $player_id
     * @param $ceiling_id
     * @param $gacha_point
     */
    public static function playerCeilingPointSave($player_id, $ceiling_id, $gacha_point)
    {
            $model = self::getByPlayerIdAndCeilingId($player_id, $ceiling_id);
            $model->gacha_point = $model->gacha_point + $gacha_point ;
            $model->save();
            return $model;
    }

}
