<?php

namespace App\Models;

use App\Utils\DebugUtil;

/**
 * VersionData:バージョンデータ のモデル
 *
 */
class VersionData extends BaseCommonModel
{
	protected $table = 'version_data';
	protected $primaryKey = 'id';

    /**
     * 最新の情報を取得
     * @return array
     */
	public static function getNewestOne($platform)
    {
        $ret = self::where('platform', $platform)->orderBy('id', 'desc')->first();
        DebugUtil::e_log("ver","pla",$platform);
        // データがなければエラー
        if (!$ret) {
            throw \App\Exceptions\DataException::makeNotFound(
                'version_data',
                'platform',
                $platform
            );
        }

        return $ret;
    }
}
