<?php
/**
 * ガチャスケジュール
 */

namespace App\Models;

class GachaCeilingSchedule extends BaseCommonModel
{
    protected $table = 'gacha_ceiling_schedule';
    protected $primaryKey = 'id';

    /**
     * スケジュール取得1件
     * @param
     * @return
     */
    public static function getGachaCeilingScheduleDataByGachaId($gachaId)
    {
        $model = self::where('gacha_id', $gachaId)
                ->first();
        return $model;

    }
}
