<?php
/**
 * ガチャ天井交換データーモデル
 *
 */

namespace App\Models;


class GachaCeilingPointExchange extends BaseCommonModel
{
    protected $table = 'gacha_ceiling_point_exchange';
    protected $primaryKey = 'id';


    /**
     * @param $ceilingId
     * @return mixed
     */
    public static function getByCeilingId($ceilingId)
    {
            $model = self::where('ceiling_id', $ceilingId)
                        ->get();
            return $model;
    }

    /**
     * @param $ceilingId
     * @param $exchangeId
     * @return mixed
     */
    public static function getByCeilingIdAndExchnageId($ceilingId, $exchangeId)
    {
        $model = self::where('ceiling_id', $ceilingId)
                ->where('exchange_id', $exchangeId)
                ->first();
        return $model;
    }
}
