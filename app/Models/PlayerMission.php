<?php

namespace App\Models;
use App\Models\MasterModels\Mission;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * player_mission:プレイヤミッション のモデル
 *
 */
class PlayerMission extends BaseGameModel
{
	protected $table = 'player_mission';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

    const REPORT_FLAG_NONE = 0;
    const REPORT_FLAG_DONE = 1;

	/*
      protected $fillable = [
      //'id',
      */

	/**
	 * プレイヤミッション一覧を取得
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId, $now)
	{
        $cond = self::composeDailyCond($now);

		$model =
               self::where('player_id', $playerId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw($cond)
               ->get();

		return $model;
	}

    /**
     * プレイヤミッション一覧を取得(カテゴリー絞り込み)
     *
     * @param $playerId
     * @param $now
     * @param $missionType
     * @return array 本クラスの配列
     */
    public static function getByPlayerIdAndMissionType($playerId, $now, $missionType)
    {
        $cond = self::composeDailyCond($now);

        $model =
            self::where('player_id', $playerId)
                ->where('start_day', '<=', $now)
                ->where('mission_type', $missionType)
                ->where('end_day', '>=', $now)
                ->whereRaw($cond)
                ->get();

        return $model;
    }

	/**
	 * 未報告のミッション一覧を取得する
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getUnreported($playerId, $now)
	{
        $cond = self::composeDailyCond($now);

		$model =
               self::where('player_id', $playerId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw('progress_count >= count')
               ->where('report_flag', 0)
               ->whereNull('taked_at')
               ->whereRaw($cond)
               ->get();

		return $model;
    }


	/**
	 * 報酬取得可能なプレイヤミッション一覧を取得
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getTakable($playerId, $now)
	{
        $cond = self::composeDailyCond($now);

		$model =
               self::where('player_id', $playerId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw('progress_count >= count')
               ->whereNull('taked_at')
               ->whereRaw($cond)
               ->get();

        // DebugUtil::e_log('getTakable', 'model', $model);
		return $model;
    }

	/**
	 * 報酬取得可能なプレイヤミッションの数を取得
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列
	 */
	public static function getTakableCount($playerId, $now)
	{
        $cond = self::composeDailyCond($now);

		$model =
               self::where('player_id', $playerId)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw('progress_count >= count')
               ->whereNull('taked_at')
               ->whereRaw($cond)
               ->count();

		return $model;
    }

	/**
	 * 報酬取得可能なプレイヤミッション一覧を取得
	 *
	 * @param string $id　プレイヤミッションID
	 * @param string $now　現在日付
	 * @return array 本クラスの配列 (1 件だが配列で返す)
	 */
	public static function getTakableOnes($id, $now)
	{
        $cond = self::composeDailyCond($now);

		$model =
               self::where('id', $id)
               ->where('start_day', '<=', $now)
               ->where('end_day', '>=', $now)
               ->whereRaw('progress_count >= count')
               ->whereNull('taked_at')
               ->whereRaw($cond)
               ->get();

		return $model;
    }

	/**
	 * 追加
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $missionId アイテムID
	 * @param integer $num アイテム数
	 * @return boolean true:成功、false:失敗
	 */
    /*
      public static function add($playerId, $missionId, $num)
      {
      $model = self::getOne($playerId, $missionId);

      if (!isset($model))
      {
      $model = new self();
      $model->player_id	= $playerId;
      $model->mission_id		= $missionId;
      }
      else
      {
      if ($model->num >= self::MAX_NUM)
      {
      // 既に所持数オーバーしていた場合エラー
      return false;
      }
      }

      $model->num += $num;
      return $model->save();
      }
    */

	/**
	 * 日付の条件句を構成
	 *
	 * @param string $now　現在日付
	 * @return string where 句用の条件式
	 */
	public static function composeDailyCond($now)
	{
        // デイリー用に、今日の始まる時間を計算する
        // 今のところ 0 時 0 分 スタート

        $dayStartDate = DateTimeUtil::dailyMissionStartDate($now);
        $dayStart = DateTimeUtil::formatDB($dayStartDate);

        // progress_count を更新するかもしれないので、
        // 古いデイリーの行かどうかは、created_at で判定する

        return
            "(mission_type <> " . Mission::TYPE_DAILY . " OR " .
            "created_at >= '" . $dayStart . "')";
    }

	/**
	 * 報告済みかを返す
	 *
	 * @return boolean true: 報告済み
	 */
    public function reported()
    {
        return $this->report_flag == self::REPORT_FLAG_DONE;
    }

	/**
	 * 報告済みフラグを設定する
	 *
	 * @return boolean true: 報告済み
	 */
	public function setReported()
	{
        $this->report_flag = self::REPORT_FLAG_DONE;
    }

	/**
	 * クリア済みかを返す
	 *
	 * @return boolean true: クリア済み
	 */
    public function cleared()
    {
        return $this->progress_count >= $this->count;
    }

	/**
	 * クリア済みかを返す
	 *
	 * @param $coll PlayerMission のコレクション
	 * @return クリア済みのレコード数を返す
	 */
    public static function clearedCount($coll)
    {
        // $coll->where('progress_count', '>=', 'count')
        // うまく働かないので、真面目にかく

        $count = 0;
        foreach ($coll as $item)
        {
            if ($item->cleared())
                ++ $count;
        }
        return $count;
    }

	/**
	 * クリア済みで未報告かを返す
	 *
	 * @return boolean true: クリア済み
	 */
    public function unreported()
    {
        return
            $this->cleared()
            && $this->report_flag == 0
            && empty($this->taked_at);
    }

    /**
     * リリーストリガーがないプレイヤミッション一覧を取得
     *
     * @param string $id　プレイヤID
     * @param array $missionIds ミッションIDリスト
     * @param string $now　現在日付
     * @return array 本クラスの配列
     */
    public static function getPlayerMissonByPlayerIdAndMissionIds($playerId, $missionIds, $now)
    {
        $model =
            self::where('player_id', $playerId)
                ->whereIn ('mission_id', $missionIds)
                ->where('start_day', '<=', $now)
                ->where('end_day', '>=', $now)
                ->get();

        return $model;
    }

    /**
     * @return bool
     */
    public function getNewState() {
        return $this->report_flag == self::REPORT_FLAG_NONE
        && !$this->cleared()
        && $this->mission_type !== 1;
    }
    /**
     * masterのリリーストリガーがないミッション
     * @param $playerId
     * @param $prepareInsertPlayerMissionDataList
     * @param $now
     */
    public static function preRegistPlayerMission($playerId, $prepareInsertPlayerMissionDataList, $now) {
        $modeldatas = [];
        foreach($prepareInsertPlayerMissionDataList as $prepareInsertPlayerMissionData) {
            $modeldatas[] = [
               'player_id' => $playerId,
               'mission_id' =>  $prepareInsertPlayerMissionData->id,
               'mission_type' => $prepareInsertPlayerMissionData->mission_type,
               'progress_count' =>  0,
               'count' =>  $prepareInsertPlayerMissionData->count,
               'remuneration_type' => $prepareInsertPlayerMissionData->remuneration_item_type,
               'remuneration' =>  $prepareInsertPlayerMissionData->mission_remuneration,
               'remuneration_count' => $prepareInsertPlayerMissionData->mission_remuneration_count,
               'report_flag' => 0,
               'start_day' => $prepareInsertPlayerMissionData->start_day,
               'end_day' => $prepareInsertPlayerMissionData->end_day,
               'created_at' => $now,
               'updated_at' => $now,
           ];
        }
        $model = new self();
        $model->insert($modeldatas);
    }
}
