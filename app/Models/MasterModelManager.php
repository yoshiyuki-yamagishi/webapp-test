<?php

namespace App\Models;

use App\Models\CacheModel;
use Illuminate\Support\Facades\Cache;
use Thrift\Transport\TMemoryBuffer;
use Thrift\Protocol\TCompactProtocol;
use App\Models\Thrift\MasterDataSet;
use App\Utils\DebugUtil;

/**
 * Masterモデルの管理クラス
 *
 */
class MasterModelManager
{
	/** キャッシュ用のタグ */
    private static $TAGS = ['master'];

	/** 基本データ */
    private static $FILE_TIME_KEY = '_masterFileTime';
    private static $MASTER_LOCATION = 'master_data/';
    private static $MASTER_FNAME_API = 'master_api.db';
    private static $MASTER_FNAME_GMS = 'master_gms.db';
    private static $AES_METHOD = 'aes-128-cbc';
    private static $AES_KEY = '5TGB&YHN7UJM(IK<';
    private static $AES_OPTIONS = OPENSSL_RAW_DATA;
    public static $AES_IV = '!QAZ2WSX#EDC4RFV';

    private static $AZR_STORAGE_URL = 'https://bbdw.blob.core.windows.net/master-data/';

    // 運用では、false にしないと、遅くなる
    public static $MEASURE_MEM_CACHE = false;

    // シングルトン //

    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton))
        {
            self::$singleton = new MasterModelManager();
            self::$singleton->masterData = new MasterDataSet();

            // ↓デバッグ用 (要コメントアウト)
            // self::$singleton->clearCaches();
            // CacheModel::clearAll(static::$TAGS);
            // Cache::store('file')->forget(static::$FILE_TIME_KEY);
        }
        return self::$singleton;
    }

    private $masterData = null;

    public function masterDataBasePath()
    {
        $env = config('app.env');
        switch ($env)
        {
            case 'azure_ldev':
                return static::$AZR_STORAGE_URL.'ldev/v1/';
                break;
            case 'docker':
            case 'docker_review':
            case 'product':
            case 'develop':
            default:
                return static::$MASTER_LOCATION;
                break;
        }
    }

    public function masterDataPath()
    {
        $appName = config('app.name');

        $loc = self::masterDataBasePath();
        if ($appName == 'BBDW-GMS') {
            $loc .= static::$MASTER_FNAME_GMS;
        } else {
            $loc .= static::$MASTER_FNAME_API;
        }

        $env = config('app.env');
        if ($env == 'azure_ldev') {
            return $loc;
        } else {
            return resource_path($loc);
        }
    }

    public function fileGet($path)
    {
        $env = config('app.env');

        $value = null;
        if ($env == 'azure_ldev') {
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $path,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FILETIME => true,
            ]);

            $value = curl_exec($curl);
            $fileTime = curl_getinfo($curl, CURLINFO_FILETIME);
            curl_close($curl);

            // ファイルタイムをキャッシュに保存しておく
            CacheModel::set(static::$TAGS, static::$FILE_TIME_KEY, $fileTime);
        } else {
            $value = file_get_contents($path);
            $fileTime = filemtime($path);
            // ファイルタイムをキャッシュに保存しておく
            CacheModel::set(static::$TAGS, static::$FILE_TIME_KEY, $fileTime);
        }

        return $value;
    }

    public function getFileTimestamp($path)
    {
        $fileTime = CacheModel::get(static::$TAGS, static::$FILE_TIME_KEY);
        if ($fileTime) return $fileTime;

        $env = config('app.env');

        if ($env == 'azure_ldev') {
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $path,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FILETIME => true,
            ]);

            $value = curl_exec($curl);
            $fileTime = curl_getinfo($curl, CURLINFO_FILETIME);
            curl_close($curl);

            // ファイルタイムをキャッシュに保存しておく
            CacheModel::set(static::$TAGS, static::$FILE_TIME_KEY, $fileTime);
        } else {
            $fileTime = filemtime($path);
            // ファイルタイムをキャッシュに保存しておく
            CacheModel::set(static::$TAGS, static::$FILE_TIME_KEY, $fileTime);
        }

        return $fileTime;
    }

    public function reload()
    {
        // パスの計算
        $path = $this->masterDataPath();

        // 全データ読み込み
        $rawData = $this->fileGet($path);

        // 復号
        $data = openssl_decrypt(
            $rawData,
            static::$AES_METHOD,
            static::$AES_KEY,
            static::$AES_OPTIONS,
            static::$AES_IV
        );

        // 読み込み

        $transport = new TMemoryBuffer($data);
        $protocol = new TCompactProtocol($transport);

        $this->masterData = new MasterDataSet();
        $this->masterData->read($protocol);

        // テーブル毎に、キャシュに保存する

        if (self::$MEASURE_MEM_CACHE)
        {
            $__start = microtime(true);
        }

        // DebugUtil::e_log('MMM_Cache', 'set', $dbg);

        foreach (MasterDataSet::$_TSPEC as $spec)
        {
            $valueName = $spec['var'];
            $values = $this->masterData->$valueName;

            if (!is_array($values)) // version など、null のままの場合あり
                continue;

            CacheModel::set(static::$TAGS, $valueName, $values);

            // $dbg = $valueName . ' (' . count($values) . ')';
            // DebugUtil::e_log('MMM_Cache', 'set', $dbg);
        }

        if (self::$MEASURE_MEM_CACHE)
        {
            $__time = microtime(true) - $__start;
            DebugUtil::e_log_line(
                'mmm_time',
                'reload()'
                . "\t" . sprintf('%.8f', $__time)
            );
        }

        // 読み込みが成功したので、更新日付を保存する //

        $fileTime = $this->getFileTimestamp($path);
        Cache::store('file')->forever(static::$FILE_TIME_KEY, $fileTime);

        // 読み込みログを出力する //
        {
            $now = new \DateTime(); // 偽装不要
            $fname = 'mmm-reload-' . $now->format('Y-m-d-H-i-s');
            DebugUtil::e_log($fname, 'fileTime', $fileTime);
        }

        // 加工済みデータを用意する //

        CacheModelManager::getInstance()->reload();

        // $this->masterData を開放しない方が速い (メモリーは使用する)
    }

    public function prepare()
    {
        if (self::clearCachesWithFileTime())
            $this->reload();
    }

    public function getData($valueName)
    {
        self::clearCachesWithFileTime();

        // メモリー内にあれば、それを使用した方が速い

        if (property_exists($this->masterData, $valueName) &&
            isset($this->masterData->$valueName))
        {
            return $this->masterData->$valueName;
        }

        if (self::$MEASURE_MEM_CACHE)
        {
            $__start = microtime(true);
        }

        if (CacheModel::has(static::$TAGS, $valueName))
        {
            $values = CacheModel::get(static::$TAGS, $valueName);

            if (self::$MEASURE_MEM_CACHE)
            {
                $__time = microtime(true) - $__start;
                DebugUtil::e_log_line(
                    'mmm_time',
                    $valueName
                    . "\t" . sprintf('%.8f', $__time)
                );
            }

            if (property_exists($this->masterData, $valueName))
                $this->masterData->$valueName = $values; // メモリー内に保持

            // $dbg = $valueName . ' (' . count($values) . ')';
            // DebugUtil::e_log('MMM_Cache', 'hit', $dbg);

            return $values;
        }

        $this->reload();
        return $this->masterData->$valueName;
    }

    public function clearCachesWithFileTime()
    {
        // Docker 環境でキャッシュをクリアするのに必要 //

        if (self::$MEASURE_MEM_CACHE)
        {
            $__start = microtime(true);
        }

        // ファイル時刻は、サーバー毎に違うので、
        // 複数サーバーで共有しない

        if (Cache::store('file')->has(static::$FILE_TIME_KEY))
        {
            $cacheTime = Cache::store('file')->get(static::$FILE_TIME_KEY);

            if (self::$MEASURE_MEM_CACHE)
            {
                $__time = microtime(true) - $__start;
                DebugUtil::e_log_line(
                    'mmm_time',
                    static::$FILE_TIME_KEY
                    . "\t" . sprintf('%.8f', $__time)
                );
            }

            $path = $this->masterDataPath();
            $fileTime = $this->getFileTimestamp($path);

            if ($fileTime == $cacheTime)
            {
                // DebugUtil::e_log('MMM_clearCaches', 'skip', $cacheTime);
                return false;
            }
        }

        if (self::$MEASURE_MEM_CACHE)
        {
            $__time = microtime(true) - $__start;
            DebugUtil::e_log_line(
                'mmm_time',
                static::$FILE_TIME_KEY . '(not_has)'
                . "\t" . sprintf('%.8f', $__time)
            );
        }

        self::clearCaches();
        // DebugUtil::e_log('MMM_clearCaches', 'clear', '');
        return true;
    }

    public function clearCaches()
    {
        if (self::$MEASURE_MEM_CACHE)
        {
            $__start = microtime(true);
        }

        // メモリーをクリア

        $this->masterData = new MasterDataSet();

        // キャッシュをクリア

        if (CacheModel::useTags())
        {
            CacheModel::clearAll(static::$TAGS);
        }
        else
        {
            // テーブル名が変わると古いテーブルがクリアできない
            foreach (MasterDataSet::$_TSPEC as $spec)
            {
                $valueName = $spec['var'];
                CacheModel::clear(static::$TAGS, $valueName);
            }
        }

        if (self::$MEASURE_MEM_CACHE)
        {
            $__time = microtime(true) - $__start;
            DebugUtil::e_log_line(
                'mmm_time',
                'clearCaches()'
                . "\t" . sprintf('%.8f', $__time)
            );
        }
    }

}
