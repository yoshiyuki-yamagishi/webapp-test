<?php

namespace App\Models;

/**
 * maintenance:メンテナンス のモデル
 *
 */
class Maintenance extends BaseCommonModel
{
	protected $table = 'maintenance';
	protected $primaryKey = 'id';
	public $incrementing = true;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 現在有効なメンテナンス情報を取得
	 *
	 * @param string $now 現在 DB 日付
	 * @return self 本クラス
	 */
	public static function getOne($now)
	{
        return
            self::where('start_at', '<=', $now)
            ->where('end_at', '>=', $now)
            ->first();
	}
    
}
