<?php
namespace App\Models\Thrift;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

class LoginBonus
{
    static public $isValidate = false;

    static public $_TSPEC = array(
        10 => array(
            'var' => 'id',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        20 => array(
            'var' => 'title_pic',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        30 => array(
            'var' => 'dialog_pic',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        40 => array(
            'var' => 'login_bonus_description',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        50 => array(
            'var' => 'start_day',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        60 => array(
            'var' => 'end_day',
            'isRequired' => false,
            'type' => TType::STRING,
        ),
        70 => array(
            'var' => 'period_display',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        80 => array(
            'var' => 'loop',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        90 => array(
            'var' => 'priority',
            'isRequired' => false,
            'type' => TType::I32,
        ),
    );

    /**
     * @var int
     */
    public $id = null;
    /**
     * @var string
     */
    public $title_pic = null;
    /**
     * @var string
     */
    public $dialog_pic = null;
    /**
     * @var int
     */
    public $login_bonus_description = null;
    /**
     * @var string
     */
    public $start_day = null;
    /**
     * @var string
     */
    public $end_day = null;
    /**
     * @var int
     */
    public $period_display = null;
    /**
     * @var int
     */
    public $loop = null;
    /**
     * @var int
     */
    public $priority = null;

    public function __construct($vals = null)
    {
        if (is_array($vals)) {
            if (isset($vals['id'])) {
                $this->id = $vals['id'];
            }
            if (isset($vals['title_pic'])) {
                $this->title_pic = $vals['title_pic'];
            }
            if (isset($vals['dialog_pic'])) {
                $this->dialog_pic = $vals['dialog_pic'];
            }
            if (isset($vals['login_bonus_description'])) {
                $this->login_bonus_description = $vals['login_bonus_description'];
            }
            if (isset($vals['start_day'])) {
                $this->start_day = $vals['start_day'];
            }
            if (isset($vals['end_day'])) {
                $this->end_day = $vals['end_day'];
            }
            if (isset($vals['period_display'])) {
                $this->period_display = $vals['period_display'];
            }
            if (isset($vals['loop'])) {
                $this->loop = $vals['loop'];
            }
            if (isset($vals['priority'])) {
                $this->priority = $vals['priority'];
            }
        }
    }

    public function getName()
    {
        return 'LoginBonus';
    }


    public function read($input)
    {
        $xfer = 0;
        $fname = null;
        $ftype = 0;
        $fid = 0;
        $xfer += $input->readStructBegin($fname);
        while (true) {
            $xfer += $input->readFieldBegin($fname, $ftype, $fid);
            if ($ftype == TType::STOP) {
                break;
            }
            switch ($fid) {
                case 10:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->id);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 20:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->title_pic);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 30:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->dialog_pic);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 40:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->login_bonus_description);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 50:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->start_day);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 60:
                    if ($ftype == TType::STRING) {
                        $xfer += $input->readString($this->end_day);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 70:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->period_display);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 80:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->loop);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 90:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->priority);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                default:
                    $xfer += $input->skip($ftype);
                    break;
            }
            $xfer += $input->readFieldEnd();
        }
        $xfer += $input->readStructEnd();
        return $xfer;
    }

    public function write($output)
    {
        $xfer = 0;
        $xfer += $output->writeStructBegin('LoginBonus');
        if ($this->id !== null) {
            $xfer += $output->writeFieldBegin('id', TType::I32, 10);
            $xfer += $output->writeI32($this->id);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->title_pic !== null) {
            $xfer += $output->writeFieldBegin('title_pic', TType::STRING, 20);
            $xfer += $output->writeString($this->title_pic);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->dialog_pic !== null) {
            $xfer += $output->writeFieldBegin('dialog_pic', TType::STRING, 30);
            $xfer += $output->writeString($this->dialog_pic);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->login_bonus_description !== null) {
            $xfer += $output->writeFieldBegin('login_bonus_description', TType::I32, 40);
            $xfer += $output->writeI32($this->login_bonus_description);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->start_day !== null) {
            $xfer += $output->writeFieldBegin('start_day', TType::STRING, 50);
            $xfer += $output->writeString($this->start_day);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->end_day !== null) {
            $xfer += $output->writeFieldBegin('end_day', TType::STRING, 60);
            $xfer += $output->writeString($this->end_day);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->period_display !== null) {
            $xfer += $output->writeFieldBegin('period_display', TType::I32, 70);
            $xfer += $output->writeI32($this->period_display);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->loop !== null) {
            $xfer += $output->writeFieldBegin('loop', TType::I32, 80);
            $xfer += $output->writeI32($this->loop);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->priority !== null) {
            $xfer += $output->writeFieldBegin('priority', TType::I32, 90);
            $xfer += $output->writeI32($this->priority);
            $xfer += $output->writeFieldEnd();
        }
        $xfer += $output->writeFieldStop();
        $xfer += $output->writeStructEnd();
        return $xfer;
    }
}
