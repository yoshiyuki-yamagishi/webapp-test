<?php
namespace App\Models\Thrift;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

class PlayerLevel
{
    static public $isValidate = false;

    static public $_TSPEC = array(
        10 => array(
            'var' => 'lv',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        20 => array(
            'var' => 'exp_player',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        30 => array(
            'var' => 'max_al',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        40 => array(
            'var' => 'max_party_cost',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        50 => array(
            'var' => 'max_follow',
            'isRequired' => false,
            'type' => TType::I32,
        ),
    );

    /**
     * @var int
     */
    public $lv = null;
    /**
     * @var int
     */
    public $exp_player = null;
    /**
     * @var int
     */
    public $max_al = null;
    /**
     * @var int
     */
    public $max_party_cost = null;
    /**
     * @var int
     */
    public $max_follow = null;

    public function __construct($vals = null)
    {
        if (is_array($vals)) {
            if (isset($vals['lv'])) {
                $this->lv = $vals['lv'];
            }
            if (isset($vals['exp_player'])) {
                $this->exp_player = $vals['exp_player'];
            }
            if (isset($vals['max_al'])) {
                $this->max_al = $vals['max_al'];
            }
            if (isset($vals['max_party_cost'])) {
                $this->max_party_cost = $vals['max_party_cost'];
            }
            if (isset($vals['max_follow'])) {
                $this->max_follow = $vals['max_follow'];
            }
        }
    }

    public function getName()
    {
        return 'PlayerLevel';
    }


    public function read($input)
    {
        $xfer = 0;
        $fname = null;
        $ftype = 0;
        $fid = 0;
        $xfer += $input->readStructBegin($fname);
        while (true) {
            $xfer += $input->readFieldBegin($fname, $ftype, $fid);
            if ($ftype == TType::STOP) {
                break;
            }
            switch ($fid) {
                case 10:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->lv);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 20:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->exp_player);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 30:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->max_al);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 40:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->max_party_cost);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 50:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->max_follow);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                default:
                    $xfer += $input->skip($ftype);
                    break;
            }
            $xfer += $input->readFieldEnd();
        }
        $xfer += $input->readStructEnd();
        return $xfer;
    }

    public function write($output)
    {
        $xfer = 0;
        $xfer += $output->writeStructBegin('PlayerLevel');
        if ($this->lv !== null) {
            $xfer += $output->writeFieldBegin('lv', TType::I32, 10);
            $xfer += $output->writeI32($this->lv);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->exp_player !== null) {
            $xfer += $output->writeFieldBegin('exp_player', TType::I32, 20);
            $xfer += $output->writeI32($this->exp_player);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->max_al !== null) {
            $xfer += $output->writeFieldBegin('max_al', TType::I32, 30);
            $xfer += $output->writeI32($this->max_al);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->max_party_cost !== null) {
            $xfer += $output->writeFieldBegin('max_party_cost', TType::I32, 40);
            $xfer += $output->writeI32($this->max_party_cost);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->max_follow !== null) {
            $xfer += $output->writeFieldBegin('max_follow', TType::I32, 50);
            $xfer += $output->writeI32($this->max_follow);
            $xfer += $output->writeFieldEnd();
        }
        $xfer += $output->writeFieldStop();
        $xfer += $output->writeStructEnd();
        return $xfer;
    }
}
