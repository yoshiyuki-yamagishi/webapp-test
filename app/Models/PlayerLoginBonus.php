<?php

namespace App\Models;



use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\DB;

/**
 * player_login_bonus:プレイヤログインボーナス のモデル
 *
 */
class PlayerLoginBonus extends BaseGameModel
{
	protected $table = 'player_login_bonus';
	protected $primaryKey = 'id';
// 	public $incrementing = false;
// 	public $timestamps = false;

    /*
	protected $fillable = [
		//'id',
	];
	*/

	/**
	 * プレイヤログインボーナスを登録する
	 *
	 * @param integer $playerId プレイヤID
	 * @param App/Models/Thrift/Item/LoginBonusReward $reward 報酬アイテム
	 * @param array $playerCount 受け取り回数
	 * @return self 本クラス
	 */
	public static function regist($playerId, $reward, $playerCount, $now)
	{
		$item = new self();
		$item->player_id = $playerId;
		$item->login_bonus_id = $reward->login_bonus_id;
		$item->count = $reward->count;
		$item->player_count = $playerCount;
		$item->remuneration = $reward->remuneration;
		$item->num = $reward->number;
		$item->created_at = $now;
		$item->save();
        return $item;
	}

	/**
	 * 取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $loginBonusId ログインボーナスID
	 * @param integer $playerCount プレイヤログイン回数
	 * @return self 本クラス
	 */
	public static function getOne($playerId, $loginBonusId, $playerCount)
	{
		$model =
			self::where('player_id', $playerId)
				->where('login_bonus_id', $loginBonusId)
				->where('player_count', $playerCount)
				->first();

		return $model;
	}

	/**
	 * 取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $loginBonusId ログインボーナスID
	 * @param integer $playerCount プレイヤログイン回数
	 * @return self 本クラス
	 */
	public static function getLatest($playerId, $loginBonusId)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('login_bonus_id', $loginBonusId)
               ->orderBy('player_count', 'desc')
               ->first();

		return $model;
	}
    

	/**
	 * ログインボーナスのログイン回数から取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $loginBonusId ログインボーナスID
	 * @param integer $count ログイン日数
	 * @return self 本クラス
	 */
	public static function findByCount($playerId, $loginBonusId, $count)
	{
		$model =
			self::where('player_id', $playerId)
				->where('login_bonus_id', $loginBonusId)
				->where('count', $count)
				->first();

		return $model;
	}


    /**
	 * ログインボーナスログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}

}