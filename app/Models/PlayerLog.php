<?php

namespace App\Models;
use App\Models\MasterModels\Item;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * player_log:プレイヤログ のモデル
 *
 */
class PlayerLog extends BaseGameLogModel
{
    public static $target_id_name = 'player_id';
    
	protected $table = 'player_log';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const TYPE_GIVE = 1;
    const TYPE_PAY = 2;
    const TYPE_EXP = 3;
    const TYPE_LV = 4;
    const TYPE_TUTORIAL_UPDATE = 5;
    const TYPE_AL_RECOVER = 6;
    const TYPE_GRIMOIRE_LIMIT_UP = 7;

	/**
	 * 経験値取得ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $exp 取得経験値
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerExp(
        $player, $exp, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $player->id;
        $model->type = self::TYPE_EXP;
        $model->value1 = $exp;
        $model->value2 = $player->experience;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * レベル取得ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $lv 取得レベル
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerLv(
        $player, $lv, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $player->id;
        $model->type = self::TYPE_LV;
        $model->value1 = $lv;
        $model->value2 = $player->player_lv;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * AL回復ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $upNum 枠増加
	 * @param integer $itemId 消費アイテムID
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerAlRecover(
        $player, $upNum, $itemId, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $player->id;
        $model->type = self::TYPE_AL_RECOVER;
        $model->value1 = $upNum;
        $model->value2 = $player->al;
        $model->value3 = $itemId;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * チュートリアル進行度更新ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $upNum 枠増加
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerTutorialUpdate(
        $player, $upNum, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $player->id;
        $model->type = self::TYPE_TUTORIAL_UPDATE;
        $model->value1 = $upNum;
        $model->value2 = $player->tutorial_progress;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * 魔道書枠増加ログを登録
     *
	 * @param object $player プレイヤ
	 * @param integer $upNum 枠増加
	 * @param integer $crystalNum 消費蒼の結晶数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerGrimoireLimitUp(
        $player, $upNum, $crystalNum, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $player->id;
        $model->type = self::TYPE_GRIMOIRE_LIMIT_UP;
        $model->value1 = $upNum;
        $model->value2 = $player->max_grimoire;
        $model->value3 = $crystalNum;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}

	/**
	 * プレイヤー経験値ログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getPlayerExpLog($playerId)
	{
		$model =
			   self::where('player_id', $playerId)
			   ->where('type', 3)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}

	/**
	 * プレイヤーレベルアップログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getPlayerLvLog($playerId)
	{
		$model =
			   self::where('player_id', $playerId)
			   ->where('type', 4)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}

	/**
	 * プレイヤーフレンドポイント取得ログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getPlayerFrienfPointLog($playerId)
	{
		$query =
			   self::where('player_id', $playerId)
			   ->whereIn('type', [1, 2])
			   ->where('value3', Item::ID_FRIEND_POINT)
			   ->orderBy('id', 'asc');
			
		DebugUtil::e_log('sql', 'query', $query->toSql());

		$model = $query->get();
		// DebugUtil::e_log('Item', 'Item', Item::ID_FRIEND_POINT);
		return $model;
	}
    
}
