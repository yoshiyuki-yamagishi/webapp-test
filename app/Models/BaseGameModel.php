<?php

namespace App\Models;


use Illuminate\Support\Facades\DB;
use App\Services\SessionService;

/**
 * Gameモデルの基底クラス
 *
 */
class BaseGameModel extends BaseModel
{
	public static function createConnectionNameImpl($dbNo)
    {
		// TODO: 4/30、平成の終わりに、DB番号を 1始まりとしたため、
		//       既に登録済みのDB番号については、ここで応急処置を行います
		if ($dbNo == 0)
		{
			$dbNo = 1;
		}

		$connection = sprintf('game_%02d', $dbNo);
		return $connection;
    }
    
	public static function createConnectionName()
	{
		$dbNo = SessionService::getDbNo();
        return self::createConnectionNameImpl($dbNo);
	}

	public function getConnectionName()
	{
		$connection = self::createConnectionName();
		return $connection;
	}

	public static function beginTransaction()
	{
		DB::connection(self::createConnectionName())->beginTransaction();
	}

	public static function commit()
	{
		DB::connection(self::createConnectionName())->commit();
	}

	public static function rollBack()
	{
		DB::connection(self::createConnectionName())->rollBack();
	}

	public static function raw($value)
	{
		DB::connection(self::createConnectionName())->raw($value);
	}
}