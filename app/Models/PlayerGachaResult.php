<?php

namespace App\Models;
use App\Models\MasterModels\Item;


/**
 * player_gacha_result:プレイヤガチャ結果 のモデル
 *
 */
class PlayerGachaResult extends BaseGameModel
{
    const FLAG_NONE = 0;
    const FLAG_CONFIRMED = 1;
    const FLAG_NEW = 2;
    
	protected $table = 'player_gacha_result';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * ガチャ結果一覧を取得
	 *
	 * @param string $playerGachaId　プレイヤガチャID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerGachaId($playerGachaId)
	{
		$model =
               self::where('player_gacha_id', $playerGachaId)
               ->get();

		return $model;
	}

	/**
	 * ガチャ結果を削除する
	 *
	 * @param string $playerGachaId　プレイヤガチャID
	 * @return array 本クラスの配列
	 */
	public static function deleteByPlayerGachaId($playerGachaId)
	{
        self::where('player_gacha_id', $playerGachaId)
            ->delete();
	}
    
	/**
	 * ガチャ結果の計算
	 *
	 * @param PlayerGacha $playerGacha プレイヤガチャ
	 * @param App/Models/Thrift/GachaGroup $gachaGroup ガチャグループ
	 * @param integer $resultFlag ガチャ結果フラグ
	 * @return PlayerGachaResult ガチャ結果
	 */
	public static function make(
        $playerGacha, $gachaGroup, $resultFlag
    )
	{
        // 1 ガチャ、1 種類のアイテムを想定している //
        
        $result = new self();
        $result->player_gacha_id = $playerGacha->id;
        $result->item_count = $gachaGroup->get_count;
        $result->gacha_result_flag = $resultFlag;
        $result->take_flag = 0;
        $result->after_repeat = 0;

        // いまのところ、ガチャのコンテントタイプは、Item::TYPE と同じ

        if (!Item::isValidType($gachaGroup->content_type))
        {
            throw \App\Exceptions\MasterException::make(
                'gacha_group invalid content_type: ' . $gachaGroup->content_type
            );
        }
        
        $result->item_type = $gachaGroup->content_type;
        $result->item_id = $gachaGroup->content_id;
        return $result;
    }
    
}
