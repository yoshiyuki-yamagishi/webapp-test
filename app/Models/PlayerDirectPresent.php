<?php

namespace App\Models;



use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\DB;

/**
 * player_direct_present:プレイヤ直接プレゼント のモデル
 *
 */
class PlayerDirectPresent extends BaseGameModel
{
	protected $table = 'player_direct_present';
	protected $primaryKey = 'id';
// 	public $incrementing = false;
// 	public $timestamps = false;

    /*
	protected $fillable = [
		//'id',
	];
	*/

	/**
	 * 取得済みの直接プレゼントIDを取得する
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 直接プレゼントIDの配列
	 */
	public static function getDirectPresentIds($playerId)
	{
        $model = self::where('player_id', $playerId)->get();

        $ids = [];
        foreach ($model as $item)
        {
            $ids[] = $item->direct_present_id;
        }
        return $ids;
	}
    
}