<?php

namespace App\Models;
use App\Models\MasterModels\ProductShop;
use App\Models\MasterModels\ProductContents;
use App\Utils\DateTimeUtil;

/**
 * player_product_buy:プレイヤ製品購入 のモデル
 *
 */
class PlayerProductBuy extends BaseGameModel
{
    const STORE_NONE = 0;
    const STORE_GOOGLE = 1;
    const STORE_APPLE = 2;
    
    const STATE_INITIAL = 0;
    const STATE_NOT_USED_MAX = 899;
    const STATE_FINISHED = 900;
    const STATE_FINISHED_GRANTED = 902;
    const STATE_CANCELED = 999;

	protected $table = 'player_product_buy';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $storeId 購入元ID
	 * @param integer $paymentId 購入ID
	 * @param integer $productId 購入ID
	 * @param integer $num 個数
	 * @return array 本クラスの配列
	 */
	public static function regist(
        $playerId, $storeId, $paymentId,
        $productId, $num
    )
	{
        $model = new self();
        $model->player_id = $playerId;
        $model->store_id = $storeId;
        $model->payment_id = $paymentId;
        $model->product_id = $productId;
        $model->num = $num;
        $model->state = static::STATE_INITIAL;
        $model->save();
		return $model;
	}
    
    /**
	 * 今日の購入数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $productId 製品ID
	 * @return array 本クラスの配列
	 */
	public static function getNumDay($playerId, $productId, $now)
    {
        $_start = DateTimeUtil::dailyProductStartDate($now);
        $start = DateTimeUtil::formatDB($_start);
        
        // キャンセルは含めない
        return 
            (int)self::where('player_id', $playerId)
            ->where('product_id', $productId)
            ->where('created_at', '>=', $start)
            ->where('state', '<=', self::STATE_FINISHED_GRANTED)
            ->sum('num');
    }

    /**
	 * 最近の購入を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $productId 製品ID
	 * @return self 本クラス
	 */
	public static function getLatest($playerId, $productId)
    {
        // キャンセルは含めない
        return 
            self::where('player_id', $playerId)
            ->where('product_id', $productId)
            ->where('state', '<=', self::STATE_FINISHED_GRANTED)
            ->orderBy('created_at', 'desc')
            ->first();
    }
    

    /**
	 * 今月の支払い金額を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getAmountMonth($playerId, $now)
    {
        // 通貨単位がまちまちでも、合算しちゃう //

        $_start = DateTimeUtil::monthlyProductStartDate($now);
        $start = DateTimeUtil::formatDB($_start);

        // キャンセルは含めない
        return 
            (int)self::where('player_id', $playerId)
            ->where('created_at', '>=', $start)
            ->where('state', '<=', self::STATE_FINISHED_GRANTED)
            ->sum('amount');
    }
    
    /**
	 * 取得
	 *
	 * @param integer $storeId ストアID
	 * @param integer $paymentId 購入ID
	 * @return self 本クラス
	 */
	public static function getByPaymentId($storeId, $paymentId, $maxState)
    {
        $model =
               self::where('store_id', $storeId)
               ->where('payment_id', $paymentId)
               ->where('state', '<=', $maxState)
               ->first();
        return $model;
    }

    /**
	 * 取得
	 *
	 * @param integer $productId 製品ID
	 * @param integer $num 購入数
	 * @param integer $maxState 状態の最大値
	 * @return array 本クラスの配列
	 */
	public static function getByProductId($productId, $num, $maxState)
    {
        $model =
               self::where('product_id', $productId)
               ->where('num', $num)
               ->where('state', '<=', $maxState)
               ->orderBy('created_at', 'asc') // 古いのから処理しちゃう?
               ->get();
        return $model;
    }

    /**
	 * 有効なデイリーパック取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getDaily($playerId)
    {
        $model =
               self::where('player_id', $playerId)
               ->where('state', self::STATE_FINISHED)
               ->where('daily_flg', ProductShop::DAILY_FLG_DAILY)
               ->orderBy('created_at', 'asc') // 古いのから処理しちゃう?
               ->get();
        return $model;
    }

    /**
	 * 取得
	 *
	 * @return boolean true: 使用済み
	 */
    public function usedState()
    {
        return $this->state <= self::STATE_NOT_USED_MAX;
    }

    /**
	 * デイリーかどうかを取得する
	 *
	 * @return boolean true: デイリー
	 */
	public function isDaily()
    {
        return $this->daily_flg == ProductShop::DAILY_FLG_DAILY;
    }

    /**
	 * デイリーパックの終了日時を計算する
	 *
	 * @return DateTime 終了日時
	 */
	public function dailyEndDate()
    {
        assert($this->isDaily());
        $endAt = DateTimeUtil::dailyPackStartDate($this->created_at);
        $endAt->modify('+' . $this->valid_period . " day");
        return $endAt;
    }

    /**
	 * 購入ログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}
    
}
