<?php

namespace App\Models;

use App\Config\Constants;

/**
 * Information:お知らせ のモデル
 *
 */
class Information extends BaseCommonModel
{
	protected $table = 'information';
	protected $primaryKey = 'id';
	public $incrementing = true;

	// お知らせタイプ
	const INFORMATION_TYPE_ALL          = 1;
    const INFORMATION_TYPE_MAINTENANCE  = 2;
    const INFORMATION_TYPE_UPDATE       = 3;
    const INFORMATION_TYPE_BUG_REPORT   = 4;

	// プラットフォームフラグ
	const PLATFORM_FLAG_IOS     = 0b001;
    const PLATFORM_FLAG_ANDROID = 0b010;
    const PLATFORM_FLAG_BROWSE  = 0b100;

    /**
     * 指定したIDのお知らせ取得
     *
     * @param $id
     * @return mixed
     */
	public static function getOne($id)
    {
        return self::find($id);
    }

    /**
     * タイプ分けなし表示リスト取得
     *
     * @param $platform
     * @param $now
     * @return mixed
     */
    public static function getAllViewList($platform, $now)
    {
        $platformFlag = self::_getPlatformFlag($platform);
        return self::whereRaw('`platform_flag` & ?', $platformFlag)
            ->where('started_at', '<=', $now)
            ->where('expired_at', '>=', $now)
            ->orderByRaw('priority DESC, updated_at DESC')
            ->get();
    }

    /**
     * お知らせタイプ別表示リスト取得
     *
     * @param $infoType
     * @param $platform
     * @param $now
     * @return mixed
     */
    public static function getTypeViewList($infoType, $platform, $now)
    {
        $platformFlag = self::_getPlatformFlag($platform);
        return self::where('info_type', $infoType)
            ->whereRaw('`platform_flag` & ?', $platformFlag)
            ->where('started_at', '<=', $now)
            ->where('expired_at', '>=', $now)
            ->orderByRaw('priority DESC, updated_at DESC')
            ->get();
    }

    /**
     * PlatformTypeからFlagパラメータ
     *
     * @param $platform
     * @return int
     */
    private static function _getPlatformFlag($platform)
    {
        switch ($platform)
        {
            case Constants::PLATFORM_IOS:
                return self::PLATFORM_FLAG_IOS;
                break;
            case Constants::PLATFORM_ANDROID:
                return self::PLATFORM_FLAG_ANDROID;
                break;
            default:
                return self::PLATFORM_FLAG_BROWSE;
                break;
        }
    }
}
