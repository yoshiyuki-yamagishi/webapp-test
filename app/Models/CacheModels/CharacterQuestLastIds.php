<?php

namespace App\Models\CacheModels;
use App\Models\BaseCacheModel;
use App\Models\MasterModels\CharacterQuestChapter;
use App\Models\MasterModels\CharacterQuestQuest;
use App\Models\PlayerCharacter;
use App\Services\QuestService;
use App\Utils\DebugUtil;

/**
 * キャラクタークエスト最終ID のモデル
 *
 */
class CharacterQuestLastIds extends BaseCacheModel
{
    public static $table = 'characterQuestLastIds';

	/**
	 * データを用意する
     *
	 */
    public static function prepare()
    {
        $all = CharacterQuestQuest::getAll();

        $keys = [];
        foreach ($all as $item)
        {
            $keys[$item->id] = 1;
            unset($keys[$item->release_trigger]);
        }

        $ids = array_keys($keys);
        static::set($ids);
    }

	/**
	 * 現在挑戦可能なクエストIDの配列を返す
     *
	 */
    public static function getActiveIds($playerId)
    {
        // 所持キャラクターのクエストのみ挑戦可能
        $pcs = PlayerCharacter::getByPlayerId($playerId);
        
        $all = static::get();
        $ids = [];
        $service = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);
        foreach ($pcs as $pc)
        {
            $charaId = $pc->character_id;
            $chapter = CharacterQuestChapter::getByCharacterId($charaId);
            $quests = $service->getQuests($chapter->id);
            foreach ($quests as $quest)
            {
                if (in_array($quest->id, $all))
                    $ids[] = $quest->id;
            }
        }

        return $ids;
    }

}
