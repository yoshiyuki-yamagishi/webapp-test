<?php

namespace App\Models\CacheModels;
use App\Http\Responses\ApiResponse;
use App\Models\BaseCacheModel;
use App\Models\MasterModels\EventQuestQuest;
use App\Utils\DebugUtil;

/**
 * 未来のイベントクエストクエスト のモデル
 */
class FutureEventQuestQuest extends BaseCacheModel
{
    public static $table = 'futureEventQuestQuest';

	/**
	 * データを用意する
     *
	 */
    public static function prepare()
    {
        // NEW バッチ用に、挑戦できるクエストが存在するかチェックする
        // 高速化のため、古いイベントは除去しておく
        
		$response = ApiResponse::getInstance();
        $now = $response->currentDate;
        
        $all = EventQuestQuest::getAll();

        $items = [];
        foreach ($all as $item)
        {
            $endDay = new \DateTime($item->end_day);

            if ($endDay >= $now)
                $items[] = $item;
        }

        static::set($items);
    }

	/**
	 * 現在有効なイベントIDの配列を返す
     *
	 */
    public static function getActiveIds()
    {
		$response = ApiResponse::getInstance();
        $now = $response->currentDate;
        
        $all = static::get();

        $ids = [];
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            if ($startDay > $now)
                continue;
            $endDay = new \DateTime($item->end_day);
            if ($endDay < $now)
                continue;

            $ids[] = $item->id;
        }

        return $ids;
    }


}
