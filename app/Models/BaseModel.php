<?php
/**
 * モデルの基底クラス
 *
 */

namespace App\Models;

use App\Utils\DebugUtil;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * モデルの基底クラス
 *
 */
class BaseModel extends Model
{
	/**
	 * 取得 (例外スロー)
	 *
	 * @param integer $id 主キー
	 * @return self 本クラス
	 */
    public static function find_($id)
    {
		$model = static::find($id);
		if (empty($model))
		{
            $_this = new static();
            throw \App\Exceptions\DataException::makeNotFound(
                $_this->table, $_this->primaryKey, $id
            );
		}
		return $model;
    }

	/**
	 * 列の長さを取得
	 *
	 * @param string $colName 列名
	 * @return integer 長さ
	 */
    /*
    public function getColLength($colName)
    {
        // composer require doctrine/dbal がうまくいかないので、とりあえず無し
        $_type = DB::connection(static::getConnectionName())->getDoctrineColumn(
            $this->table, $colName
        )->getType();
		DebugUtil::e_log('PC', '_type', $_type);
        return null;
    }
    */

    /**
     * テーブルのカラム一覧取得
     *
     * @return array
     */
    public static function getColumns()
    {
        $_this = new static();
        $cols = [];
        if ($_this->table !== "") $cols = Schema::getColumnListing($_this->table);
        return $cols;
    }

}
