<?php

namespace App\Models;

use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\DB;


/**
 * player_login:プレイヤログイン のモデル
 *
 */
class PlayerLogin extends BaseGameModel
{
    protected $table = 'player_login';
    protected $primaryKey = 'id';
// 	public $incrementing = false;
// 	public $timestamps = false;

    /*
	protected $table = 'Business';
	protected $primaryKey = 'id';
	//public $incrementing = false;
	//public $timestamps = false;

	protected $fillable = [
		//'id',
	];
	*/

    /**
     * ログインを登録する
     *
     * @param integer $playerId プレイヤID
     * @return self 本モデル
     */
    public static function register($playerId)
    {
        $model = new self();
        $model->player_id = $playerId;
        $model->save();
        return $model;
    }

    /**
     * ログインを登録する
     *
     * @param integer $playerId プレイヤID
     * @return self 本モデル
     */
    public static function countUp($id, $count)
    {
        $countUp = $count + 1;
        $model = new self();
        $model->where('id', $id)
            ->update(['count' => $countUp]);
        return $model;
    }

    /**
     * 今日のログインを返す
     *
     * @param integer $playerId プレイヤID
     * @param string $start 期間開始
     * @param string $end 期間終了
     * @return integer ログイン回数
     */
    public static function getByPlayerIdToday($playerId, $now)
    {
        $_now = strtotime($now);
        $start = date('Y-m-d 00:00:00', $_now);
        $end = date('Y-m-d 23:59:59', $_now);
        $res = self::where('player_id', $playerId)
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->first();

        return $res;
    }

	/**
	 * 期間中のログイン回数を返す
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $start 期間開始
	 * @param string $end 期間終了
	 * @return integer ログイン回数
	 */
	public static function loginCount($playerId, $start, $end)
	{
        if (empty($start))
            $start = DateTimeUtil::getFarPast();
        if (empty($end))
            $end = DateTimeUtil::getFarFuture();

        $count = self::where('player_id', $playerId)
               ->where('created_at', '>=', $start)
               ->where('created_at', '<=', $end)
               ->count();

        return $count;
	}


	/**
	 * 期間中にログインしたか返す
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $start 期間開始
	 * @param string $end 期間終了
	 * @param string $now 現在でチェックする場合、現在日時
	 * @return true: ログインした
	 */
	public static function isLogin($playerId, $start, $end, $now = null)
	{
        if (!empty($now))
        {
            if (DateTimeUtil::isBetween($now, $start, $end))
                return true;
        }

        return static::loginCount($playerId, $start, $end) > 0;
    }

}