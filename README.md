# BBDW API

　

# 1. 概要

本書は、BBDWのAPIについてのドキュメントです。

　

# 2. 操作手順

## 2.1. migration

BBDW APIでは、複数のデータベースにアクセスする。

そのため、migration ファイルは、database/migrations ではなく、データベースごとに、database/migrations_xxxx に格納されている。
(xxxxxは、データベース毎に変わる)

具体的な migration の方法を以下に示す。

```shell
# Common DB の migration
php artisan migrate --database="common"  --path="database/migrations_common"

# Game DB の migration。 _01の部分は、データベース毎に変わる
php artisan migrate --database="game_01" --path="database/migrations_game"

# Log DB の migration
php artisan migrate --database="log"     --path="database/migrations_log"

# GMS DB の migration
php artisan migrate --database="gms"     --path="database/migrations_gms"
```

　

# 3. 環境構築手順

## 3.1. ベガ開発環境

### 3.1.1. 概要

ベガ開発環境は、1つのEC2インスタンスに、APIサーバ、DBサーバ、Memcacheサーバ、そして、phpMyAdminが含まれる。

EC2インスタンスのOSは「Amazon Linux AMI 2018.03.0 (HVM)」を使用している。

EC2インスタンスの作成後、以下の設定を順に行うことにより、環境を構築することができる。

　

### 3.1.2. OSの設定

#### 3.1.2.1. ホスト名の変更

```shell
[ec2-user@localhost ~]$ sudo su -

[root@localhost ~]# vi /etc/sysconfig/network
----------
# 変更
HOSTNAME=localhost.localdomain
↓
HOSTNAME=bbdw-dev-api-01 ← 任意のホスト名に変更
----------

[root@localhost ~]# vi /etc/hosts
----------
# 変更
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
↓
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 bbdw-dev-api-01 ← 任意のホスト名を追記
----------
```

　

#### 3.1.2.2. タイムゾーンの変更

インスタンスのタイムゾーンが、UTCになっているので変更する。

```shell
# オリジナルをバックアップ
[root@localhost ~]# cp /etc/localtime /etc/localtime.org

# タイムゾーンファイルの変更
[root@localhost ~]# ln -sf  /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

# ※この時点でタイムゾーンが切り替わる。
# ※以下、再起動してもタイムゾーンが戻らないように設定する。

# バックアップ取得
[root@localhost ~]# cp /etc/sysconfig/clock /etc/sysconfig/clock.org

[root@localhost ~]# vi /etc/sysconfig/clock
----------
# 変更
ZONE="UTC"
UTC=true
↓
ZONE="Asia/Tokyo"
UTC=false
----------
```
※ 終了後に、サーバを再起動する

　

#### 3.1.2.3. ソフトウェアのインストール

```shell
[ec2-user@localhost ~]$ sudo su -

# インストール済みのソフトウェアの アップデート
[root@localhost ~]# yum -y update

# Apache 2.4
[root@localhost ~]# yum -y install httpd24

# PHP 7.3
[root@localhost ~]# yum -y install php73 php73-mbstring php73-pdo php73-mysqlnd

# MySQLクライアント、サーバ 5.7
[root@localhost ~]# yum -y install mysql57 mysql57-common mysql57-devel mysql57-server

# Memcached
[root@localhost ~]# yum -y install memcached

# Git (デプロイ時に使用するため)
[root@localhost ~]# yum -y install git

# Subversion (デプロイ時に使用するため)
[root@localhost ~]# yum -y install subversion
```

　

#### 3.1.2.4. ログローテートの設定

##### 3.1.2.4.1. Apache

Apacheのログのローテートを以下のように変更する

```shell
[root@localhost ~]# vi /etc/logrotate.d/httpd

/var/log/httpd/*log {
    rotate 53    ← 追記する。53週(約1年)分。
    compress     ← 追記する。圧縮保存。
    missingok
    notifempty
    sharedscripts
    delaycompress
    postrotate
        /etc/init.d/httpd reload > /dev/null 2>/dev/null || true
    endscript
}
```
※ 終了後に、サーバを再起動する

　

### 3.1.3. Apacheの設定

#### 3.1.3.1. httpd.confの設定

```shell
[root@localhost ~]# vi /etc/httpd/conf/httpd.conf
----------
# サーバ名の変更
ServerName bbdw-dev-api-01 ← /etc/hostsで設定した値

# ドキュメントルートの変更
DocumentRoot "/var/www/html"
↓
DocumentRoot "/var/www/html/bbdw_api/public"

<Directory "/var/www/html">
↓
<Directory "/var/www/html/bbdw_api/public">

# <Directory "/var/www/html/bbdw_api/public">内の設定の変更
# ※セキュリティ上、良くないので、Indexesは削除する。
# ※.htaccessを使用するため、AllowOverride Allにする。
Options Indexes FollowSymLinks
↓
Options FollowSymLinks

AllowOverride None
↓
AllowOverride All

# <IfModule dir_module>内の設定の変更
DirectoryIndex index.html
↓
DirectoryIndex index.html index.php

# CGIを無効化する
#ScriptAlias /cgi-bin/ "/var/www/cgi-bin/" 
#
#<Directory "/var/www/cgi-bin">
#    AllowOverride None
#    Options None
#    Order allow,deny
#    Allow from all
#</Directory>


############################################################
# ↓アクセスログの設定
#     <IfModule log_config_module>ディレクティブ内に記載する
#     元のCustomLogはコメントする
############################################################
# アクセスログの設定
#   ログフォーマットを LTSV にする
#   IP アドレスを ELB の IP アドレスではなく X-Forwarded-For を見るようにする
#   ELB のヘルスチェックをログに書き出さない
#   ELB の internal dummy connection をログに書き出さない
#
    LogFormat "host:%{X-Forwarded-For}i\tident:%l\tuser:%u\ttime:%{%d/%b/%Y:%H:%M:%S %z}t\tmethod:%m\tpath:%U%q\tprotocol:%H\tstatus:%>s\tsize:%b\treferer:%{Referer}i\tagent:%{User-Agent}i\tresponse_time:%D" ltsv
    SetEnvIf User-Agent "ELB-HealthChecker" nolog
    SetEnvIf User-Agent "internal dummy connection" nolog
    CustomLog "logs/access_log" ltsv env=!nolog


############################################################
# ↓以下の設定は、ファイルの最終行に追記する
############################################################

# タイムアウト値などの設定
#   AWSの推奨値を設定
#   https://aws.amazon.com/jp/premiumsupport/knowledge-center/apache-backend-elb/
Timeout                 120
KeepAlive               On
KeepAliveTimeout        120
MaxKeepAliveRequests    100
AcceptFilter            http none 
AcceptFilter            https none

# Apache のバージョンを表示させない
#   ServerSignature : サーバーエラー時に Apache のバージョン表示消す
#   ServerTokens    : HTTP ヘッダから Apache のバージョン消す
ServerSignature         Off
ServerTokens            Prod

# 環境変数の追加 (最終行に追加)
#   開発の場合        ：develop
#   ステージングの場合：staging
#   本番の場合        ：product
SetEnv APPLICATION_ENV develop


############################################################
# ↓phpMyAdminを公開するための設定
#     <IfModule alias_module>ディレクティブ内に記載する
############################################################

    Alias /phpMyAdmin "/var/www/html/phpMyAdmin"
    <Directory "/var/www/html/phpMyAdmin">
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>

```

　

#### 3.1.3.2. mpm.confの設定

```shell
# 以下のコマンドを実行し、preforkであることを確認する
[root@localhost ~]# apachectl -V | grep MPM
Server MPM:     prefork

# mpm.confを新規作成する
[root@localhost ~]# vi /etc/httpd/conf.d/mpm.conf
----------
##########
# 参考：
# StartServers:         起動時に生成される子サーバプロセスの数。デフォルトは5。
# MinSpareServers:      アイドル状態にいる子サーバプロセスの最小（希望）個数。デフォルトは5。
# MaxSpareServers:      アイドル状態にいる子サーバプロセスの最大（希望）個数。デフォルトは10。
# ServerLimit:          MaxClientsに指定可能な値の上限。（MaxClientより大きな数にする）
# MaxClients:           応答できる同時リクエスト数。デフォルトは256。
# MaxRequestsPerChild:  個々の子サーバプロセスが扱うことのできるリクエストの総数。デフォルトは10000。0にすると無制限
##########

# デフォルト値 (ステージングで使用) ★数値は、未確定
<IfModule mpm_prefork_module>
    StartServers            5
    MinSpareServers         5
    MaxSpareServers        10
    ServerLimit           100
    MaxClients            100
    MaxRequestsPerChild  1000
</IfModule>

# 計算した値 (本番で使用) ★数値は、未確定
<IfModule mpm_prefork_module>
    StartServers          200
    MinSpareServers       200
    MaxSpareServers       200
    ServerLimit           200
    MaxClients            200
    MaxRequestsPerChild 10000
</IfModule>
# 
# ※上記の値は、以下の計算により算出した値
#   サーバのメモリ： 4GB → 使用可能 50% → 2GB
#   １回リクエストの平均使用メモリ：6MB → 10MBとして計算
#     ｜
#     └→ 2000MB ÷ 10MB ＝ 200
#
# ※以下のサイトなども参考にすること
#   Amazon EC2 Linux インスタンスで実行されている Apache ウェブサーバーへのメモリ割り当てを調整する方法を教えてください。
#   https://aws.amazon.com/jp/premiumsupport/knowledge-center/ec2-apache-memory-tuning/
#
----------
```

　

#### 3.1.3.3. 自動起動の設定と起動

```shell
[root@localhost ~]# chkconfig httpd on
[root@localhost ~]# /etc/init.d/httpd start
```

　

### 3.1.4. PHPの設定

#### 3.1.4.1. php.iniの設定

```shell
[root@localhost ~]# vi /etc/php.ini
----------
# 変更
;date.timezone =
↓
date.timezone = Asia/Tokyo
----------
```
※上記外にも、php.ini を「Development」「Production」で検索し、確認しておく。

　

### 3.1.5. MySQLの設定

#### 3.1.5.1. my.cnfの設定

```shell
[root@localhost ~]# vi /etc/my.cnf
----------
# [mysqld]のセクションに、以下を追加
explicit_defaults_for_timestamp=1
----------
```

　

#### 3.1.5.2. 自動起動の設定と起動

```shell
[root@localhost ~]# chkconfig mysqld on
[root@localhost ~]# /etc/init.d/mysqld start
```

　

#### 3.1.5.3. データベースの作成

```shell
[root@localhost ~]# mysql -u root -p
Enter password:  ← パスワードを入力

mysql> CREATE DATABASE bbdw_common CHARACTER SET utf8mb4;
mysql> GRANT ALL PRIVILEGES ON bbdw_common.* TO bbdw_user@localhost IDENTIFIED BY 'bbdw_pass!';

mysql> CREATE DATABASE bbdw_game_01 CHARACTER SET utf8mb4;
mysql> GRANT ALL PRIVILEGES ON bbdw_game_01.* TO bbdw_user@localhost IDENTIFIED BY 'bbdw_pass!';

mysql> CREATE DATABASE bbdw_game_02 CHARACTER SET utf8mb4;
mysql> GRANT ALL PRIVILEGES ON bbdw_game_02.* TO bbdw_user@localhost IDENTIFIED BY 'bbdw_pass!';

mysql> CREATE DATABASE bbdw_log CHARACTER SET utf8mb4;
mysql> GRANT ALL PRIVILEGES ON bbdw_log.* TO bbdw_user@localhost IDENTIFIED BY 'bbdw_pass!';

mysql> CREATE DATABASE bbdw_gms CHARACTER SET utf8mb4;
mysql> GRANT ALL PRIVILEGES ON bbdw_gms.* TO bbdw_user@localhost IDENTIFIED BY 'bbdw_pass!';

mysql> FLUSH PRIVILEGES;

mysql> exit
```

　

### 3.1.6. Memcached

#### 3.1.6.1. memcachedの設定

以下のファイルを参照して、設定値を確認しておく。

```shell
[root@localhost ~]# cat /etc/sysconfig/memcached
PORT="11211"
USER="memcached"
MAXCONN="1024"
CACHESIZE="64"
OPTIONS=""
```

　

#### 3.1.6.2. 自動起動の設定と起動

```shell
[root@localhost ~]# chkconfig memcached on
[root@localhost ~]# /etc/init.d/memcached start
```

　

### 3.1.7. ソースコードのデプロイと、テーブルの作成

#### 3.1.7.1. 1回目

```shell
# Subversion checkout (kanazawaは、担当により変更する)
[root@localhost ~]# cd /var/www/html
[root@localhost ~]# svn --username kanazawa checkout https://svn.vega-net.ne.jp/svn/os_bbdw/trunk/src/bbdw bbdw_api

# strageディレクトリの権限の変更
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# chmod 777 -R storage

# bootstrap/cacheディレクトリの権限の変更
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# chmod 777 -R bootstrap/cache

# .envファイルをコピーする (developは環境名)
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# cp -f .env.develop .env

# migrate (テーブルの作成)
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# php artisan migrate --database="common"  --path="database/migrations_common"
[root@localhost ~]# php artisan migrate --database="game_01" --path="database/migrations_game"
[root@localhost ~]# php artisan migrate --database="game_02" --path="database/migrations_game"
[root@localhost ~]# php artisan migrate --database="log"     --path="database/migrations_log"
[root@localhost ~]# php artisan migrate --database="gms"     --path="database/migrations_gms"
```

#### 3.1.7.2. 2回目以降

2回目以降は、ソースコードの更新のみを行う

```shell
# Subversion update (kanazawaは、担当により変更する)
[root@localhost ~]# cd /var/www/html/bbdw_api; svn --username kanazawa update
```

　

### 3.1.8. phpMyAdmin

#### 3.1.8.1. インストール

```shell
[root@localhost ~]# cd /root
[root@localhost ~]# wget https://files.phpmyadmin.net/phpMyAdmin/4.8.5/phpMyAdmin-4.8.5-all-languages.tar.gz
[root@localhost ~]# tar xzvf phpMyAdmin-4.8.5-all-languages.tar.gz
[root@localhost ~]# mv phpMyAdmin-4.8.5-all-languages phpMyAdmin
[root@localhost ~]# mv phpMyAdmin /var/www/html
```

インストール後は、http://xxx.xxx.xxx.xxx/phpMyAdmin に、アクセスすることができる。

　

　

　

## 3.2. develop環境

### 3.2.1. 概要

develop環境の構築手順書です。

　

### 3.2.2. OSの設定

・3.2.2.1. タイムゾーンの変更
　⇒ ★ /etc/sysconfig/clockを変更していないので気になる

・3.2.2.2. ログローテートの設定
　⇒ ★ S3にしたいので、一旦、保留

　

### 3.2.3. Apacheの設定

#### 3.2.3.1. httpd.confの設定

```shell
[root@localhost ~]# vi /etc/httpd/conf/httpd.conf
----------
# サーバ名の変更
ServerName bbdw-dev-api01 ← /etc/hostsで設定した値

# ドキュメントルートの変更
DocumentRoot "/var/www/html"
↓
DocumentRoot "/var/www/html/bbdw_api/public"

<Directory "/var/www/html">
↓
<Directory "/var/www/html/bbdw_api/public">

# <Directory "/var/www/html/bbdw_api/public">内の設定の変更
# ※セキュリティ上、良くないので、Indexesは削除する。
# ※.htaccessを使用するため、AllowOverride Allにする。
Options Indexes FollowSymLinks
↓
Options FollowSymLinks

AllowOverride None
↓
AllowOverride All

# <IfModule dir_module>内の設定の変更
DirectoryIndex index.html
↓
DirectoryIndex index.html index.php

# CGIを無効化する
#ScriptAlias /cgi-bin/ "/var/www/cgi-bin/" 
#
#<Directory "/var/www/cgi-bin">
#    AllowOverride None
#    Options None
#    Order allow,deny
#    Allow from all
#</Directory>


############################################################
# ↓アクセスログの設定
#     <IfModule log_config_module>ディレクティブ内に記載する
#     元のCustomLogはコメントする
############################################################
# アクセスログの設定
#   ログフォーマットを LTSV にする
#   IP アドレスを ELB の IP アドレスではなく X-Forwarded-For を見るようにする
#   ELB のヘルスチェックをログに書き出さない
#   ELB の internal dummy connection をログに書き出さない
#
    LogFormat "host:%{X-Forwarded-For}i\tident:%l\tuser:%u\ttime:%{%d/%b/%Y:%H:%M:%S %z}t\tmethod:%m\tpath:%U%q\tprotocol:%H\tstatus:%>s\tsize:%b\treferer:%{Referer}i\tagent:%{User-Agent}i\tresponse_time:%D" ltsv
    SetEnvIf User-Agent "ELB-HealthChecker" nolog
    SetEnvIf User-Agent "internal dummy connection" nolog
    CustomLog "logs/access_log" ltsv env=!nolog


############################################################
# ↓以下の設定は、ファイルの最終行に追記する
############################################################

# タイムアウト値などの設定
#   AWSの推奨値を設定
#   https://aws.amazon.com/jp/premiumsupport/knowledge-center/apache-backend-elb/
Timeout                 120
KeepAlive               On
KeepAliveTimeout        120
MaxKeepAliveRequests    100
AcceptFilter            http none 
AcceptFilter            https none

# Apache のバージョンを表示させない
#   ServerSignature : サーバーエラー時に Apache のバージョン表示消す
#   ServerTokens    : HTTP ヘッダから Apache のバージョン消す
ServerSignature         Off
ServerTokens            Prod

# 環境変数の追加 (最終行に追加)
#   開発の場合        ：develop
#   ステージングの場合：staging
#   本番の場合        ：product
SetEnv APPLICATION_ENV develop

```

　

#### 3.2.3.2. mpm.confの設定

```shell
# 以下のコマンドを実行し、preforkであることを確認する
[root@localhost ~]# apachectl -V | grep MPM
Server MPM:     prefork

# mpm.confを新規作成する
[root@localhost ~]# vi /etc/httpd/conf.d/mpm.conf
----------
##########
# 参考：
# StartServers:         起動時に生成される子サーバプロセスの数。デフォルトは5。
# MinSpareServers:      アイドル状態にいる子サーバプロセスの最小（希望）個数。デフォルトは5。
# MaxSpareServers:      アイドル状態にいる子サーバプロセスの最大（希望）個数。デフォルトは10。
# ServerLimit:          MaxClientsに指定可能な値の上限。（MaxClientより大きな数にする）
# MaxClients:           応答できる同時リクエスト数。デフォルトは256。
# MaxRequestsPerChild:  個々の子サーバプロセスが扱うことのできるリクエストの総数。デフォルトは10000。0にすると無制限
##########

# デフォルト値 (ステージングで使用) ★数値は、未確定
<IfModule mpm_prefork_module>
    StartServers            5
    MinSpareServers         5
    MaxSpareServers        10
    ServerLimit           100
    MaxClients            100
    MaxRequestsPerChild  1000
</IfModule>

# 計算した値 (本番で使用) ★数値は、未確定
<IfModule mpm_prefork_module>
    StartServers          200
    MinSpareServers       200
    MaxSpareServers       200
    ServerLimit           200
    MaxClients            200
    MaxRequestsPerChild 10000
</IfModule>
# 
# ※上記の値は、以下の計算により算出した値
#   サーバのメモリ： 4GB → 使用可能 50% → 2GB
#   １回リクエストの平均使用メモリ：6MB → 10MBとして計算
#     ｜
#     └→ 2000MB ÷ 10MB ＝ 200
#
# ※以下のサイトなども参考にすること
#   Amazon EC2 Linux インスタンスで実行されている Apache ウェブサーバーへのメモリ割り当てを調整する方法を教えてください。
#   https://aws.amazon.com/jp/premiumsupport/knowledge-center/ec2-apache-memory-tuning/
#
----------
```

　

#### 3.2.3.3. 自動起動の設定と起動

実際には、ドキュメントルート(/var/www/html/bbdw_api/public)を作成してから起動する

```shell
# 自動起動の確認
[root@localhost ~]# systemctl is-enabled httpd.service

# 自動起動の設定
[root@localhost ~]# systemctl enable httpd.service

# 起動の確認
[root@localhost ~]# systemctl status httpd.service

# 起動
[root@localhost ~]# systemctl start httpd.service
```

　

### 3.2.4. PHPの設定

#### 3.2.4.1. php.iniの設定

```shell
[root@localhost ~]# vi /etc/php.ini
----------
# 変更
;date.timezone =
↓
date.timezone = Asia/Tokyo
----------
```
※上記外にも、php.ini を「Development」「Production」で検索し、確認しておく。

　

#### 3.2.5.3. データベースの作成

* common

```shell
[root@localhost ~]# mysql -u root -h bbdw-dev-db-common.cceok9m7fef8.ap-northeast-1.rds.amazonaws.com -p
Enter password:  ← パスワードを入力

mysql> CREATE DATABASE bbdw_common CHARACTER SET utf8mb4;
mysql> CREATE USER 'bbdw_user'@'%' IDENTIFIED BY 'bbdw_pass!';
mysql> GRANT ALL PRIVILEGES ON bbdw_common.* TO 'bbdw_user'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

mysql> exit
```

* game01

```shell
[root@localhost ~]# mysql -u root -h bbdw-dev-db-game01.cceok9m7fef8.ap-northeast-1.rds.amazonaws.com -p
Enter password:  ← パスワードを入力

mysql> CREATE DATABASE bbdw_game_01 CHARACTER SET utf8mb4;
mysql> CREATE USER 'bbdw_user'@'%' IDENTIFIED BY 'bbdw_pass!';
mysql> GRANT ALL PRIVILEGES ON bbdw_game_01.* TO 'bbdw_user'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

mysql> exit
```

* game02

```shell
[root@localhost ~]# mysql -u root -h bbdw-dev-db-game02.cceok9m7fef8.ap-northeast-1.rds.amazonaws.com -p
Enter password:  ← パスワードを入力

mysql> CREATE DATABASE bbdw_game_02 CHARACTER SET utf8mb4;
mysql> CREATE USER 'bbdw_user'@'%' IDENTIFIED BY 'bbdw_pass!';
mysql> GRANT ALL PRIVILEGES ON bbdw_game_02.* TO 'bbdw_user'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

mysql> exit
```

* log

```shell
[root@localhost ~]# mysql -u root -h bbdw-dev-db-log.cceok9m7fef8.ap-northeast-1.rds.amazonaws.com -p
Enter password:  ← パスワードを入力

mysql> CREATE DATABASE bbdw_log CHARACTER SET utf8mb4;
mysql> CREATE USER 'bbdw_user'@'%' IDENTIFIED BY 'bbdw_pass!';
mysql> GRANT ALL PRIVILEGES ON bbdw_log.* TO 'bbdw_user'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

mysql> exit
```

* gms

```shell
[root@localhost ~]# mysql -u root -h bbdw-dev-db-gms.cceok9m7fef8.ap-northeast-1.rds.amazonaws.com -p
Enter password:  ← パスワードを入力

mysql> CREATE DATABASE bbdw_gms CHARACTER SET utf8mb4;
mysql> CREATE USER 'bbdw_user'@'%' IDENTIFIED BY 'bbdw_pass!';
mysql> GRANT ALL PRIVILEGES ON bbdw_gms.* TO 'bbdw_user'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

mysql> exit
```

　

### 3.2.6. ソースコードのデプロイと、テーブルの作成

#### 3.2.6.1. 1回目

```shell
# Git Clone (developは、環境により変更する)
[root@localhost ~]# cd /var/www/html
[root@localhost ~]# git clone -b develop https://bitbucket.org/bbdw/bbdw_api.git bbdw_api
Cloning into 'bbdw_api'...
Username for 'https://bitbucket.org':   ← ユーザー名を入力する
Password for 'https://kanazawa_takayuki@bitbucket.org':   ← パスワードを入力する

# strageディレクトリの権限の変更
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# chmod 777 -R storage

# bootstrap/cacheディレクトリの権限の変更
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# chmod 777 -R bootstrap/cache

# .envファイルをコピーする (developは環境名)
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# cp -f .env.develop .env

# migrate (テーブルの作成)
[root@localhost ~]# cd /var/www/html/bbdw_api
[root@localhost ~]# php artisan migrate --database="common"  --path="database/migrations_common"
[root@localhost ~]# php artisan migrate --database="game_01" --path="database/migrations_game"
[root@localhost ~]# php artisan migrate --database="game_02" --path="database/migrations_game"
[root@localhost ~]# php artisan migrate --database="log"     --path="database/migrations_log"
[root@localhost ~]# php artisan migrate --database="gms"     --path="database/migrations_gms"
```

#### 3.2.6.2. 2回目以降

2回目以降は、ソースコードの更新のみを行う

```shell
# Git Pull
[root@localhost ~]# cd /var/www/html/bbdw_api; git pull
Username for 'https://bitbucket.org':   ← ユーザー名を入力する
Password for 'https://kanazawa_takayuki@bitbucket.org':   ← パスワードを入力する
```

　

