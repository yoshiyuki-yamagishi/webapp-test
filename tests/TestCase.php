<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Exceptions\ApiException;
use App\Utils\DebugUtil;

abstract class TestCase extends BaseTestCase
{
    const PLAYER_ID_1 = 101;
    const PLAYER_ID_2 = 2001;
    const PLAYER_UNIQUE_ID_1 = 'AAAAAAAAAABBBBBB';
    const PLAYER_UNIQUE_ID_2 = 'HOGEHOGEHOGEHOGE';
    const PLAYER_UNIQUE_ID_3 = 'FUGAFUGAFUGAFUGA';
    const PLAYER_UNIQUE_ID_4 = 'CCCCCCCCDDDDDDDD';
    public static $PLAYER_TOKEN_1 = 'aaaaaaaaaaaaaaabbbbbbbbbbbbbcccccccccc';

	use CreatesApplication;

	/**
	 * テスト前
	 *
	 * {@inheritDoc}
	 * @see \Illuminate\Foundation\Testing\TestCase::setUp()
	 */
	public function setUp()
	{
		parent::setUp();
	}

	/**
	 * テスト後
	 *
	 * {@inheritDoc}
	 * @see \Illuminate\Foundation\Testing\TestCase::tearDown()
	 */
	public function tearDown()
	{
		parent::tearDown();
	}

	protected function _getVal($coll, $name)
    {
        if(is_array($coll))
           return $coll[$name];

        return $coll->{$name};
    }


	/**
	 * 正常なレスポンスかチェックする
	 *
	 * @param object $response レスポンス
	 */
	protected function _assertResponseOk($response)
	{
        $this->assertEquals(ApiException::S_OK, $response->statusCode);
	}

	/**
	 * 変数があるかチェックする (NULL でもよい)
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertIsKey($array, $key)
	{
        $this->assertTrue(array_key_exists($key, $array));
	}

	/**
	 * 配列かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertArray($array, $key)
	{
        $this->assertTrue(is_array($array[$key]));
	}

	/**
	 * 連想配列かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertList($array, $key)
	{
        $this->assertTrue(is_array($array[$key]));
	}

    /**
     * オブジェクトかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
	protected function _assertObject($array, $key)
    {
        $this->assertTrue(is_object($array[$key]));
    }


	/**
	 * int32 かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertInt($array, $key)
	{
	    $this->assertTrue(is_numeric($array[$key]));
	}

	/**
	 * uint8 かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertUInt8($array, $key)
	{
        $this->assertTrue(is_numeric($array[$key]));
        $this->assertGreaterThanOrEqual(0, $array[$key]);
        $this->assertLessThanOrEqual(255, $array[$key]);
	}

	/**
	 * uint64 かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertUInt64($array, $key, $required = true)
	{
	    if (!$required || is_null($array[$key]))
            return;

        $this->assertTrue(is_numeric($array[$key]));
        $this->assertGreaterThanOrEqual(0, $array[$key]);
	}

	/**
	 * 文字列 かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertStr($array, $key)
	{
	    if(is_null($array[$key]) || $array[$key] === "")
	        return;
	    $this->assertTrue(is_string($array[$key]));
	}

	/**
	 * 正常な日時かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertDateTime($array, $key, $required = true)
	{
        if (!$required && $array[$key] == '')
            return;

        $this->assertRegExp(
            '|[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}|',
            $array[$key]
        );
	}

    /**
     * 正常な日付かチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertDate($array, $key, $required = true)
    {
        if (!$required && $array[$key] == '')
            return;

        $this->assertRegExp(
            '|[0-9]{4}-[0-9]{2}-[0-9]{2}|',
            $array[$key]
        );
    }

    /**
     * 正常な時間かチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertTime($array, $key)
    {
        $this->assertRegExp(
            '|[0-9]{2}:[0-9]{2}:[0-9]{2}|',
            $array[$key]
        );
    }

	/**
	 * float かチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertFloat($array, $key)
	{
        $this->assertTrue(is_numeric($array[$key]));
	}

    /**
     * Player レスポンスかチェックする
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayer($array, $key)
    {
        $this->_assertList($array, $key);

        $player = $array[$key];

        $this->_assertStr($player, 'name');
        $this->_assertDate($player, 'birthday', false);
        $this->_assertDate($player, 'legal_birthday', false);
        $this->_assertUInt8($player, 'gender');
        $this->_assertUInt8($player, 'tutorial_progress');
        $this->_assertInt($player, 'tutorial_flag');
        $this->_assertInt($player, 'lv');
        $this->_assertUInt64($player, 'experience');
        $this->_assertInt($player, 'start_experience');
        $this->_assertInt($player, 'next_experience');
        $this->_assertInt($player, 'al');
        $this->_assertDateTime($player, 'al_recovery_at');
        $this->_assertInt($player, 'max_al');
        $this->_assertStr($player, 'message');
        $this->_assertInt($player, 'character_num');
        $this->_assertInt($player, 'grimoire_num');
        $this->_assertInt($player, 'free_blue_crystal_num');
        $this->_assertInt($player, 'blue_crystal_num');
        $this->_assertUInt64($player,'platinum_dollar_num');
        $this->_assertUInt64($player,'powder');
        $this->_assertUInt64($player,'magic_num');
        $this->_assertUInt64($player,'friend_point');
        $this->_assertInt($player,'max_grimoire');
        $this->_assertUInt64($player,'favorite_character', false);

	}

	/**
	 * PlayerPartyList レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertPlayerPartyList($array, $key)
	{
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerParty($array[$key], $_key);
        }
    }

	/**
	 * PlayerParty レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertPlayerParty($array, $key)
	{
        $this->_assertList($array, $key);

        $party = $array[$key];

        $this->_assertInt($party, 'player_party_id');
        $this->_assertUInt8($party, 'party_no');
        $this->_assertArray($party, 'party_character_list');
        foreach ($party['party_character_list'] as $chara)
        {
            $this->_assertUInt8($chara, 'position');
            $this->_assertIsKey($chara, 'player_character_id');
            $this->_assertIsKey($chara, 'player_grimoire_id');
        }
	}

	/**
	 * PlayerCharacterList レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertPlayerCharacterList($array, $key)
	{
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerCharacter($array[$key], $_key);
        }
    }

	/**
	 * PlayerCharacter レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertPlayerCharacter($array, $key)
	{
        $this->_assertList($array, $key);

        $chara = $array[$key];

        $this->_assertInt($chara, 'character_id');
        $this->_assertInt($chara, 'character_base_id');
        $this->_assertUInt64($chara, 'player_character_id');
        $this->_assertInt($chara, 'spine');
        $this->_assertInt($chara, 'evolve');
        $this->_assertInt($chara, 'grade');
        $this->_assertInt($chara, 'level');
        $this->_assertDateTime($chara, 'acquired_at');
        $this->_assertUInt64($chara, 'experience');
        $this->_assertInt($chara, 'start_experience');
        $this->_assertInt($chara, 'next_experience');
        $this->_assertUInt8($chara, 'party_flag');
        $this->_assertUInt8($chara, 'favorite_flag');
        $this->_assertInt($chara, 'piece_num');
        $this->_assertUInt8($chara, 'active_skill_1_level');
        $this->_assertUInt8($chara, 'active_skill_2_level');
        $this->_assertIsKey($chara, 'player_orb_id_1');
        $this->_assertIsKey($chara, 'player_orb_id_2');
        $this->_assertIsKey($chara, 'player_orb_id_3');
        $this->_assertIsKey($chara, 'player_orb_id_4');
        $this->_assertIsKey($chara, 'player_orb_id_5');
        $this->_assertIsKey($chara, 'player_orb_id_6');
	}

	/**
	 * PlayerGrimoireList レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertPlayerGrimoireList($array, $key)
	{
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerGrimoire($array[$key], $_key);
        }
    }

	/**
	 * PlayerGrimoire レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertPlayerGrimoire($array, $key)
	{
        $this->_assertList($array, $key);

        $grim = $array[$key];

        $this->_assertInt($grim, 'grimoire_id');
        $this->_assertUInt64($grim, 'player_grimoire_id');
        $this->_assertInt($grim, 'slot_release_num');
        $this->_assertUInt8($grim, 'slot_release_flag_1');
        $this->_assertIsKey($grim, 'slot_id_1');
        $this->_assertUInt8($grim, 'slot_release_flag_2');
        $this->_assertIsKey($grim, 'slot_id_2');
        $this->_assertUInt8($grim, 'slot_release_flag_3');
        $this->_assertIsKey($grim, 'slot_id_3');
        $this->_assertUInt8($grim, 'awake');
        $this->_assertUInt8($grim, 'lock_flag');
        $this->_assertUInt8($grim, 'use_flag');
        $this->_assertDateTime($grim, 'acquired_at');
	}

	/**
	 * BattleEnemyList レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertBattleEnemyList($array, $key)
	{
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertBattleEnemy($array[$key], $_key);
        }
    }

	/**
	 * BattleEnemy レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertBattleEnemy($array, $key)
	{
        $this->_assertList($array, $key);

        $enemy = $array[$key];

        $this->_assertUInt8($enemy, 'wave');
        $this->_assertUInt8($enemy, 'position');
        $this->_assertInt($enemy, 'enemy_id');
        $this->_assertInt($enemy, 'info_sort');
        $this->_assertUInt8($enemy, 'boss_flag');
        $this->_assertInt($enemy, 'level');
        $this->_assertUInt8($enemy, 'element');
        $this->_assertInt($enemy, 'hp_bar');
        $this->_assertFloat($enemy, 'hp_coefficient');
        $this->_assertFloat($enemy, 'atk_coefficient');
        $this->_assertFloat($enemy, 'def_coefficient');
        $this->_assertInt($enemy, 'resist_abnormal_id');
        $this->_assertInt($enemy, 'active1');
        $this->_assertInt($enemy, 'active2');
        $this->_assertInt($enemy, 'enemy_ai_id');
        $this->_assertInt($enemy, 'battle_info_id');
        $this->_assertUInt8($enemy, 'drop_box_type');
	}

	/**
	 * QuestRewardList レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertQuestRewardList($array, $key)
	{
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertQuestReward($array[$key], $_key);
        }
    }

	/**
	 * QuestReward レスポンスかチェックする
	 *
	 * @param object $array チェックする配列
	 * @param string $key キー名
	 */
	protected function _assertQuestReward($array, $key)
	{
        $this->_assertList($array, $key);

        $reward = $array[$key];

        $this->_assertUInt8($reward, 'frame_count');
        $this->_assertUInt8($reward, 'item_type');
        $this->_assertInt($reward, 'item_id');
        $this->_assertInt($reward, 'item_num');
        $this->_assertUInt8($reward, 'take_flag');
	}

    /**
     * SkipQuestRewardList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
	protected function _assertSkipQuestRewardList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertSkipQuestReward($array[$key], $_key);
        }
    }

    /**
     * SkipQuestReward レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertSkipQuestReward($array, $key)
    {
        $this->_assertList($array, $key);

        $reward = $array[$key];

        $this->_assertQuestRewardList($reward, 'fix_reward_list');
        $this->_assertQuestRewardList($reward, 'drop_reward_list');
    }

    /**
     * PlayerMissionList レスポンスかチェックする
     *
     * @param object $array チェックする名前
     * @param string $key キー名
     */
    protected function _assertPlayerMissionList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerMission($array[$key], $_key);
        }
	}

    /**
     * PlayerMission レスポンスかチェックする
     *
     * @param object $array チェックする名前
     * @param string $key キー名
     */
    protected function _assertPlayerMission($array, $key)
    {
        $this->_assertList($array, $key);

        $mission = $array[$key];

        $this->_assertInt($mission, 'mission_id');
        $this->_assertUInt64($mission, 'player_mission_id');
        $this->_assertUInt8($mission, 'mission_type');
        $this->_assertInt($mission, 'progress_count');
        $this->_assertInt($mission, 'count');
        $this->_assertUInt8($mission, 'take_flag');
        $this->_assertInt($mission, 'state_flag');
        $this->_assertInt($mission, 'remuneration');
        $this->_assertInt($mission, 'remuneration_count');
        //TODO:APIエラー YY[/]MM → YY[-]MM
        //$this->_assertDateTime($mission, 'start_day');
        //$this->_assertDateTime($mission, 'end_day');
    }

    /**
     * FriendList レスポンスかチェックする
     *
     * @param object $array チェックする名前
     * @param string $key キー名
     */
    protected function _assertFriendList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertFriend($array[$key], $_key);
        }
    }

    /**
     * Friend レスポンスかチェックする
     *
     * @param object $array チェックする名前
     * @param string $key キー名
     */
    protected function _assertFriend($array, $key)
    {
        $this->_assertList($array, $key);

        $friend = $array[$key];

        $this->_assertInt($friend, 'id');
        $this->_assertStr($friend, 'disp_id');
        $this->_assertUInt8($friend, 'friend_type');
        $this->_assertStr($friend, 'name');
        $this->_assertInt($friend, 'lv');
        $this->_assertInt($friend, 'main_character');
        $this->_assertStr($friend, 'message');
        $this->_assertDateTime($friend, 'last_login_at');
    }

    /**
     * GachaList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertGachaList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertGacha($array[$key], $_key);
        }
    }

    /**
     * Gacha レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertGacha($array, $key)
    {
        $this->_assertArray($array, $key);

        $gacha = $array[$key];

        $this->_assertInt($gacha, 'gacha_id');
        $this->_assertInt($gacha, 'limit_count');
    }

    /**
     * GachaResultList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertGachaResultList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertGachaResult($array[$key], $_key);
        }
    }

    /**
     * GachaResult レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertGachaResult($array, $key)
    {
        $this->_assertArray($array, $key);

        $gachaResult = $array[$key];

        $this->_assertUInt8($gachaResult, 'item_type');
        $this->_assertInt($gachaResult, 'item_id');
        $this->_assertInt($gachaResult, 'item_count');
        $this->_assertUInt8($gachaResult, 'gacha_result_flag');
        $this->_assertUInt8($gachaResult, 'take_flag');
    }

    /**
     * PlayerItemList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerItemList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerItem($array[$key], $_key);
        }
    }

    /**
     * PlayerItem レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerItem($array, $key)
    {
        $this->_assertArray($array, $key);

        $playerItem = $array[$key];

        $this->_assertInt($playerItem, 'item_id');
        $this->_assertUInt64($playerItem, 'player_item_id');
        $this->_assertInt($playerItem, 'num');
    }

    /**
     * LoginBonusList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertLoginBonusList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertLoginBonus($array[$key], $_key);
        }
    }

    /**
     * LoginBonus レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertLoginBonus($array, $key)
    {
        $this->_assertArray($array, $key);
        $loginBonus = $array[$key];

        $this->_assertInt($loginBonus, 'id');
        $this->_assertInt($loginBonus, 'count');
        $this->_assertInt($loginBonus, 'remuneration');
        $this->_assertInt($loginBonus, 'num');
        $this->_assertUInt8($loginBonus, 'take_flag');
    }

    /**
     * BannerList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertBannerList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertBanner($array[$key], $_key);
        }
    }

    /**
     * Banner レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertBanner($array, $key)
    {
        $this->_assertArray($array, $key);
        $banner = $array[$key];

        $this->_assertInt($banner, 'id');
        $this->_assertInt($banner, 'banner_type');
        $this->_assertInt($banner, 'priority');
        $this->_assertStr($banner, 'banner_pic');
        $this->_assertStr($banner, 'link_type');
        $this->_assertInt($banner, 'link_id');
        //TODO:APIエラー YY[/]MM → YY[-]MM
        //$this->_assertDateTime($banner, 'start_day');
        //$this->_assertDateTime($banner, 'end_day');
    }

    /**
     * PlayerPresentList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerPresentList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerPresent($array[$key], $_key);
        }
    }

    /**
     * PlayerPresent レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerPresent($array, $key)
    {
        $this->_assertArray($array, $key);
        $player_present = $array[$key];

        $this->_assertUInt64($player_present, 'player_present_id');
        $this->_assertUInt8($player_present, 'type');
        $this->_assertInt($player_present, 'general_id');
        $this->_assertInt($player_present, 'num');
        $this->_assertStr($player_present, 'message');
        $this->_assertDateTime($player_present, 'taked_at', false);
        $this->_assertDateTime($player_present, 'expired_at');
    }

    /**
     * PlayerQuestList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerQuestList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerQuest($array[$key], $_key);
        }
    }

    /**
     * PlayerQuest レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerQuest($array, $key)
    {
        $this->_assertArray($array, $key);
        $quest = $array[$key];

        $this->_assertUInt64($quest, 'player_quest_id');
        $this->_assertUInt8($quest, 'quest_category');
        $this->_assertInt($quest, 'chapter_id');
        $this->_assertInt($quest, 'quest_id');
        $this->_assertUint8($quest, 'clear_flag');
        $this->_assertUInt8($quest, 'mission_flag_1');
        $this->_assertUInt8($quest, 'mission_flag_2');
        $this->_assertUInt8($quest, 'mission_flag_3');
        $this->_assertInt($quest, 'clear_count');
        $this->_assertInt($quest, 'daily_clear_count');
    }

    /**
     * ProductList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertProductList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertProduct($array[$key], $_key);
        }
    }

    /**
     * Product レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertProduct($array, $key)
    {
        $this->_assertArray($array, $key);
        $product = $array[$key];

        $this->_assertInt($product, 'product_id');
        //TODO:APIエラー YY[/]MM → YY[-]MM
/*        $this->_assertDateTime($product, 'start_day');
        $this->_assertDateTime($product, 'end_day');*/
        $this->_assertInt($product, 'limit');
        $this->_assertInt($product, 'num');
        //TODO:APIエラー YY[/]MM → YY[-]MM
/*        $this->_assertDateTime($product, 'buy_at');
        $this->_assertDateTime($product, 'daily_end_at');*/
    }

    protected function _assertShopList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertShop($array[$key], $_key);
        }
    }

    protected function _assertShop($array, $key)
    {
        $this->_assertArray($array, $key);
        $shop = $array[$key];

        $this->_assertInt($shop, 'shop_id');
        $this->_assertUInt64($shop, 'shop_category');
        //TODO:APIエラー YY[/]MM → YY[-]MM
        /*        $this->_assertDateTime($product, 'start_day');
                $this->_assertDateTime($product, 'end_day');*/
        $this->_assertStr($shop, 'reset_time1');
        $this->_assertStr($shop, 'reset_time2');
        $this->_assertStr($shop, 'reset_time3');
        $this->_assertInt($shop, 'reset_item_id');
        $this->_assertInt($shop, 'reset_item_count');
        $this->_assertStr($shop, 'event_banner');
    }


    /**
     * ShopItemList レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertShopItemList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertShopItem($array[$key], $_key);
        }
    }

    /**
     * ShopItem レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertShopItem($array, $key)
    {
        $this->_assertArray($array, $key);
        $shop = $array[$key];

        $this->_assertInt($shop, 'shop_exchange_id');
        $this->_assertUInt64($shop, 'player_shop_item_id');
        $this->_assertInt($shop, 'shop_id');
        $this->_assertUInt8($shop, 'item_type');
        $this->_assertInt($shop, 'item_id');
        $this->_assertInt($shop, 'get_count');
        $this->_assertInt($shop, 'priority');
        $this->_assertInt($shop, 'pay_item_contents_id');
        $this->_assertInt($shop, 'pay_item_count');
        $this->_assertInt($shop, 'buy_count');
        $this->_assertInt($shop, 'limit');
    }

    /**
     * PlayerDatalist レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerDataList($array, $key)
    {
        foreach (array_keys($array[$key]) as $_key)
        {
            $this->_assertPlayerData($array[$key], $_key);
        }
    }

    /**
     * PlayerData レスポンスかチェックする
     *
     * @param object $array チェックする配列
     * @param string $key キー名
     */
    protected function _assertPlayerData($array, $key)
    {
        $this->_assertArray($array, $key);
        $data = $array[$key];

        $this->_assertInt($data, 'type');
        $this->_assertInt($data, 'value');
    }

}
