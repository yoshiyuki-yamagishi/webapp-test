<?php
/**
 * memcached のテスト
 *
 */

namespace Test\Unit\Services;
use Tests\TestCase;
use App\Utils\DebugUtil;

class MemcachedTest extends TestCase
{
	/**
	 * memcached の動作確認
	 */
	public function testSetGet()
	{
        $tags = ['test'];
        $key = 'abc';
        $value = 3;

        $memc = new \Memcached();
        // $memc->addServer('localhost', 11211);
        $memc->addServer('memcached', 11211);
        $memc->set($key, $value);
        
        $this->assertEquals($value, $memc->get($key));

        $memc->set($key, null);

        $this->assertNull($memc->get($key));
	}
    
}
