<?php
/**
 * Cache のテスト
 *
 */

namespace Test\Unit\Services;
use Tests\TestCase;
use App\Utils\DebugUtil;
use Illuminate\Support\Facades\Cache;

class CacheTest extends TestCase
{
	/**
	 * Cache の動作確認
	 */
	public function testTagFlush()
	{
        $tags1 = ['test1'];
        $tags2 = ['test2'];
        $key = 'abc';
        $value1 = 3;
        $value2 = 4;

        Cache::tags($tags1)->forever($key, $value1);
        Cache::tags($tags2)->forever($key, $value2);
        
        $this->assertEquals($value1, Cache::tags($tags1)->get($key));
        $this->assertEquals($value2, Cache::tags($tags2)->get($key));

        Cache::tags($tags1)->flush();

        $this->assertNull(Cache::tags($tags1)->get($key));
        $this->assertEquals($value2, Cache::tags($tags2)->get($key));

        Cache::tags($tags2)->flush();
        
        $this->assertNull(Cache::tags($tags1)->get($key));
        $this->assertNull(Cache::tags($tags2)->get($key));
	}
    
}
