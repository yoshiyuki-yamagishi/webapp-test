<?php
/**
 * 開発用サーバー プレイヤ引き継ぎ テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class DevTransitTest extends TransitTestCase
{
    public static $BASE_NAME = 'dev_trans_';
    // public static $BASE_URL = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com/v1/';
    public static $BASE_URL = 'http://localhost/public/index.php/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        static::_testAll();
	}
}
