<?php
/**
 * ローカルサーバー API テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class LocalServerLoginAndBattleTest extends LoginAndBattleTestCase
{
    public static $BASE_NAME = 'local_login_and_battle_';
    public static $BASE_URL = 'http://localhost/public/index.php/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        static::_testAll();
	}
}
