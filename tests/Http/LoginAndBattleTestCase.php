<?php
/**
 * サーバー API テスト基底クラス
 *
 */

namespace Tests\Http;

use Tests\TestCase;
use App\Utils\DebugUtil;
use App\Utils\EncryptUtil;

class LoginAndBattleTestCase extends TestCase
{
    public $testCases = [];

	/**
	 * 初期化
	 *
	 */
    public function setUp()
    {
        parent::setUp();

        Player_Regist_TestCase::append(
            $this->testCases
        );
        Login_TestCase::append(
            $this->testCases
        );
        Home_TestCase::append(
            $this->testCases
        );
        PlayerCharacter_List_TestCase::append(
            $this->testCases
        );
        PlayerParty_List_TestCase::append(
            $this->testCases
        );
        Battle_Start_Story_TestCase::append(
            $this->testCases
        );
        Battle_End_TestCase::append(
            $this->testCases
        );
        PlayerQuest_List_TestCase::append(
            $this->testCases
        );

        // 共通設定 //
        
        $tmpl = Api\Login_TestCase::make();
        $tmpl->baseUrl = static::$BASE_URL;
        $tmpl->baseName = static::$BASE_NAME;
        $tmpl->outputMsgPack = false;
        $tmpl->outputResponse = true;
        $tmpl->outputResult = true;

        ServerApiTestCase::_setCommonImpl($this->testCases, $tmpl);
    }    

	/**
	 * 全 API のテスト
	 * @param string $filter 実行する API の正規表現
	 *
	 */
    protected function _testAll()
	{
        static::_deleteAllLog();
        static::_putSettingLog();

        $ress = [];

        foreach ($this->testCases as $key => $testCase)
        {
            if ($key != 'player/regist')
            {
                // プレイヤー ID 設定
                $resR = $ress['player/regist'];
                $testCase->param['player_id'] = $resR["body"]["player_id"];
            }

            if ($key != 'player/regist' && $key != 'login')
            {
                // 認証コード設定
                $resL = $ress['login'];
                unset($testCase->param['_api']);
                $testCase->param['auth_code'] = $resL["body"]["auth_code"];
            }
            
            if ($key == 'battle/end')
            {
                // バトルコード設定
                $resB = $ress['battle/start'];
                $testCase->param['battle_code'] = $resB["body"]["battle_code"];
            }
            
            $ress[$key] = $testCase->test();
        }

        $this->assertTrue(true);
	}

	/**
	 * 設定ログの出力
	 *
	 */
	protected function _putSettingLog()
    {
        $encryptRequest = EncryptUtil::isDecryptRequest();
        $decriptResponse = EncryptUtil::isEncryptResponse();
        
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "BASE_URL", static::$BASE_URL
        );
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "リクエスト暗号化", ($encryptRequest ? "○" : "×")
        );
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "レスポンス暗号化", ($decriptResponse ? "○" : "×")
        );
    }

	/**
	 * ログの削除
	 *
	 */
	protected function _deleteAllLog()
    {
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'setting.txt');
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'response.txt');
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'result.tsv');
    }
}
