<?php
/**
 * 開発サーバー API 計測テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class DevMeasureApiTest extends MeasureApiTestCase
{
    public static $BASE_NAME = 'dev_m_';
    public static $BASE_URL = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        $this->testCount = 10;
        // $this->testSeconds = 10;
        
        // static::_testAll();
        static::_testSingle();
	}
}
