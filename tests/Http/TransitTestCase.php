<?php
/**
 * プレイヤ引継ぎ テスト基底クラス
 *
 */

namespace Tests\Http;

use Tests\TestCase;
use App\Utils\DebugUtil;
use App\Utils\EncryptUtil;
use App\Utils\StrUtil;

class TransitTestCase extends ServerApiTestCase
{
	/**
	 * 初期化
	 *
	 */
    public function setUp()
    {
        TestCase::setUp();
        
        // 共通設定 //

        if (empty($this->tmpl))
        {
            $this->tmpl = Api\Login_TestCase::make();
            $this->tmpl->baseUrl = static::$BASE_URL;
            $this->tmpl->baseName = static::$BASE_NAME;
            $this->tmpl->outputMsgPack = false;
            $this->tmpl->outputResponse = true;
            $this->tmpl->outputResult = true;
        }

        Api\Login_TestCase::append(
            $this->testCases
        );
        Api\Player_TransitSrc_TestCase::append(
            $this->testCases
        );
        Api\Player_Regist_TestCase::append(
            $this->testCases
        );
        Api\Player_TransitDst_TestCase::append(
            $this->testCases
        );
        
        ServerApiTestCase::_setCommonImpl($this->testCases, $this->tmpl);
    }

	/**
	 * 全 API のテスト
	 * @param $testCases 実行するテストの配列
	 * @param string $filter 実行する API の正規表現
	 *
	 */
    public static function _testImpl($testCases, $filter = null)
    {
        $ress = [];
        $testAuthCode = false;

        // 何度か繰り返す
        for ($i = 0; $i < 10; ++ $i)
        {
            foreach ($testCases as $key => $testCase)
            {
                if (isset($filter) && strlen($filter) > 0)
                {
                    if (!preg_match($filter, $key))
                        continue;
                }

                if ($key == 'login' || $key == 'player/transit_src')
                {
                    if ($i > 0)
                    {
                        // 前のループ最後のプレイヤでログインする
                        self::_copyRess(
                            $testCase, $ress, '#^player/transit_dst#',
                            'player_id'
                        );
                    }
                }
                else if ($key == 'player/transit_dst')
                {
                    self::_copyRess(
                        $testCase, $ress, '#^player/regist#',
                        'player_id'
                    );
                    self::_copyRess(
                        $testCase, $ress, '#^player/transit_src#',
                        'passcode'
                    );
                }

                $ress[$key] = $testCase->test();
            }
        }
    }
    
}
