<?php
/**
 * サーバー API テスト基底クラス
 *
 */

namespace Tests\Http;

use Tests\TestCase;
use App\Utils\DebugUtil;
use App\Utils\EncryptUtil;
use App\Utils\StrUtil;

class ServerApiTestCase extends TestCase
{
    public $tmpl = null;
    public $testCases = [];

	/**
	 * 初期化
	 *
	 */
    public function setUp()
    {
        parent::setUp();

        // 共通設定 //

        if (empty($this->tmpl))
        {
            $this->tmpl = Api\Login_TestCase::make();
            $this->tmpl->baseUrl = static::$BASE_URL;
            $this->tmpl->baseName = static::$BASE_NAME;
            $this->tmpl->outputMsgPack = true;
            $this->tmpl->outputResponse = true;
            $this->tmpl->outputResult = true;
        }

        Api\Login_TestCase::append(
            $this->testCases
        );
        Api\Login_Update_TestCase::append(
            $this->testCases
        );
        Api\Player_Regist_TestCase::append(
            $this->testCases, 'a'
        );
        Api\Player_Regist_TestCase::append(
            $this->testCases, 'b'
        );
        Api\Player_Get_TestCase::append(
            $this->testCases
        );
        Api\Player_Update_TestCase::append(
            $this->testCases
        );
        Api\Player_TutorialUpdate_TestCase::append(
            $this->testCases
        );
        Api\Player_AlRecover_Item_TestCase::append(
            $this->testCases, 'item'
        );
        Api\Player_AlRecover_Crystal_TestCase::append(
            $this->testCases, 'crystal'
        );
        Api\Player_TransitSrc_TestCase::append(
            $this->testCases
        );
        Api\Player_TransitDst_TestCase::append(
            $this->testCases
        );
        Api\Player_AccountStart_TestCase::append(
            $this->testCases
        );
        Api\Player_AccountInfo_TestCase::append(
            $this->testCases
        );
        Api\Player_AccountRestore_TestCase::append(
            $this->testCases
        );
        Api\Player_AccountEnd_TestCase::append(
            $this->testCases
        );
        Api\Home_TestCase::append(
            $this->testCases
        );
        Api\PlayerPresent_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerPresent_Take_One_TestCase::append(
            $this->testCases, 'one'
        );
        Api\PlayerPresent_Take_All_TestCase::append(
            $this->testCases, 'all'
        );
        Api\PlayerCharacter_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerCharacter_Update_TestCase::append(
            $this->testCases
        );
        Api\PlayerCharacter_Evolve_TestCase::append(
            $this->testCases
        );
        Api\PlayerCharacter_Reinforce_TestCase::append(
            $this->testCases
        );
        Api\PlayerCharacter_SkillReinforce_TestCase::append(
            $this->testCases
        );
        Api\PlayerCharacter_OrbEquip_TestCase::append(
            $this->testCases
        );
        Api\PlayerCharacter_FavoriteSelect_TestCase::append(
            $this->testCases
        );
        Api\PlayerGrimoire_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerGrimoire_Lock_TestCase::append(
            $this->testCases
        );
        Api\PlayerGrimoire_Reinforce_TestCase::append(
            $this->testCases
        );
        Api\PlayerGrimoire_Reinforce_Release_TestCase::append(
            $this->testCases, 'release'
        );
        Api\PlayerGrimoire_Reinforce_Equip_TestCase::append(
            $this->testCases, 'equip'
        );
        Api\PlayerGrimoire_LimitUp_TestCase::append(
            $this->testCases
        );
        Api\PlayerGrimoire_Sell_TestCase::append(
            $this->testCases
        );
        Api\PlayerParty_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerParty_Update_TestCase::append(
            $this->testCases
        );
        Api\Battle_Start_Story_TestCase::append(
            $this->testCases, 'story'
        );
        Api\Battle_Restart_TestCase::append(
            $this->testCases, 'story'
        );
        Api\Battle_Continue_TestCase::append(
            $this->testCases, 'story'
        );
        Api\Battle_End_TestCase::append(
            $this->testCases, 'story'
        );
        Api\Battle_Skip_TestCase::append(
            $this->testCases, 'story'
        );
        Api\Battle_Start_Character_TestCase::append(
            $this->testCases, 'character'
        );
        Api\Battle_Interrupt_TestCase::append(
            $this->testCases, 'character'
        );
        Api\Battle_Start_Event_TestCase::append(
            $this->testCases, 'event'
        );
        Api\Battle_End_TestCase::append(
            $this->testCases, 'event'
        );
        Api\Gacha_List_TestCase::append(
            $this->testCases
        );
        Api\Gacha_Exec_TestCase::append(
            $this->testCases
        );
        Api\Gacha_Retry_TestCase::append(
            $this->testCases
        );
        Api\Gacha_Commit_TestCase::append(
            $this->testCases
        );
        Api\PlayerQuest_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerQuest_Update_TestCase::append(
            $this->testCases
        );
        Api\Shop_List_TestCase::append(
            $this->testCases
        );
        Api\Shop_Buy_TestCase::append( // LineupUpdate 前
            $this->testCases
        );
        Api\Shop_LineupUpdate_TestCase::append(
            $this->testCases
        );
        Api\PlayerItem_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerItem_List_Orb_TestCase::append(
            $this->testCases, 'orb'
        );
        Api\PlayerItem_Sell_TestCase::append(
            $this->testCases
        );
        Api\PlayerItem_PowderPurify_TestCase::append(
            $this->testCases
        );
        Api\PlayerItem_OrbSynthesize_TestCase::append(
            $this->testCases
        );
        Api\PlayerData_Get_TestCase::append(
            $this->testCases
        );
        Api\PlayerData_Set_TestCase::append(
            $this->testCases
        );
        Api\PlayerMission_List_TestCase::append(
            $this->testCases
        );
        Api\PlayerMission_RewardTake_One_TestCase::append(
            $this->testCases, 'one'
        );
        Api\PlayerMission_RewardTake_All_TestCase::append(
            $this->testCases, 'all'
        );
        Api\Product_List_TestCase::append(
            $this->testCases
        );
        Api\Product_Buy_TestCase::append(
            $this->testCases, 'app_store_0'
        );
        Api\Product_Cancel_TestCase::append(
            $this->testCases, 'app_store_0'
        );
        Api\Product_Buy_TestCase::append(
            $this->testCases, 'app_store_1'
        );
        Api\Product_Regist_TestCase::append(
            $this->testCases, 'app_store_1'
        );
        Api\Product_Buy_TestCase::append(
            $this->testCases, 'app_store_2'
        );
        Api\Product_Resume_TestCase::append(
            $this->testCases, 'app_store_2'
        );
        Api\Product_Buy_TestCase::append(
            $this->testCases, 'google_store_0'
        );
        Api\Product_Cancel_TestCase::append(
            $this->testCases, 'google_store_0'
        );
        Api\Product_Buy_TestCase::append(
            $this->testCases, 'google_store_1'
        );
        Api\Product_Regist_TestCase::append(
            $this->testCases, 'google_store_1'
        );
        Api\Product_Buy_TestCase::append(
            $this->testCases, 'google_store_2'
        );
        Api\Product_Resume_TestCase::append(
            $this->testCases, 'google_store_2'
        );
        Api\Follow_Add_TestCase::append(
            $this->testCases
        );
        Api\Friend_List_TestCase::append(
            $this->testCases
        );
        Api\Follow_Delete_TestCase::append(
            $this->testCases
        );
        
        ServerApiTestCase::_setCommonImpl($this->testCases, $this->tmpl);
    }

	/**
	 * API 毎に共通の設定を行う
	 * @param array $testCase 設定先
	 * @param $template 共通の設定
	 *
	 */
    public static function _setCommonImpl($testCase, $template)
    {
        if (!is_array($testCase))
        {
            $testCase->baseUrl = $template->baseUrl;
            $testCase->baseName = $template->baseName;
            $testCase->outputMsgPack = $template->outputMsgPack;
            $testCase->outputResponse = $template->outputResponse;
            $testCase->outputResult = $template->outputResult;
            return;
        }
        
        foreach ($testCase as $testItem)
        {
            self::_setCommonImpl($testItem, $template);
        }
    }

	/**
	 * API 毎に共通のリクエストパラメータ設定を行う
	 * @param array $testCase 設定先
	 * @param $values 共通の設定
	 *
	 */
    public static function _setParamImpl($testCase, $values)
    {
        if (!is_array($testCase))
        {
            foreach ($values as $key => $value)
                $testCase->param[$key] = $value;
            return;
        }
        
        foreach ($testCase as $testItem)
        {
            self::_setParamImpl($testItem, $values);
        }
    }

	/**
	 * API 毎に共通の追加リクエストパラメータ設定を行う
	 * @param array $testCase 設定先
	 * @param $values 共通の設定
	 *
	 */
    public static function _setExParamImpl($testCase, $values)
    {
        if (!is_array($testCase))
        {
            foreach ($values as $key => $value)
                $testCase->exParam[$key] = $value;
            return;
        }
        
        foreach ($testCase as $testItem)
        {
            self::_setExParamImpl($testItem, $values);
        }
    }

    public static function _searchRess(&$ress, $filter)
    {
        $ret = null;
        foreach ($ress as $key => $value)
        {
            // 一番最後にマッチするものを返す
            if (preg_match($filter, $key))
                $ret = $value;
        }
        return $ret;
    }

    public static function _copyRess(
        &$testCase, &$ress, $filter, $from, $to = null
    )
    {
        $res = self::_searchRess($ress, $filter);
        if (!isset($res))
            return;
        if (!isset($to))
            $to = $from;
        
        $testCase->param[$to] = $res["body"][$from];
    }

    public static function _copyRessHeader(
        &$testCase, &$ress, $filter, $from, $to = null
    )
    {
        $res = self::_searchRess($ress, $filter);
        if (!isset($res))
            return;
        if (!isset($to))
            $to = $from;
        
        $testCase->param[$to] = $res["header"][$from];
    }
    
	/**
	 * 全 API のテスト
	 * @param $testCases 実行するテストの配列
	 * @param string $filter 実行する API の正規表現
	 *
	 */
    public static function _testImpl($testCases, $filter = null)
    {
        $ress = [];
        $testAuthCode = false;

        foreach ($testCases as $key => $testCase)
        {
            if (isset($filter) && strlen($filter) > 0)
            {
                if (!preg_match($filter, $key))
                    continue;
            }
            
            if ($key == 'player/transit_dst')
            {
                self::_copyRess(
                    $testCase, $ress, '#^player/regist a$#', 'player_id'
                );
                self::_copyRess(
                    $testCase, $ress, '#^player/transit_src#', 'passcode'
                );
            }
            else if ($key == 'player/account_restore' ||
                     $key == 'player/account_info')
            {
                self::_copyRess(
                    $testCase, $ress, '#^player/regist b$#', 'player_id'
                );
            }
            else if (
                StrUtil::prefixEq($key, 'battle/restart') ||
                StrUtil::prefixEq($key, 'battle/continue') ||
                StrUtil::prefixEq($key, 'battle/end') ||
                StrUtil::prefixEq($key, 'battle/interrupt')
            )
            {
                self::_copyRess(
                    $testCase, $ress, '#^battle/start#', 'battle_code'
                );
            }
            else if ($key == 'gacha/commit' ||
                     $key == 'gacha/retry')
            {
                self::_copyRess(
                    $testCase, $ress, '#^gacha/exec#', 'player_gacha_id'
                );
            }
            else if (StrUtil::prefixEq($key, 'player_present/take'))
            {
                self::_copyRessHeader(
                    $testCase, $ress, '#^player_present/list#', 'current_date'
                );
            }
            else if (
                StrUtil::prefixEq($key, 'product/regist') ||
                StrUtil::prefixEq($key, 'product/cancel')
            )
            {
                self::_copyRess(
                    $testCase, $ress, '#^product/buy#', 'player_buy_id'
                );
            }

            // auth_code のテスト
            if ($testAuthCode)
            {
                if ($key == 'player/transit_dst')
                {
                    // regist a で、ログインしてないから無理
                }
                else if ($key == 'player/account_restore' ||
                         $key == 'player/account_info')
                {
                    // regist b で、ログインしてないから無理
                }
                else
                {
                    unset($testCase->param['_api']);
                    self::_copyRess(
                        $testCase, $ress, '#^login#', 'auth_code'
                    );
                }
            }
            
            $ress[$key] = $testCase->test();
        }
    }

	/**
	 * 全 API のテスト
	 * @param string $filter 実行する API の正規表現
	 *
	 */
    protected function _testAll($filter = null)
	{
        DebugUtil::unlink_laravel_logs();
        
        static::_deleteAllLog();
        static::_putSettingLog();

        static::_testImpl($this->testCases, $filter);
        
        $this->assertTrue(true);
	}

	/**
	 * 設定ログの出力
	 *
	 */
	protected function _putSettingLog()
    {
        $encryptRequest = EncryptUtil::isDecryptRequest();
        $decriptResponse = EncryptUtil::isEncryptResponse();
        
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "BASE_URL", static::$BASE_URL
        );
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "リクエスト暗号化", ($encryptRequest ? "○" : "×")
        );
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "レスポンス暗号化", ($decriptResponse ? "○" : "×")
        );
    }

	/**
	 * ログの削除
	 *
	 */
	protected function _deleteAllLog()
    {
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'setting.txt');
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'response.txt');
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'result.tsv');
        DebugUtil::unlink_e_dir(static::$BASE_NAME . 'mpk');
    }
    
}
