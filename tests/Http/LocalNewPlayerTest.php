<?php
/**
 * ローカルサーバー 新規プレイヤ テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class LocalNewPlayerTest extends NewPlayerTestCase
{
    public static $BASE_NAME = 'local_np_';
    public static $BASE_URL = 'http://localhost/public/index.php/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        static::_testAll();
	}
}
