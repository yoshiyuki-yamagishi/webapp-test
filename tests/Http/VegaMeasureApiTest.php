<?php
/**
 * VEGAサーバー API 計測テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class VegaMeasureApiTest extends MeasureApiTestCase
{
    public static $BASE_NAME = 'vega_dev_m_';
    public static $BASE_URL = 'http://54.92.78.202/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        $this->testCount = 10;
        // $this->testSeconds = 10;
        
        // static::_testAll();
        static::_testSingle();
	}
}
