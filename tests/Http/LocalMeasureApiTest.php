<?php
/**
 * ローカルサーバー API 計測テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class LocalMeasureApiTest extends MeasureApiTestCase
{
    public static $BASE_NAME = 'local_m_';
    public static $BASE_URL = 'http://localhost/public/index.php/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        $this->testCount = 10;
        // $this->testSeconds = 10;
        
        // static::_testAll();
        static::_testSingle();
	}
}
