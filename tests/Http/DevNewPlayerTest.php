<?php
/**
 * 開発用サーバー 新規プレイヤ テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class DevNewPlayerTest extends NewPlayerTestCase
{
    public static $BASE_NAME = 'dev_np_';
    public static $BASE_URL = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        static::_testAll();
	}
}
