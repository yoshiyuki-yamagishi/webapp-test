<?php
/**
 * 開発用サーバー API テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class DevServerApiTest extends ServerApiTestCase
{
    public static $BASE_NAME = 'dev_';
    public static $BASE_URL = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        static::_testAll();
	}
}
