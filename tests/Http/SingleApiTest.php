<?php
/**
 * 開発中に API をテストする用
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class SingleApiTest extends ServerApiTestCase
{
    public static $BASE_NAME = 'single_';
    public static $BASE_URL = 'http://localhost/public/index.php/v1/';
    // public static $BASE_URL = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        // static::_testAll('#login$#');
        // static::_testAll('#login#');
        // static::_testAll('#login/update#');
        static::_testAll('#player/regist a#');
        // static::_testAll('#player/get#');
        // static::_testAll('#player/update#');
        // static::_testAll('#player/tutorial_update#');
        // static::_testAll('#player/al_recover item#');
        // static::_testAll('#player/al_recover crystal#');
        // static::_testAll('#player/account_start#');
        // static::_testAll('#player/account_end#');
        // static::_testAll('#player/(account_(start|info)|regist b)#');
        // static::_testAll('#player/(account_(start|restore)|regist b)#');
        // static::_testAll('#home#');
        // static::_testAll('#player_present/list#');
        // static::_testAll('#player_present/take one#');
        // static::_testAll('#player_present/take all#');
        // static::_testAll('#player_character/list#');
        // static::_testAll('#player_character/update#');
        // static::_testAll('#player_character/evolve#');
        // static::_testAll('#player_character/reinforce#');
        // static::_testAll('#player_character/skill_reinforce#');
        // static::_testAll('#player_character/orb_equip#');
        // static::_testAll('#player_character/favorite_select#');
        // static::_testAll('#player_grimoire/list#');
        // static::_testAll('#player_grimoire/lock$#');
        // static::_testAll('#player_grimoire/reinforce#');
        // static::_testAll('#player_grimoire/reinforce$#');
        // static::_testAll('#player_grimoire/limit_up#');
        // static::_testAll('#player_grimoire/sell#');
        // static::_testAll('#player_party/update#');
        // static::_testAll('#player_party/list#');
        // static::_testAll('#battle/start story#');
        // static::_testAll('#battle/(start|restart) story#');
        // static::_testAll('#battle/(start|interrupt) character#');
        // static::_testAll('#battle/(start|end) story#');
        // static::_testAll('#battle/(start|continue) story#');
        // static::_testAll('#battle/(start|end|skip) story#');
        // static::_testAll('#battle/(start|end) character#');
        // static::_testAll('#battle/(start|end) event#');
        // static::_testAll('#gacha/list#');
        // static::_testAll('#gacha/exec#');
        // static::_testAll('#gacha/(exec|commit)#');
        // static::_testAll('#gacha/(exec|retry|commit)#');
        // static::_testAll('#player_quest/list#');
        // static::_testAll('#player_quest/update#');
        // static::_testAll('#shop/list#');
        // static::_testAll('#shop/buy#');
        // static::_testAll('#shop/lineup_update#');
        // static::_testAll('#player_item/list#');
        // static::_testAll('#player_item/sell#');
        // static::_testAll('#player_item/powder_purify#');
        // static::_testAll('#player_item/orb_synthesize#');
        // static::_testAll('#player_mission/list#');
        // static::_testAll('#player_mission/reward_take one#');
        // static::_testAll('#^(battle/(start|end) story|player_mission/reward_take all)#');
        // static::_testAll('#product/list#');
        // static::_testAll('#product/.*app_store_0#'); // buy-cancel
        // static::_testAll('#product/.*app_store_1#'); // buy-regist
        // static::_testAll('#product/.*app_store_2#'); // buy-resume
        // static::_testAll('#product/.*google_store_0#'); // buy-cancel
        // static::_testAll('#product/.*google_store_1#'); // buy-regist
        // static::_testAll('#product/.*google_store_2#'); // buy-resume
        // static::_testAll('#follow/add#');
        // static::_testAll('#friend/list#');
        // static::_testAll('#follow/(add|delete)#');
        // static::_testAll('#player/(regist a|transit_)#');
        // static::_testAll('#player/(regist a|transit_dst)#');
        // static::_testAll('#DebugCommand/addItem#');
    }
}
