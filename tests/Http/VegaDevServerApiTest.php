<?php
/**
 * VEGA 開発用サーバー API テスト
 *
 */

namespace Tests\Http;

use App\Utils\DebugUtil;

class VegaDevServerApiTest extends ServerApiTestCase
{
    public static $BASE_NAME = 'vega_dev_';
    public static $BASE_URL = 'http://54.92.78.202/v1/';

	/**
	 * 全 API のテスト
	 *
	 */
	public function testAll()
	{
        static::_testAll();
	}
}
