<?php
/**
 * API 計測テスト基底クラス
 *
 */

namespace Tests\Http;

use Tests\TestCase;
use App\Utils\DebugUtil;
use App\Utils\EncryptUtil;

class MeasureApiTestCase extends TestCase
{
    public $testGroups = [];
    public $testCount = 0; // ループ回数指定 (優先)
    public $testSeconds = 60; // 60 秒 / API グループ

	/**
	 * 初期化
	 *
	 */
    public function setUp()
    {
        parent::setUp();

        // 単体で実行できない API は組にする //

        Api\Login_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_Regist_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_Get_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_Update_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_TutorialUpdate_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_AlRecover_Item_TestCase::appendNewG(
            $this->testGroups, 'item'
        );
        /*
        Api\Player_AlRecover_Crystal_TestCase::appendNewG(
            $this->testGroups, 'crystal'
        ); // 最大 AL 回復しちゃうから無理
        Api\Player_TransitSrc_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_TransitDst_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Player_AccountRestore_TestCase::appendNewG(
            $this->testGroups
        );
        */
        Api\Home_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerPresent_List_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\PlayerPresent_Take_One_TestCase::appendNewG(
            $this->testGroups, 'one'
        );
        Api\PlayerPresent_Take_All_TestCase::appendNewG(
            $this->testGroups, 'all'
        );
        */
        Api\PlayerCharacter_List_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\PlayerCharacter_Evolve_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerCharacter_Reinforce_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerCharacter_SkillReinforce_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerCharacter_OrbEquip_TestCase::appendNewG(
            $this->testGroups
        );
        */
        Api\PlayerCharacter_FavoriteSelect_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerGrimoire_List_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\PlayerGrimoire_Reinforce_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerGrimoire_Reinforce_Release_TestCase::appendNewG(
            $this->testGroups, 'release'
        );
        Api\PlayerGrimoire_Reinforce_Equip_TestCase::appendNewG(
            $this->testGroups, 'equip'
        );
        */
        Api\PlayerGrimoire_LimitUp_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\PlayerGrimoire_Sell_TestCase::appendNewG(
            $this->testGroups
        );
        */
        Api\PlayerParty_List_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerParty_Update_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Battle_Start_Story_TestCase::appendNewG(
            $this->testGroups, 'story'
        );
        Api\Battle_Restart_TestCase::appendG( // 1
            $this->testGroups
        );
        Api\Battle_Continue_TestCase::appendG( // 2
            $this->testGroups
        );
        Api\Battle_End_TestCase::appendG( // 3
            $this->testGroups
        );
        Api\Battle_Skip_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Battle_Start_Character_TestCase::appendNewG(
            $this->testGroups, 'character'
        );
        Api\Battle_Interrupt_TestCase::appendG( // 1
            $this->testGroups
        );
        Api\Gacha_List_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Gacha_Exec_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Gacha_Retry_TestCase::appendG( // 1
            $this->testGroups
        );
        Api\Gacha_Commit_TestCase::appendG( // 2
            $this->testGroups
        );
        Api\PlayerQuest_List_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerQuest_Update_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Shop_List_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\Shop_LineupUpdate_TestCase::appendNewG(
            $this->testGroups
        );
        Api\Shop_Buy_TestCase::appendNewG(
            $this->testGroups
        );
        */
        Api\PlayerItem_List_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\PlayerItem_List_Orb_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerItem_Sell_TestCase::appendNewG(
            $this->testGroups
        );
        */
        Api\PlayerData_Get_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerData_Set_TestCase::appendNewG(
            $this->testGroups
        );
        Api\PlayerMission_List_TestCase::appendNewG(
            $this->testGroups
        );
        /*
        Api\PlayerMission_RewardTake_One_TestCase::appendNewG(
            $this->testGroups, 'one'
        );
        Api\PlayerMission_RewardTake_All_TestCase::appendNewG(
            $this->testGroups, 'all'
        );
        */

        // 共通設定 //
        
        $tmpl = Api\Login_TestCase::make();
        $tmpl->baseUrl = static::$BASE_URL;
        $tmpl->baseName = static::$BASE_NAME;
        $tmpl->outputMsgPack = false;
        $tmpl->outputResponse = true;
        $tmpl->outputResult = true;

        ServerApiTestCase::_setCommonImpl($this->testGroups, $tmpl);

        // 共通リクエスト設定 //
        $values = [];
        $values['_measure_time'] = '1';

        ServerApiTestCase::_setExParamImpl($this->testGroups, $values);
    }    

	/**
	 * 全 API のテスト
	 * @param string $filter 実行する API の正規表現
	 *
	 */
    protected function _testAll($filter = null)
	{
        static::_deleteAllLog();
        static::_putSettingLog();

        foreach ($this->testGroups as $testCases)
        {
            if (isset($filter) && strlen($filter) > 0)
            {
                $key = array_keys($testCases)[0];
                
                if (!preg_match($filter, $key))
                    continue;
            }
            
            $start = microtime(true);
            $count = 0;
            $time = 0;

            while (true)
            {
                ServerApiTestCase::_testImpl($testCases);
                
                ++ $count;
                $time = microtime(true) - $start; // かかっている時間
                
                if ($this->testCount > 0) // 回数指定
                {
                    if ($count >= $this->testCount)
                        break;
                }
                else // 秒数指定
                {
                    if ($time >= $this->testSeconds)
                        break;
                }
            }

            $apis = "";
            foreach ($testCases as $name => $testCase)
            {
                if (strlen($apis) > 0)
                    $apis = $apis . ", " . $name;
                else
                    $apis = $name;
            }

            $firstValue = reset($testCases);

            DebugUtil::e_log_line(
                static::$BASE_NAME . 'time.tsv',
                $apis .
                "\t" . count($testCases) .
                "\t" . sprintf("%.8f", $time) .
                "\t" . $count
            );
        }
        
        $this->assertTrue(true);
	}

	/**
	 * 1 API セットのテスト
	 *
	 */
    protected function _testSingle()
	{
        static::_testAll('#^battle/start story#');
        // static::_testAll('#^login#');
    }

	/**
	 * 設定ログの出力
	 *
	 */
	protected function _putSettingLog()
    {
        $encryptRequest = EncryptUtil::isDecryptRequest();
        $decriptResponse = EncryptUtil::isEncryptResponse();
        
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "BASE_URL", static::$BASE_URL
        );
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "リクエスト暗号化", ($encryptRequest ? "○" : "×")
        );
        DebugUtil::e_log(
            static::$BASE_NAME . 'setting.txt',
            "レスポンス暗号化", ($decriptResponse ? "○" : "×")
        );
    }

	/**
	 * ログの削除
	 *
	 */
	protected function _deleteAllLog()
    {
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'setting.txt');
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'response.txt');
        DebugUtil::unlink_e_log(static::$BASE_NAME . 'result.tsv');
    }
}
