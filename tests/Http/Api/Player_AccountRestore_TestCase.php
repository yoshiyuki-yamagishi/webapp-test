<?php
/**
 * player/account_restore API テスト
 *
 */

namespace Tests\Http\Api;

class Player_AccountRestore_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/account_restore', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => -1,
                'os_type' => 1,
                'token' => static::$PLAYER_TOKEN_1,
            ],
            'player'
        );
    }
}
