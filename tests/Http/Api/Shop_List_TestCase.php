<?php
/**
 * shop/list API テスト
 *
 */

namespace Tests\Http\Api;

class Shop_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'shop/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                // 'shop_category' => '1',
            ],
            'shop_list'
        );
    }
}
