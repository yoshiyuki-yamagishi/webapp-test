<?php
/**
 * friend/list API テスト
 *
 */

namespace Tests\Http\Api;

class Friend_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'friend/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                // 'friend_type' => 1, // フォロー
                // 'friend_type' => 2, // フォロワー
                // 'friend_type' => 3, // フレンド
                'friend_type' => 4, // おすすめ|検索
                // 'search_id' => 'AAAA0101', // 自分
                // 'search_id' => 'AAAA0102', // フォロー
                // 'search_id' => 'AAAA0103', // フォロワー
                // 'search_id' => 'AAAA0104', // フレンド
                'search_id' => '',
                'sort_order' => 3,
                'from' => 0,
                'count' => 30,
            ],
            'friend_list'
        );
    }
}
