<?php
/**
 * gacha/exec API テスト
 *
 */

namespace Tests\Http\Api;

class Gacha_Exec_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'gacha/exec', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                // 'gacha_id' => 10001, // レギュラー蒼の結晶
                // 'gacha_id' => 40001, // 初回ピックアップガチャ
                // 'gacha_id' => 20001,
                // 'gacha_id' => 80001, // フレンドポイントガチャ
                'gacha_id' => 90001, // 引き直しガチャ
                'gacha_count' => 10,
            ],
            'gacha_result_list'
        );
    }
}
