<?php
/**
 * player_item/orb_synthesize API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerItem_OrbSynthesize_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        /*
        return $_this->set(
            'player_item/orb_synthesize', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'item_id' => 4135001,
                'count' => 1,
                'player_item_list' => [
                    [
                        'player_item_id' => '29',
                        'item_num' => '25',
                    ],
                ],
                'platinum_dollar' => 15000,
            ],
            'player_item_list'
        );
        */
        return $_this->set(
            'player_item/orb_synthesize', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'item_id' => 4211131,
                'count' => 1,
                'player_item_list' => [
                    [
                        'player_item_id' => '301',
                        'item_num' => '5',
                    ],
                    [
                        'player_item_id' => '302',
                        'item_num' => '1',
                    ],
                ],
                'platinum_dollar' => 20000,
            ],
            'player_item_list'
        );
    }
}
