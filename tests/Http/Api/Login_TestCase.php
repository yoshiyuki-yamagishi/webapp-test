<?php
/**
 * login API テスト
 *
 */

namespace Tests\Http\Api;

class Login_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'login', $subName,
            [
                'player_id' => static::PLAYER_ID_1,
            ],
            'auth_code'
        );
    }
}
