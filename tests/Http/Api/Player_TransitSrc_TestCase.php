<?php
/**
 * player/transit_src API テスト
 *
 */

namespace Tests\Http\Api;

class Player_TransitSrc_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/transit_src', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'password' => 'abc',
            ],
            'passcode'
        );
    }
}
