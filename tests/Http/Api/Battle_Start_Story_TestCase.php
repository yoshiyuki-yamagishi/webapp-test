<?php
/**
 * battle/start story API テスト
 *
 */

namespace Tests\Http\Api;

class Battle_Start_Story_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'battle/start', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'quest_category' => '1', // ストーリー
                'chapter_id' => '0', // ストーリー
                'quest_id' => '1001', // ストーリー
                // 'quest_category' => '2', // キャラクエ
                // 'chapter_id' => '10001', // キャラクエ
                // 'quest_id' => '10001001', // キャラクエ
                'party_no' => '1',
            ],
            'player_party'
        );
    }
}
