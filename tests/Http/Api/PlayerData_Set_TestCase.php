<?php
/**
 * player_data/set API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerData_Set_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_data/set', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'type' => '1',
                'value' => '234',
            ],
            null
        );
    }
}
