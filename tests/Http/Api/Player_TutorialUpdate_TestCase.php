<?php
/**
 * player_tutorial_update API テスト
 *
 */

namespace Tests\Http\Api;

class Player_TutorialUpdate_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();

        return $_this->set(
            'player/tutorial_update', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'tutorial_progress' => 1,
                'tutorial_flag' => 1,
                // 'tutorial_flag' => bindec('10000000000000000000000000000000'),
            ],
            null
        );
    }
}
