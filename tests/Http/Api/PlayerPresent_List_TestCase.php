<?php
/**
 * player_present/list API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerPresent_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_present/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'take_flag' => '0',
                'count' => '100',
            ],
            'player_present_list'
        );
    }
}
