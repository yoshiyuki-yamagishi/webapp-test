<?php
/**
 * API テストの基底クラス
 *
 */

namespace Tests\Http\Api;

use Tests\TestCase;
use Carbon\Carbon;
use App\Exceptions\ApiException;
use App\Utils\EncryptUtil;
use App\Utils\DebugUtil;
use MessagePack\MessagePack; // debug

class Api_TestCase extends TestCase
{
    public $baseUrl = "";
    public $baseName = "";
    public $outputReqData = false;
    public $outputMsgPack = true;
    public $outputResponse = true;
    public $outputResult = true;
    
    public $api = "";
    public $subName = "";
    public $param = [];
    public $exParam = [];
    public $condition = "";
            
	/**
	 * API のテストケースを作成し、連想配列に格納する
	 * 
	 * @param array $array 設定先の連想配列
	 * @param string $subName リクエストのバリエーションを表す名前
	 *
	 */
    public static function append(&$array, $subName = '')
    {
        $obj = static::make($subName);
        $key = $obj->api;
        if (isset($subName) && strlen($subName) > 0)
            $key .= ' ' . $subName;

        $array[$key] = $obj;
    }

	/**
	 * テストケースグループを作成し、配列に追加する
	 * 
	 * @param array $array 設定先の連想配列
	 * @param string $subName リクエストのバリエーションを表す名前
	 *
	 */
    public static function appendNewG(
        &$array, $subName = ''
    )
    {
        $testCase = [];
        static::append($testCase, $subName);
        $array[] = $testCase;
    }

	/**
	 * 最後のテストケースグループに追加する
	 * 
	 * @param array $array 設定先の連想配列
	 * @param string $subName リクエストのバリエーションを表す名前
	 *
	 */
    public static function appendG(
        &$array, $subName = ''
    )
    {
        static::append($array[count($array) - 1], $subName);
    }
    
	/**
	 * API のインスタンス変数をまとめて設定する
	 * 
	 * @param array $array 設定先の連想配列
	 * @param string $api API の名前
	 * @param string $subName リクエストのバリエーションを表す名前
	 * @param string $param リクエストパラメータ
	 * @param string $condition テスト条件 (変数名)
	 *
	 */
    public function set(
        $api,
        $subName,
        $param,
        $condition
    )
    {
        $this->api = $api;
        $this->subName = $subName;
        $this->param = $param;
        $this->condition = $condition;
        return $this;
    }

	/**
	 * テストを実行する
	 * 
	 * @return array レスポンス
	 *
	 */
    public function test()
    {
        $req_data = $this->param;
        
        $url = $this->baseUrl . $this->api;
        $json_req_data = json_encode($req_data);

        $encryptRequest = EncryptUtil::isDecryptRequest();
        if ($encryptRequest)
        {
            $req_code = null;
            $req_data = EncryptUtil::encryptRequest($req_data, $req_code);
            
            $req_data = [
                '__req_data' => $req_data,
                '__req_code' => $req_code,
            ];
        }

        // 送信データを掃き出す //
        if ($this->outputReqData)
        {
            $reqMpk = MessagePack::pack($this->param); // メッセージパック
            $reqEnc = EncryptUtil::encryptData($reqMpk); // 暗号化
            $req64 = base64_encode($reqEnc); // Base64エンコード
            $reqUrlEnc = urlencode($req64); // URL エンコード
            $reqChk = EncryptUtil::calcCode($reqUrlEnc); // チェックコード生成

            assert($reqChk == $req_code);

            $this->outBin('1_req_mpk', '.mpk', $reqMpk);
            $this->outBin('2_req_enc', '.dat', $reqEnc);
            $this->outBin('3_req_64_enc', '.txt', $req64);
            $this->outBin('4_req_url_enc', '.txt', $reqUrlEnc);
            $this->outBin('5_req_chk', '.dat', $reqChk);
        }

        // exParam は暗号化しない //
        foreach ($this->exParam as $key => $value)
            $req_data[$key] = $value;

        $req_query = http_build_query($req_data);
        
        $requestBodySize = strlen($req_query); // 送信サイズ (body)

        $curl = curl_init($url);
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $req_query,
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseBodySize = strlen($response); // 受信サイズ (body)

        $decriptResponse = EncryptUtil::isEncryptResponse();
        if ($decriptResponse)
        {
            // バイナリデータ出力
            
            if ($this->outputMsgPack)
            {
                $msgPackData = EncryptUtil::decryptData($response);
                $this->outBin('mpk', '.mpk', $msgPackData);
            }
            
            $response = EncryptUtil::decryptResponse($response);
        }

        $title = $this->api;
        if (strlen($this->subName) > 0)
        {
            $title .= (' ' . $this->subName);
        }

        if ($this->outputResponse)
        {
            DebugUtil::e_log(
                $this->baseName . 'response.txt',
                $title,
                $response
            );
        }

        if ($this->outputResult)
        {
            // 結果計算 //
            $code = -1;
            $msg = '';
            $mark = $this->_calcResult(
                $code, $msg, $response, $this->condition
            );
            
            DebugUtil::e_log_line(
                $this->baseName . 'result.tsv',
                $this->api .
                "\t" . $this->subName .
                "\t" . $mark .
                "\t" . $code .
                "\t" . $msg .
                "\t" . $this->condition .
                "\t" . $json_req_data .
                "\t" . $requestBodySize .
                "\t" . $responseBodySize
            );
        }
        
       return $response;
    }

	/**
	 * テスト結果を計算する
	 * 
	 * @param integer $code エラーコード
	 * @param string $msg エラーメッセージ
	 * @param string $response レスポンス
	 * @param string $condition テスト条件 (変数名)
	 * @return string 結果、○ など
	 *
	 */
    protected function _calcResult(&$code, &$msg, $response, $condition)
    {
        $code = -1;
        $msg = '';
        $mark = '×';
        
        if (!isset($response['header']['status_code']))
            return $mark;

        $mark = '△';

        if (isset($response['header']['error']))
            $msg = $response['header']['error']['message'];
        
        $code = $response['header']['status_code'];
        if ($code > ApiException::S_WARN)
            return $mark;

        if ($condition != null)
        {
            if (!isset($response['body'][$condition]))
                return $mark; // 設定されていないときは、△
            
            $testValue = $response['body'][$condition];

            if (is_array($testValue))
            {
                if (array_values($testValue) === $testValue)
                {
                    // 普通の配列
                    if (count($testValue) == 0)
                        return $mark; // 空のときは、△
                }
                // 連想配列なら OK
            }
            else if (is_string($testValue))
            {
                if (strlen($testValue) <= 0)
                    return $mark; // 空のときは、△
            }
            else if (is_numeric($testValue))
            {
                if ($testValue <= 0)
                    return $mark; // 負のときは、△
            }
            else
            {
                // 設定されていれば OK
            }
        }

        $mark = '○';
        return $mark;
    }

	/**
	 * バイナリデータをファイル出力する
	 * 
	 * @param string $folderPostfix フォルダー名の接尾辞
	 * @param string $fnamePostfix ファイル名の接尾辞
	 * @param object $data 書き出しデータ
	 *
	 */
    public function outBin($folderPostfix, $fnamePostfix, $data)
    {
        $fname = $this->baseName . $folderPostfix . '/';
        $fname .= str_replace('/', '_', $this->api);
        if (strlen($this->subName) > 0)
            $fname .= ('_' . $this->subName);
        $fname .= $fnamePostfix;
        
        DebugUtil::e_log_bin($fname, $data);
    }
    
}
 