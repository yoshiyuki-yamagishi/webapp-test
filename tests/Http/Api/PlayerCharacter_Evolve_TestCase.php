<?php
/**
 * player_character/evolve API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_Evolve_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_character/evolve', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '5',
                'player_item_list' => [
                    [
                        'player_item_id' => '249',
                        'item_num' => '2',
                    ],
                    [
                        'player_item_id' => '293',
                        'item_num' => '5',
                    ]
                ]
            ],
            'player_character'
        );
    }
}
