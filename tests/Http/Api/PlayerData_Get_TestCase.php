<?php
/**
 * player_data/get API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerData_Get_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_data/get', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
            ],
            'player_data_list'
        );
    }
}
