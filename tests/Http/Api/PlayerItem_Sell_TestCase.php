<?php
/**
 * player_item/sell API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerItem_Sell_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_item/sell', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_item_id' => 10,
                'num' => 1,
            ],
            'player_item_list'
        );
    }
}
