<?php
/**
 * player/regist API テスト
 *
 */

namespace Tests\Http\Api;

class Player_Regist_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/regist', $subName,
            [
                'unique_id' => 'AAAAAAAAAABBBBBB',
                'os_type' => '1',
                // 'os_version' => '10.0',
                'os_version' => 'Android OS 8.1.0 / API-27 (OPM3.171019.014/4503998)',
                // 'os_version' => '１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０',
                'model_name' => 'Xperia 1 Professional Edition (J9150)',
                // 'legal_birthday' => '1974-02-07',
            ],
            'player_id'
        );
    }
}
