<?php
/**
 * player_update API テスト
 *
 */

namespace Tests\Http\Api;

class Player_Update_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/update', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_name' => '山田太郎',
                // 'player_name' => '使用 不可 文言',
                'gender' => '2',
                'message' => 'てきとうに頑張ります',
                // 'message' => 'てきとうに使用不可文言頑張ります',
                'birthday' => '2020-02-29',
                'legal_birthday' => '2000-01-11',
            ],
            'player'
        );
    }
}
