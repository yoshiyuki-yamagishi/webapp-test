<?php
/**
 * player/get API テスト
 *
 */

namespace Tests\Http\Api;

class Player_Get_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/get', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
            ],
            'player'

        );
    }
}
