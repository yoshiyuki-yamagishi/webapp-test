<?php
/**
 * product/list API テスト
 *
 */

namespace Tests\Http\Api;

class Product_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'product/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'category' => '1',
                // 'category' => '2',
            ],
            'product_list'
        );
    }
    
}
