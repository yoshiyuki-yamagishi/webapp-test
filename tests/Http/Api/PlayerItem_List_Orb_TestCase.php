<?php
/**
 * player_item/list API テスト (オーブ)
 *
 */

namespace Tests\Http\Api;

class PlayerItem_List_Orb_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_item/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'item_category' => 6,
            ],
            'player_item_list'
        );
    }
}
