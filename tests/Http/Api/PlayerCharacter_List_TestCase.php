<?php
/**
 * player_character/list API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_character/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
            ],
            'player_character_list'
        );
    }
}
