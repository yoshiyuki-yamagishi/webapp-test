<?php
/**
 * gacha/retry API テスト
 *
 */

namespace Tests\Http\Api;

class Gacha_Retry_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'gacha/retry', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_gacha_id' => 0,
            ],
            'gacha_result_list'
        );
    }
}
