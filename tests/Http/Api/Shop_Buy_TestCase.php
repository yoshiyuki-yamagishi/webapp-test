<?php
/**
 * shop/buy API テスト
 *
 */

namespace Tests\Http\Api;

class Shop_Buy_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'shop/buy', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                // 'player_shop_item_id' => '10050',
                'player_shop_item_id' => '1', // P$
                // 'player_shop_item_id' => '101', // 魔道書
                // 'player_shop_item_id' => '401', // イベント交換所
            ],
            'player_item_list'
        );
    }
}
