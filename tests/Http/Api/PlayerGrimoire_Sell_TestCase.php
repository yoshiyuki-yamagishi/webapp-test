<?php
/**
 * player_grimoire/sell API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerGrimoire_Sell_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_grimoire/sell', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_grimoire_list' => [
                    [
                        'player_grimoire_id' => '1',
                    ],
                ],
            ],
            'magic_num'
        );
    }
}
