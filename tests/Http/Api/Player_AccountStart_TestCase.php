<?php
/**
 * player/account_start API テスト
 *
 */

namespace Tests\Http\Api;

class Player_AccountStart_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/account_start', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'os_type' => 1,
                'token' => static::$PLAYER_TOKEN_1,
            ],
            null
        );
    }
}
