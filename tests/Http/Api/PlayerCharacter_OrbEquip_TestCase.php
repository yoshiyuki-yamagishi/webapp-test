<?php
/**
 * player_character/orb_equip API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_OrbEquip_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();

        // グレードアップのみ
        return $_this->set(
            'player_character/orb_equip', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'equip_mask' => bindec('000000'),
                'player_item_list' => [
                ],
                'platinum_dollar' => 0,
                'grade_up' => 1,
            ],
            'player_character'
        );
        
        /*
        return $_this->set(
            'player_character/orb_equip', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'equip_mask' => bindec('111111'),
                'player_item_list' => [
                    [
                        'player_item_id' => '295',
                        'item_num' => '1',
                    ],
                    [
                        'player_item_id' => '296',
                        'item_num' => '1',
                    ],
                    [
                        'player_item_id' => '297',
                        'item_num' => '1',
                    ],
                    [
                        'player_item_id' => '298',
                        'item_num' => '1',
                    ],
                    [
                        'player_item_id' => '299',
                        'item_num' => '1',
                    ],
                    [
                        'player_item_id' => '300',
                        'item_num' => '1',
                    ],
                ],
                'platinum_dollar' => 0,
                'grade_up' => 0,
            ],
            'player_character'
        );
        */

        /*
        return $_this->set(
            'player_character/orb_equip', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'equip_mask' => bindec('111111'),
                'player_item_list' => [
                    [
                        'player_item_id' => '162',
                        'item_num' => '5',
                    ],
                    [
                        'player_item_id' => '29',
                        'item_num' => '25',
                    ],
                    [
                        'player_item_id' => '163',
                        'item_num' => '5',
                    ],
                    [
                        'player_item_id' => '30',
                        'item_num' => '25',
                    ],
                    [
                        'player_item_id' => '161',
                        'item_num' => '5',
                    ],
                    [
                        'player_item_id' => '31',
                        'item_num' => '25',
                    ],
                ],
                'platinum_dollar' => 60000,
                'grade_up' => 1,
            ],
            'player_character'
        );
        */
    }
}
