<?php
/**
 * player/al_recover crystal API テスト
 *
 */

namespace Tests\Http\Api;

class Player_AlRecover_Crystal_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/al_recover', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'crystal_num' => '50',
            ],
            'al'
        );
    }
}
