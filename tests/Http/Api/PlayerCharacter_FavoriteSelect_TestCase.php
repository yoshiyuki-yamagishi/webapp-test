<?php
/**
 * player_character/favorite_select API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_FavoriteSelect_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_character/favorite_select', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'favorite_flag' => '1',
            ],
            'player_character'
        );
    }
}
