<?php
/**
 * player/al_recover item API テスト
 *
 */

namespace Tests\Http\Api;

class Player_AlRecover_Item_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/al_recover', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'item_id' => '7000002',
                'item_num' => '1',
            ],
            'al'
        );
    }
}
