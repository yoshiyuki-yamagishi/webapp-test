<?php
/**
 * home API テスト
 *
 */

namespace Tests\Http\Api;

class Home_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'home', $subName = '',
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
            ],
            'login_bonus_list'
        );
    }
}
