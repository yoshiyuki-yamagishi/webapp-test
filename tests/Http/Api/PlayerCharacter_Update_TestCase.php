<?php
/**
 * player_character/update API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_Update_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_character/update', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'spine' => '1',
            ],
            'player_character'
        );
    }
}
