<?php
/**
 * battle/end API テスト
 *
 */

namespace Tests\Http\Api;

class Battle_End_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'battle/end', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'battle_code' => '',
                'result' => 1,
                'dead_flags' => 0, // 000000
                'used_skill_count' => 3,
                'used_od_count' => 3,
                'used_dd_count' => 2,
                'used_ah_count' => 1,
                'max_combo_count' => 20,
                'max_chain_count' => 3,
                'finish_character_no' => 1,
                'finish_skill_type' => 5, // 101
                'wave_list' => [
                    [
                        'turn_count' => 11,
                        'total_damage' => 12531,
                        'total_receive_damage' => 4321,
                    ],
                    [
                        'turn_count' => 12,
                        'total_damage' => 12532,
                        'total_receive_damage' => 4322,
                    ],
                    [
                        'turn_count' => 13,
                        'total_damage' => 12533,
                        'total_receive_damage' => 4323,
                    ],
                ],
            ],
            'player'
        );
    }
}
