<?php
/**
 * battle/skip API テスト
 *
 */

namespace Tests\Http\Api;

class Battle_Skip_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'battle/skip', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'quest_category' => '1',
                // 'chapter_id' => '99',
                // 'quest_id' => '9901001',
                'chapter_id' => '0',
                'quest_id' => '1001',
                'skip_ticket_id' => '9',
                'skip_count' => '3',
                // 'skip_count' => '999',
            ],
            'skip_quest_reward_list'
        );
    }
}
