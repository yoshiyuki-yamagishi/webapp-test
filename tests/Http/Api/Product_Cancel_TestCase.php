<?php
/**
 * product/cancel API テスト
 *
 */

namespace Tests\Http\Api;

class Product_Cancel_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'product/cancel', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_buy_id' => 0,
            ],
            null
        );
    }

}
