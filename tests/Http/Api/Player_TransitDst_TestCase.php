<?php
/**
 * player/transit_dst API テスト
 *
 */

namespace Tests\Http\Api;

class Player_TransitDst_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player/transit_dst', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1, // 同じ ID のままだとエラー
                'passcode' => 'tekitounaUsoPass',
                'password' => 'abc',
                'os_type' => '1',
                'os_version' => '10.0',
                'model_name' => 'Xperia 1 Destination Edition (DST)',
            ],
            'player_id'
        );
    }
}
