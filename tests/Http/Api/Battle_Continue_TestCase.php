<?php
/**
 * battle/continue API テスト
 *
 */

namespace Tests\Http\Api;

class Battle_Continue_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'battle/continue', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'battle_code' => '',
                'wave_list' => [
                    [
                        'turn_count' => 21,
                        'total_damage' => 12231,
                        'total_receive_damage' => 3331,
                    ],
                    [
                        'turn_count' => 22,
                        'total_damage' => 12232,
                        'total_receive_damage' => 33332,
                    ],
                ],
            ],
            'blue_crystal_num'
        );
    }
}
