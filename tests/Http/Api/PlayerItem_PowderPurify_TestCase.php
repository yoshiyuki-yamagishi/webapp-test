<?php
/**
 * player_item/powder_purify API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerItem_PowderPurify_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_item/powder_purify', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'powder' => 120,
                'player_item_list' => [
                    [
                        // 'player_item_id' => '241', // initial_rarity:2
                        'player_item_id' => '243', // initial_rarity:4
                        'item_num' => '1',
                    ],
                ],
            ],
            'player_item_list'
        );
    }
}
