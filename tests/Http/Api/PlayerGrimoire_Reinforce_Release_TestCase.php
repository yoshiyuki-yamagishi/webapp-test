<?php
/**
 * player_grimoire/reinforce release API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerGrimoire_Reinforce_Release_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_grimoire/reinforce', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_grimoire_id' => '1',
                'reinforce_type' => '1',
                'slot_no' => '1',
            ],
            'player_grimoire'
        );
    }
}
