<?php
/**
 * player_quest/list API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerQuest_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_quest/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'quest_category' => '0',
                'flags' => '0', // クローズしたイベントを返さない
                // 'flags' => '1', // クローズしたイベントを返す
            ],
            'player_quest_list'
        );
    }
}
