<?php
/**
 * player_quest/update API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerQuest_Update_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_quest/update', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'quest_category' => '1',
                'chapter_id' => '1',
                'quest_id' => '101001',
            ],
            null
        );
    }
}
