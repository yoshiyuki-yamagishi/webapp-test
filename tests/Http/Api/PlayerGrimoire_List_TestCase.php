<?php
/**
 * player_grimoire/list API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerGrimoire_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_grimoire/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
            ],
            'player_grimoire_list'
        );
    }
}
