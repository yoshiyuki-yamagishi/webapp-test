<?php
/**
 * shop/lineup_update API テスト
 *
 */

namespace Tests\Http\Api;

class Shop_LineupUpdate_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'shop/lineup_update', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'shop_id' => '1001',
                // 'shop_id' => '4001', // イベント交換所
                'pay_item' => 1,
            ],
            'shop_item_list'
        );
    }
}
