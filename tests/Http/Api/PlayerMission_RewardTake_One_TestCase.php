<?php
/**
 * player_mission/reward_take one API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerMission_RewardTake_One_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_mission/reward_take', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'take_type' => '1',
                'player_mission_id' => '1',
            ],
            'take_list'
        );
    }
}
