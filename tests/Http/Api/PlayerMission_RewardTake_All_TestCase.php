<?php
/**
 * player_mission/reward_take all API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerMission_RewardTake_All_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_mission/reward_take', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'take_type' => '2',
            ],
            'take_list'
        );
    }
}
