<?php
/**
 * battle/start event API テスト
 *
 */

namespace Tests\Http\Api;

class Battle_Start_Event_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'battle/start', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'quest_category' => '3',
                'chapter_id' => '99101',
                'quest_id' => '99101001',
                'party_no' => '1',
            ],
            'player_party'
        );
    }
}
