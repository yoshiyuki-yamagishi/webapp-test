<?php
/**
 * player_grimoire/lock API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerGrimoire_Lock_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_grimoire/lock', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'lock_grimoire_list' => [
                    [
                        'player_grimoire_id' => '2',
                    ],
                ],
                'unlock_grimoire_list' => [
                    [
                        'player_grimoire_id' => '2', // 結局ロックされる
                    ],
                ],
            ],
            null
        );
    }
}
