<?php
/**
 * gacha/list API テスト
 *
 */

namespace Tests\Http\Api;

class Gacha_List_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'gacha/list', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
            ],
            'gacha_list'
        );
    }
}
