<?php
/**
 * player_present/take one API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerPresent_Take_One_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_present/take', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'current_date' => '2019-07-23 00:00:00',
                'player_id' => static::PLAYER_ID_1,
                'take_type' => '1',
                'player_present_id' => '1',
            ],
            null
        );
    }
}
