<?php
/**
 * player_party/update API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerParty_Update_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_party/update', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'party_no' => 1,
                'party_character_list' => [
                    [
                        'player_character_id' => '0',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '2',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '0',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '4',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '5',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '6',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '1',
                        'player_grimoire_id' => '0',
                    ],
                    [
                        'player_character_id' => '3',
                        'player_grimoire_id' => '0',
                    ],
                ],
            ],
            'player_party'
        );
    }
}
