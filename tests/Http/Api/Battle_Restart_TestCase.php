<?php
/**
 * battle/restart story API テスト
 *
 */

namespace Tests\Http\Api;

class Battle_Restart_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'battle/restart', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'battle_code' => '',
            ],
            'player_party'
        );
    }
}
