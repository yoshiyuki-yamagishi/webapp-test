<?php
/**
 * product/buy API テスト
 *
 */

namespace Tests\Http\Api;

class Product_Buy_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'product/buy', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'store_id' => self::$params[$subName]['store_id'],
                'product_id' => self::$params[$subName]['product_id'],
                'num' => self::$params[$subName]['num'],
                'currency' => self::$params[$subName]['currency'],
                'amount' => self::$params[$subName]['amount'],
            ],
            'player_buy_id'
        );
    }

    public static $params = [
        'app_store_0' => [
            'store_id' => 2,
            'product_id' => 10007,
            'num' => 1,
            'currency' => 'JPY',
            'amount' => 10000,
        ],
        'app_store_1' => [
            'store_id' => 2,
            'product_id' => 10007,
            'num' => 1,
            'currency' => 'JPY',
            'amount' => 10000,
        ],
        'app_store_2' => [
            'store_id' => 2,
            'product_id' => 10007,
            'num' => 1,
            'currency' => 'JPY',
            'amount' => 10000,
        ],
        'google_store_0' => [
            'store_id' => 1,
            'product_id' => 10001,
            'num' => 1,
            'currency' => 'JPY',
            'amount' => 120,
        ],
        'google_store_1' => [
            'store_id' => 1,
            'product_id' => 10001,
            'num' => 1,
            'currency' => 'JPY',
            'amount' => 120,
        ],
        'google_store_2' => [
            'store_id' => 1,
            'product_id' => 10001,
            'num' => 1,
            'currency' => 'JPY',
            'amount' => 120,
        ],
    ];
    
}
