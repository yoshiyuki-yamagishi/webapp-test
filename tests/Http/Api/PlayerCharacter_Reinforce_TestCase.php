<?php
/**
 * player_character/reinforce API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_Reinforce_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_character/reinforce', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'player_item_list' => [
                    [
                        'player_item_id' => '6',
                        'item_num' => '1',
                    ],
                ],
            ],
            'player_character'
        );
    }
}
