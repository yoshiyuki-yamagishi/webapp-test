<?php
/**
 * player_character/skill_reinforce API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerCharacter_SkillReinforce_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_character/skill_reinforce', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_character_id' => '1',
                'skill_id' => '1001001',
                'platinum_dollar' => 50000,
            ],
            'player_character'
        );
    }
}
