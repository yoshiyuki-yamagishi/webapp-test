<?php
/**
 * player_grimoire/limit_up API テスト
 *
 */

namespace Tests\Http\Api;

class PlayerGrimoire_LimitUp_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'player_grimoire/limit_up', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'limit_up_num' => '5',
            ],
            'limit'
        );
    }
}
