<?php
/**
 * product/resume API テスト
 *
 */

namespace Tests\Http\Api;

class Product_Resume_TestCase extends Api_TestCase
{
    public static function make($subName = '')
    {
        $_this = new self();
        return $_this->set(
            'product/resume', $subName,
            [
                '_api' => 'api_noauth',
                'auth_code' => 'a',
                'player_id' => static::PLAYER_ID_1,
                'player_buy_id' => 0,
                'receipt' => Product_Regist_TestCase::$$subName,
            ],
            'player'
        );
    }
    
}
