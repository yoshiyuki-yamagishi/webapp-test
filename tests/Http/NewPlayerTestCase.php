<?php
/**
 * 新規プレイヤ テスト基底クラス
 *
 */

namespace Tests\Http;

use Tests\TestCase;
use App\Utils\DebugUtil;
use App\Utils\EncryptUtil;
use App\Utils\StrUtil;

class NewPlayerTestCase extends ServerApiTestCase
{
	/**
	 * 初期化
	 *
	 */
    public function setUp()
    {
        TestCase::setUp();
        
        // 共通設定 //

        if (empty($this->tmpl))
        {
            $this->tmpl = Api\Login_TestCase::make();
            $this->tmpl->baseUrl = static::$BASE_URL;
            $this->tmpl->baseName = static::$BASE_NAME;
            $this->tmpl->outputMsgPack = true;
            $this->tmpl->outputResponse = true;
            $this->tmpl->outputResult = true;
        }

        Api\Player_Regist_TestCase::append(
            $this->testCases,
        );
        Api\Login_TestCase::append(
            $this->testCases
        );
        Api\Player_Update_TestCase::append(
            $this->testCases
        );
        Api\Player_TutorialUpdate_TestCase::append(
            $this->testCases
        );
        
        ServerApiTestCase::_setCommonImpl($this->testCases, $this->tmpl);
    }

	/**
	 * 全 API のテスト
	 * @param $testCases 実行するテストの配列
	 * @param string $filter 実行する API の正規表現
	 *
	 */
    public static function _testImpl($testCases, $filter = null)
    {
        $ress = [];
        $testAuthCode = false;

        foreach ($testCases as $key => $testCase)
        {
            if (isset($filter) && strlen($filter) > 0)
            {
                if (!preg_match($filter, $key))
                    continue;
            }

            if ($key == 'player/regist')
            {
            }
            else if ($key == 'login')
            {
                self::_copyRess(
                    $testCase, $ress, '#^player/regist#', 'player_id'
                );
            }
            else
            {
                unset($testCase->param['_api']);
                
                self::_copyRess(
                    $testCase, $ress, '#^player/regist#', 'player_id'
                );
                self::_copyRess(
                    $testCase, $ress, '#^login#', 'auth_code'
                );
            }

            $ress[$key] = $testCase->test();
        }
    }
    
}
