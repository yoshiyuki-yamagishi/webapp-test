<?php
/**
 * プレイヤサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\PlayerAccountEndRequest;
use App\Http\Requests\PlayerAccountInfoRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerAccountStartRequest;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitDstRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerTutorialUpdateRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Models\PlayerCommon;
use Tests\TestCase;
use App\Http\Requests\PlayerRegistRequest;
use App\Services\PlayerService;
use App\Utils\DebugUtil;

class PlayerServiceTest extends TestCase
{
	/**
	 * regist のテスト
	 */
	public function testRegist()
	{
		$request = new PlayerRegistRequest();
		$request->merge([
            'unique_id' => 'AAAAAAAAAABBBBBB',
            'os_type' => '1',
            'os_version' => 'Android OS 8.1.0 / API-27',
            'model_name' => 'Xperia 1 Professional Edition (J9150)',
            'legal_birthday' => '2000-01-11',
		]);

		$response = PlayerService::regist($request);
		// DebugUtil::e_log('PST_regist', 'response', $response);

		// レスポンス確認
		$this->_assertResponseOk($response);
		$this->_assertUInt64($response->body, 'player_id');
	}

	/*
	 * get のテスト
	 */
	public function testGet()
    {
        $request = new PlayerGetRequest();
        $request->player_id = static::PLAYER_ID_1;

        $response = PlayerService::get($request);
        // DebugUtil::e_log('PST_get', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayer($body, 'player');
    }

    /*
     * update のテスト
     */
    public function testUpdate()
    {
        $request = new PlayerUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_name' => '山田太郎',
            // 'player_name' => '使用 不可 文言',
            'gender' => '2',
            'message' => 'てきとうに頑張ります',
            // 'message' => 'てきとうに使用不可文言頑張ります',
            'birthday' => '2020-02-29',
            'legal_birthday' => '2000-01-11',
        ]);

        $response = PlayerService::update($request);
        //DebugUtil::e_log('PST_update', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayer($body, 'player');
    }

    /*
     * tutorialUpdate のテスト
     */
    public function testTutorialUpdate()
    {
        $request = new PlayerTutorialUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'tutorial_progress' => 1,
            'tutorial_flag' => 1,
        ]);
        $response = PlayerService::tutorialUpdate($request);
        //DebugUtil::e_log('PST_tutorialUpdate', 'response', $response);
        $this->_assertResponseOk($response);
    }

    /*
     * alRecover のテスト。アイテム使用
     */
    public function testAlRecoverItem()
    {
        $request = new PlayerAlRecoverRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'item_id' => '7000002',
            'item_num' => '1',
        ]);

        $response = PlayerService::alRecover($request);
        //DebugUtil::e_log('PST_alRecoverItem', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertInt($body, 'al');
    }

    /*
     * alRecover のテスト。結晶使用
     */
    public function testAlRecoverCrystal()
    {
        $request = new PlayerAlRecoverRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'crystal_num' => '50',
        ]);

        $response = PlayerService::alRecover($request);
        //DebugUtil::e_log('PST_alRecoverCrystal', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertInt($body, 'al');
    }

    /*
     * transitSrc のテスト
     */
    public function testTransitSrc()
    {
        $user = $this->getUserTransitPassForUniqueID(static::PLAYER_UNIQUE_ID_1);
        $request = new PlayerTransitSrcRequest();
        $request->merge([
            'player_id' => $user->player_id,
            'password' => 'abc',
        ]);

        $response = PlayerService::transitSrc($request);
        //DebugUtil::e_log('PST_transitSrc', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertStr($body, 'passcode');
    }

    /*
     * transDst のテスト
     */
    public function testTransitDst()
    {
        $new_account = $this->registerNewUser(static::PLAYER_UNIQUE_ID_2);
        $old_account = $this->getUserTransitPassForUniqueID(static::PLAYER_UNIQUE_ID_1);
        $request = new PlayerTransitDstRequest();
        $request->merge([
            'player_id' => $new_account['player_id'], // 同じ ID のままだとエラー
            'passcode' => $old_account->passcode,
            'password' => 'abc',
            'os_type' => '1',
            'os_version' => '10.0',
            'model_name' => 'Xperia 1 Destination Edition (DST)',
        ]);

        $response = PlayerService::transitDst($request);
        //DebugUtil::e_log('PST_transitDst', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertInt($body, 'player_id');
    }

    /*
     * accountStart のテスト
     */
    public function testAccountStart()
    {
        $new_account = $this->registerNewUser(static::PLAYER_UNIQUE_ID_3);
        $request = new PlayerAccountStartRequest();
        $request->merge([
            'player_id' => $new_account['player_id'],
            'os_type' => 1,
            'token' => static::$PLAYER_TOKEN_1,
        ]);

        $response = PlayerService::accountStart($request);
        //DebugUtil::e_log('PST_AccountStart', 'request', $request);
        $this->_assertResponseOk($response);
    }

    /*
     * accountInfo のテスト
     */
    public function testAccountInfo()
    {
        $account = $this->registerNewUser(static::PLAYER_UNIQUE_ID_4);
        $request = new PlayerAccountInfoRequest();
        $request->merge([
            'player_id' => $account['player_id'],
            'os_type' => 1,
            'token' => static::$PLAYER_TOKEN_1,
        ]);

        $response = PlayerService::accountInfo($request);
        //DebugUtil::e_log('PST_AccountInfo', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertInt($body, 'player_id');
        $this->_assertStr($body, 'player_disp_id');
        $this->_assertPlayer($body, 'player');
    }

    /*
     * accountRestore のテスト
     */
    public function testAccountRestore()
    {
        $user = $this->getUserTransitPassForUniqueID(static::PLAYER_UNIQUE_ID_4);
        $request = new PlayerAccountRestoreRequest();
        $request->merge([
            'player_id' => $user->player_id,
            'os_type' => 1,
            'token' => static::$PLAYER_TOKEN_1,
        ]);

        $response = PlayerService::accountRestore($request);
        //DebugUtil::e_log('PST_AccountRestore', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertInt($body, 'player_id');
        $this->_assertStr($body, 'player_disp_id');
        $this->_assertPlayer($body, 'player');
    }

    /*
     * accountEnd のテスト
     */
    //TODO:Unique_idはユニークではないので、3人以上被っていた場合の処理結果が読めない。代替案を考えないといけない。
    public function testAccountEnd()
    {
        $user = $this->getUserTransitPassForUniqueID(static::PLAYER_UNIQUE_ID_4);
        $request = new PlayerAccountEndRequest();
        //DebugUtil::e_log('PST_AccountEnd', 'user', $user);
        $request->player_id = $user->player_id;

        $response = PlayerService::accountEnd($request);

        $this->_assertResponseOk($response);
    }

    /**
     * ユニークIDから引継ぎコードを取得する
     * @param $unique_id
     * @return mixed
     */
    public function getUserTransitPassForUniqueID($unique_id)
    {
        $query = PlayerCommon::where('unique_id', $unique_id)
            ->where('valid_flag', PlayerCommon::VALID_FLAG_YES)
            ->first();
        //DebugUtil::e_log('userTransitPass', 'query', $query);
        return $query;
    }

    /**
     * ユーザー登録する。(簡易版)
     * @param $unique_id
     * @return array
     */
    public function registerNewUser($unique_id){
        $request = new PlayerRegistRequest();
        $request->merge([
            'unique_id' => $unique_id,
            'os_type' => '1',
            'os_version' => 'Android OS 8.1.0 / API-27',
            'model_name' => 'Xperia 1 Professional Edition (J9150)',
        ]);

        $response = PlayerService::regist($request);
        return $response->body;
    }
}
