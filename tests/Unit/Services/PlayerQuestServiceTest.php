<?php


namespace Tests\Unit\Services;


use App\Http\Requests\PlayerQuestListRequest;
use App\Http\Requests\PlayerQuestUpdateRequest;
use App\Services\PlayerQuestService;
use Tests\TestCase;

class PlayerQuestServiceTest extends TestCase
{
    /*
     * list のテスト。クローズイベントなし
     */
    public function testListWithOutClose()
    {
        $request = new PlayerQuestListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '0',
            'flags' => '0',
        ]);
        $resposne = PlayerQuestService::list($request);

        $this->_assertResponseOk($resposne);
        $body = $resposne->body;

        $this->_assertPlayerQuestList($body, 'player_quest_list');
    }

   /*
    * list のテスト。クローズイベントあり
    */
    public function testListWithClose()
    {
        $request = new PlayerQuestListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '0',
            'flags' => '1',
        ]);
        $resposne = PlayerQuestService::list($request);

        $this->_assertResponseOk($resposne);
        $body = $resposne->body;

        $this->_assertPlayerQuestList($body, 'player_quest_list');
    }

    /*
     * update のテスト
     */
    public function testUpdate()
    {
        $request = new PlayerQuestUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '1',
            'chapter_id' => '1',
            'quest_id' => '101001',
        ]);

        $response = PlayerQuestService::update($request);
        $this->_assertResponseOk($response);
    }
}
