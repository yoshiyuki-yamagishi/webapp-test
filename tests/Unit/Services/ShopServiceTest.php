<?php
/**
 * ショップサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\ShopBuyRequest;
use App\Http\Requests\ShopLineupUpdateRequest;
use Tests\TestCase;
use App\Http\Requests\ShopListRequest;
use App\Services\ShopService;
use App\Utils\DebugUtil;

class ShopServiceTest extends TestCase
{
	/**
	 * list のテスト
	 */
	public function testList()
	{
		$request = new ShopListRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            // 'shop_category' => '1',
        ]);

		$response = ShopService::list($request);
		//DebugUtil::e_log('SST_list', 'response', $response);

        $body = $response->body;
        $this->_assertShopList($body, 'shop_list');
	}


    // lineupUpdate のテスト
    public function testLineupUpdate()
    {
        $request = new ShopLineupUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'shop_id' => 1001,
            'pay_item' => 0,
        ]);

        $response = ShopService::lineupUpdate($request);
        // DebugUtil::e_log('ShopServiceTest_shopItem.txt', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertShopItemList($body, 'shop_item_list');
        $this->_assertInt($body,'pay_item_count');
    }



    // lineupUpdate のテスト
	public function testLineupUpdate_manual()
    {
        $request = new ShopLineupUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'shop_id' => 1001,
            'pay_item' => 1,
        ]);

        $response = ShopService::lineupUpdate($request);
        // DebugUtil::e_log('ShopServiceTest_shopItem.txt', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertShopItemList($body, 'shop_item_list');
        $this->_assertInt($body,'pay_item_count');
    }
	// buy のテスト
	public function testBuy()
	{
		$request = new ShopBuyRequest();

		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_shop_item_id' => static::PLAYER_ID_1, // P$
			]);

		$response = ShopService::buy($request);
        //DebugUtil::e_log('SST_buy', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;
        $this->_assertPlayer($body, 'player');
        $this->_assertPlayerItemList($body, 'player_item_list');
	}
}
