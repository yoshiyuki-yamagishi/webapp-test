<?php


namespace Tests\Unit\Services;


use App\Http\Requests\PlayerMissionListRequest;
use App\Http\Requests\PlayerMissionRewardTakeRequest;
use App\Services\PlayerMissionService;
use Tests\TestCase;

class PlayerMissionServiceTest extends TestCase
{
    /*
     * list のテスト
     */
    public function testList()
    {
        $request = new PlayerMissionListRequest();
        $request->player_id = static::PLAYER_ID_1;

        $response = PlayerMissionService::list($request);
        // DebugUtil::e_log('PMST_list', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerMissionList($body, 'mission_list');
    }

    /*
     * rewardTake の単体受け取りテスト
     */
    public function testRewardTakeOne()
    {
        $request = new PlayerMissionRewardTakeRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'take_type' => '1',
            'player_mission_id' => '1',
        ]);

        $response = PlayerMissionService::rewardTake($request);
        // DebugUtil::e_log('PMST_rewardTake_one', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertUInt8($body, 'take_flag');
        $this->_assertPlayerMissionList($body, 'take_list');
    }

    /*
    * rewardTake の一括受け取りテスト
    */
    public function testRewardTakeAll()
    {
        $request = new PlayerMissionRewardTakeRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'take_type' => '2',
        ]);

        $response = PlayerMissionService::rewardTake($request);
        // DebugUtil::e_log('PMST_rewardTake_all', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertUInt8($body, 'take_flag');
        $this->_assertPlayerMissionList($body, 'take_list');
    }
}
