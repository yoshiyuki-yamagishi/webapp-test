<?php
/**
 * ガチャサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\GachaCommitRequest;
use App\Http\Requests\GachaRetryRequest;
use App\Models\PlayerGacha;
use App\Models\MasterModels\Gacha;
use Tests\TestCase;
use App\Http\Requests\GachaListRequest;
use App\Http\Requests\GachaExecRequest;
use App\Services\GachaService;
use App\Utils\DebugUtil;

class GachaServiceTest extends TestCase
{
	/**
	 * list のテスト
	 */
	public function testList()
	{
		$request = new GachaListRequest();
        $request->player_id = static::PLAYER_ID_1;
		$response = GachaService::list($request);
		// DebugUtil::e_log('GST_list', 'response', $response);

        $this->_assertResponseOk($response);

        $body = $response->body;
        $this->_assertGachaList($body, 'gacha_list');
	}

	/**
	 * exec のテスト
	 */
	public function testExec()
	{
        $request = new GachaExecRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'gacha_id' => 90001,
            'gacha_count' => 10,
        ]);
        $response = GachaService::exec($request);

        $this->_assertResponseOk($response);

        $body = $response->body;
        DebugUtil::e_log('GST_exec', 'response', $response);

		$this->_assertUInt64($body, 'player_gacha_id');
		$this->_assertGachaResultList($body, 'gacha_result_list');
		$this->_assertInt($body, 'effect_table_id');
		$this->_assertInt($body, 'pay_item_id');
		$this->_assertInt($body, 'pay_item_count');
		$this->_assertPlayerItem($body, 'pay_item');
		$this->_assertInt($body, 'free_blue_crystal_num');
		$this->_assertInt($body, 'blue_crystal_num');
		$this->_assertUInt64($body, 'friend_point');
	}


    /**
     * retry のテスト
     */

	public function testRetry()
    {
        $request = new GachaRetryRequest();
        $pid = static::PLAYER_ID_1;
        $gachaID = 90001;

        $playerGacha = $this->getPlayerLoopGachaID($pid, $gachaID);

        $request->merge([
            'player_id' => $pid,
            'player_gacha_id' => $playerGacha->id,
        ]);
        $response = GachaService::retry($request);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertUInt64($body, 'player_gacha_id');
        $this->_assertGachaResultList($body, 'gacha_result_list');
        $this->_assertInt($body, 'effect_table_id');
        $this->_assertInt($body, 'pay_item_id');
        $this->_assertInt($body, 'pay_item_count');
        $this->_assertPlayerItem($body, 'pay_item');
        $this->_assertInt($body, 'free_blue_crystal_num');
        $this->_assertInt($body, 'blue_crystal_num');
        $this->_assertUInt64($body, 'friend_point');
    }



    /**
     * commit のテスト
     * @throws \App\Exceptions\ApiException
     */

    public function testCommit()
    {
        $request = new GachaCommitRequest();
        $pid = static::PLAYER_ID_1;
        $gachaID = 90001;

        $playerGacha = $this->getPlayerLoopGachaID($pid, $gachaID);
        $request->merge([
            'player_id' => $pid,
            'player_gacha_id' => $playerGacha->id,
        ]);
        $response = GachaService::commit($request);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertUInt64($body, 'player_gacha_id');
        $this->_assertGachaResultList($body, 'gacha_result_list');
        $this->_assertInt($body, 'effect_table_id');
        $this->_assertInt($body, 'pay_item_id');
        $this->_assertInt($body, 'pay_item_count');
        $this->_assertPlayerItem($body, 'pay_item');
        $this->_assertInt($body, 'free_blue_crystal_num');
        $this->_assertInt($body, 'blue_crystal_num');
        $this->_assertUInt64($body, 'friend_point');
    }

    /**
     * exec のテスト
     */
    public function testExecConfirm()
    {
        $request = new GachaExecRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'gacha_id' => 20001,
            'gacha_count' => 10,
        ]);
        $response = GachaService::exec($request);

        $this->_assertResponseOk($response);

        $body = $response->body;
        DebugUtil::e_log('GST_exec', 'response', $response);

        $this->_assertUInt64($body, 'player_gacha_id');
        $this->_assertGachaResultList($body, 'gacha_result_list');
        $this->_assertInt($body, 'effect_table_id');
        $this->_assertInt($body, 'pay_item_id');
        $this->_assertInt($body, 'pay_item_count');
        $this->_assertPlayerItem($body, 'pay_item');
        $this->_assertInt($body, 'free_blue_crystal_num');
        $this->_assertInt($body, 'blue_crystal_num');
        $this->_assertUInt64($body, 'friend_point');
    }

    /**
     * exec のテスト
     */
    public function testExecOnce()
    {
        $request = new GachaExecRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'gacha_id' => 20001,
            'gacha_count' => 1,
        ]);
        $response = GachaService::exec($request);

        $this->_assertResponseOk($response);

        $body = $response->body;
        DebugUtil::e_log('GST_exec', 'response', $response);

        $this->_assertUInt64($body, 'player_gacha_id');
        $this->_assertGachaResultList($body, 'gacha_result_list');
        $this->_assertInt($body, 'effect_table_id');
        $this->_assertInt($body, 'pay_item_id');
        $this->_assertInt($body, 'pay_item_count');
        $this->_assertPlayerItem($body, 'pay_item');
        $this->_assertInt($body, 'free_blue_crystal_num');
        $this->_assertInt($body, 'blue_crystal_num');
        $this->_assertUInt64($body, 'friend_point');
    }


    /**
     * プレイヤガチャテーブルから該当ユーザの引き直し可能なガチャ情報を1件取得する。
     * TODO:※過去のものを指定したら引き直せる？そのあたりもチェック必須
     * @param $player_id
     * @param $gacha_id
     * @return mixed
     */
    public function getPlayerLoopGachaID($player_id, $gacha_id)
    {
        return PlayerGacha::where('player_id', $player_id)
            ->where('gacha_id', $gacha_id)
            ->where('gacha_loop', Gacha::LOOP_CAN)
            ->orderBy('updated_at', 'desc')
            ->first();
    }


}
