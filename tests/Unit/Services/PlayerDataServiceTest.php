<?php


namespace Tests\Unit\Services;


use App\Http\Requests\PlayerDataGetRequest;
use App\Http\Requests\PlayerDataSetRequest;
use App\Services\PlayerDataService;
use Tests\TestCase;

class PlayerDataServiceTest extends TestCase
{
    /**
     * Get のテスト
     */
    public function testGet()
    {
        $request = new PlayerDataGetRequest();
        $request->player_id = static::PLAYER_ID_1;

        $response = PlayerDataService::get($request);
        //DebugUtil::e_log('PDS_get', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerDataList($body, 'player_data_list');
    }

    /**
     * set のテスト
     */
    public function testSet()
    {
        $request = new PlayerDataSetRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'type' => '1',
            'value' => '234',
        ]);

        $response = PlayerDataService::set($request);
        //DebugUtil::e_log('PDS_get', 'response', $response);

        $this->_assertResponseOk($response);
    }


}
