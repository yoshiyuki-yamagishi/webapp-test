<?php
/**
 * プレイヤパーティサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\PlayerPartyUpdateRequest;
use Tests\TestCase;
use App\Http\Requests\PlayerPartyListRequest;
use App\Services\PlayerPartyService;
use App\Utils\DebugUtil;

class PlayerPartyServiceTest extends TestCase
{
	/**
	 * list のテスト
	 */
	public function testList()
	{
		$request = new PlayerPartyListRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
		]);

		$response = PlayerPartyService::list($request);
		$this->_assertResponseOk($response);
		// DebugUtil::e_log('PPST_list', 'response', $response);

        $this->_assertPlayerPartyList($response->body, 'player_party_list');
	}

	/*
	 * update のテスト
	 */
    public function testUpdate()
    {
        $request = new PlayerPartyUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'party_no' => 1,
            'party_character_list' => [
                [
                    'player_character_id' => '0',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '2',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '0',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '4',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '5',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '6',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '1',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '3',
                    'player_grimoire_id' => '0',
                ],
            ],
        ]);
        $response = PlayerPartyService::update($request);
        $this->_assertResponseOk($response);
        $body = $response->body;
        //DebugUtil::e_log('PPST_update', 'response', $response);

        $this->_assertPlayerParty($body, 'player_party');
	}


    /*
 * update のテスト
 */
    public function testUpdateNewParty()
    {
        $request = new PlayerPartyUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'party_no' => 2,
            'party_character_list' => [
                [
                    'player_character_id' => '1',
                    'player_grimoire_id' => '1',
                ],
                [
                    'player_character_id' => '0',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '2',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '4',
                    'player_grimoire_id' => '4',
                ],
                [
                    'player_character_id' => '0',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '6',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '0',
                    'player_grimoire_id' => '0',
                ],
                [
                    'player_character_id' => '0',
                    'player_grimoire_id' => '0',
                ],
            ],
        ]);
        $response = PlayerPartyService::update($request);
        $this->_assertResponseOk($response);
        $body = $response->body;
        //DebugUtil::e_log('PPST_update', 'response', $response);

        $this->_assertPlayerParty($body, 'player_party');
    }
}
