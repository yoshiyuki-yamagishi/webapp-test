<?php


namespace Tests\Unit\Services;


use App\Http\Requests\LoginRequest;
use App\Services\LoginService;
use App\Utils\DebugUtil;
use Tests\Unit\Services\BattleServiceTest;
use Tests\TestCase;

class LoginServiceTest extends TestCase
{
    /*
     * index のテスト
     */
    public function testIndex()
    {
        //バトルコードの取得
        $battle = new BattleServiceTest();
        $battleBody = $battle->issueBattleCode(3, 99102, 99102001);

        $request = new LoginRequest();
        $request->player_id = static::PLAYER_ID_1;

        $response = LoginService::index($request);

        $this->_assertResponseOk($response);

        $body = $response->body;

        DebugUtil::e_log('LST_index.log','Response', $response);

        $this->_assertStr($body, 'player_disp_id');
        $this->_assertPlayer($body, 'player');
        $this->_assertStr($body, 'auth_code');
        $this->_assertStr($body,'battle_code');

        //バトルを終わらせておく
        $battle->battleExit($body['battle_code']);
    }

    /*
     * update のテスト
     */
    public function testUpdate()
    {
        $request = new LoginRequest();
        $request->player_id = static::PLAYER_ID_1;

        $response = LoginService::update($request);

        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertStr($body, 'auth_code');
    }


}
