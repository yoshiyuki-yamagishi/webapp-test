<?php
/**
 * バトルサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\BattleEndRequest;
use App\Http\Requests\BattleInterruptRequest;
use App\Http\Requests\BattleSkipRequest;
use App\Models\PlayerBattle;
use Tests\TestCase;
use App\Http\Requests\BattleStartRequest;
use App\Http\Requests\BattleRestartRequest;
use App\Http\Requests\BattleContinueRequest;
use App\Services\BattleService;
use App\Utils\DebugUtil;

class BattleServiceTest extends TestCase
{
	/**
	 * start(Story) のテスト
	 */
	public function testStartStory()
	{
		$request = new BattleStartRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '1',
            'chapter_id' => '0',
            'quest_id' => '1001',
            'party_no' => '1',
		]);

		$response = BattleService::start($request);
		// DebugUtil::e_log('BST_start', 'response', $response);

		$this->_assertResponseOk($response);

        $body = $response->body;

		$this->_assertStr($body, 'battle_code');
        $this->_assertPlayerParty($body, 'player_party');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertPlayerGrimoireList($body, 'player_grimoire_list');
        $this->_assertBattleEnemyList($body, 'battle_enemy_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertInt($body, 'continue_blue_crystal_num');
	}

	/**
	 * restart のテスト
	 */
	public function testRestartStory()
	{
		// DebugUtil::e_log('BST_restart', 'startRes', $startRes);
        $battleCode = $this->getUserBattleCode(static::PLAYER_ID_1);
		$request = new BattleRestartRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
		]);

		$response = BattleService::restart($request);
		// DebugUtil::e_log('BST_restart', 'response', $response);

		$this->_assertResponseOk($response);

        $body = $response->body;

		$this->_assertInt($body, 'quest_category');
		$this->_assertInt($body, 'chapter_id');
		$this->_assertInt($body, 'quest_id');
		$this->_assertInt($body, 'party_no');
        $this->_assertPlayerParty($body, 'player_party');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertPlayerGrimoireList($body, 'player_grimoire_list');
        $this->_assertBattleEnemyList($body, 'battle_enemy_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertInt($body, 'continue_blue_crystal_num');
	}

	/**
	 * continue のテスト
	 */
	public function testContinueStory()
	{
        $battleCode = $this->getUserBattleCode(static::PLAYER_ID_1);
		$request = new BattleContinueRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'wave_list' => [
                [
                    'turn_count' => 21,
                    'total_damage' => 12231,
                    'total_receive_damage' => 3331,
                ],
                [
                    'turn_count' => 22,
                    'total_damage' => 12232,
                    'total_receive_damage' => 33332,
                ],
            ],
		]);

		$response = BattleService::continue_($request);
		// DebugUtil::e_log('BSTS_continue', 'response', $response);

		$this->_assertResponseOk($response);

        $body = $response->body;

		$this->_assertInt($body, 'free_blue_crystal_num');
		$this->_assertInt($body, 'blue_crystal_num');
	}

	/**
     * end のテスト
     */
    public function testEndStory()
    {
        $battleCode = $this->getUserBattleCode(static::PLAYER_ID_1);

        $request = new BattleEndRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => 1,
            'dead_flags' => 0,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $response = BattleService::end($request);
        // DebugUtil::e_log('BSTS_end', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertPlayer($body, 'player');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertQuestRewardList($body, 'drop_reward_list');
        $this->_assertPlayerMissionList($body, 'mission_reward_list');
    }

    /**
     * start(Story) のテスト
     * パーティ内キャラの1人が倒れた状態でクリア
     */
    public function testStartStory2()
    {
        $request = new BattleStartRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '1',
            'chapter_id' => '0',
            'quest_id' => '1001',
            'party_no' => '1',
        ]);

        $response = BattleService::start($request);
        // DebugUtil::e_log('BST_start', 'response', $response);

        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertStr($body, 'battle_code');
        $this->_assertPlayerParty($body, 'player_party');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertPlayerGrimoireList($body, 'player_grimoire_list');
        $this->_assertBattleEnemyList($body, 'battle_enemy_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertInt($body, 'continue_blue_crystal_num');
    }

    /**
     * end のテスト
     * パーティ内キャラの1人が倒れた状態でクリア
     */
    public function testEndStory2()
    {
        $battleCode = $this->getUserBattleCode(static::PLAYER_ID_1);

        $request = new BattleEndRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => 1,
            'dead_flags' => 10,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $response = BattleService::end($request);
        // DebugUtil::e_log('BSTS_end', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertPlayer($body, 'player');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertQuestRewardList($body, 'drop_reward_list');
        $this->_assertPlayerMissionList($body, 'mission_reward_list');
    }
    /**
     * skip のテスト
     */
    public function testSkipStory()
    {
        $request = new BattleSkipRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '1',
            'chapter_id' => '0',
            'quest_id' => '1001',
            'skip_ticket_id' => '9',
            'skip_count' => '2',
            // 'skip_count' => '999',
        ]);

        $response = BattleService::skip($request);
        // DebugUtil::e_log('BST_skip', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertPlayer($body, 'player');
        $this->_assertInt($body, 'ticket_num');
        $this->_assertSkipQuestRewardList($body, 'skip_quest_reward_list');
    }

    /**
     * start(Character) のテスト
     */
    public function testStartCharacter()
    {
        $body = $this->issueBattleCode(2, 10001, 10001001);

        $this->_assertStr($body, 'battle_code');
        $this->_assertPlayerParty($body, 'player_party');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertPlayerGrimoireList($body, 'player_grimoire_list');
        $this->_assertBattleEnemyList($body, 'battle_enemy_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertInt($body, 'continue_blue_crystal_num');
    }

    /**
     * interrupt のテスト
     */
    public function testInterrupt()
    {
        $request = new BattleInterruptRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
        ]);

        $response = BattleService::interrupt($request);
        // DebugUtil::e_log('BSTC_interrupt, 'response', $response);
        $this->_assertResponseOk($response);
    }

    /**
     * start(Event) のテスト
     */
    public function testStartEvent()
    {
        $body = $this->issueBattleCode(3, 90101, 90101001);
        $this->_assertStr($body, 'battle_code');
        $this->_assertPlayerParty($body, 'player_party');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertPlayerGrimoireList($body, 'player_grimoire_list');
        $this->_assertBattleEnemyList($body, 'battle_enemy_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertInt($body, 'continue_blue_crystal_num');
    }

    /**
     * end のテスト
     */
    public function testEndEvent()
    {
        $battleCode = $this->getUserBattleCode(static::PLAYER_ID_1);

        $request = new BattleEndRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => 1,
            'dead_flags' => 0,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $response = BattleService::end($request);
        // DebugUtil::e_log('BSTS_end', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertPlayer($body, 'player');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertQuestRewardList($body, 'drop_reward_list');
        $this->_assertPlayerMissionList($body, 'mission_reward_list');
    }

    /**
     * end のテスト(負け)
     */
    public function testEndEventLose()
    {
        //先にバトルコードの発行
        $body = $this->issueBattleCode(1, 0, 2001);
        $battleCode = $body['battle_code'];

        $request = new BattleEndRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => BattleEndRequest::RESULT_LOSE,
            'dead_flags' => 1,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $response = BattleService::end($request);
        // DebugUtil::e_log('BSTS_end', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertPlayer($body, 'player');
        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertQuestRewardList($body, 'first_reward_list');
        $this->_assertQuestRewardList($body, 'fix_reward_list');
        $this->_assertQuestRewardList($body, 'drop_reward_list');
        $this->_assertPlayerMissionList($body, 'mission_reward_list');
    }

    /**
     * skip のテスト(LvUP)
     */
    public function testSkipStoryLvUP()
    {
        //スキップするには一度クリアしておく必要がある。
        $body = $this->issueBattleCode(3, 99102, 99102001);
        $battleCode = $body['battle_code'];

        $end_request = new BattleEndRequest();
        $end_request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => BattleEndRequest::RESULT_WIN,
            'dead_flags' => 0,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $end_response = BattleService::end($end_request);

        $request = new BattleSkipRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => '3',
            'chapter_id' => '99102',
            'quest_id' => '99102001',
            'skip_ticket_id' => '9',
            'skip_count' => '5',
        ]);

        $response = BattleService::skip($request);
        DebugUtil::e_log('BST_skip_lvup', 'response', $response);
        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertPlayer($body, 'player');
        $this->_assertInt($body, 'ticket_num');
        $this->_assertSkipQuestRewardList($body, 'skip_quest_reward_list');
    }

    /*
     * ミッションを各種調査するためのもの。
     */
    /*public function battleMissionChecker()
    {

        $body = $this->issueBattleCode(3, 99102, 99102001);
        $battleCode = $body['battle_code'];

        $end_request = new BattleEndRequest();
        $end_request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => BattleEndRequest::RESULT_WIN,
            'dead_flags' => 0,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $end_response = BattleService::end($end_request);

    }*/


    /**
     * バトルコードを取得
     * @param   integer $player_id
     * @return  string  バトルコード
     */
    public function getUserBattleCode($player_id)
    {
        $playerBattle = PlayerBattle::getActive($player_id);
        return $playerBattle->battle_code;
    }

    public function issueBattleCode($quest_category, $chapter_id, $quest_id)
    {
        $request = new BattleStartRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'quest_category' => $quest_category,
            'chapter_id' => $chapter_id,
            'quest_id' => $quest_id,
            'party_no' => '1',
        ]);
        $response = BattleService::start($request);
        // DebugUtil::e_log('BSTE_start', 'response', $response);
        $this->_assertResponseOk($response);

        return $response->body;
    }

    /*
     * バトルを終わらせる処理。
     */
    public function battleExit($battleCode)
    {
        $request = new BattleEndRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'battle_code' => $battleCode,
            'result' => BattleEndRequest::RESULT_LOSE,
            'dead_flags' => 1,
            'used_skill_count' => 3,
            'used_od_count' => 3,
            'used_dd_count' => 2,
            'used_ah_count' => 1,
            'max_combo_count' => 20,
            'max_chain_count' => 3,
            'finish_character_no' => 1,
            'finish_skill_type' => 5, // 101
            'wave_list' => [
                [
                    'turn_count' => 11,
                    'total_damage' => 12531,
                    'total_receive_damage' => 4321,
                ],
                [
                    'turn_count' => 12,
                    'total_damage' => 12532,
                    'total_receive_damage' => 4322,
                ],
                [
                    'turn_count' => 13,
                    'total_damage' => 12533,
                    'total_receive_damage' => 4323,
                ],
            ],
        ]);

        $response = BattleService::end($request);
        // DebugUtil::e_log('BSTS_end', 'response', $response);
        $this->_assertResponseOk($response);
    }


}
