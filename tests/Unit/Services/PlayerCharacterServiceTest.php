<?php
/**
 * プレイヤキャラクターサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\PlayerCharacterFavoriteSelectRequest;
use App\Http\Requests\PlayerCharacterListRequest;
use App\Http\Requests\PlayerCharacterOrbEquipRequest;
use App\Http\Requests\PlayerCharacterReinforceRequest;
use App\Http\Requests\PlayerCharacterUpdateRequest;
use Tests\TestCase;
use App\Http\Requests\PlayerCharacterEvolveRequest;
use App\Services\PlayerCharacterService;
use App\Utils\DebugUtil;

class PlayerCharacterServiceTest extends TestCase
{
    /*
     * list のテスト
     */
    public function testList()
    {
        $request = new PlayerCharacterListRequest();
        $request->player_id = static::PLAYER_ID_1;

        $response = PlayerCharacterService::list($request);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacterList($body, 'player_character_list');
        $this->_assertInt($body, 'player_character_num');
    }

    /*
     * update のテスト
     */
    public function testUpdate()
    {
        $request = new PlayerCharacterUpdateRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '1',
            'spine' => '1',
        ]);

        $response = PlayerCharacterService::update($request);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacter($body, 'player_character');
    }


	/**
	 * evolve のテスト
	 */
	public function testEvolve()
	{
		$request = new PlayerCharacterEvolveRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '5',
            'player_item_list' => [
                [
                    'player_item_id' => '249',
                    'item_num' => '2',
                ],
                [
                    'player_item_id' => '293',
                    'item_num' => '5',
                ]
            ]
        ]);

		$response = PlayerCharacterService::evolve($request);
		// DebugUtil::e_log('PCST_evolve', 'response', $response);

		$this->_assertResponseOk($response);

        $this->_assertPlayerCharacter($response->body, 'player_character');
	}


    /**
     * evolveとスパイン のテスト
     */
    public function testEvolveAndSpine()
    {
        $request = new PlayerCharacterEvolveRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '1',
            'player_item_list' => [
                [
                    'player_item_id' => '241',
                    'item_num' => '70',
                ],
            ]
        ]);

        $response = PlayerCharacterService::evolve($request);
        // DebugUtil::e_log('PCST_evolve', 'response', $response);

        $this->_assertResponseOk($response);

        $this->_assertPlayerCharacter($response->body, 'player_character');
    }
	/*
	 * reinforce のテスト
	 */
    public function testReinforce()
    {
        $request = new PlayerCharacterReinforceRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '1',
            'player_item_list' => [
                [
                    'player_item_id' => '6',
                    'item_num' => '1',
                ],
            ],
        ]);

        $response = PlayerCharacterService::reinforce($request);
        // DebugUtil::e_log('PCST_evolve', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacter($body, 'player_character');
	}

	/*
	 * skillReinforce のテスト
	 */
    public function testSkillReinforce()
    {
        $request = new PlayerCharacterReinforceRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '1',
            'skill_id' => '1001001',
            'platinum_dollar' => 50000,
        ]);

        $response = PlayerCharacterService::skillReinforce($request);
        // DebugUtil::e_log('PCST_evolve', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacter($body, 'player_character');
        $this->_assertUInt64($body, 'platinum_dollar');
	}

	/*
	 * orbEquip のテスト
	 */
    public function testOrbEquip()
    {
        $request = new PlayerCharacterOrbEquipRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '1',
            'equip_mask' => bindec('000000'),
            'player_item_list' => [
            ],
            'platinum_dollar' => 0,
            'grade_up' => 1,
        ]);

        $response = PlayerCharacterService::orbEquip($request);
        // DebugUtil::e_log('PCST_evolve', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacter($body, 'player_character');
        $this->_assertUInt64($body, 'platinum_dollar');
	}

    public function testOrbEquip2()
    {
        $request = new PlayerCharacterOrbEquipRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '3',
            'equip_mask' => bindec('000010'),
            'player_item_list' => [
                [
                    'player_item_id' => '298',
                    'item_num' => '1',
                ],
            ],
            'platinum_dollar' => 0,
            'grade_up' => 0,
        ]);

        $response = PlayerCharacterService::orbEquip($request);
        // DebugUtil::e_log('PCST_evolve', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacter($body, 'player_character');
        $this->_assertUInt64($body, 'platinum_dollar');
    }

	/*
	 * favoriteSelect のテスト
	 */
    public function testFavoriteSelect()
    {
        $request = new PlayerCharacterFavoriteSelectRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_character_id' => '1',
            'favorite_flag' => '1',
        ]);

        $response = PlayerCharacterService::favoriteSelect($request);
        // DebugUtil::e_log('PCST_evolve', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerCharacter($body, 'player_character');
	}


}
