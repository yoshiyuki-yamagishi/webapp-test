<?php
/**
 * プレイヤ魔道書サービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use Tests\TestCase;
use App\Http\Requests\PlayerGrimoireListRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Services\PlayerGrimoireService;
use App\Utils\DebugUtil;

class PlayerGrimoireServiceTest extends TestCase
{
	/**
	 * list のテスト
	 */
	public function testList()
	{
		$request = new PlayerGrimoireListRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
		]);

		$response = PlayerGrimoireService::List($request);
		// DebugUtil::e_log('PGST_list', 'response', $response);

		$this->_assertResponseOk($response);

        $body = $response->body;

		$this->_assertPlayerGrimoireList($body, 'player_grimoire_list');
	}

	/*
	 * lock のテスト。レスポンスなし
	 */
    public function testLock()
    {
        $request = new PlayerGrimoireListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'lock_grimoire_list' => [
                [
                    'player_grimoire_id' => '2',
                ],
            ],
            'unlock_grimoire_list' => [
                [
                    'player_grimoire_id' => '2', // 結局ロックされる
                ],
            ],
        ]);

        $response = PlayerGrimoireService::lock($request);
        // DebugUtil::e_log('PGST_list', 'response', $response);

        $this->_assertResponseOk($response);

	}

	/*
	 * reinforce のテスト
	 */
/*    public function testReinforce()
    {
        $request = new PlayerGrimoireReinforceRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_grimoire_id' => '1',
            'reinforce_type' => '2',
            'slot_no' => '1',
            'player_item_id' => '6',
        ]);

        $response = PlayerGrimoireService::reinforce($request);
        // DebugUtil::e_log('PGST_r', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerGrimoire($body, 'player_grimoire');
    }*/

    /*
     * 覚醒 のテスト
     */
    public function testReinforceAwake()
    {
        $request = new PlayerGrimoireReinforceRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_grimoire_id' => '1',
            'reinforce_type' => '0',
            'player_item_id' => '7',
        ]);

        $response = PlayerGrimoireService::reinforce($request);
         DebugUtil::e_log('PGST_r_a', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerGrimoire($body, 'player_grimoire');
    }

    /*
     * reinforce のテスト
     */
/*    public function testReinforceSlot()
    {
        $request = new PlayerGrimoireReinforceRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_grimoire_id' => '1',
            'reinforce_type' => '1',
            'slot_no' => '1',
        ]);

        $response = PlayerGrimoireService::reinforce($request);
         DebugUtil::e_log('PGST_r_s', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;
        $test = new PlayerGrimoireService();
        $
        $this->_assertPlayerGrimoire($body, 'player_grimoire');
    }*/

    /*
     * limitUp のテスト
     */
    public function testLimitUp()
    {
        $request = new PlayerGrimoireLimitUpRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'limit_up_num' => '5',
        ]);

        $response = PlayerGrimoireService::limitUp($request);
        // DebugUtil::e_log('PGST_list', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertInt($body, 'limit');
        $this->_assertInt($body, 'free_blue_crystal_num');
        $this->_assertInt($body, 'blue_crystal_num');
    }

	/**
	 * Sell のテスト
	 */
	public function testSell()
	{
		$request = new PlayerGrimoireSellRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_grimoire_list' => [
                [
                    'player_grimoire_id' => '9',
                ],
            ],
		]);

		$response = PlayerGrimoireService::sell($request);
        $this->_assertResponseOk($response);
		// DebugUtil::e_log('PGST_sell', 'response', $response);

        $this->_assertInt($response->body, 'magic_num');
	}

}
