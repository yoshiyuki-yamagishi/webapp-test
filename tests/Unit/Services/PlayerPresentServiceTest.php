<?php
/**
 * プレイヤプレゼントサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use App\Http\Requests\PlayerPresentTakeRequest;
use Tests\TestCase;
use App\Http\Requests\PlayerPresentListRequest;
use App\Services\PlayerPresentService;
use App\Utils\DebugUtil;

class PlayerPresentServiceTest extends TestCase
{
	/**
	 * list のテスト
	 */
	public function testList()
	{
		$request = new PlayerPresentListRequest();
		$request->merge([
            'player_id' => static::PLAYER_ID_1,
            'take_flag' => '0',
            'count' => '100',
		]);

		$response = PlayerPresentService::list($request);
		// DebugUtil::e_log('PPST_list', 'response', $response);
        $this->_assertResponseOk($response);

        $this->_assertArray($response->body, 'player_present_list');

        foreach ($response->body['player_present_list'] as $present)
        {
            $this->_assertUInt64($present, 'player_present_id');
            $this->_assertUInt8($present, 'type');
            $this->_assertInt($present, 'general_id');
            $this->_assertInt($present, 'num');
            $this->_assertStr($present, 'message');
            $this->_assertIsKey($present, 'taked_at');
            $this->_assertDateTime($present, 'expired_at');
        }
	}

    /*
    * Take の単体受け取りテスト
    */
    public function testTakeOne()
    {
        $request = new PlayerPresentTakeRequest();
        $request->merge([
            'current_date' => '2019-07-23 00:00:00',
            'player_id' => static::PLAYER_ID_1,
            'take_type' => '1',
            'player_present_id' => '1',
        ]);

        $response = PlayerPresentService::take($request);
        // DebugUtil::e_log('PPST_take_one', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerPresentList($body, 'take_over_list');
	}

    /*
    * Take の一括受け取りテスト
    */
    public function testTakeAll()
    {
        $request = new PlayerPresentTakeRequest();
        $request->merge([
            'current_date' => '2019-07-23 00:00:00',
            'player_id' => static::PLAYER_ID_1,
            'take_type' => '2',
            'count' => '100',
        ]);

        $response = PlayerPresentService::take($request);
        // DebugUtil::e_log('PPST_take_all', 'response', $response);
        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerPresentList($body, 'take_over_list');
    }
}
