<?php


namespace Tests\Unit\Services;


use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemOrbSynthesizeRequest;
use App\Http\Requests\PlayerItemPowderPurifyRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;
use Tests\TestCase;

class PlayerItemServiceTest extends TestCase
{
    /*
     * list テスト
     */
    public function testList()
    {
        $request = new PlayerItemListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
        ]);

        $response = PlayerItemService::list($request);
        // DebugUtil::e_log('PIS_list', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerItemList($body, 'player_item_list');
    }

    /*
     * list(Orbのみ) テスト
     */
    public function testListOrb()
    {
        $request = new PlayerItemListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'item_category' => 6,
        ]);

        $response = PlayerItemService::list($request);
        // DebugUtil::e_log('PIS_list_orb', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerItemList($body, 'player_item_list');
    }

    /*
     * sell のテスト
     */
    public function testSell()
    {
        $request = new PlayerItemSellRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'player_item_id' => 10,
            'num' => 1,
        ]);

        $response = PlayerItemService::sell($request);
        // DebugUtil::e_log('PIS_sell', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerItemList($body, 'player_item_list');
        $this->_assertUInt64($body, 'platinum_dollar_num');
    }

    /*
     * powderPurify のテスト
     */
    public function testPowderPurify()
    {
        $request = new PlayerItemPowderPurifyRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'powder' => 120,
            'player_item_list' => [
                [
                    // 'player_item_id' => '241', // initial_rarity:2
                    'player_item_id' => '243', // initial_rarity:4
                    'item_num' => '1',
                ],
            ],
        ]);

        $response = PlayerItemService::powderPurify($request);
        // DebugUtil::e_log('PIS_powder', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertUInt64($body, 'powder');
        $this->_assertPlayerItemList($body, 'player_item_list');
    }

    public function testOrbSynthesize()
    {
        $request = new PlayerItemOrbSynthesizeRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'item_id' => 4211131,
            'count' => 1,
            'player_item_list' => [
                [
                    'player_item_id' => '301',
                    'item_num' => '5',
                ],
                [
                    'player_item_id' => '302',
                    'item_num' => '1',
                ],
            ],
            'platinum_dollar' => 20000,
        ]);

        $response = PlayerItemService::orbSynthesize($request);
        // DebugUtil::e_log('PIS_orb', 'response', $response);

        $this->_assertResponseOk($response);
        $body = $response->body;

        $this->_assertPlayerItemList($body, 'player_item_list');
        $this->_assertUInt64($body, 'platinum_dollar');
    }

}
