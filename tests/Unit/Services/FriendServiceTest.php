<?php


namespace Tests\Unit\Services;

use App\Http\Requests\FriendListRequest;
use App\Services\FriendService;
use Tests\TestCase;

class FriendServiceTest extends  TestCase
{
    /*
     * list のテスト
     */
    public function testList()
    {
        $request = new FriendListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'friend_type' => 1,
            'search_id' => '',
            'sort_order' => 3,
            'from' => 0,
            'count' => 30,
        ]);

        $response = FriendService::list($request);

        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertInt($body, 'follow_num');
        $this->_assertInt($body, 'follower_num');
        $this->_assertInt($body, 'friend_num');
        $this->_assertFriendList($body, 'friend_list');
    }

    /*
     * list のテスト（時間順）
     */
    public function testListSortByLogin()
    {
        $request = new FriendListRequest();
        $request->merge([
            'player_id' => static::PLAYER_ID_1,
            'friend_type' => 1,
            'search_id' => '',
            'sort_order' => 1,
            'from' => 0,
            'count' => 30,
        ]);

        $response = FriendService::list($request);

        $this->_assertResponseOk($response);

        $body = $response->body;

        $this->_assertInt($body, 'follow_num');
        $this->_assertInt($body, 'follower_num');
        $this->_assertInt($body, 'friend_num');
        $this->_assertFriendList($body, 'friend_list');
    }

}
