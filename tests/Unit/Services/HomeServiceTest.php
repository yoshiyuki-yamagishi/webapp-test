<?php


namespace Tests\Unit\Services;


use App\Http\Requests\HomeRequest;
use App\Models\PlayerProductBuy;
use App\Models\PlayerProductBuyItem;
use App\Services\HomeService;
use App\Services\ProductService;
use App\Utils\DebugUtil;
use App\Utils\MathUtil;
use Tests\TestCase;

class HomeServiceTest extends TestCase
{
    const DAILY_FLAG = 2;
    const VALID_PERIOD = 30;
    /**
     * index のテスト
     * @throws \App\Exceptions\ApiException
     */
    public function testIndex()
    {
        $product_ids = self::insertDailyPack();
        $request = new HomeRequest();
        $request->player_id = static::PLAYER_ID_1;
        $response = HomeService::index($request);

        $this->_assertResponseOk($response);
        $body =  $response->body;

        DebugUtil::e_log('HST_index','response', $response);
        $this->_assertPlayer($body, 'player');
        $this->_assertLoginBonusList($body, 'new_login_bonus_list');
        $this->_assertLoginBonusList($body, 'login_bonus_list');
        $this->_assertInt($body, 'takable_present_num');
        $this->_assertInt($body, 'takable_mission_num');
        $this->_assertInt($body, 'quest_flag');
        $this->_assertInt($body, 'gacha_flag');
        $this->_assertBannerList($body, 'banner_list');

        self::deleteDailyPack($product_ids);
    }

    //1日前にデイリーパックを登録しておく
    public function insertDailyPack()
    {
        $ret = [];
        $grant_progress = 1;
        $date = date('Y/m/d H:i:s');
        $granted_at = null;
        for ($i = 0; $i < 3; $i++) {
            switch($i) {
                case 0:
                    //前日購入
                    $date = date('Y/m/d H:i:s', strtotime('- 1 day'));
                    break;
                case 1:
                    //期間ギリギリで且つ、その日のうちに受け取っている
                    $grant_progress = 25;
                    $granted_at = date('Y/m/d H:00:00');
                    $date = date('Y/m/d H:i:s', strtotime('- 25 day'));
                    break;
                case 2:
                    //期間が切れている
                    $grant_progress = 25;
                    $date = date('Y/m/d H:i:s', strtotime('- 30 day'));
                    break;
            }

            //購入したことにする
            $playerBuy = new PlayerProductBuy();
            $product_id = $playerBuy->insertGetId([
                'player_id' => ProductServiceTest::PLAYER_ID_1,
                'store_id' => ProductServiceTest::APP_STORE_ID,
                'payment_id' => null,
                'product_id' => ProductServiceTest::PRODUCT_ID_2,
                'daily_flg' => self::DAILY_FLAG,
                'valid_period' => self::VALID_PERIOD,
                'num' => 1,
                'currency' => ProductServiceTest::CURRENCY,
                'amount' => 2980,
                'state' => $playerBuy::STATE_FINISHED,
                'granted_at' => $granted_at,
                'created_at' => $date,
                'updated_at' => $date,
            ]);

            $productBuyItem = new PlayerProductBuyItem();
            $buy_item_id = $productBuyItem->insert([[
                'player_product_buy_id' => $product_id,
                'item_type' => 3,
                'item_id' => 9000000,
                'item_count' => 1000,
                'grant_type' => 1,
                'grant_progress' => 1,
                'grant_count' => 1,
                'created_at' => $date,
                'updated_at' => $date,
            ], [
                'player_product_buy_id' => $product_id,
                'item_type' => 3,
                'item_id' => 1000000,
                'item_count' => 100,
                'grant_type' => 2,
                'grant_progress' => $grant_progress,
                'grant_count' => 25,
                'created_at' => $date,
                'updated_at' => $date,
            ], [
                'player_product_buy_id' => $product_id,
                'item_type' => 3,
                'item_id' => 5000000,
                'item_count' => 5,
                'grant_type' => 2,
                'grant_progress' => $grant_progress,
                'grant_count' => 25,
                'created_at' => $date,
                'updated_at' => $date,
            ]]);

            $ret[] = $product_id;
        }
        return $ret;
    }

    public function deleteDailyPack($product_ids)
    {
        //消す
        $playerBuy = new PlayerProductBuy();
        $productBuyItem = new PlayerProductBuyItem();
        $playerBuy->whereIn('id', $product_ids)->delete();


        $productBuyItem->whereIn('player_product_buy_id', $product_ids)->delete();
    }

    /*
     * リワードやもろもろの処理が終わった後、もう一度接続して確かめるテスト
     * 初回の通信では通らなかった処理を通るかの確認
     */
    public function testCheckIndex()
    {
        $request = new HomeRequest();
        $request->player_id = static::PLAYER_ID_1;
        $response = HomeService::index($request);

        $this->_assertResponseOk($response);
        $body =  $response->body;

        DebugUtil::e_log('HST_index_check','response', $response);
        $this->_assertPlayer($body, 'player');
        $this->_assertLoginBonusList($body, 'new_login_bonus_list');
        $this->_assertLoginBonusList($body, 'login_bonus_list');
        $this->_assertInt($body, 'takable_present_num');
        $this->_assertInt($body, 'takable_mission_num');
        $this->_assertInt($body, 'quest_flag');
        $this->_assertInt($body, 'gacha_flag');
        $this->_assertBannerList($body, 'banner_list');
    }
}
