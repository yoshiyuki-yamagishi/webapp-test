<?php
/**
 * キャッシュサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use Tests\TestCase;
use App\Utils\DebugUtil;
use App\Models\CacheModel;

class CacheServiceTest extends TestCase
{
	/**
	 * CacheModel のテスト
	 */
	public function testTags()
	{
        // api/phpunit.xml
        // <env name="CACHE_DRIVER" value="array"/>
        // により、テストでは、array がディフォルト
        // だが、遅いので memcached に変更
        //
        // 調べてみたところ、array は、ただのメモリなので cache じゃない
        //
        // apc は、プロセス間共有メモリなので、一番速そうだが、
        // サーバー間の情報共有はできない。マスタには向いてるかも
        //
        // file は、タグが使えない。apc の下位互換なので使う意味なし

        $tags1 = ['test1'];
        $tags2 = ['test2'];
        $tags3 = ['test3'];
        $key = 'abc';
        $value1 = 3;
        $value2 = 4;
        $value3 = 5;

        CacheModel::set($tags1, $key, $value1);
        CacheModel::set($tags2, $key, $value2);
        CacheModel::set($tags3, $key, $value3);

        $this->assertEquals($value1, CacheModel::get($tags1, $key));
        $this->assertEquals($value2, CacheModel::get($tags2, $key));
        $this->assertEquals($value3, CacheModel::get($tags3, $key));

        CacheModel::clearAll($tags1);

        $this->assertNull(CacheModel::get($tags1, $key));
        $this->assertEquals($value2, CacheModel::get($tags2, $key));
        $this->assertEquals($value3, CacheModel::get($tags3, $key));

        CacheModel::clearAll($tags2);

        $this->assertNull(CacheModel::get($tags1, $key));
        $this->assertNull(CacheModel::get($tags2, $key));
        $this->assertEquals($value3, CacheModel::get($tags3, $key));

        CacheModel::clear($tags3, $key);

        $this->assertNull(CacheModel::get($tags3, $key));

    }

}
