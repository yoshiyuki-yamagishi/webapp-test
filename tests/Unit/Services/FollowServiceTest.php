<?php
/**
 * フォローサービスのテスト
 *
 */

namespace Tests\Unit\Services;

use Tests\TestCase;
use App\Http\Requests\FollowAddRequest;
use App\Http\Requests\FollowDeleteRequest;
use App\Services\FollowService;

class FollowServiceTest extends TestCase
{
	/**
	 * add delete のテスト
	 */
 	public function testAddDelete()
    {
        {
            $request = new FollowAddRequest();
            $request->player_id = static::PLAYER_ID_1;
            $request->follow_player_id = static::PLAYER_ID_2;
            $response = FollowService::add($request);
            $this->_assertResponseOk($response);
            $this->_assertInt($response->body, 'follow_num');
        }

        {
            $request = new FollowDeleteRequest();
            $request->player_id = static::PLAYER_ID_1;
            $request->follow_player_id = static::PLAYER_ID_2;
            $response = FollowService::delete($request);
            $this->_assertResponseOk($response);
            $this->_assertInt( $response->body, 'follow_num');
        }
    }

}
