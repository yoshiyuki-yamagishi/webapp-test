<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>BLAZBLUE ALTERNATIVE DARKWAR</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script language="JavaScript">
            /**
             * postForm
             * @desc  指定したURLに固定のパラメータをセットして送る
             * @param postUrl
             */
            function postForm(postUrl)
            {
                var form = document.createElement('form');
                document.body.appendChild(form);

                // 必要なパラメータを追加？
                // playerID
                var player_id = document.createElement('input');
                player_id.setAttribute('type', 'hidden' );
                player_id.setAttribute('name', 'player_id' );
                player_id.setAttribute('value' , {{$player_id}});
                form.appendChild(player_id);

                // playerID
                var app_version = document.createElement('input');
                app_version.setAttribute('type', 'hidden' );
                app_version.setAttribute('name', 'app_version' );
                app_version.setAttribute('value' , "{{$app_version}}");
                form.appendChild(app_version);

                form.setAttribute('method', 'post');
                form.setAttribute('action', postUrl);
                form.submit();
            }
        </script>
        {{-- 個別のjavaScript読み込み --}}
        @yield('javascript-head')

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        {{-- 個別のCSS読み込み --}}
        @yield('css')
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @yield('content')
        </div>

        {{-- 個別のjavaScript読み込み --}}
        @yield('javascript-head')
    </body>
</html>
