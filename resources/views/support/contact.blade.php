@extends('layout.webview')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            SUPPORT CONTACT
        </div>

        <hr />

        <div class="list">
            お電話によるサポートは行っておりません。<br />
            すべてお問い合わせは、サポート専用メール窓口からお問い合わせください。<br />
            対応言語は日本語のみとなっております。<br />
            また、ゲームの攻略方法や仕様についての質問はお受けできませんので、あらかじめご了承ください。<br />
            <br />
            ◆メールの返信について<br />
            返信の際はお問い合わせいただいたメールアドレスのみへ返信いたします。<br />
            <br />
            必ず返信可能なメールアドレスを入力してください<br />
            お客様がご利用になっているメールサービス、メールソフト、プロバイダの設定等によって、迷惑メールとして振り分けられる場合がございます。ご注意ください。<br />
            ドメイン指定されている場合「{{config('const.ContactMail.domain')}}」からのメールアドレスが受信できるように変更してください。<br />
            <br />
            メールアドレス内に、連続して「.」を使用されている、または@の前に「.」を使用されている場合、メールが届かないことがございます。<br />
            別のメールアドレスからお問い合わせください。<br />
            返信についてお時間をいただくことがございますので、あらかじめご了承ください。<br />
            <br />
            ※ご意見・ご感想への返信はいたしておりませんが、内容はすべて確認し今後の参考とさせていただきます。<br />
        </div>

        <hr />

        <div class="links">
            <a href="mailto:{{config('const.ContactMail.main')}}{{config('const.ContactMail.domain')}}?subject={{$subject}}&body={{$body}}">お問い合わせメールを送信する</a>
        </div>

        <hr />

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTop')}}')">return</a>
        </div>
    </div>
@endsection
