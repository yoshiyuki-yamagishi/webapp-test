@extends('layout.webview')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            SUPPORT COPYRIGHT
        </div>

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTop')}}')">return</a>
        </div>
    </div>
@endsection
