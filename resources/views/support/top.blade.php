@extends('layout.webview')

@section('content')
    <div class="content">
        <div class="title logo">
            <img class="logo" src="{{ asset('img/top.png') }}" alt="logo">
        </div>
        <div class="title m-b-md">
            SUPPORT TOP
        </div>

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportFaq')}}')">FAQ</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTerms')}}')">利用規約</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportPrivacy')}}')">プライバシーポリシー</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportCopyright')}}')">権利表記</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportContactSelect')}}')">お問い合わせ</a>
        </div>
    </div>
@endsection
