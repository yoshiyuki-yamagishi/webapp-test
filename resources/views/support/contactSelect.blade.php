@extends('layout.webview')

@section('javascript-head')
    <script language="JavaScript">
        /**
         * リンクフォーム
         * @desc  固定URLに固定のパラメータ+お問い合わせタイプをセットして送る
         * @param contactType
         */
        function contactForm(contactType)
        {
            var form = document.createElement('form');
            document.body.appendChild(form);

            // playerID
            var player_id = document.createElement('input');
            player_id.setAttribute('type', 'hidden' );
            player_id.setAttribute('name', 'player_id' );
            player_id.setAttribute('value', {{$player_id}});
            form.appendChild(player_id);

            // appVersion
            var app_version = document.createElement('input');
            app_version.setAttribute('type', 'hidden' );
            app_version.setAttribute('name', 'app_version' );
            app_version.setAttribute('value', '{{$app_version}}');
            form.appendChild(app_version);

            // contactType
            var type = document.createElement('input');
            type.setAttribute('type', 'hidden' );
            type.setAttribute('name', 'contact_type' );
            type.setAttribute('value', contactType);
            form.appendChild(type);

            form.setAttribute('method', 'post');
            form.setAttribute('action', "{{url('/webview/supportContact')}}");
            form.submit();
        }
    </script>
@endsection

@section('content')
    <div class="content">
        <div class="title m-b-md">
            SUPPORT CONTACT SELECT
        </div>

        <hr />

        {{-- お問い合わせタイプ一覧 --}}
        <div class="links">
            @foreach($subject_list as $id => $subject)
                <a href="javascript:void()" onclick="contactForm({{$id}})">{!! $subject !!}</a><br />
            @endforeach
        </div>

        <hr />

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTop')}}')">return</a>
        </div>
    </div>
@endsection
