@extends('layout.webview')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            SUPPORT PRIVACY
        </div>

        <div class="links">
            @if ($viewReturn)
                <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTop')}}')">return</a>
            @endif
        </div>
    </div>
@endsection
