@extends('layout.webview')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            BLAZBLUE ALTERNATIVE DARKWAR<br />
            TEST PAGE
        </div>

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTop')}}')">Support Top</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportFaq')}}')">FAQ</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTerms')}}')">利用規約</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportPrivacy')}}')">プライバシーポリシー</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportCopyright')}}')">権利表記</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportContactSelect')}}')">お問い合わせ</a>
        </div>
        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/infoList')}}')">News List</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/info')}}')">News Info</a>
        </div>
        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/help')}}')">Help</a>
            <a href="javascript:void()" onclick="postForm('{{url('/webview/credit')}}')">Credit</a>
        </div>
    </div>
@endsection
