@extends('layout.webview')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            HELP
        </div>

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/supportTop')}}')">return</a>
        </div>
    </div>
@endsection
