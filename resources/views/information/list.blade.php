@extends('layout.webview')

@section('javascript-head')
    <script language="JavaScript">
        /**
         * タブリンクフォーム
         * @desc  固定URLに固定のパラメータ+お知らせタイプをセットして送る
         * @param infoType
         */
        function tabForm(infoType)
        {
            var form = document.createElement('form');
            document.body.appendChild(form);

            // playerID
            var player_id = document.createElement('input');
            player_id.setAttribute('type', 'hidden' );
            player_id.setAttribute('name', 'player_id' );
            player_id.setAttribute('value' , {{$player_id}});
            form.appendChild(player_id);

            // infoType
            var type = document.createElement('input');
            type.setAttribute('type', 'hidden' );
            type.setAttribute('name', 'info_type' );
            type.setAttribute('value' , infoType);
            form.appendChild(type);

            form.setAttribute('method', 'post');
            form.setAttribute('action', "{{url('/webview/infoList')}}");
            form.submit();
        }

        /**
         * お知らせリンクフォーム
         * @desc  指定したURLに固定のパラメータ+お知らせIDやタイプをセットして送る
         * @param infoType
         * @param infoId
         */
        function infoForm(infoType, infoId)
        {
            var form = document.createElement('form');
            document.body.appendChild(form);

            // playerID
            var player_id = document.createElement('input');
            player_id.setAttribute('type', 'hidden' );
            player_id.setAttribute('name', 'player_id' );
            player_id.setAttribute('value' , {{$player_id}});
            form.appendChild(player_id);

            // infoType
            var type = document.createElement('input');
            type.setAttribute('type', 'hidden' );
            type.setAttribute('name', 'info_type' );
            type.setAttribute('value' , infoType);
            form.appendChild(type);

            // infoType
            var info_id = document.createElement('input');
            info_id.setAttribute('type', 'hidden' );
            info_id.setAttribute('name', 'info_id' );
            info_id.setAttribute('value' , infoId);
            form.appendChild(info_id);

            form.setAttribute('method', 'post');
            form.setAttribute('action', "{{url('/webview/info')}}");
            form.submit();
        }
    </script>
@endsection

@section('content')
    <div class="content">
        <div class="title m-b-md">
            News List
        </div>

        {{-- タブ --}}
        <div class="links">
            @if ($info_type != config('const.InformationType.ALL'))
                <a href="javascript:void()" onclick="tabForm({{config('const.InformationType.ALL')}})">お知らせ</a>
            @else
                <a href="javascript:void()" onclick="noFunction();return false;">お知らせ</a>
            @endif
            @if ($info_type != config('const.InformationType.MAINTENANCE'))
                <a href="javascript:void()" onclick="tabForm({{config('const.InformationType.MAINTENANCE')}})">メンテナンス</a>
            @else
                <a href="javascript:void()" onclick="noFunction();return false;">メンテナンス</a>
            @endif
            @if ($info_type != config('const.InformationType.UPDATE'))
                <a href="javascript:void()" onclick="tabForm({{config('const.InformationType.UPDATE')}})">アップデート</a>
            @else
                <a href="javascript:void()" onclick="noFunction();return false;">アップデート</a>
            @endif
            @if ($info_type != config('const.InformationType.BUG_REPORT'))
                <a href="javascript:void()" onclick="tabForm({{config('const.InformationType.BUG_REPORT')}})">不具合報告</a>
            @else
                <a href="javascript:void()" onclick="noFunction();return false;">不具合報告</a>
            @endif
        </div>

        <hr />

        {{-- お知らせ一覧 --}}
        <div class="links">
            @foreach($info_list as $info)
                <a href="javascript:void()" onclick="infoForm({{$info_type}}, {{$info->id}})">{!! $info->title !!}</a><br />
            @endforeach
        </div>

        <hr />

        <div class="links">
            <a href="javascript:void()" onclick="postForm('{{url('/webview/info')}}')">Info</a>
        </div>
    </div>
@endsection
