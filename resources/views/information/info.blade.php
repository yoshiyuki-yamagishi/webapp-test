@extends('layout.webview')

@section('javascript-head')
    <script language="JavaScript">
        /**
         * タブリンクフォーム
         * @desc  固定URLに固定のパラメータ+お知らせタイプをセットして送る
         * @param postUrl
         * @param infoType
         */
        function tabForm(infoType)
        {
            var form = document.createElement('form');
            document.body.appendChild(form);

            // playerID
            var player_id = document.createElement('input');
            player_id.setAttribute('type', 'hidden' );
            player_id.setAttribute('name', 'player_id' );
            player_id.setAttribute('value' , {{$player_id}});
            form.appendChild(player_id);

            // infoType
            var type = document.createElement('input');
            type.setAttribute('type', 'hidden' );
            type.setAttribute('name', 'info_type' );
            type.setAttribute('value' , infoType);
            form.appendChild(type);

            form.setAttribute('method', 'post');
            form.setAttribute('action', "{{url('/webview/infoList')}}");
            form.submit();
        }
    </script>
@endsection

@section('content')
    <div class="content">
        {{-- 画像 --}}
        @if($info->image_name !== "")
            <div class="title logo">
                <img class="logo" src="{{ asset('img/'.$info->image_name) }}" alt="logo">
            </div>
        @endif

        <hr />

        {{-- タイトル --}}
        <div class="title m-b-md">
            {!! $info->title !!}
        </div>

        <hr />

        {{-- 本文 --}}
        <div class="m-b-md">
            {!! $info->content !!}
        </div>

        <hr />

        <div class="links">
            <a href="javascript:void()" onclick="tabForm('{{$info_type}}')">戻る</a>
        </div>
    </div>
@endsection
