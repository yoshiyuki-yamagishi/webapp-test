<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// v1
Route::group(['prefix' => '/v1'], function () {

    Route::post('/app_ver', 'AppVerController@index');

    Route::post('/player/regist', 'PlayerController@regist');
    Route::post('/player/get', 'PlayerController@get');
    Route::post('/player/update', 'PlayerController@update');
    Route::post('/player/tutorial_update', 'PlayerController@tutorialUpdate');
    Route::post('/player/al_recover', 'PlayerController@alRecover');
    Route::post('/player/transit_src', 'PlayerController@transitSrc');
    Route::post('/player/transit_dst', 'PlayerController@transitDst');
    Route::post('/player/account_start', 'PlayerController@accountStart');
    Route::post('/player/account_end', 'PlayerController@accountEnd');
    Route::post('/player/account_info', 'PlayerController@accountInfo');
    Route::post('/player/account_restore', 'PlayerController@accountRestore');

    Route::post('/login', 'LoginController@index');;
    Route::post('/login/update', 'LoginController@update');
    Route::post('/login/battle_code', 'LoginController@battleCode');

    Route::post('/home', 'HomeController@index');

    Route::post('/event/top', 'EventController@top');
    Route::post('/event/player_quest_list', 'EventController@playerQuestList');

    Route::post('/event_shop/lineup', 'EventShopController@lineup');
    Route::post('/event_shop/buy', 'EventShopController@buy');

    Route::post('/player_present/list', 'PlayerPresentController@list');
    Route::post('/player_present/take', 'PlayerPresentController@take');

    Route::post('/player_mission/list', 'PlayerMissionController@list');
    Route::post('/player_mission/reward_take', 'PlayerMissionController@rewardTake');

    Route::post('/player_character/list', 'PlayerCharacterController@list');
    Route::post('/player_character/update', 'PlayerCharacterController@update');
    Route::post('/player_character/evolve', 'PlayerCharacterController@evolve');
    Route::post('/player_character/reinforce', 'PlayerCharacterController@reinforce');
    Route::post('/player_character/skill_reinforce', 'PlayerCharacterController@skillReinforce');
    Route::post('/player_character/orb_equip', 'PlayerCharacterController@orbEquip');
    Route::post('/player_character/leader_select', 'PlayerCharacterController@leaderSelect');
    Route::post('/player_character/favorite_select', 'PlayerCharacterController@favoriteSelect');

    Route::post('/player_grimoire/list', 'PlayerGrimoireController@list');
    Route::post('/player_grimoire/lock', 'PlayerGrimoireController@lock');
    Route::post('/player_grimoire/reinforce', 'PlayerGrimoireController@reinforce');
    Route::post('/player_grimoire/limit_up', 'PlayerGrimoireController@limitUp');
    Route::post('/player_grimoire/sell', 'PlayerGrimoireController@sell');

    Route::post('/player_party/list', 'PlayerPartyController@list');
    Route::post('/player_party/update', 'PlayerPartyController@update');

    Route::post('/player_quest/list', 'PlayerQuestController@list');
    Route::post('/player_quest/update', 'PlayerQuestController@update');

    Route::post('/battle/start', 'BattleController@start');
    Route::post('/battle/restart', 'BattleController@restart');
    Route::post('/battle/end', 'BattleController@end');
    Route::post('/battle/continue', 'BattleController@continue_');
    Route::post('/battle/skip', 'BattleController@skip');
    Route::post('/battle/interrupt', 'BattleController@interrupt');

    Route::post('/gacha/list', 'GachaController@list');
    Route::post('/gacha/exec', 'GachaController@exec');
    Route::post('/gacha/retry', 'GachaController@retry');
    Route::post('/gacha/commit', 'GachaController@commit');

    Route::post('/gacha_ceiling/list', 'GachaCeilingController@list');
    Route::post('/gacha_ceiling/exec', 'GachaCeilingController@exec');

//	Route::post('/gacha_history/list', 'GachaHistoryController@list');

    Route::post('/product/list', 'ProductController@list');
    Route::post('/product/buy', 'ProductController@buy');
    Route::post('/product/regist', 'ProductController@regist');
    Route::post('/product/resume', 'ProductController@resume');
    Route::post('/product/cancel', 'ProductController@cancel');

    Route::post('/shop/list', 'ShopController@list');
    Route::post('/shop/lineup_update', 'ShopController@lineupUpdate');
    Route::post('/shop/buy', 'ShopController@buy');

	Route::post('/player_item/list', 'PlayerItemController@list');
	Route::post('/player_item/sell', 'PlayerItemController@sell');
	Route::post('/player_item/powder_purify', 'PlayerItemController@powderPurify');
	Route::post('/player_item/orb_synthesize', 'PlayerItemController@orbSynthesize');

    Route::post('/player_event_item/list', 'PlayerEventItemController@list');
//	Route::post('/player_story/list', 'PlayerStoryController@list');

	Route::post('/player_data/get', 'PlayerDataController@get');
	Route::post('/player_data/set', 'PlayerDataController@set');

	Route::post('/friend/list', 'FriendController@list');
	Route::post('/follow/add', 'FollowController@add');
	Route::post('/follow/delete', 'FollowController@delete');

	Route::post('/debug_command/add_item', 'DebugCommandController@addItem');
});

Route::group(['prefix' => '/webview'], function () {
    // Test
    Route::get('/test', 'Webview\TestController@index');
    Route::post('/test', 'Webview\TestController@index');

    // Support =====================================
    // Top
    Route::get('/supportTop', 'Webview\SupportController@top');
    Route::post('/supportTop', 'Webview\SupportController@top');

    // Faq
    Route::get('/supportFaq', 'Webview\SupportController@faq');
    Route::post('/supportFaq', 'Webview\SupportController@faq');

    // Terms
    Route::get('/supportTermsTop', 'Webview\SupportController@terms_top');  // アプリ初回起動の規約からの遷移
    Route::post('/supportTermsTop', 'Webview\SupportController@terms_top'); // アプリ初回起動の規約からの遷移
    Route::get('/supportTerms', 'Webview\SupportController@terms');
    Route::post('/supportTerms', 'Webview\SupportController@terms');

    // Privacy
    Route::get('/supportPrivacyTop', 'Webview\SupportController@privacy_top');  // アプリ初回起動の規約からの遷移
    Route::post('/supportPrivacyTop', 'Webview\SupportController@privacy_top'); // アプリ初回起動の規約からの遷移
    Route::get('/supportPrivacy', 'Webview\SupportController@privacy');
    Route::post('/supportPrivacy', 'Webview\SupportController@privacy');

    // Copyright
    Route::get('/supportCopyright', 'Webview\SupportController@copyright');
    Route::post('/supportCopyright', 'Webview\SupportController@copyright');

    // ContactSelect
    Route::get('/supportContactSelect', 'Webview\SupportController@contactSelect');
    Route::post('/supportContactSelect', 'Webview\SupportController@contactSelect');

    // Contact
    Route::get('/supportContact', 'Webview\SupportController@contact');
    Route::post('/supportContact', 'Webview\SupportController@contact');
    // Support =====================================

    // News =====================================
    // List
    Route::get('/infoList', 'Webview\InformationController@list');
    Route::post('/infoList', 'Webview\InformationController@list');

    // Info
    Route::get('/info', 'Webview\InformationController@info');
    Route::post('/info', 'Webview\InformationController@info');
    // News =====================================

    // Browse =====================================
    // Help
    Route::get('/help', 'Webview\BrowseController@help');
    Route::post('/help', 'Webview\BrowseController@help');

    // credit
    Route::get('/credit', 'Webview\BrowseController@credit');
    Route::post('/credit', 'Webview\BrowseController@credit');
    // Browse =====================================
});
