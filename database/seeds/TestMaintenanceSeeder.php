<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class TestMaintenanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $IDC = 'id < 3000'; // 9999 までテスト用に確保してある
        $startHours = - 24 + 9;
        $endHours = 24 + 9;

$query = <<<QUERY
delete from `maintenance` where $IDC;
insert into `maintenance` values
(1,'12345','テストのためメンテナンス中です',NOW()+INTERVAL $startHours HOUR,NOW()+INTERVAL $endHours HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')

QUERY;

        SqlUtil::execRawSqls($query);
    }
}
