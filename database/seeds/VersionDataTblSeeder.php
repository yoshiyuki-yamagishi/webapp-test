<?php

use Illuminate\Database\Seeder;
use App\Config\Constants;
use App\Models\VersionData;

class VersionDataTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // BaseData
        $keys = ['platform', 'app_version', 'master_version', 'ab_version'];
        $values = [
            [Constants::PLATFORM_IOS, '["1.0.0"]', 1.00, 1.00],
            [Constants::PLATFORM_ANDROID, '["1.0.0"]', 1.00, 1.00],
            [Constants::PLATFORM_IOS, '["1.0.0"]', 1.01, 1.00],
            [Constants::PLATFORM_ANDROID, '["1.0.0"]', 1.01, 1.00],
        ];
        foreach ($values as $idx => $v) {
            $id = $idx + 1;
            $insValue = array_combine($keys, $v);
            VersionData::firstOrCreate(['id' => $id], $insValue);
        }
    }

}
