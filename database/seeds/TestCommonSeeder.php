<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class TestCommonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $IDC = 'id < 3000'; // 9999 までテスト用に確保してある
        
$query = <<<QUERY
delete from `player_common` where $IDC;
delete from `player_common_cache` where $IDC;
delete from `player_friend` where $IDC;
delete from `maintenance` where $IDC;
delete from `direct_present` where $IDC;

INSERT INTO `player_common` VALUES
(1,101,'AAAA0101','hogehoge1',1,'10.0','huaway p20-1 pro',1,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(2,102,'AAAA0102','hogehoge2',1,'10.0','huaway p20-2 pro',1,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(3,103,'AAAA0103','hogehoge3',1,'10.0','huaway p20-3 pro',1,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(4,104,'AAAA0104','hogehoge4',1,'10.0','huaway p20-4 pro',1,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(5,105,'AAAA0105','hogehoge5',2,'13.0','iPhone 8',1,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(6,106,'AAAA0106','hogehoge6',2,'13.0','iPhone 9',1,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(7,2001,'AAAA2001','mogamoga1',2,'13.0','iPhone 10',2,NULL,NULL,NULL,1,'2019-04-16 16:04:30','2019-04-16 16:04:30');

INSERT INTO `player_common_cache` VALUES
(1,101,'山田太郎',15,100,'てきとうに頑張ります',NOW() + INTERVAL 8 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(2,102,'テスト2',10,300,'メッセージ2',NOW() + INTERVAL 9 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(3,103,'テスト3',20,500,'メッセージ3',NOW() + INTERVAL 1 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(4,104,'テスト4',12,700,'メッセージ4',NOW() + INTERVAL 4 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(5,105,'テスト5',17,900,'メッセージ5',NOW() + INTERVAL 3 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(6,106,'テスト6',3,1000,'メッセージ6',NOW() + INTERVAL 7 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(7,2001,'佐藤花子',18,100,'それなりにやります',NOW() + INTERVAL 1 HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30');

INSERT INTO `player_friend` VALUES
(1,101,102,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(2,101,104,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(3,103,101,'2019-04-16 16:04:30','2019-04-16 16:04:30')
,(4,104,101,'2019-04-16 16:04:30','2019-04-16 16:04:30');

INSERT INTO `direct_present` VALUES
(1,3,101,'2019-10-26 00:00:00','2119-10-28 00:00:00',1,100,1,1,0,'お詫びのキャラです',30,0,'2019-10-10 00:00:00','2050-10-10 00:00:00','2019-04-16 16:04:30','2019-04-16 16:04:30');

QUERY;

        SqlUtil::execRawSqls($query);
    }
}
