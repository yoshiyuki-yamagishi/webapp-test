<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class TestGame02Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PC_ = 'id >= 2000 and id < 3000';
        $PC = 'player_id >= 2000 and player_id < 3000';
        
$query = <<<QUERY
delete from `player` where $PC_;
delete from `player_ban` where $PC;
delete a from `player_battle_character` as a inner join player_battle as b on a.player_battle_id = b.id where $PC;
delete a from `player_battle_grimoire` as a inner join player_battle as b on a.player_battle_id = b.id where $PC;
delete a from `player_battle_result` as a inner join player_battle as b on a.player_battle_id = b.id where $PC;
delete a from `player_battle_reward` as a inner join player_battle as b on a.player_battle_id = b.id where $PC;
delete from `player_battle` where $PC;
delete from `player_blue_crystal` where $PC;
delete from `player_blue_crystal_log` where $PC;
delete from `player_character` where $PC;
delete from `player_character_log` where $PC;
delete from `player_data` where $PC;
delete a from `player_gacha_result` as a inner join player_gacha as b on a.player_gacha_id = b.id where $PC;
delete from `player_gacha` where $PC;
delete from `player_grimoire` where $PC;
delete from `player_grimoire_log` where $PC;
delete from `player_item` where $PC;
delete from `player_item_buy` where $PC;
delete from `player_item_log` where $PC;
delete from `player_log` where $PC;
delete from `player_login` where $PC;
delete from `player_login_bonus` where $PC;
delete from `player_mission` where $PC;
delete from `player_party` where $PC;
delete from `player_present` where $PC;
delete a from `player_product_buy_item` as a inner join player_product_buy as b on a.player_product_buy_id = b.id where $PC;
delete from `player_product_buy` where $PC;
delete a from `player_quest_mission` as a inner join player_quest as b on a.player_quest_id = b.id where $PC;
delete from `player_quest` where $PC;
delete from `player_shop_item` where $PC;

INSERT INTO `player` VALUES
(2001,'佐藤花子',18,950,'2006-03-06 00:00:00','2005-03-06 00:00:00',2,0,0,1,2,3,4,80,80,'2019-04-24 14:33:33',100,null,'それなりにやります',4,'2019-04-16 18:18:40','2019-04-24 14:36:38','2019-04-16 18:18:40','2019-05-14 17:30:13');

QUERY;

        SqlUtil::execRawSqls($query);
    }
}
