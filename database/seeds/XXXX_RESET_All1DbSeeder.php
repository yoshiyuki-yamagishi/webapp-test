<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class XXXX_RESET_All1DbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            XXXX_CLEAR_CommonSeeder::class,
            XXXX_CLEAR_GameSeeder::class,
            TestCommonSeeder::class,
            TestGame01Seeder::class,
            TestGame02Seeder::class,
            //TestLogSeeder::class,
        ]);
    }
}
