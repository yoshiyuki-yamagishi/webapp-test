<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class TestWarnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PC = 'player_id < 2000';
        $startHours = - 24 + 9;
        $endHours = 24 + 9;

$query = <<<QUERY
delete from `player_ban` where $PC;
insert into `player_ban` values
(1,101,1,'規約違反のため、警告中です',0,NOW()+INTERVAL $startHours HOUR,NOW()+INTERVAL $endHours HOUR,'2019-04-16 16:04:30','2019-04-16 16:04:30')

QUERY;

        SqlUtil::execRawSqls($query);
    }
}
