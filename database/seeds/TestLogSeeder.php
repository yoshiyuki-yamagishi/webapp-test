<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class TestLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PC_ = 'id >= 2000 and id < 3000';
        $PC = 'player_id >= 2000 and player_id < 3000';
        //テストデータなので簡単に+9Hして日本時間にする。

$query = <<<QUERY
delete from `login_log` where $PC;
delete from `mission_start_log` where $PC;
delete from `mission_end_log` where $PC;
delete a from `player_quest_character_log` as a inner join `player_quest_log` as b on a.player_battle_id = b.player_battle_id where $PC;
delete a from `player_quest_grimoire_log` as a inner join `player_quest_log` as b on a.player_battle_id = b.player_battle_id where $PC;
delete from `player_quest_log` where $PC;
delete from `item_buy_log` where $PC;
delete from `product_buy_log` where $PC;
delete a from `gacha_item_log` as a inner join `gacha_log` as b on a.player_gacha_id = b.player_gacha_id where $PC;
delete from `gacha_log` where $PC;

INSERT INTO `login_log` VALUES
(101, 12, NOW()),
(2001, 16, NOW()),
(2002, 33, NOW()),
(2003, 1, NOW()),
(2004, 93, NOW()),
(2005, 62, NOW());

INSERT INTO `mission_start_log` VALUES
(101, 12, 101001, 1, 0, 5, NOW()),
(101, 12, 102001, 1, 0, 1, NOW()),
(101, 12, 106001, 1, 0, 1, NOW()),
(101, 12, 108001, 1, 0, 1, NOW()),
(101, 12, 109001, 1, 0, 5, NOW()),
(101, 12, 111001, 1, 0, 1, NOW()),
(101, 12, 121001, 1, 0, 7, NOW());

INSERT INTO `mission_end_log` VALUES
(101, 12, 101001, 1, 5, 5, NOW()),
(101, 12, 102001, 1, 1, 1, NOW()),
(101, 12, 106001, 1, 1, 1, NOW()),
(101, 12, 108001, 1, 1, 1, NOW()),
(101, 12, 109001, 1, 5, 5, NOW()),
(101, 12, 111001, 1, 1, 1, NOW()),
(101, 12, 121001, 1, 7, 7, NOW());

INSERT INTO `player_quest_log` VALUES
(101, 12, 1, 101002, 0, 3, 422001, 435001, 425001, 1, 1, 0, 1000, 1002, 1223, 132, 230, 277, 3, 4, 4, NOW()),
(101, 12, 2, 101003, 0, 3, 422001, 435001, 425001, 1, 1, 0, 1000, 1002, 1223, 132, 230, 277, 3, 4, 4, NOW()),
(101, 12, 3, 101004, 0, 3, 422001, 435001, 425001, 1, 1, 0, 1000, 1002, 1223, 132, 230, 277, 3, 4, 4, NOW()),
(101, 12, 4, 102002, 0, 3, 422001, 435001, 425001, 1, 1, 0, 1000, 1002, 1223, 132, 230, 277, 3, 4, 4, NOW()),
(101, 12, 5, 102003, 0, 3, 422001, 435001, 425001, 1, 1, 0, 1000, 1002, 1223, 132, 230, 277, 3, 4, 4, NOW());

INSERT INTO `player_quest_character_log` VALUES
(1, 1, 1, 100, 2, 3, 5, 4112001, null, null, null, null, null, 1, 1, NOW()),
(1, 2, 2, 300, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(1, 3, 3, 400, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(2, 1, 1, 100, 2, 3, 5, 4112001, null, null, null, null, null, 1, 1, NOW()),
(2, 2, 2, 300, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(2, 3, 3, 400, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(3, 1, 1, 100, 2, 3, 5, 4112001, null, null, null, null, null, 2, 1, NOW()),
(3, 2, 2, 300, 1, 1, 3, null, null, null, null, null, null, 1, 1, NOW()),
(3, 3, 3, 400, 1, 1, 2, null, null, null, null, null, null, 1, 1, NOW()),
(4, 1, 1, 100, 2, 3, 5, 4112001, null, null, null, null, null, 2, 1, NOW()),
(4, 2, 2, 300, 1, 1, 3, null, null, null, null, null, null, 1, 1, NOW()),
(4, 3, 3, 400, 1, 1, 2, null, null, null, null, null, null, 1, 1, NOW()),
(4, 4, 4, 1400, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(5, 1, 1, 100, 2, 3, 5, 4112001, null, null, null, null, null, 2, 1, NOW()),
(5, 2, 2, 300, 1, 1, 3, null, null, null, null, null, null, 1, 1, NOW()),
(5, 3, 3, 400, 1, 1, 2, null, null, null, null, null, null, 1, 1, NOW()),
(5, 4, 4, 1400, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(5, 5, 5, 600, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(5, 6, 6, 1000, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(5, 7, 7, 3500, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW()),
(5, 8, 8, 500, 1, 1, 1, null, null, null, null, null, null, 1, 1, NOW());

INSERT INTO `player_quest_grimoire_log` VALUES
(1, 1, 1, 2001, 1, null, null, null, NOW()),
(2, 1, 1, 2001, 1, null, null, null, NOW()),
(2, 2, 2, 5011, 1, null, null, null, NOW()),
(3, 1, 1, 2001, 1, null, null, null, NOW()),
(3, 2, 2, 5011, 1, null, null, null, NOW()),
(4, 1, 1, 2001, 1, null, null, null, NOW()),
(4, 2, 2, 5011, 1, null, null, null, NOW()),
(5, 1, 1, 2001, 1, null, null, null, NOW()),
(5, 2, 2, 5011, 1, null, null, null, NOW()),
(5, 3, 3, 2005, 1, null, null, null, NOW()),
(5, 4, 4, 2002, 1, null, null, null, NOW()),
(5, 5, 5, 5001, 1, null, null, null, NOW()),
(5, 6, 6, 4022, 1, null, null, null, NOW()),
(5, 7, 7, 4028, 1, null, null, null, NOW()),
(5, 8, 8, 3022, 1, null, null, null, NOW());

INSERT INTO `item_buy_log` VALUES
(101, 12, 1001, 1, 1, 4142003, 10000000, 10, NOW()),
(101, 12, 2001, 20, 1, 6000300, 12000000, 10, NOW()),
(101, 12, 3001, 34, 2, 5001, 11000000, 100, NOW()),
(101, 12, 4001, 53, 2, 5004, 13000001, 500, NOW());

INSERT INTO `product_buy_log` VALUES
(101, 12, 2, 10001, 1, 'jp', 120, NOW()),
(101, 12, 2, 10002, 1, 'jp', 480, NOW()),
(101, 12, 2, 10003, 1, 'jp', 840, NOW()),
(101, 12, 2, 10004, 1, 'jp', 2000, NOW()),
(2005, 62, 1, 20002, 1, 'jp', 9800, NOW());

INSERT INTO `gacha_log` VALUES
(101, 12, 1, 20002, 10, 0, 0, NOW()),
(101, 12, 2, 20001, 1, 1, 1, NOW()),
(101, 12, 3, 10001, 10, 1, 2, NOW()),
(101, 12, 4, 30001, 10, 1, 2, NOW());

INSERT INTO `gacha_item_log` VALUES
(1, 2, 3006, 1, 0, 0, NOW()),
(1, 2, 4027, 1, 0, 0, NOW()),
(1, 1, 3500, 1, 0, 0, NOW()),
(1, 1, 900, 1, 0, 0, NOW()),
(1, 2, 3008, 1, 0, 0, NOW()),
(1, 2, 3011, 1, 0, 0, NOW()),
(1, 2, 3023, 1, 0, 0, NOW()),
(1, 1, 800, 1, 0, 0, NOW()),
(1, 2, 3006, 1, 0, 0, NOW()),
(1, 1, 700, 1, 1, 0, NOW()),
(2, 1, 2800, 1, 0, 0, NOW()),
(3, 1, 9100, 1, 0, 0, NOW()),
(3, 1, 6300, 1, 0, 0, NOW()),
(3, 1, 3500, 1, 0, 0, NOW()),
(3, 1, 5900, 1, 0, 0, NOW()),
(3, 1, 1600, 1, 0, 0, NOW()),
(3, 1, 2300, 1, 0, 0, NOW()),
(3, 1, 7400, 1, 0, 0, NOW()),
(3, 1, 980, 1, 0, 0, NOW()),
(3, 1, 3006, 1, 0, 0, NOW()),
(3, 1, 6000, 1, 1, 0, NOW()),
(4, 3, 7999999, 1, 0, 0, NOW()),
(4, 3, 7000100, 1, 0, 0, NOW()),
(4, 3, 7000050, 1, 0, 0, NOW()),
(4, 3, 7000050, 1, 0, 0, NOW()),
(4, 3, 7000050, 1, 0, 0, NOW()),
(4, 3, 7000050, 1, 0, 0, NOW()),
(4, 3, 7000050, 1, 0, 0, NOW()),
(4, 3, 7000100, 1, 0, 0, NOW()),
(4, 3, 7000050, 1, 0, 0, NOW()),
(4, 3, 7000100, 1, 0, 0, NOW());

QUERY;

        SqlUtil::execRawSqls($query);
    }
}
