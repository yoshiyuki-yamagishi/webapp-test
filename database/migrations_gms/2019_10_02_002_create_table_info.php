<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableInfo extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table info (
	id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'お知らせID'
	, info_id TINYINT UNSIGNED not null comment 'お知らせ種別:1:お知らせ
  2:メンテナンス
  3:アップデート
  4:不具合報告'
	, fname VARCHAR(64) not null comment 'ファイル名'
	, title VARCHAR(1024) not null comment 'タイトル'
	, content TEXT not null comment '本文'
	, priority INT UNSIGNED not null comment '優先度'
	, status_flag TINYINT UNSIGNED default 0 not null comment '状態フラグ:0:未公開
  1:公開予定
  2:公開済
  3:削除予定
  4:削除済'
	, started_at DATETIME not null comment '掲載開始日'
	, expired_at DATETIME not null comment '掲載終了日'
	, created_at DATETIME not null comment '作成日時'
	, updated_at DATETIME not null comment '更新日時'
	, constraint info_PKC primary key (id)
  ) comment 'お知らせ' AUTO_INCREMENT=10001;
  
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('info');
	}

}
