<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGmsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //メールのカラム追加
        Schema::table('gms_user', function (Blueprint $table) {
            $table->string('email',256)->after('name')
                ->nullable(false)->comment("メールアドレス");
            //パスワードの許容文字数変更
            $table->string('password',256)->change();
            //なんでPKにAUTOINCREMENTないの？
            $table->increments('id')->change();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gms_user', function (Blueprint $table) {
            $table->dropColumn('email');
        });
        Schema::table('gms_user', function (Blueprint $table) {
            $table->string('password',16)->change();
        });
    }
}
