<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableGmsUser extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table gms_user (
	id INT not null comment '管理者ID'
	, name VARCHAR(64) not null comment '管理者名'
	, password VARCHAR(16) not null comment 'パスワード'
	, manage_user_auth TINYINT default 0 not null comment 'ユーザー管理:0:権限なし
  1:閲覧可
  2:編集可'
	, edit_player_auth TINYINT default 0 not null comment 'プレイヤー編集'
	, edit_master_auth TINYINT default 0 not null comment 'マスター管理'
	, edit_page_auth TINYINT default 0 not null comment 'ページ編集'
	, maintenance_auth TINYINT default 0 not null comment 'メンテナンス権限'
	, present_auth TINYINT default 0 not null comment '補填権限'
	, debug_auth TINYINT default 0 not null comment 'デバッグ権限'
	, constraint gms_user_PKC primary key (id)
  ) comment '管理者' ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gms_user');
	}

}
