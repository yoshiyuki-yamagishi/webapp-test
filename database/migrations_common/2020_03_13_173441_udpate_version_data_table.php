<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UdpateVersionDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('version_data', function (Blueprint $table) {
            // app_version変更
            $table->string('app_version')->default("[1.0.0]")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('version_data', function (Blueprint $table) {
            // app_version変更
            DB::statement('ALTER TABLE version_data MODIFY COLUMN app_version double(8,2) default 1.00 comment "アプリバージョン";');
        });
    }
}
