<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id')->comment('お知らせID');
            $table->unsignedTinyInteger('info_type')
                ->default(0)->comment('お知らせ種別:1:お知らせ\n  2:メンテナンス\n  3:アップデート\n  4:不具合報告');
            $table->unsignedTinyInteger('platform_flag')
                ->default(0)->comment('表示プラットフォーム：BitFlag管理:001-iOS,010-Android,100-公式');
            $table->string('title',1024)->comment('タイトル');
            $table->text('content')->comment('本文');
            $table->string('image_name', 255)->comment('使用画像名');
            $table->integer('priority')->unsigned()->default(1)->comment('優先度');
            $table->dateTime('started_at')->comment('掲載開始日時');
            $table->dateTime('expired_at')->comment('掲載終了日時');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            // index
            $table->index('info_type');

            $table->engine = 'InnoDB';
            $table->collation = 'utf8mb4_bin';
        });

        // テーブルにコメント追加
        DB::statement("ALTER TABLE information COMMENT 'お知らせ'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}
