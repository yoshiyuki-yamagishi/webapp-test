<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableMaintenance extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table maintenance (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'メンテナンスID'
  , info_id VARCHAR(64) not null comment '情報ID'
  , message VARCHAR(512) not null comment 'メッセージ'
  , start_at DATETIME not null comment '開始時刻'
  , end_at DATETIME not null comment '終了時刻'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint maintenance_PKC primary key (id)
) comment 'メンテナンス' AUTO_INCREMENT=10001;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maintenance');
	}

}
