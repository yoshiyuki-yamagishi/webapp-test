<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerCommon extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_common (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ共通ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_disp_id VARCHAR(16) not null comment '表示用プレイヤID'
  , unique_id VARCHAR(255) not null comment '端末固有ID'
  , os_type TINYINT UNSIGNED not null comment 'OS種別'
  , os_version VARCHAR(128) not null comment 'OSバージョン'
  , model_name VARCHAR(128) not null comment '機種名'
  , db_no INT UNSIGNED not null comment 'データベースNO'
  , account_token VARCHAR(512) comment 'アカウントトークン'
  , passcode VARCHAR(64) comment '引継ぎコード'
  , password VARCHAR(128) comment 'パスワード'
  , valid_flag TINYINT UNSIGNED not null comment '有効フラグ:0:無効
1:有効'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_common_PKC primary key (id)
) comment 'プレイヤ共通' AUTO_INCREMENT=10001;

create unique index player_common_IX1
  on player_common(player_id);

create unique index player_common_IX2
  on player_common(player_disp_id);

alter table player_common add unique player_common_IX3 (passcode,password) ;

alter table player_common add unique player_common_IX4 (os_type,account_token) ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_common');
	}

}
