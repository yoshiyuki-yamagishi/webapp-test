<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGachaCeilingSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gacha_ceiling_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gacha_id')->unsigned()->comment('ガチャID');
            $table->integer('ceiling_id')->unsigned()->comment('天井ID');
            $table->dateTime('started_at')->comment('交換開始時刻');
            $table->dateTime('end_at')->comment('交換終了時刻');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        Schema::table('gacha_ceiling_schedule', function (Blueprint $table) {
            $table->index(['gacha_id','ceiling_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gacha_ceiling_schedule');
    }
}
