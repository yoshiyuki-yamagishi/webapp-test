<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVersionDataTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('version_data', function (Blueprint $table) {
            // assetbundle_version追加
            $table->float('ab_version')->after('master_version')
                ->unsigned()->default(1.00)->comment('アセットバンドルバージョン');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('version_data', function (Blueprint $table) {
            // assetbundle_version削除
            $table->dropColumn('ab_version');
        });
    }
}
