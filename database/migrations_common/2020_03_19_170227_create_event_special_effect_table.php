<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSpecialEffectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_special_effect', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned()->comment('イベント→event_questのid');
            $table->integer('target_type')->unsigned()->comment('キャラ=>1 魔導書=>2');
            $table->integer('target_id')->unsigned()->comment('キャラもしくは魔導書のID');
            $table->integer('effect_type')->unsigned()->comment('ボーナスタイプ 1=>ドロップ');
            $table->integer('effect_value')->unsigned()->comment('倍率値(千分率) 1000で2倍になる（元々の分＋１倍になるから）');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->collation = 'utf8mb4_bin';
        });

        Schema::table('event_special_effect', function (Blueprint $table) {
            $table->index(['event_id','effect_type']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_special_effect');
    }
}
