<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestBonusSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quest_bonus_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chapter_id')->unsigned()->comment('チャプターID ストーリー=>story_quest_chapterのid, キャラクター→character_quest_chapterのid, イベント→event_questのid');
            $table->integer('quest_category')->unsigned()->comment('クエストカテゴリー ストーリー=>1 キャラ=>2 イベント=>3');
            $table->integer('limit')->unsigned()->comment('リミット 0で無限');
            $table->integer('bonus_type')->unsigned()->comment('倍率タイプ 1=>経験値 2=>お金 3=>ドロップ 4=>キャラ経験値');
            $table->integer('bonus_value')->unsigned()->comment('倍率値(千分率) 1000で2倍になる（元々の分＋１倍になるから）');
            $table->dateTime('start_date')->comment('開始日時');
            $table->dateTime('end_date')->comment('終了日時');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->collation = 'utf8mb4_bin';

        });

        Schema::table('quest_bonus_setting', function (Blueprint $table) {
            $table->index(['chapter_id','quest_category','bonus_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quest_bonus_setting');
    }
}
