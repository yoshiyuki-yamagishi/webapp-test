<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('version_data', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->integer('platform')->unsigned()->default(1)->comment('プラットフォーム 1->iOS 2->Android');
            $table->float('app_version')->unsigned()->default(1.00)->comment('アプリバージョン');
            $table->float('master_version')->unsigned()->default(1.00)->comment('マスターバージョン');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->index('platform');

            $table->engine = 'InnoDB';
            $table->collation = 'utf8mb4_bin';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('version_data');
    }
}
