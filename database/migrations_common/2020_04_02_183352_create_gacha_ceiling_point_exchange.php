<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGachaCeilingPointExchange extends Migration
{
    /**
     * Run the migrations.
     *
     */

    public function up()
    {
        Schema::create('gacha_ceiling_point_exchange', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ceiling_id')->unsigned()->comment('天井ID');
            $table->integer('exchange_id')->unsigned()->comment('交換アイテム識別番号');
            $table->integer('item_type')->unsigned()->comment('交換アイテムタイプ');
            $table->integer('item_id')->unsigned()->comment('交換アイテムID');
            $table->integer('item_count')->unsigned()->comment('交換アイテム数');
            $table->integer('ceiling_count')->unsigned()->comment('天井の値');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        Schema::table('gacha_ceiling_point_exchange', function (Blueprint $table) {
            $table->index(['exchange_id','ceiling_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gacha_ceiling_point_exchange');
    }
}
