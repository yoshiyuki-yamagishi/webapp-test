<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableMissionEndLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table mission_end_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , mission_id INT UNSIGNED not null comment 'ミッションID'
  , mission_type TINYINT UNSIGNED not null comment 'ミッション種類'
  , progress_count INT UNSIGNED default 0 not null comment '進捗数'
  , count INT UNSIGNED not null comment '進捗数上限'
  , got_at DATETIME not null comment '取得日時'
) comment 'ミッション終了ログ' ;

create index mission_end_log_IX1
  on mission_end_log(player_id,mission_id);
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mission_end_log');
	}

}
