<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerGrimoireSnapshot extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_grimoire_snapshot (
  snapshot_id BIGINT UNSIGNED not null comment 'スナップショットID'
  , player_grimoire_id INT UNSIGNED not null comment 'プレイヤ魔道書ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , grimoire_id INT UNSIGNED not null comment '魔道書ID'
  , awake TINYINT UNSIGNED not null comment '覚醒数'
  , slot_1 BIGINT UNSIGNED comment 'スロット1'
  , slot_2 BIGINT UNSIGNED comment 'スロット2'
  , slot_3 BIGINT UNSIGNED comment 'スロット3'
  , constraint player_grimoire_snapshot_PKC primary key (snapshot_id,player_id)
) comment 'プレイヤ魔道書スナップショット' ;

alter table player_grimoire_snapshot add unique player_grimoire_snapshot_IX1 (snapshot_id,player_grimoire_id) ;

alter table player_grimoire_snapshot add unique player_grimoire_snapshot_IX2 (player_id,grimoire_id) ;

create unique index player_grimoire_snapshot_IX3
  on player_grimoire_snapshot(grimoire_id);

create unique index player_grimoire_snapshot_IX4
  on player_grimoire_snapshot(player_grimoire_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_grimoire_snapshot');
	}

}
