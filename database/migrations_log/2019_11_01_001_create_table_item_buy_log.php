<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableItemBuyLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table item_buy_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , shop_category INT UNSIGNED not null comment 'ショップカテゴリ'
  , shop_id INT UNSIGNED not null comment 'ショップID'
  , shop_exchange_id INT UNSIGNED not null comment 'ショップ交換所ID'
  , pay_item_contents_id INT UNSIGNED comment '消費アイテムID'
  , pay_item_count INT UNSIGNED comment '消費アイテム個数'
  , got_at DATETIME comment '取得日時'
) comment 'アイテム購入ログ' ;

create index item_buy_log_IX1
  on item_buy_log(player_id,shop_exchange_id,shop_id);

create index item_buy_log_IX2
  on item_buy_log(pay_item_contents_id,pay_item_count);
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_buy_log');
	}

}
