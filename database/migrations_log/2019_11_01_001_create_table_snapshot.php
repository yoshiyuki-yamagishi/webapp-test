<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableSnapshot extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table snapshot (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'ID'
  , save_at DATETIME comment '保存日時'
  , constraint snapshot_PKC primary key (id)
) comment 'スナップショット' ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('snapshot');
	}

}
