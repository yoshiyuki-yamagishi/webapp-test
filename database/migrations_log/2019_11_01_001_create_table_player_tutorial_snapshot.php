<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerTutorialSnapshot extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_tutorial_snapshot (
  snapshot_id BIGINT UNSIGNED not null comment 'スナップショットID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , tutorial_progress TINYINT UNSIGNED not null comment 'チュートリアル進行度'
  , constraint player_tutorial_snapshot_PKC primary key (snapshot_id,player_id)
) comment 'プレイヤチュートリアルスナップショット' ;

create index player_tutorial_snapshot_IX1
  on player_tutorial_snapshot(tutorial_progress);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_tutorial_snapshot');
	}

}
