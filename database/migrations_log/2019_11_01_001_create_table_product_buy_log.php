<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableProductBuyLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table product_buy_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , store_id INT UNSIGNED not null comment '購入元ID'
  , product_id INT UNSIGNED not null comment '製品ID'
  , num INT UNSIGNED not null comment '購入数'
  , currency VARCHAR(16) comment '通貨単位'
  , amount INT UNSIGNED comment '支払額'
  , got_at DATETIME comment '取得日時'
) comment '課金情報ログ' ;

create index product_buy_log_IX1
  on product_buy_log(player_id);

create index product_buy_log_IX2
  on product_buy_log(product_id);

create index product_buy_log_IX3
  on product_buy_log(store_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_buy_log');
	}

}
