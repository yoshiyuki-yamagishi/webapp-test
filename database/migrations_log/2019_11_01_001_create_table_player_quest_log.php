<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerQuestLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<QUERY
create table player_quest_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV:1:お知らせ
2:メンテナンス
3:アップデート
4:不具合報告'
  , player_battle_id BIGINT UNSIGNED not null comment 'プレイヤバトルID'
  , quest_id INT UNSIGNED not null comment 'クエストID'
  , continue_count TINYINT UNSIGNED not null comment 'コンティニュー数'
  , result INT UNSIGNED default 0 not null comment '結果:0: バトル継続中
1: 敗北し、終了
2: (未使用)
3: 勝利
4: スキップ'
  , mission_id_1 INT UNSIGNED not null comment 'ミッションID1'
  , mission_id_2 INT UNSIGNED not null comment 'ミッションID2'
  , mission_id_3 INT UNSIGNED not null comment 'ミッションID3'
  , mission_flag_1 TINYINT UNSIGNED default 0 not null comment 'ミッションフラグ1:0:未クリア1:クリア済み'
  , mission_flag_2 TINYINT UNSIGNED default 0 not null comment 'ミッションフラグ2:0:未クリア1:クリア済み'
  , mission_flag_3 TINYINT UNSIGNED default 0 not null comment 'ミッションフラグ3:0:未クリア1:クリア済み'
  , total_damage_1 INT UNSIGNED default 0 not null comment '総ダメージ1'
  , total_damage_2 INT UNSIGNED default 0 not null comment '総ダメージ2'
  , total_damage_3 INT UNSIGNED default 0 not null comment '総ダメージ3'
  , tatal_receive_damage_1 INT UNSIGNED default 0 not null comment '総被ダメージ1'
  , total_receive_damage_2 INT UNSIGNED default 0 not null comment '総被ダメージ2'
  , total_receive_damage_3 INT UNSIGNED default 0 not null comment '総被ダメージ3'
  , turn_count_1 INT UNSIGNED default 0 not null comment 'ターン数1:WAVE1の経過ターン数'
  , turn_count_2 INT UNSIGNED default 0 not null comment 'ターン数2:WAVE2の経過ターン数'
  , turn_count_3 INT UNSIGNED default 0 not null comment 'ターン数3:WAVE3の経過ターン数'
  , got_at DATETIME not null comment '取得日時:ログ取得日時'
  , constraint player_quest_log_PKC primary key (player_id,player_battle_id)
) comment 'プレイヤクエストログ' ;

create index player_quest_log_IX1
  on player_quest_log(quest_id);
QUERY;

        SqlUtil::execRawSqls($query);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('player_quest_log');
    }

}
