<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerSnapshot extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_snapshot (
  snapshot_id BIGINT UNSIGNED not null comment 'スナップショットID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , experience BIGINT UNSIGNED not null comment '経験値'
  , tutorial_progress TINYINT UNSIGNED not null comment 'チュートリアル進行度'
  , platinum_dollar BIGINT UNSIGNED not null comment 'P$'
  , powder BIGINT UNSIGNED not null comment '欠片パウダー'
  , magic_num BIGINT UNSIGNED not null comment '所持魔素数'
  , friend_point BIGINT UNSIGNED not null comment 'フレンドポイント'
  , max_grimoire INT UNSIGNED not null comment '最大魔道書枠'
  , favorite_character BIGINT UNSIGNED not null comment 'お気に入りキャラクタ'
  , login_days INT UNSIGNED default 1 not null comment 'ログイン日数'
  , constraint player_snapshot_PKC primary key (snapshot_id,player_id)
) comment 'プレイヤスナップショット' ;

create index player_snapshot_IX1
  on player_snapshot(player_lv);

create unique index player_snapshot_IX2
  on player_snapshot(player_id,platinum_dollar);


QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_snapshot');
	}

}
