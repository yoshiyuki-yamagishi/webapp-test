<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerQuestGrimoireLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_quest_grimoire_log (
  player_battle_id BIGINT UNSIGNED not null comment 'プレイヤバトルID'
  , player_grimoire_id BIGINT UNSIGNED not null comment 'プレイヤ魔導書ID'
  , position TINYINT UNSIGNED comment 'ポジション'
  , grimoire_id INT UNSIGNED not null comment '魔導書ID'
  , awake TINYINT UNSIGNED comment '覚醒数'
  , slot_1 BIGINT UNSIGNED comment 'スロット1'
  , slot_2 BIGINT UNSIGNED comment 'スロット2'
  , slot_3 BIGINT UNSIGNED comment 'スロット3'
  , got_at DATETIME comment '取得日時'
  , constraint player_quest_grimoire_log_PKC primary key (player_battle_id,player_grimoire_id)
) comment 'プレイヤクエスト魔導書ログ' ;

create index player_quest_grimoire_log_IX1
  on player_quest_grimoire_log(grimoire_id);
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_quest_grimoire_log');
	}

}
