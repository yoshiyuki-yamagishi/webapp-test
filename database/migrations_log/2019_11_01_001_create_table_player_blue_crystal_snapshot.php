<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBlueCrystalSnapshot extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_blue_crystal_snapshot (
  snapshot_id BIGINT UNSIGNED not null comment 'スナップショットID'
  , player_blue_crystal_id BIGINT UNSIGNED not null comment 'プレイヤ蒼の結晶ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , payment_type INT UNSIGNED not null comment '決済種別:0:無料
1:GooglePay
2:AppleStore'
  , src_type TINYINT UNSIGNED default 0 not null comment '取得元種別'
  , src_id BIGINT UNSIGNED default 0 not null comment '取得元ID'
  , payment_num INT UNSIGNED not null comment '購入数'
  , num INT UNSIGNED not null comment '残り数'
  , delete_flag TINYINT UNSIGNED not null comment '削除フラグ'
  , expired_at DATETIME default '9999/01/01 00:00:00' not null comment '有効期限'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_blue_crystal_snapshot_PKC primary key (snapshot_id,player_blue_crystal_id)
) comment 'プレイヤ蒼の結晶スナップショット' ;

alter table player_blue_crystal_snapshot add unique player_blue_crystal_snapshot_IX1 (player_id,player_blue_crystal_id) ;

create index player_blue_crystal_snapshot_IX2
  on player_blue_crystal_snapshot(player_id);

create index player_blue_crystal_snapshot_IX3
  on player_blue_crystal_snapshot(payment_type);

create unique index player_blue_crystal_snapshot_IX4
  on player_blue_crystal_snapshot(src_type,src_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_blue_crystal_snapshot');
	}

}
