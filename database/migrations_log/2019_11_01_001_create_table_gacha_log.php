<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableGachaLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table gacha_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , player_gacha_id BIGINT UNSIGNED not null comment 'プレイヤガチャID'
  , gacha_id INT UNSIGNED not null comment 'ガチャID'
  , gacha_count TINYINT UNSIGNED not null comment 'ガチャ回数'
  , loop_count INT UNSIGNED default 0 not null comment '引き直し回数'
  , got_at DATETIME not null comment '取得日時'
  , constraint gacha_log_PKC primary key (player_id,player_gacha_id)
) comment 'ガチャログ' ;


create index gacha_log_IX1
  on gacha_log(gacha_id);
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gacha_log');
	}

}
