<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerQuestCharacterLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_quest_character_log (
  player_battle_id INT UNSIGNED not null comment 'プレイヤバトルID'
  , player_character_id INT UNSIGNED not null comment 'プレイヤキャラクタID'
  , position TINYINT UNSIGNED not null comment 'ポジション'
  , character_id INT UNSIGNED not null comment 'キャラクタID'
  , evolve TINYINT UNSIGNED default 0 not null comment '進化度'
  , grade TINYINT UNSIGNED default 1 not null comment 'グレード'
  , character_lv INT UNSIGNED not null comment 'キャラクタLV'
  , orb_id_1 INT UNSIGNED comment 'オーブID1'
  , orb_id_2 INT UNSIGNED comment 'オーブID2'
  , orb_id_3 INT UNSIGNED comment 'オーブID3'
  , orb_id_4 INT UNSIGNED comment 'オーブID4'
  , orb_id_5 INT UNSIGNED comment 'オーブID5'
  , orb_id_6 INT UNSIGNED comment 'オーブID6'
  , active_skill_1_lv TINYINT UNSIGNED comment 'アクティブスキル1LV'
  , active_skill_2_lv TINYINT UNSIGNED comment 'アクティブスキル2LV'
  , got_at DATETIME comment '取得日時'
  , constraint player_quest_character_log_PKC primary key (player_battle_id,player_character_id)
) comment 'プレイヤクエストキャラクターログ' ;

create index player_quest_character_log_IX1
  on player_quest_character_log(character_id);
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_quest_character_log');
	}

}
