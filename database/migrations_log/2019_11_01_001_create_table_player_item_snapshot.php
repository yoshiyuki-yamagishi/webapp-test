<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerItemSnapshot extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_item_snapshot (
  snapshot_id BIGINT UNSIGNED not null comment 'スナップショットID'
  , player_item_id BIGINT UNSIGNED not null comment 'プレイヤアイテムID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , num INT UNSIGNED not null comment '所持数'
  , constraint player_item_snapshot_PKC primary key (snapshot_id,player_id)
) comment 'プレイヤアイテムスナップショット' ;

create unique index player_item_snapshot_IX1
  on player_item_snapshot(player_id,item_id);

alter table player_item_snapshot add unique player_item_snapshot_IX2 (snapshot_id,player_item_id) ;

create index player_item_snapshot_IX3
  on player_item_snapshot(item_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_item_snapshot');
	}

}
