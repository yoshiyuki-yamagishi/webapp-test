<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableTutorialLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table tutorial_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , tutorial_progress TINYINT UNSIGNED not null comment 'チュートリアル進行度'
  , got_at DATETIME not null comment '取得日時'
  , constraint tutorial_log_PKC primary key (player_id)
) comment 'チュートリアルログ' ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tutorial_log');
	}

}
