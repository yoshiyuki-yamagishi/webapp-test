<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableLoginLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table login_log (
  player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , got_at DATETIME not null comment '取得日時'
) comment 'ログインログ' ;

create index login_log_IX1
  on login_log(player_id);
QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('login_log');
	}

}
