<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableGachaItemLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table gacha_item_log (
  player_gacha_id BIGINT UNSIGNED not null comment 'プレイヤガチャID'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , item_count INT UNSIGNED not null comment 'アイテム数'
  , gacha_result_flag TINYINT UNSIGNED not null comment 'ガチャ結果フラグ'
) comment 'ガチャアイテムログ' ;

create index gacha_item_log_IX1
  on gacha_item_log(player_gacha_id);

create index gacha_item_log_IX2
  on gacha_item_log(item_type);

create index gacha_item_log_IX3
  on gacha_item_log(item_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gacha_item_log');
	}

}
