<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCountPlayerLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_login', function (Blueprint $table) {
            $table->integer('count')->after('player_id')
                ->unsigned()->default(1)->comment("ログイン回数");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_login', function (Blueprint $table) {
            $table->dropColumn('count');
        });
    }
}
