<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerShopItem extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_shop_item (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤショップアイテムID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , shop_category INT UNSIGNED not null comment 'ショップカテゴリ'
  , shop_id INT UNSIGNED not null comment 'ショップID'
  , shop_exchange_id INT UNSIGNED not null comment 'ショップ交換所ID'
  , priority INT UNSIGNED not null comment '優先度'
  , buy_count INT UNSIGNED default 0 not null comment '購入回数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_shop_item_PKC primary key (id)
) comment 'プレイヤショップアイテム' AUTO_INCREMENT=10001;

create index player_shop_item_IX1
  on player_shop_item(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_shop_item');
	}

}
