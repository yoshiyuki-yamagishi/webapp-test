<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBattleResult extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_battle_result (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤバトル結果ID'
  , player_battle_id BIGINT UNSIGNED not null comment 'プレイヤバトルID'
  , result TINYINT UNSIGNED not null comment '結果:1: 敗北し、終了
2: 敗北したが、コンティニュー
3: 勝利
4: (未使用)'
  , dead_flags INT UNSIGNED default 0 not null comment '死亡フラグ'
  , used_skill_count INT UNSIGNED default 0 not null comment 'アクティブスキル使用回数'
  , used_od_count INT UNSIGNED default 0 not null comment 'OD 使用数'
  , used_dd_count INT UNSIGNED default 0 not null comment 'DD 使用数'
  , used_ah_count INT UNSIGNED default 0 not null comment 'AH 使用数'
  , max_combo_count INT UNSIGNED default 0 not null comment '最大コンボ数'
  , max_chain_count INT UNSIGNED default 0 not null comment '最大チェイン数'
  , finish_character_no TINYINT UNSIGNED default 0 not null comment 'フィニッシュ位置番号'
  , finish_skill_type INT UNSIGNED default 0 not null comment 'フィニッシュ技種別'
  , turn_count_1 INT UNSIGNED default 0 not null comment 'ターン数1'
  , total_damage_1 INT UNSIGNED default 0 not null comment '総ダメージ1'
  , total_receive_damage_1 INT UNSIGNED default 0 not null comment '総被ダメージ1'
  , turn_count_2 INT UNSIGNED default 0 not null comment 'ターン数2'
  , total_damage_2 INT UNSIGNED default 0 not null comment '総ダメージ2'
  , total_receive_damage_2 INT UNSIGNED default 0 not null comment '総被ダメージ2'
  , turn_count_3 INT UNSIGNED default 0 not null comment 'ターン数3'
  , total_damage_3 INT UNSIGNED default 0 not null comment '総ダメージ2'
  , total_receive_damage_3 INT UNSIGNED default 0 not null comment '総被ダメージ3'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_battle_result_PKC primary key (id)
) comment 'プレイヤバトル結果' AUTO_INCREMENT=10001;

create index player_battle_result_IX1
  on player_battle_result(player_battle_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_battle_result');
	}

}
