<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBattleReward extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_battle_reward (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤバトル報酬ID'
  , player_battle_id BIGINT UNSIGNED not null comment 'プレイヤバトルID'
  , skip_no INT UNSIGNED default 0 not null comment 'スキップ番号:0: スキップチケット未使用'
  , reward_type TINYINT UNSIGNED not null comment '報酬種別:1: 初回
2: 固定
3: ドロップ'
  , frame_count TINYINT UNSIGNED not null comment '報酬枠番号'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別'
  , item_id INT UNSIGNED not null comment 'アイテムID:報酬で手に入るアイテムID'
  , item_num INT UNSIGNED not null comment 'アイテム個数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_battle_reward_PKC primary key (id)
) comment 'プレイヤバトル報酬' AUTO_INCREMENT=10001;

create index player_battle_reward_IX1
  on player_battle_reward(player_battle_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_battle_reward');
	}

}
