<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerGachaCeilingPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_gacha_ceiling_point', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('player_id')->comment('ユーザーID');
            $table->integer('ceiling_id')->unsigned()->comment('天井ID');
            $table->integer('gacha_point')->unsigned()->comment('ガチャポイント');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
        Schema::table('player_gacha_ceiling_point', function (Blueprint $table) {
            $table->index(['player_id','ceiling_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_gacha_ceiling_point');
    }
}
