<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerEventStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_event_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->unsigned()->comment('ユーザーID');
            $table->integer('event_id')->unsigned()->comment('イベントID event_questのid');
            $table->integer('point')->unsigned()->comment('イベントのランキング用ポイント');
            $table->integer('clear_percent')->unsigned()->comment('達成率');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        Schema::table('player_event_status', function (Blueprint $table) {
            $table->index(['player_id','event_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_event_status');
    }
}
