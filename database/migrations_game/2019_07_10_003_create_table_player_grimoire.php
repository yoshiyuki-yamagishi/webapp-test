<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerGrimoire extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_grimoire (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ魔道書ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , grimoire_id INT UNSIGNED not null comment '魔道書ID'
  , awake TINYINT UNSIGNED default 0 not null comment '覚醒数'
  , slot_1 BIGINT UNSIGNED comment 'スロット1'
  , slot_1_flag TINYINT UNSIGNED default 0 not null comment 'スロット1解放フラグ:0:未解放
1:解放済み'
  , slot_2 BIGINT UNSIGNED comment 'スロット2'
  , slot_2_flag TINYINT UNSIGNED default 0 not null comment 'スロット2解放フラグ:0:未解放
1:解放済み'
  , slot_3 BIGINT UNSIGNED comment 'スロット3'
  , slot_3_flag TINYINT UNSIGNED default 0 not null comment 'スロット3解放フラグ:0:未解放
1:解放済み'
  , lock_flag TINYINT UNSIGNED default 0 not null comment 'ロックフラグ:0:アンロック
1:ロック'
  , delete_flag TINYINT UNSIGNED default 0 not null comment '削除フラグ:使用できなくなったら1になる想定(売却、覚醒等)'
  , acquired_at DATETIME not null comment '入手日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_grimoire_PKC primary key (id)
) comment 'プレイヤ魔道書' AUTO_INCREMENT=10001;

create index player_grimoire_IX1
  on player_grimoire(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_grimoire');
	}

}
