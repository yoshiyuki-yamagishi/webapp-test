<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerItemBuy extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_item_buy (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤアイテム購入ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , shop_category INT UNSIGNED not null comment 'ショップカテゴリ'
  , shop_id INT UNSIGNED not null comment 'ショップID'
  , shop_exchange_id INT UNSIGNED not null comment 'ショップ交換所ID'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , get_count INT UNSIGNED not null comment '獲得個数'
  , pay_item_contents_id INT UNSIGNED not null comment '消費アイテムID'
  , pay_item_count INT UNSIGNED not null comment '消費アイテム個数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_item_buy_PKC primary key (id)
) comment 'プレイヤアイテム購入' AUTO_INCREMENT=10001;

create index player_item_buy_IX1
  on player_item_buy(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_item_buy');
	}

}
