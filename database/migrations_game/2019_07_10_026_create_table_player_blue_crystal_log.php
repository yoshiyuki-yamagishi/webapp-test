<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBlueCrystalLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_blue_crystal_log (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ蒼の結晶ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_blue_crystal_id BIGINT UNSIGNED not null comment 'プレイヤ蒼の結晶ID'
  , type TINYINT UNSIGNED not null comment '種別'
  , value1 BIGINT UNSIGNED comment '値1'
  , value2 BIGINT UNSIGNED comment '値2'
  , value3 BIGINT UNSIGNED comment '値3'
  , src_type TINYINT UNSIGNED default 0 not null comment '取得元種別'
  , src_id BIGINT UNSIGNED default 0 not null comment '取得元ID'
  , called_at DATETIME not null comment '処理日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_blue_crystal_log_PKC primary key (id)
) comment 'プレイヤ蒼の結晶ログ' AUTO_INCREMENT=10001;

create index player_blue_crystal_log_IX1
  on player_blue_crystal_log(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_blue_crystal_log');
	}

}
