<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddIndexPlayerMissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_mission', function (Blueprint $table) {
            //
            $table->index(['player_id','mission_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_mission', function (Blueprint $table) {
            //
            $table->dropIndex(['player_id','mission_id']);
        });
    }
}
