<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerCharacter extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_character (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤキャラクタID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , character_id INT UNSIGNED not null comment 'キャラクタID'
  , spine TINYINT UNSIGNED default 0 not null comment 'スパイン'
  , evolve TINYINT UNSIGNED default 0 not null comment '進化度'
  , grade TINYINT UNSIGNED default 1 not null comment 'グレード'
  , character_lv INT UNSIGNED not null comment 'キャラクタLV'
  , experience BIGINT UNSIGNED not null comment '経験値'
  , player_orb_id_1 BIGINT UNSIGNED comment 'プレイヤオーブID1'
  , player_orb_id_2 BIGINT UNSIGNED comment 'プレイヤオーブID2'
  , player_orb_id_3 BIGINT UNSIGNED comment 'プレイヤオーブID3'
  , player_orb_id_4 BIGINT UNSIGNED comment 'プレイヤオーブID4'
  , player_orb_id_5 BIGINT UNSIGNED comment 'プレイヤオーブID5'
  , player_orb_id_6 BIGINT UNSIGNED comment 'プレイヤオーブID6'
  , active_skill_1_lv TINYINT UNSIGNED comment 'アクティブスキル1LV'
  , active_skill_2_lv TINYINT UNSIGNED comment 'アクティブスキル2LV'
  , acquired_at DATETIME not null comment '入手日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_character_PKC primary key (id)
) comment 'プレイヤキャラクタ' AUTO_INCREMENT=10001;

create index player_character_IX1
  on player_character(player_id);

create unique index player_character_IX2
  on player_character(player_id,character_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_character');
	}

}
