<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIllustNumberToPlayerCharacter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_character', function (Blueprint $table) {
            // カード画像指定
            $table->tinyInteger('illust_number')->after('character_id')
                ->unsigned()->default(0)->comment("カード画像指定番号");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_character', function (Blueprint $table) {
            // 報酬タイプ削除
            $table->dropColumn('illust_number');
        });
    }
}
