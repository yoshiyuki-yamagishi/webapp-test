<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerPresent extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_present (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤプレゼントID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別:1:キャラクタ
2:魔道書
3:アイテム'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , item_num INT UNSIGNED not null comment 'アイテム個数'
  , src_type TINYINT UNSIGNED not null comment '取得元種別:1:ログインボーナス
2:クエスト報酬
3:ミッション報酬
4:ガチャ報酬
5:プレゼント機能（管理ツール）'
  , src_id BIGINT UNSIGNED not null comment '取得元ID'
  , message VARCHAR(512) not null comment 'メッセージ'
  , take_flag TINYINT UNSIGNED not null comment '受け取りフラグ:0:未受け取り
1:受け取り済み'
  , taked_at DATETIME comment '受取日時'
  , expired_at DATETIME comment '受取期限'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_present_PKC primary key (id)
) comment 'プレイヤプレゼント' AUTO_INCREMENT=10001;

create index player_present_IX1
  on player_present(player_id);

create index player_present_IX2
  on player_present(take_flag);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_present');
	}

}
