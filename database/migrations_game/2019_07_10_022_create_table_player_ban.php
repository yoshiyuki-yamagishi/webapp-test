<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBan extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_ban (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤバンID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , ban_status TINYINT UNSIGNED not null comment 'BANステータス:0:なし
1:警告
2:BAN'
  , message VARCHAR(512) not null comment '本文'
  , delete_flag TINYINT UNSIGNED default 0 not null comment '削除フラグ:0:なし
1:削除済'
  , start_at DATETIME not null comment '開始日時'
  , end_at DATETIME not null comment '終了日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_ban_PKC primary key (id)
) comment 'プレイヤバン' AUTO_INCREMENT=10001;

create index player_ban_IX1
  on player_ban(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_ban');
	}

}
