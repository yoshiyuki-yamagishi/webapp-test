<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerGacha extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_gacha (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤガチャID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , gacha_id INT UNSIGNED not null comment 'ガチャID'
  , gacha_count TINYINT UNSIGNED not null comment 'ガチャ回数'
  , gacha_loop TINYINT UNSIGNED not null comment '引き直し:1:あり
2:なし'
  , loop_count INT UNSIGNED default 0 not null comment '引き直し回数'
  , taked_at DATETIME comment '取得日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_gacha_PKC primary key (id)
) comment 'プレイヤガチャ' AUTO_INCREMENT=10001;

create index player_gacha_IX1
  on player_gacha(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_gacha');
	}

}
