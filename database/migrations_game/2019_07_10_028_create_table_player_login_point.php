<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerLoginPoint extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_login_point (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤログインポイントID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , type TINYINT UNSIGNED not null comment 'ポイント種別'
  , point INT UNSIGNED not null comment 'ポイント'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_login_point_PKC primary key (id)
) comment 'プレイヤログインポイント' AUTO_INCREMENT=10001;

create index player_login_point_IX1
  on player_login_point(player_id);

create index player_login_point_IX2
  on player_login_point(player_id,type);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_login_point');
	}

}
