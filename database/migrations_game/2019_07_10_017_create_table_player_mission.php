<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerMission extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_mission (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤミッションID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , mission_id INT UNSIGNED not null comment 'ミッションID'
  , mission_type TINYINT UNSIGNED not null comment 'ミッション種類'
  , progress_count INT UNSIGNED default 0 not null comment '進捗数'
  , count INT UNSIGNED not null comment '進捗数上限'
  , remuneration INT UNSIGNED default 0 not null comment 'ミッション報酬ID'
  , remuneration_count INT UNSIGNED default 0 not null comment 'ミッション報酬個数'
  , start_day DATETIME not null comment 'ミッション開始日'
  , end_day DATETIME not null comment 'ミッション終了日'
  , report_flag TINYINT UNSIGNED default 0 not null comment 'ミッション報告フラグ:0:未報告
1:報告済み'
  , cleared_at DATETIME comment 'ミッション達成日時'
  , taked_at DATETIME comment '報酬取得日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_mission_PKC primary key (id)
) comment 'プレイヤミッション' AUTO_INCREMENT=10001;

create index player_mission_IX1
  on player_mission(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_mission');
	}

}
