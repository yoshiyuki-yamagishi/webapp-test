<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBattleGrimoire extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_battle_grimoire (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤバトル魔道書ID'
  , player_battle_id BIGINT UNSIGNED not null comment 'プレイヤバトルID'
  , position TINYINT UNSIGNED not null comment 'ポジション'
  , grimoire_id INT UNSIGNED not null comment '魔道書ID'
  , awake TINYINT UNSIGNED default 0 not null comment '覚醒数'
  , slot_1 INT UNSIGNED comment 'スロット1'
  , slot_1_flag TINYINT UNSIGNED default 0 not null comment 'スロット1解放フラグ:0:未解放
1:解放済み'
  , slot_2 INT UNSIGNED comment 'スロット2'
  , slot_2_flag TINYINT UNSIGNED default 0 not null comment 'スロット2解放フラグ:0:未解放
1:解放済み'
  , slot_3 INT UNSIGNED comment 'スロット3'
  , slot_3_flag TINYINT UNSIGNED default 0 not null comment 'スロット3解放フラグ:0:未解放
1:解放済み'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_battle_grimoire_PKC primary key (id)
) comment 'プレイヤバトル魔道書' AUTO_INCREMENT=10001;

create index player_battle_grimoire_IX1
  on player_battle_grimoire(player_battle_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_battle_grimoire');
	}

}
