<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerProductBuyItem extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_product_buy_item (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ製品購入アイテムID'
  , player_product_buy_id BIGINT UNSIGNED not null comment 'プレイヤ製品購入ID'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , item_count INT UNSIGNED not null comment 'アイテム数'
  , grant_type TINYINT UNSIGNED not null comment '付与タイプ'
  , grant_progress INT UNSIGNED default 0 not null comment '受取り進捗数'
  , grant_count INT UNSIGNED default 0 not null comment '受取り数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_product_buy_item_PKC primary key (id)
) comment 'プレイヤ製品購入アイテム' AUTO_INCREMENT=10001;

create index player_product_buy_item_IX1
  on player_product_buy_item(player_product_buy_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_product_buy_item');
	}

}
