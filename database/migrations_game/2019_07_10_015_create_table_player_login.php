<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerLogin extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_login (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤログインID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_login_PKC primary key (id)
) comment 'プレイヤログイン' AUTO_INCREMENT=10001;

create index player_login_IX1
  on player_login(player_id);

create index player_login_IX2
  on player_login(created_at);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_login');
	}

}
