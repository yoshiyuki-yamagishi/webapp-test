<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBlueCrystal extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_blue_crystal (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ蒼の結晶ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , payment_type INT UNSIGNED not null comment '決済種別:0:無料
1:GooglePay
2:AppleStore'
  , src_type TINYINT UNSIGNED default 0 not null comment '取得元種別'
  , src_id BIGINT UNSIGNED default 0 not null comment '取得元ID'
  , payment_num INT UNSIGNED not null comment '購入数'
  , num INT UNSIGNED not null comment '残り数'
  , delete_flag TINYINT UNSIGNED default 0 not null comment '削除フラグ:使用できなくなったら1になる想定'
  , expired_at DATETIME default '9999/01/01 00:00:00' not null comment '有効期限'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_blue_crystal_PKC primary key (id)
) comment 'プレイヤ蒼の結晶' AUTO_INCREMENT=10001;

create index player_blue_crystal_IX1
  on player_blue_crystal(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_blue_crystal');
	}

}
