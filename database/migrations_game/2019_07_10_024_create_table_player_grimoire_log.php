<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerGrimoireLog extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_grimoire_log (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ魔道書ログID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_grimoire_id BIGINT UNSIGNED not null comment 'プレイヤ魔道書ID'
  , type TINYINT UNSIGNED not null comment '種別'
  , value1 BIGINT UNSIGNED comment '値1'
  , value2 BIGINT UNSIGNED comment '値2'
  , value3 BIGINT UNSIGNED comment '値3'
  , src_type TINYINT UNSIGNED default 0 not null comment '取得元種別'
  , src_id BIGINT UNSIGNED default 0 not null comment '取得元ID'
  , called_at DATETIME not null comment '処理日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_grimoire_log_PKC primary key (id)
) comment 'プレイヤ魔道書ログ' AUTO_INCREMENT=10001;

create index player_grimoire_log_IX1
  on player_grimoire_log(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_grimoire_log');
	}

}
