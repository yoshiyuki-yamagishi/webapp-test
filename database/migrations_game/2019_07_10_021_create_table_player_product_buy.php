<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerProductBuy extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_product_buy (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ製品購入ID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , store_id INT UNSIGNED not null comment '購入元ID'
  , payment_id VARCHAR(64) comment '製品購入ID'
  , product_id INT UNSIGNED not null comment '製品ID'
  , daily_flg TINYINT UNSIGNED not null comment 'デイリーフラグ'
  , valid_period INT UNSIGNED not null comment '有効期間'
  , num INT UNSIGNED not null comment '購入数'
  , currency VARCHAR(16) comment '通貨単位'
  , amount INT UNSIGNED comment '支払額'
  , state INT UNSIGNED default 0 not null comment '状態'
  , granted_at DATETIME comment '付与日付'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_product_buy_PKC primary key (id)
) comment 'プレイヤ製品購入' AUTO_INCREMENT=10001;

create index player_product_buy_IX1
  on player_product_buy(player_id);

alter table player_product_buy add unique player_product_buy_IX2 (store_id,payment_id) ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_product_buy');
	}

}
