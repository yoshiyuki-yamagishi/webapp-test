<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRemunerationTypePlayerQuestMission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_quest_mission', function (Blueprint $table) {
            // 報酬タイプ追加
            $table->integer('remuneration_type')->after('count')
                ->nullable(false)->unsigned()->comment("ミッション報酬タイプ");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_quest_mission', function (Blueprint $table) {
            // 報酬タイプ削除
            $table->dropColumn('remuneration_type');
        });
    }
}
