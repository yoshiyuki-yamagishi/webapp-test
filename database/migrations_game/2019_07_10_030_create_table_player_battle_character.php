<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBattleCharacter extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_battle_character (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤバトルキャラクタID'
  , player_battle_id BIGINT UNSIGNED not null comment 'プレイヤバトルID'
  , position TINYINT UNSIGNED not null comment 'ポジション'
  , character_id INT UNSIGNED not null comment 'キャラクタID'
  , spine TINYINT UNSIGNED default 0 not null comment 'スパイン'
  , evolve TINYINT UNSIGNED default 0 not null comment '進化度'
  , grade TINYINT UNSIGNED default 1 not null comment 'グレード'
  , character_lv INT UNSIGNED not null comment 'キャラクタLV'
  , orb_id_1 INT UNSIGNED comment 'オーブID1'
  , orb_id_2 INT UNSIGNED comment 'オーブID2'
  , orb_id_3 INT UNSIGNED comment 'オーブID3'
  , orb_id_4 INT UNSIGNED comment 'オーブID4'
  , orb_id_5 INT UNSIGNED comment 'オーブID5'
  , orb_id_6 INT UNSIGNED comment 'オーブID6'
  , active_skill_1_lv TINYINT UNSIGNED comment 'アクティブスキル1LV'
  , active_skill_2_lv TINYINT UNSIGNED comment 'アクティブスキル2LV'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_battle_character_PKC primary key (id)
) comment 'プレイヤバトルキャラクタ' AUTO_INCREMENT=10001;

create index player_battle_character_IX1
  on player_battle_character(player_battle_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_battle_character');
	}

}
