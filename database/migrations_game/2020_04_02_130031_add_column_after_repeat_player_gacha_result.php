<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAfterRepeatPlayerGachaResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_gacha_result', function (Blueprint $table) {
            // 変更後の重複ボーナス
            $table->tinyInteger('after_repeat')->after('take_flag')
                ->unsigned()->default(0)->comment("追加後の重複ボーナス");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_gacha_result', function (Blueprint $table) {
            $table->dropColumn('after_repeat');
        });
    }
}
