<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerItem extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_item (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤアイテムID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , num INT UNSIGNED not null comment '所持数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_item_PKC primary key (id)
) comment 'プレイヤアイテム' AUTO_INCREMENT=10001;

create index player_item_IX1
  on player_item(player_id);

alter table player_item add unique player_item_IX2 (player_id,item_id) ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_item');
	}

}
