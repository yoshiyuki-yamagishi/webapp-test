<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRepeatToPlayerCharacter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_character', function (Blueprint $table) {
            // 重複（リピート）ボーナス
            $table->tinyInteger('repeat')->after('evolve')
                ->unsigned()->default(0)->comment("重複ボーナス");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_character', function (Blueprint $table) {
            $table->dropColumn('repeat');
        });
    }
}
