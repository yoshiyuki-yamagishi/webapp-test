<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerParty extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_party (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤパーティID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , party_no TINYINT UNSIGNED not null comment 'パーティ番号'
  , player_character_id_1 BIGINT UNSIGNED comment 'プレイヤキャラクタID1'
  , player_grimoire_id_1 BIGINT UNSIGNED comment 'プレイヤ魔道書ID1'
  , player_character_id_2 BIGINT UNSIGNED comment 'プレイヤキャラクタID2'
  , player_grimoire_id_2 BIGINT UNSIGNED comment 'プレイヤ魔道書ID2'
  , player_character_id_3 BIGINT UNSIGNED comment 'プレイヤキャラクタID3'
  , player_grimoire_id_3 BIGINT UNSIGNED comment 'プレイヤ魔道書ID3'
  , player_character_id_4 BIGINT UNSIGNED comment 'プレイヤキャラクタID4'
  , player_grimoire_id_4 BIGINT UNSIGNED comment 'プレイヤ魔道書ID4'
  , player_character_id_5 BIGINT UNSIGNED comment 'プレイヤキャラクタID5'
  , player_grimoire_id_5 BIGINT UNSIGNED comment 'プレイヤ魔道書ID5'
  , player_character_id_6 BIGINT UNSIGNED comment 'プレイヤキャラクタID6'
  , player_grimoire_id_6 BIGINT UNSIGNED comment 'プレイヤ魔道書ID6'
  , player_character_id_7 BIGINT UNSIGNED comment 'プレイヤキャラクタID7'
  , player_grimoire_id_7 BIGINT UNSIGNED comment 'プレイヤ魔道書ID7'
  , player_character_id_8 BIGINT UNSIGNED comment 'プレイヤキャラクタID8'
  , player_grimoire_id_8 BIGINT UNSIGNED comment 'プレイヤ魔道書ID8'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_party_PKC primary key (id)
) comment 'プレイヤパーティ' AUTO_INCREMENT=10001;

create index player_party_IX1
  on player_party(player_id);

alter table player_party add unique player_party_IX2 (player_id,party_no) ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_party');
	}

}
