<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerBattle extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_battle (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤバトルID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , battle_code VARCHAR(64) not null comment 'バトルコード'
  , party_no TINYINT UNSIGNED default 0 not null comment 'パーティ番号'
  , quest_category TINYINT UNSIGNED not null comment 'クエスト種類:1: ストーリ
2: キャラクタ'
  , chapter_id INT UNSIGNED not null comment 'チャプタID'
  , quest_id INT UNSIGNED not null comment 'クエストID'
  , result INT UNSIGNED default 0 not null comment '結果:0: バトル継続中
1: 敗北し、終了
2: (未使用)
3: 勝利
4: スキップ'
  , repeat_count INT UNSIGNED default 1 not null comment '繰り返し回数'
  , continue_count INT UNSIGNED default 0 not null comment 'コンティニュー回数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_battle_PKC primary key (id)
) comment 'プレイヤバトル' AUTO_INCREMENT=10001;

create index player_battle_IX1
  on player_battle(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_battle');
	}

}
