<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerDirectPresent extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_direct_present (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ直接プレゼントID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , direct_present_id BIGINT UNSIGNED not null comment '直接プレゼントID'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_direct_present_PKC primary key (id)
) comment 'プレイヤ直接プレゼント' AUTO_INCREMENT=10001;

create index player_direct_present_IX1
  on player_direct_present(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_direct_present');
	}

}
