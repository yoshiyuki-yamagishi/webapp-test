<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayer extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player (
  id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_name VARCHAR(255) comment 'プレイヤ名'
  , player_lv INT UNSIGNED not null comment 'プレイヤLV'
  , experience BIGINT UNSIGNED not null comment '経験値'
  , birthday DATETIME comment '誕生日'
  , legal_birthday DATETIME comment '法律上の誕生日'
  , gender TINYINT UNSIGNED comment '性別:1:男性
2:女性'
  , tutorial_progress TINYINT UNSIGNED not null comment 'チュートリアル進行度'
  , tutorial_flag INT UNSIGNED default 0 not null comment 'チュートリアルフラグ'
  , platinum_dollar BIGINT UNSIGNED default 0 not null comment 'p$'
  , powder BIGINT UNSIGNED default 0 not null comment '欠片パウダー'
  , magic_num BIGINT UNSIGNED default 0 not null comment '所持魔素数'
  , friend_point BIGINT UNSIGNED default 0 not null comment 'フレンドポイント'
  , al INT UNSIGNED not null comment 'AL'
  , max_al INT UNSIGNED not null comment '最大AL'
  , al_recovery_at DATETIME not null comment 'AL回復日時'
  , max_grimoire INT UNSIGNED not null comment '最大魔道書枠'
  , favorite_character BIGINT UNSIGNED comment 'お気に入りキャラクタ'
  , message VARCHAR(512) not null comment 'メッセージ:プロフィールで表示するメッセージ'
  , login_days INT UNSIGNED default 1 not null comment 'ログイン日数'
  , first_login_at DATETIME not null comment '初ログイン日時'
  , last_login_at DATETIME not null comment '最終ログイン日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_PKC primary key (id)
) comment 'プレイヤ' ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player');
	}

}
