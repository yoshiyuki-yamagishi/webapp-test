<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerData extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_data (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤデータID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , type TINYINT UNSIGNED not null comment '種別'
  , value INT UNSIGNED not null comment '値'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_data_PKC primary key (id)
) comment 'プレイヤデータ' AUTO_INCREMENT=10001;

create index player_data_IX1
  on player_data(player_id);

alter table player_data add unique player_data_IX2 (player_id,type) ;

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_data');
	}

}
