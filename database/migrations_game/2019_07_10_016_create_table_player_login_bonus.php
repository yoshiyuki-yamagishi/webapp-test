<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerLoginBonus extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_login_bonus (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤログインボーナスID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , login_bonus_id INT UNSIGNED not null comment 'ログインボーナスID'
  , count INT UNSIGNED not null comment 'ログイン回数'
  , player_count INT UNSIGNED not null comment 'プレイヤログイン回数:ログインボーナス期間内のプレイヤのログイン回数
ログインボーナスが繰り返しもらう場合にログイン回数とプレイヤログイン回数からログインボーナスを取得済みか判断する'
  , remuneration INT UNSIGNED not null comment '報酬'
  , num INT UNSIGNED not null comment '報酬個数'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_login_bonus_PKC primary key (id)
) comment 'プレイヤログインボーナス' AUTO_INCREMENT=10001;

create index player_login_bonus_IX1
  on player_login_bonus(player_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_login_bonus');
	}

}
