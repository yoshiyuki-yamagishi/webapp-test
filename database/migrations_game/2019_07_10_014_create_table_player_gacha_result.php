<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerGachaResult extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_gacha_result (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤガチャ結果ID'
  , player_gacha_id BIGINT UNSIGNED not null comment 'プレイヤガチャID'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , item_count INT UNSIGNED not null comment 'アイテム数'
  , gacha_result_flag TINYINT UNSIGNED not null comment 'ガチャ結果フラグ:0:フラグなし
1:確定枠'
  , take_flag TINYINT UNSIGNED not null comment '取得フラグ:0:フラグなし
1:キャラクター
2:欠片変換
4:所持数オーバー'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_gacha_result_PKC primary key (id)
) comment 'プレイヤガチャ結果' AUTO_INCREMENT=10001;

create index player_gacha_result_IX1
  on player_gacha_result(player_gacha_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_gacha_result');
	}

}
