<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerQuest extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_quest (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤクエストID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , quest_category TINYINT UNSIGNED not null comment 'クエスト種類:1: ストーリ
2: キャラクタ'
  , chapter_id INT UNSIGNED not null comment 'チャプタID'
  , quest_id INT UNSIGNED not null comment 'クエストID'
  , close_day DATETIME comment 'イベント終了日'
  , mission_flag_1 TINYINT UNSIGNED default 0 not null comment 'ミッションフラグ1:0:未クリア
1:クリア済み'
  , mission_flag_2 TINYINT UNSIGNED default 0 not null comment 'ミッションフラグ2:0:未クリア
1:クリア済み'
  , mission_flag_3 TINYINT UNSIGNED default 0 not null comment 'ミッションフラグ3:0:未クリア
1:クリア済み'
  , clear_flag TINYINT UNSIGNED not null comment 'クリアフラグ:0:未クリア
1:クリア済み'
  , clear_count INT UNSIGNED default 0 not null comment 'クリア回数'
  , cleared_at DATETIME comment 'クリア日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_quest_PKC primary key (id)
) comment 'プレイヤクエスト' AUTO_INCREMENT=10001;

create index player_quest_IX1
  on player_quest(player_id);

create index player_quest_IX2
  on player_quest(close_day);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_quest');
	}

}
